% This function describe nonlinear constraints CEQ(X) = 0
% X   the variable/argument of constraints given in SE(3) space
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% M_s    the cell structure got from GetTriangle3D_PoseSLAM
%      M_s reflects indices for relative poses described in SE(3) group/space
%      M_s{i}: the i-th triangle
%      M_s{i}: num_odometry [odom1 odm2...domK] OdomToEndPose1 OdomToEndPose2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ CEQ ] = Nonlinear_Con_3D_PoseSLAM( X, M_s )

num_trian = size(M_s,2);
cur_trian = 1;
CEQ = zeros(6*num_trian,1);

while(cur_trian<=num_trian)
    num_odom = M_s{cur_trian}(1);
    %%%%%%%%% odometry 1...k  
    count = 1;
    while(count<= num_odom)
        od(count)= M_s{cur_trian}(count+1);
        count = count + 1;
    end
    %%%%%%%%%observation 1
    OdomToEndPose1=M_s{cur_trian}(num_odom+2);
    %%%%%%%%% observation 2
    OdomToEndPose2=M_s{cur_trian}(num_odom+3);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    cur_odom = num_odom;
    R_cur_odom = [X(od(cur_odom)+3), X(od(cur_odom)+4), X(od(cur_odom)+5);
                  X(od(cur_odom)+6), X(od(cur_odom)+7), X(od(cur_odom)+8);
                  X(od(cur_odom)+9), X(od(cur_odom)+10), X(od(cur_odom)+11);];
    
    F = [X(od(cur_odom));X(od(cur_odom)+1);X(od(cur_odom)+2)] + ...
                R_cur_odom*[X(OdomToEndPose2);X(OdomToEndPose2+1);X(OdomToEndPose2+2)];
            
    Rota = [X(OdomToEndPose2+3), X(OdomToEndPose2+4), X(OdomToEndPose2+5);
            X(OdomToEndPose2+6), X(OdomToEndPose2+7), X(OdomToEndPose2+8);
            X(OdomToEndPose2+9), X(OdomToEndPose2+10), X(OdomToEndPose2+11);];
    
    Rota = R_cur_odom*Rota; 
    
    while( cur_odom~=1)
        cur_odom = cur_odom -1;
        R_cur_odom = [X(od(cur_odom)+3), X(od(cur_odom)+4), X(od(cur_odom)+5);
                      X(od(cur_odom)+6), X(od(cur_odom)+7), X(od(cur_odom)+8);
                      X(od(cur_odom)+9), X(od(cur_odom)+10), X(od(cur_odom)+11);];
                  
        F = [X(od(cur_odom));X(od(cur_odom)+1);X(od(cur_odom)+2)] + R_cur_odom*F;  
        Rota = R_cur_odom*Rota; 
    end
            
    CEQ([6*cur_trian-5; 6*cur_trian-4; 6*cur_trian-3],1) = F - [X(OdomToEndPose1);X(OdomToEndPose1+1);X(OdomToEndPose1+2)];
    
    R_cur_odom = [X(OdomToEndPose1+3), X(OdomToEndPose1+4), X(OdomToEndPose1+5);
                  X(OdomToEndPose1+6), X(OdomToEndPose1+7), X(OdomToEndPose1+8);
                  X(OdomToEndPose1+9), X(OdomToEndPose1+10), X(OdomToEndPose1+11);];

    
    CEQ([6*cur_trian-2; 6*cur_trian-1; 6*cur_trian],1) = Log_R2a((Rota.') * R_cur_odom);

    cur_trian = cur_trian + 1;
    
end

end

% This function gives the Logarithm mapping to tranform a rotation R to the minimal representation Theta
% The minimal representation is described by axis angle representation
% Theta is a vertical vector
function [ Theta ] = Log_R2a( R )
threshold = 1e-10;
angle = acos( (trace(R) -1 )/2 );

if (abs(angle) <threshold ) % 0
    axis = [1; 0; 0;];
elseif(abs(angle - pi) <threshold )  % pi
   % disp('reach pi');
    axis = - [sign(R(2,3))*sqrt((1+R(1,1))/2); sign(R(1,3))*sqrt((1+R(2,2))/2); sign(R(1,2))*sqrt((1+R(3,3))/2) ];
else
    axis = [ R(3,2)-R(2,3);
             R(1,3)-R(3,1);
             R(2,1)-R(1,2); ];
    axis = axis/(2*sin(angle));
end

Theta = angle*axis;
[ Theta ] = WrapX( Theta );
end
% Wrap a arbitrary axis angle rotation into a rotation at interval [0, pi]
function [ X_v ] = WrapX( X_vec )
if (norm(X_vec)== 0)
    X_v = X_vec;
    return; 
end
angle = norm(X_vec);
axis  = X_vec/angle;
while (angle >= 2*pi)
    angle = angle - 2*pi;
end
while ( angle > pi)
    angle = 2*pi - angle;
    axis = -axis;  
end
if(pi-angle<1e-10)
   if(axis(3)<0)
       axis = -axis;
   end    
end

X_v = angle*axis;
end



