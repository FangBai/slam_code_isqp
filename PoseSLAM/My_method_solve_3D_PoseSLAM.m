% This function is used to solve SQP SLAM formulation
% X0_SE is the intial value given in SE(3) space
% Data_SE is the observation and odometry data given in SE(3) space
% CovMatrix is the covariance matrix of the meausrement noise rather than CovMatrixInv (the inversion of covariance matrix)
function [ X_value_SE, F_value, Iteration_count, Var_Increment_EU ] = My_method_solve_3D_PoseSLAM( X0_SE, Data_SE, CovMatrix, M_e, M_s, BenchMark )
%%%%%%%%%%%%%%%%% Set pricision for computation %%%%%%%%%%%%%%%%%
Max_error = 1e-6;
Cons_error = 1e-6;
Max_Iteration = 20;
Iteration_count = 1;

DISPLAY_OBJECTIVE = false;
DISPLAY_INCREMENT = false;
if(strcmp(BenchMark, 'Probabilistic'))
    CovOption = true;
    DISPLAY_OBJECTIVE = false;
    DISPLAY_INCREMENT = false;    
elseif(strcmp(BenchMark, 'Heuristic'))
    CovOption = false;
    DISPLAY_OBJECTIVE = false;
    DISPLAY_INCREMENT = false;    
elseif(strcmp(BenchMark, 'None'))
    CovOption = false;    
    DISPLAY_OBJECTIVE = true;
    DISPLAY_INCREMENT = true;
else
    disp('Unknown BenchMark type!');
    return;
end

% Linearize constraint into euclidean space by initial point X0 given in SE(3)
t_con1 = cputime;
[ A,  b ] = Linear_Con_3D_PoseSLAM( X0_SE, M_e, M_s );
t_cons_linearization = cputime - t_con1;

% Linearize objective into euclidean space with error state;
t_obj1 = cputime;
[ J, eta ] = Linear_Obj_3D_PoseSLAM(Data_SE, X0_SE );
t_obj_linearization = cputime - t_obj1;

% add colums to A if A is not include all variables
if(size(A,2) ~=max(size(eta)))
   A(1,size(eta,1)) = 0; 
end
% Solve linear system
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%  min  || J^{-1} *X - eta ||_P^2   %%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%      st.  AX = b        %%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t_solve_quadratic = cputime;
[ Increment_EU,  F_value] = Equilibrium_Direct_Method_3D(J, eta, CovMatrix, A ,b);
t_solve_quddratic_system = cputime - t_solve_quadratic;
% Update Euclidean increments to SE(3) space
[ X0_SE ] = Update_SEbyEU_3D_PoseSLAM(X0_SE, Increment_EU );
% Compute constraints value 
[ Con_value ] = Nonlinear_Con_3D_PoseSLAM( X0_SE, M_s );
% NonlinearCons = max(abs(Con_value))

if(strcmp(BenchMark, 'None'))
   figure(100004)
   spy( A )
   title('A')   
   figure(100005)
   spy(A*J*CovMatrix*J.'*A.')
   title('A Q A^T')   
   disp(['Time constraint linearization = ', num2str(t_cons_linearization) ]);
   disp(['Time objective linearization = ', num2str(t_obj_linearization)]);
   disp(['Time solve quadratic system = ', num2str(t_solve_quddratic_system)]);
   pause(0.001)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(DISPLAY_OBJECTIVE)                                                     %
Obj = eta.' * (CovMatrix\eta);                                         %
disp(['Obj = ', num2str(Obj)]);                                           %
end                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  


while(norm(Increment_EU)>Max_error || max(abs(Con_value))>Cons_error )  
    if ( Iteration_count < Max_Iteration) 

        if(DISPLAY_INCREMENT)
            inc =  norm(Increment_EU);
            cons =   norm(Con_value);
            disp(['Euclidean Increment Norm = ', num2str(inc), '      Constraint Norm = ', num2str(cons)]);
        end
        
        % Linearize constraint into euclidean space by initial point X0 given in SE(3)
        [ A,  b ] = Linear_Con_3D_PoseSLAM( X0_SE, M_e, M_s );
        % Linearize objective into euclidean space with error state;
        [ J, eta ] = Linear_Obj_3D_PoseSLAM(Data_SE, X0_SE );
        % add colums to A if A is not include all variables
        if(size(A,2) ~=max(size(eta)))
            A(1,size(eta,1)) = 0;        
        end
        % Solve linear system
        [ Increment_EU, F_value] = Equilibrium_Direct_Method_3D(J, eta, CovMatrix, A ,b);
        % Update Euclidean increments to SE(3) space
        [ X0_SE ] = Update_SEbyEU_3D_PoseSLAM(X0_SE, Increment_EU );
        % Compute constraints value 
        [ Con_value ] = Nonlinear_Con_3D_PoseSLAM( X0_SE, M_s );
        % NonlinearCons = max(abs(Con_value))
        
        Iteration_count = Iteration_count + 1;
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(DISPLAY_OBJECTIVE)                                                     %
Obj = eta.' * (CovMatrix\eta);                                          %
disp(['Obj = ', num2str(Obj)]);                                           %
end                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
        
    else
        disp('-> warning: My_method_solve has reach maximum iteration. The reuslt may be not accurate any more!')
        break
    end
end

X_value_SE = X0_SE;

%

if(CovOption)
    t_v_b = cputime;
    
    Q = J * sparse(CovMatrix) * (J.');
    
    % Var_Increment_EU = Q;
    
    Var_Increment_EU = Q - Q * A.' * ((A * Q* A.')\(A * Q));
    
%     TmpVar = [];
%     
%     count = 1;
%     
%     t_tv_b = cputime;
%     
%     while(count < size(Var_Increment_EU,1) )
%        
%         Tmp = Var_Increment_EU([count: count+2], [count : count+2]);
%         
%         TmpVar = blkdiag(TmpVar, Tmp);
%         
%         count = count + 3;
%     end
%     
%     t_tv_e = cputime;
%     
%     Var_Increment_EU = TmpVar;
    
    t_v_e = cputime;
    
    disp(['Timing Covariance Increment =   ', num2str(t_v_e - t_v_b), ...
         % 'Timing Choose Main Diagonal =   ', num2str(t_tv_e - t_tv_b) ...
         ]);
else
    Var_Increment_EU = [];
end
%

F_value = full(F_value);

end