% Wrap a arbitrary axis angle rotation into a rotation at interval [0, pi]
function [ X_v ] = WrapX( X_vec )
if (norm(X_vec)== 0)
    X_v = X_vec;
    return; 
end
angle = norm(X_vec);
axis  = X_vec/angle;
while (angle >= 2*pi)
    angle = angle - 2*pi;
end
while ( angle > pi)
    angle = 2*pi - angle;
    axis = -axis;  
end
if(pi-angle<1e-10)
   if(axis(3)<0)
       axis = -axis;
   end    
end

X_v = angle*axis;
end

