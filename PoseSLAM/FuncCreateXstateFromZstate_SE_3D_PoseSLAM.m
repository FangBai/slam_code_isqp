function [ Xstate_EU ]  = FuncCreateXstateFromZstate_SE_3D_PoseSLAM( Zstate_SE )

Zcount = 1;
Xcount = 1;
Xstate_EU = [];
CurrentPosi = [0; 0; 0];
CurrentRota = eye(3);

Zstate_SE = Zstate_SE(find((Zstate_SE(:,2) - Zstate_SE(:,3)) == 1), :);
DataDim = size(Zstate_SE,1);

while ( Zcount <= DataDim )
    
     R_cur =  [Zstate_SE([Zcount+3, Zcount+6, Zcount+9],1) Zstate_SE([Zcount+4, Zcount+7, Zcount+10],1) Zstate_SE([Zcount+5, Zcount+8, Zcount+11],1)];
        
     CurrentPosi = CurrentRota*Zstate_SE([Zcount, Zcount+1, Zcount+2], 1) + CurrentPosi;
     CurrentRota = CurrentRota*R_cur;  
        
     Xstate_EU([Xcount, Xcount+1, Xcount+2], 1) = CurrentPosi;
     Xstate_EU([Xcount+3, Xcount+4, Xcount+5], 1) = Log_R2a(CurrentRota);
        
     Xstate_EU([Xcount : Xcount+5],[2]) = Zstate_SE( [Zcount : Zcount+5],[2]); 
     Xcount = Xcount + 6;
     Zcount = Zcount + 12;   
        
end
end

