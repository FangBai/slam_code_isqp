clear all
close all
clc

num_outlier = 20;  %20 % 10 % 5
groupsize = 1;
Niko = false;

add_outliers = 'DataSet/generateDataset.py';
data_file_outlier = 'Data_G2O/tmp_Outlier.g2o';

%data_file = 'DataSet/MITb.g2o';
data_file = 'G2O/input_MITb_g2o.g2o';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load('Benchmark/MITb_benchmark.mat');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Now add outliers to the dataset, and solve again with iSQP and G2O
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% add outliers to the dataset
if(Niko)
string_cmd2 = [add_outliers, ' -i ', data_file, ' -o ', data_file_outlier, ...
               ' -n ', num2str(num_outlier), ' -g ', num2str(groupsize), ' -s'];
[result2_cmd, info2_cmd] = system(string_cmd2);
%
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( data_file_outlier );
%
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
CovMatrix = inv(CovMatrixInv);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
else
    
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( data_file );
%
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
%
[Zstate, CovMatrixInv] = AddOutliersPoseSLAM( Zstate, CovMatrixInv, num_outlier, 0 );
%
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
CovMatrix = inv(CovMatrixInv);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tic
[Res_iSQP, Obj_iSQP, PR_iSQP, N_All ] = PoseSLAM_iSQP_2D ( Zstate, CovMatrix);
toc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Res_DCS, Obj_DCS, PR_DCS, Weight_DCS ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'DCS', 660, 200); % 660
PlotXstate2D_PoseSLAM( Res_DCS, 'b.--' );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Res_Cauchy, Obj_Cauchy, PR_Cauchy, Weight_Cauchy ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'Cauchy', 1.0, 200); 
PlotXstate2D_PoseSLAM( Res_Cauchy, 'g.--' );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Res_Cauchy_GN, Obj_Cauchy_GN ] = PoseSLAM_G2O_GN( Zstate, CovMatrixInv, Res_Cauchy, 100, '2D' );
PlotXstate2D_PoseSLAM( Res_Cauchy_GN, 'c.--' );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[ RMSE_isqp ] = CompareXstateResult( Res_MITb, Res_iSQP);
[ RMSE_dcs ] = CompareXstateResult( Res_MITb, Res_DCS);
[ RMSE_cauchy] = CompareXstateResult( Res_MITb, Res_Cauchy);
[ RMSE_cauchy_gn ] = CompareXstateResult( Res_MITb, Res_Cauchy_GN);

disp(['Objective : MITb - iSQP - DCS - Cauchy - Cauchy+GN ']);

disp([num2str([Obj_MITb, Obj_iSQP, Obj_DCS, Obj_Cauchy, Obj_Cauchy_GN])]);

disp(['RMSE : isqp - dcs - cauchy - cauchy_gn =>   ', num2str([RMSE_isqp, ...
                                                            RMSE_dcs, ...
                                                            RMSE_cauchy, ...
                                                            RMSE_cauchy_gn])]);
PR_iSQP
PR_DCS
PR_Cauchy

[ success_iSQP ] = Success_Decision_Strategy1( PR_iSQP )
[ success_DCS ] = Success_Decision_Strategy1( PR_DCS )
[ success_Cauchy ] = Success_Decision_Strategy1( PR_Cauchy )

% OutlierConsAdded = [find(N_All(:,4)==-4), N_All(find(N_All(:,4)==-4),:)]
% CorrectConsNotAdded = [find(N_All(:,4)==1), N_All(find(N_All(:,4)==1),:)]


% save MITb_Data/MITb10_16 Zstate CovMatrixInv


