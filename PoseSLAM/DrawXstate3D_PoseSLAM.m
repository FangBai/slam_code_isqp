function [ ] = DrawXstate3D_PoseSLAM( Xstate)
% set up parameters here
RobotSize = 0.4;
Tranparency = 0.4;
PoseColor = 'red';
FeatureColorShape = 'ko';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%[0 sqrt(3)/2]
%[-3/4 -sqrt(3)/4]
%[3/4 -sqrt(3)/4]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

vorigin = [ 0, 0, sqrt(3)/2;
            0, -3/4, -sqrt(3)/4;
            0, 3/4, -sqrt(3)/4;
            10, 0, 0;] * RobotSize ;
faces = [1 2 3; 2 3 4; 3 4 1; 4 1 2];

count = 1;
DataDim = size(Xstate, 1);
figure;
hold on;
% axis equal;

% DrawRoom([5,10,0], [20, 20, 20] ,'b', 0.1);

while (count < DataDim)

    PoseX = Xstate(count,1);
    PoseY = Xstate(count+1,1);
    PoseZ = Xstate(count+2,1);
    Rota = Xstate([count+3, count+4, count+5],1);
    Posi = [PoseX PoseY PoseZ;
            PoseX PoseY PoseZ;
            PoseX PoseY PoseZ;
            PoseX PoseY PoseZ;];

    vertices = Exp_a2R(Rota)*vorigin.';
    vertices = vertices' + Posi;

    patch('Faces', faces, 'Vertices', vertices, 'FaceColor', PoseColor, 'FaceAlpha', Tranparency);       
    count = count + 6;

end
hold off;
end
% transform a vector into a rotation matrix by exponential mapping
function [ RotationMatrixExp ] = Exp_a2R( X_vec )
if (norm(X_vec) ==0)
    RotationMatrixExp = eye(3);
else
    RotationMatrixExp = eye(3) + SkewSem(X_vec)*sin(norm(X_vec))/norm(X_vec) + ...
        (SkewSem(X_vec)^2)*(1 - cos(norm(X_vec)))/(norm(X_vec))^2;    
end
end

%DRAWROOM function to draw a 3-D voxel in a 3-D plot
%
%Usage
%   drawRoom(start,size,color,alpha);
%
%   will draw a voxel at 'start' of size 'size' of color 'color' and
%   transparency alpha (1 for opaque, 0 for transparent)
%   Default size is 1
%   Default color is blue
%   Default alpha value is 1
%
%   start is a three element vector [x,y,z]
%   size the a three element vector [dx,dy,dz]
%   color is a character string to specify color
%       (type 'help plot' to see list of valid colors)
%
%
%   drawRoom([2 3 4],[1 2 3],'r',0.7);
%   axis([0 10 0 10 0 10]);
%
function h = DrawRoom(i,d,c,alpha)
switch(nargin),
case 0
    disp('Too few arguements for voxel');
    return;
case 1
    l=1;    %default length of side of voxel is 1
    c='b';  %default color of voxel is blue
case 2,
    c='b';
case 3,
    alpha=1;
case 4,
    %do nothing
otherwise
    disp('Too many arguements for voxel');
end;

x=[i(1)+[-d(1) -d(1) -d(1) -d(1) d(1) d(1) d(1) d(1)]; ...
        i(2)+[-d(2) -d(2) d(2) d(2) -d(2) -d(2) d(2) d(2)]; ...
        i(3)+[-d(3) d(3) -d(3) d(3) -d(3) d(3) -d(3) d(3)]]';

for n=1:3
    if n==3,
        x=sortrows(x,[n,1]);
    else
        x=sortrows(x,[n n+1]);
    end;
    temp=x(3,:);
    x(3,:)=x(4,:);
    x(4,:)=temp;
    h=patch(x(1:4,1),x(1:4,2),x(1:4,3),c);
    set(h,'FaceAlpha',alpha);
    temp=x(7,:);
    x(7,:)=x(8,:);
    x(8,:)=temp;
    h=patch(x(5:8,1),x(5:8,2),x(5:8,3),c);
    set(h,'FaceAlpha',alpha);
end
axis equal;
xlabel('x');
ylabel('y');
zlabel('z');
end

