function [ PrecisionRecall ] = PrecisionRecallStatistics_G2O_M_estimator( Weight )

Weight_Inlier = Weight(find(Weight(:,4) > 0), :);
Weight_Outlier = Weight(find(Weight(:,4) < 0), :);

Total_Odometry = size(Weight_Inlier(find(Weight_Inlier(:,2) - Weight_Inlier(:,3) == 1), :), 1);    %%%%%

Weight_Inlier = Weight_Inlier(find(Weight_Inlier(:,2) - Weight_Inlier(:,3) > 1), :);

Total_Inlier = size(Weight_Inlier, 1);
Total_Outlier = size(Weight_Outlier, 1);

OutlierAdded = 0;
OutlierNotAdded = 0;
InlierAdded = 0;
InlierNotAdded = 0;

% count inlier
count = 1;
while(count <= Total_Inlier)
    if(Weight_Inlier(count,1) > 0.5)
        InlierAdded = InlierAdded + 1;
    else
        InlierNotAdded = InlierNotAdded + 1;
    end
    count = count + 1;
end

% count outlier
count = 1;
while(count <= Total_Outlier) 
    if(Weight_Outlier(count, 1) > 0.5)
        OutlierAdded = OutlierAdded + 1;
    else
        OutlierNotAdded = OutlierNotAdded + 1;
    end
    count = count + 1;
end

PrecisionRecall.Total_Odometry = Total_Odometry;
PrecisionRecall.Total_Inlier = Total_Inlier;
PrecisionRecall.Total_Outlier = Total_Outlier;

PrecisionRecall.TP = InlierAdded; 
PrecisionRecall.TN = OutlierNotAdded;
PrecisionRecall.FP = OutlierAdded;
PrecisionRecall.FN = InlierNotAdded;

PrecisionRecall.precision = PrecisionRecall.TP/(PrecisionRecall.TP + PrecisionRecall.FP);
PrecisionRecall.recall = PrecisionRecall.TP/(PrecisionRecall.TP + PrecisionRecall.FN);


end

