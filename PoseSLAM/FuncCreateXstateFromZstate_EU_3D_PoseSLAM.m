function [ Xstate_EU ]  = FuncCreateXstateFromZstate_EU_3D_PoseSLAM( Zstate_EU )

Zcount = 1;
Xcount = 1;
Xstate_EU = [];
CurrentPosi = [0; 0; 0];
CurrentRota = eye(3);

Zstate_EU = Zstate_EU(find((Zstate_EU(:,2) - Zstate_EU(:,3)) == 1), :);
DataDim = size(Zstate_EU,1);

while ( Zcount <= DataDim )

    CurrentPosi = CurrentRota*Zstate_EU([Zcount, Zcount+1, Zcount+2], 1) + CurrentPosi;
    CurrentRota = CurrentRota*Exp_a2R( Zstate_EU([Zcount+3, Zcount+4, Zcount+5], 1));  
        
    Xstate_EU([Xcount, Xcount+1, Xcount+2], 1) = CurrentPosi;
    Xstate_EU([Xcount+3, Xcount+4, Xcount+5], 1) = Log_R2a(CurrentRota);
        
    Xstate_EU([Xcount : Xcount+5],[2]) = Zstate_EU( [Zcount : Zcount+5],[2]); 
    Xcount = Xcount + 6;
    Zcount = Zcount + 6;    

end
end
