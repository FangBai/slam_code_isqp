%
% function: main loop, framework
%

function MainLoop

%clc; clear all;
%close all;
%close(1);

global Store;
global Truth;

% to record the time used 
% timeStart = cputime;
tic;

% Generate ground truth for the simulation environment
DoGroundTruth;

% load true position and control data stored 
load simu_256_ground_truth

% pause
Truth.Beacons = store_beaconsTrue;
Truth.robot = store_robotTrue;

DoSetupParam;

for loop = 1:size(Truth.robot,2)-2
    
    DoSimulation(loop); % produce the current obs;

end

DoLoopClosre(size(Truth.robot,2)-1)


timeUsed = toc;

Zstate = Store.Zstate;


save Zstate_Simu_256 Zstate

%DoFigure;

return;

