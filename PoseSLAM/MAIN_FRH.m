clear all
close all
clc

num_outlier = 400;

choice = 2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

datafile = 'FRH_P';    % rec 2 % --- % 1, 2  % 1 - SQP correct  %  2 - same     
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ResultFolder = ['Results/', datafile, '_', num2str(choice), '/Outlier_', num2str(num_outlier), '/'];
mkdir(ResultFolder);
delete([ResultFolder, 'ConsoleOutput.txt']);
diary([ResultFolder, 'ConsoleOutput.txt']);
diary on

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Directory = 'NoisyDataSet/LocalMinima_Odom/';
NoiseLevelStr = 'OM_0.05_0.05_0.1_LC_0.02_0.02_0.02';
DataFile = [Directory, datafile, '_Noisy_', NoiseLevelStr, '_', num2str(choice), '.g2o']


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( DataFile );
%
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
%
[Zstate, CovMatrixInv] = AddOutliersPoseSLAM( Zstate, CovMatrixInv, num_outlier, 0 );
%
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
CovMatrix = inv(CovMatrixInv);
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tic
[Res_iSQP, Obj_iSQP, PR_iSQP, N_All, ProcessInfo ] = PoseSLAM_iSQP_2D ( Zstate, CovMatrix);
toc


[ success_iSQP ] = Success_Decision_Strategy1( PR_iSQP )


diary off

save([ResultFolder, 'Result_iSQP.mat'], 'Res_iSQP', 'Obj_iSQP', 'PR_iSQP', 'N_All', 'ProcessInfo');
save([ResultFolder, 'DataZstate.mat'], 'Zstate', 'CovMatrix');


Xstate0 = FuncCreateXstateFromZstate_2D_PoseSLAM( Zstate );
CreateG2OFormatByZstatePS2D ([ResultFolder, datafile, '_', num2str(choice), '_Outlier_', num2str(num_outlier), '.g2o'], Zstate, CovMatrixInv, Xstate0);





