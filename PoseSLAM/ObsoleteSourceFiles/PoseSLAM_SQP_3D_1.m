function [Res_SQP, Fval_SQP] = PoseSLAM_SQP_3D ( Zstate_EU, CovMatrix)

%CovMatrix = inv(CovMatrixInv);

[ Data_SE, M_e, M_s ] = ExtractDataSetInfo_PoseSLAM( Zstate_EU );

%disp(['Number of Edges -> ', num2str(size(Zstate_EU,1)/6) ]);
%disp(['Number of Triangles - > ', num2str(size(M_e,2))]);


X0_SE = Data_SE;

[ X_value_SE, Fval_SQP ] = My_method_solve_3D_PoseSLAM( X0_SE, Data_SE, CovMatrix, M_e, M_s);

% get Xstate form of solution
[ Zstate_SE ] = ZstateFromEuclideanToSE3_PoseSLAM( Zstate_EU );
Zstate_SE(:,1) = X_value_SE;
Res_SQP  = FuncCreateXstateFromZstate_SE_3D_PoseSLAM( Zstate_SE);    

% test different ways to define the objective
% [ eta ] = Update_EUbySE_3D_PoseSLAM_Test(Data_SE, X_value_SE);
% Fval_SQP2 =  eta.'*(CovMatrix\eta);

end