% This function is used to solve SLAM incrementally by SQP, which is robust to outliers
function [Res_iSQP, Fval_iSQP, N_All] = PoseSLAM_iSQP_3D ( Zstate_EU, CovMatrix)
% Extract information
[ Data_SE, Me_All, Ms_All, N_All ] = ExtractDataSetInfo_PoseSLAM_iSQP( Zstate_EU );
%[  Edge_Index, Edges_Vertex ] = Edge_Related_Constraints( N_All, Zstate_EU, '3D');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%disp(['Number of Edges -> ', num2str(size(Zstate_EU,1)/6) ]);
%disp(['Number of Triangles - > ', num2str(size(Me_All,2))]);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%% SET RECOVERY RATIO THRESHOLD %%%%%%%%%%%%%%%%%%%%%%%%
% This value reflects the change of objective and fluctuation of iterations
RecoveryRatioThreshold = 150;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The Last colum of N_All is the flag representing the constraint status
%%%%%%%%%%%%%%%%%% FLAGS for Correct Loop Closure %%%%%%%%%%%%%%%%%%%%%%%%%
% 1 :    Not considered and Not added   ( Correct Constraints )
% 2 :    considered but Not independant ( Dependant constraints ) ( Correct Constraints )
% 3 :    considered and ready to be added ( Correct Constraints )
% 4 :    considered and already added ( Correct Constraints )
%%%%%%%%%%%%%%%%%% FLAGS for Outliers %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% -1 :    Not considered and Not added   ( Wrong Constraints/Outliers )
% -2 :    considered but Not independant ( Dependant constraints ) ( Wrong Constraints/Outliers )
% -3 :    considered and ready to be added ( Wrong Constraints/Outliers )
% -4 :    considered and already added ( Wrong Constraints/Outliers )
%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Initialization %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Con_TotalAmount = size(N_All,1);
con_count = 1;  
%set initial value with data
X0_SE = Data_SE;
%stop sign
stopsign = 1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ConsNorm = [];
EdgeAver = [];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
FvalGrowth = [];
IterTimes = [];
RATIO_FVAL = [];
RATIO_ITER = [];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Fval_iSQP = 0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%ConNormInit = Nonlinear_Con_3D_PoseSLAM_Norm( X0_SE, Ms_All);
%%%%%%%%%%%%%%%%%%%%%%%%% FOR DISPLAY FIGURES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
firstIteration = true;
FLAG_CONS = sign(N_All(:,4));
%EDGES_CONS = N_All(:,2) - N_All(:,1) + 2 * ones(size(N_All,1),1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
while(stopsign>0)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    disp('==========================================================================================='); 
    %%%%%%%%%%%%%%%%%%%%% SELECCT CONSTRAINTS TO ADD %%%%%%%%%%%%%%%%%%%%%% 
    [N_All, ConsNorm, EdgeAver] = DetermineConToAddByNormAndIndependancy(Ms_All, X0_SE, N_All, ConsNorm, EdgeAver);

    ConToBeAdded = find(abs(N_All(:,4)) == 3);   % FLAG = 3 OR -3
    
    con_num = size(ConToBeAdded, 1);
    %%%%%%%%%%%%%%%%%%%%%%% SELECCT CONSTRAINTS END %%%%%%%%%%%%%%%%%%%%%%%   
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % stop criteria
    if(con_num>0) 
        % Add selected constraints into the problem
        tb_Mcell = cputime;
        Me(con_count : (con_count+con_num-1)) = Me_All (ConToBeAdded);  
        Ms(con_count : (con_count+con_num-1)) = Ms_All (ConToBeAdded);
        con_count = con_count + con_num;
        te_Mcell = cputime;
        t_Mcell = te_Mcell - tb_Mcell
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%% plot constraint value %%%%%%%%%%%%%%%%%%%%%
        if(0)
            tb_plotCons = cputime;
            Con_Norm_All = Nonlinear_Con_3D_PoseSLAM_Norm( X0_SE, Ms_All);
            figure(1)
            clf
            hold on
            for count = 1 : size(Con_Norm_All, 1)
                if(FLAG_CONS(count) > 0)
                    if(N_All(count, 4) == 1)
                        plot(count, Con_Norm_All(count), 'rd');                     
                    elseif(N_All(count, 4) == 2)
                        plot(count, Con_Norm_All(count), 'rx');                      
                    elseif(N_All(count, 4) == 3)  
                        plot(count, Con_Norm_All(count), 'r+');                      
                    elseif(N_All(count, 4) == 4) 
                        plot(count, Con_Norm_All(count), 'ro');
                    else
                        disp('$$$$$$$$$$$$$$$$$$$$$$$ data error! $$$$$$$$$$$$$$$$$$$$$$$');
                        disp('The FLAG SET for N Triangle is incorrect!')
                        disp([ 'FLAG IS  ', num2str(N_All(count, 4)), '    FLAG_CONS  ', num2str(FLAG_CONS(count)) ]);
                       % return;              
                    end
                elseif(FLAG_CONS(count) < 0)
                    if(N_All(count, 4) == -1)
                        plot(count, Con_Norm_All(count), 'bd');                     
                    elseif(N_All(count, 4) == -2)
                        plot(count, Con_Norm_All(count), 'bx');                      
                    elseif(N_All(count, 4) == -3)  
                        plot(count, Con_Norm_All(count), 'b+');                      
                    elseif(N_All(count, 4) == -4) 
                        plot(count, Con_Norm_All(count), 'bo');
                    else
                        disp('$$$$$$$$$$$$$$$$$$$$$$$ data error! $$$$$$$$$$$$$$$$$$$$$$$');
                        disp('The FLAG SET for N Triangle is incorrect!')
                        disp([ 'FLAG IS  ', num2str(N_All(count, 4)), '    FLAG_CONS  ', num2str(FLAG_CONS(count)) ]);
                      %  return;              
                    end
                else
                    disp('Flag error in N triangle');
                    return;
                end
            end
          %  plot(EDGES_CONS, 'y*--');            
            title('Constraints Norm Value')            
            hold off
            pause(0.01);
            te_plotCons = cputime;
            t_plotCons = te_plotCons - tb_plotCons 
        end        
        if(0)
          %  Con_Norm_All = Nonlinear_Con_3D_PoseSLAM_Norm( X0_SE, Ms_All);
            figure(2)
            clf
            hold on
            for count = 1 : size(Con_Norm_All, 1)
                if(FLAG_CONS(count) > 0)
                    if(N_All(count, 4) == 1)
                        plot(count, (ConNormInit(count)-Con_Norm_All(count))/ConNormInit(count), 'rd');                     
                    elseif(N_All(count, 4) == 2)
                        plot(count, (ConNormInit(count)-Con_Norm_All(count))/ConNormInit(count), 'rx');                      
                    elseif(N_All(count, 4) == 3)  
                        plot(count, (ConNormInit(count)-Con_Norm_All(count))/ConNormInit(count), 'r+');                      
                    elseif(N_All(count, 4) == 4) 
                        plot(count, (ConNormInit(count)-Con_Norm_All(count))/ConNormInit(count), 'ro');
                    else
                        disp('$$$$$$$$$$$$$$$$$$$$$$$ data error! $$$$$$$$$$$$$$$$$$$$$$$');
                        disp('The FLAG SET for N Triangle is incorrect!')
                        disp([ 'FLAG IS  ', num2str(N_All(count, 4)), '    FLAG_CONS  ', num2str(FLAG_CONS(count)) ]);
                        return;              
                    end
                elseif(FLAG_CONS(count) < 0)
                    if(N_All(count, 4) == -1)
                        plot(count, (ConNormInit(count)-Con_Norm_All(count))/ConNormInit(count), 'bd');                     
                    elseif(N_All(count, 4) == -2)
                        plot(count, (ConNormInit(count)-Con_Norm_All(count))/ConNormInit(count), 'bx');                      
                    elseif(N_All(count, 4) == -3)  
                        plot(count, (ConNormInit(count)-Con_Norm_All(count))/ConNormInit(count), 'b+');                      
                    elseif(N_All(count, 4) == -4) 
                        plot(count, (ConNormInit(count)-Con_Norm_All(count))/ConNormInit(count), 'bo');
                    else
                        disp('$$$$$$$$$$$$$$$$$$$$$$$ data error! $$$$$$$$$$$$$$$$$$$$$$$');
                        disp('The FLAG SET for N Triangle is incorrect!')
                        disp([ 'FLAG IS  ', num2str(N_All(count, 4)), '    FLAG_CONS  ', num2str(FLAG_CONS(count)) ]);
                        return;              
                    end
                else
                    disp('Flag error in N triangle');
                    return;
                end
            end         
            title('Constraints Norm Value Decrease Percentage')            
            hold off
            pause(0.01);
        end     
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%% CONTINUTE THE CODE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        N_All(ConToBeAdded,4) = 4*sign(N_All(ConToBeAdded,4));  % set FLAG to 4 OR -4  
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
        X0_SE_ORIGINAL = X0_SE;
        FVAL_ORIGINAL = Fval_iSQP;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
        [ X0_SE, Fval_iSQP, iter ] = My_method_solve_3D_PoseSLAM( X0_SE, Data_SE, CovMatrix, Me, Ms); 
        % -----------------------------------------------------------------
        %FvalChange = Fval_iSQP-FVAL_ORIGINAL;
        FvalChange = (Fval_iSQP-FVAL_ORIGINAL)/con_num;       % Objective growth averaged by number of constraints
        %FvalChange = (Fval_iSQP-FVAL_ORIGINAL)/sqrt(con_num);       % Objective growth averaged by number of constraints
        % -----------------------------------------------------------------
        IterTimes = [IterTimes; iter];
        FvalGrowth = [FvalGrowth; FvalChange];
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %Display adding constraints process
        Constraint_added = max(size(Ms));
        str = ['=>> Constraint_add/Constraint_total=', num2str(Constraint_added), '/', num2str(Con_TotalAmount)];
        disp(str);    
           
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%% plot intermediate result %%%%%%%%%%%%%%%%%%%%          
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % For display figures
        RATIO_ITER = [RATIO_ITER; iter/median(IterTimes)];
        RATIO_FVAL = [RATIO_FVAL; FvalChange/median(FvalGrowth)];  
        if(0)
            figure(3)
            clf
            hold on
            plot([1:size(RATIO_ITER,1)], RATIO_ITER, 'k.-');
            title('Iterations Ratio: iter/median')
            pause(0.01);
            hold off          
        end  
        
        if(0)           
            figure(4)
            clf
            hold on
            plot([1:size(RATIO_FVAL,1)], RATIO_FVAL, 'ms-');
            title('Objective Function Growth Ratio: Growth/median')
            pause(0.01);
            hold off 
        end   
        
        if(1)            
            TMP = RATIO_FVAL .* RATIO_ITER;
            figure(5)
            clf
            hold on
            plot([1:size(TMP,1)], TMP, 'ms-');
            title('Ratio .* times')
            pause(0.01);
            hold off             
        end   
        
        if(1)
            tb_plotOdometry = cputime;
            figure(100)   
            clf
            hold on
            [ Zstate_SE ] =  ZstateFromEuclideanToSE3_PoseSLAM( Zstate_EU );
            Zstate_SE(:,1) = X0_SE;
            [ Xstate_EU ]  = FuncCreateXstateFromZstate_SE_3D_PoseSLAM( Zstate_SE );
            count = 1;
            X_pose = [0];
            Y_pose = [0];
            Z_pose = [0];
            % extract pose and feature coordinate
            % pose coordinates are saved in X_pose, Y_pose
            % feature coordiantes are saved in X_feature, Y_feature
            while (count<=size(Xstate_EU,1))
                X_pose = [X_pose Xstate_EU(count,1)];
                Y_pose = [Y_pose Xstate_EU(count+1,1)];
                Z_pose = [Z_pose Xstate_EU(count+2,1)];
                count = count + 6;
            end
            % plot pose and feature
            plot3(X_pose,Y_pose,Z_pose, 'r.--');
            title('iSQP Trajectory')
            pause(0.01);
            hold off
            te_plotOdometry = cputime;
            t_plotOdometry = te_plotOdometry - tb_plotOdometry
            %%%%
        end
        %%%%%%%%%%%%%%%%%%%%%%%%% FINISHED FIGURE %%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        disp(['+>> Recovery Ratio (ObjeGrowth * Iteration) = ', ...
            num2str((FvalChange/median(FvalGrowth)) * (iter/median(IterTimes))), ...
            '    ObjectiveGrowth = ', num2str((FvalChange/median(FvalGrowth)), ...
            '    IterFluctuation = ', num2str(iter/median(IterTimes))), ...
            ]);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%% DETECTION AND RECOVERY %%%%%%%%%%%%%%%%%%%%%
        if( ((FvalChange/median(FvalGrowth)) * (iter/median(IterTimes))) > RecoveryRatioThreshold )
            disp('-------------------------------------------------------------------------------------------'); 
            disp('An outlier constraint detected!');
            disp('Start recovery proccess to romove outliers and reset variables');
            
            % reset varibales
            X0_SE = X0_SE_ORIGINAL;
            Fval_iSQP = FVAL_ORIGINAL;

            con_count = con_count - con_num;
            
            IterTimes = IterTimes(1 : end-1);
            FvalGrowth = FvalGrowth(1 : end-1);    
            
            if(con_count == 1)
                Me = cell(0);
                Ms = cell(0);
            else
                Me = Me(1 : (con_count-1));
                Ms = Ms(1 : (con_count-1));
            end

            % reset FLAGS for related constraints in N_All
            [ N_All ] = RemoveOutlierConstraint(X0_SE_ORIGINAL, FVAL_ORIGINAL, Data_SE, CovMatrix, Me, Ms, Me_All, Ms_All, N_All, ConToBeAdded, IterTimes, FvalGrowth, RecoveryRatioThreshold);
            
            disp('-------------------------------------------------------------------------------------------');  
            
            RATIO_FVAL = RATIO_FVAL(1 : end-1);
            RATIO_ITER = RATIO_ITER(1 : end-1);
            
        end
        %%%%%%%%%%%%%%%%% FINISHED DETECTION AND RECOVERY %%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if(firstIteration)
            firstIteration = false;
          %  disp('Please press any key to continue');
          %  pause; % PAUSE AT FIRST ITERATION TO ARRANGE FIGURES
        end
        disp('==========================================================================================='); 
    else  
        stopsign = 0;
    end
    
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% N_All
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%save FvalIter/FvalIter_ IterTimes FvalGrowth

% get Xstate form of solution
Zstate_SE = ZstateFromEuclideanToSE3_PoseSLAM( Zstate_EU );
Zstate_SE(:,1) = X0_SE;
Res_iSQP  = FuncCreateXstateFromZstate_SE_3D_PoseSLAM( Zstate_SE );    
end

% This function is used to decide the constraints to be added by norm of constraints and
% independancies with constraints that have already been added
function [N_All, ConsNorm, EdgeAver] = DetermineConToAddByNormAndIndependancy(Ms_All, X0_SE, N_All, ConsNorm, EdgeAver)

%%%%%%%%%%%%%%%%%%%%%%% TUNE PARAMETERS HERE%%%%%%%%%%%%%%%%%%%%%%%%%%%
ConsRatioThreshold = 50.0;  % Average Constraint Error Ratio
EdgeRatioThreshold = 10.0;  % Average Edge Error Ratio
ConsNormRelativeRatio = 1.1; % Control the number of constraints added each time
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The Last colum of N_All is the flag representing the constraint status
%%%%%%%%%%%%%%%%%% FLAGS for Correct Loop Closure %%%%%%%%%%%%%%%%%%%%%%
% 1 :    Not considered and Not added   ( Correct Constraints )
% 2 :    considered but Not independant ( Dependant constraints ) ( Correct Constraints )
% 3 :    considered and ready to be added ( Correct Constraints )
% 4 :    considered and already added ( Correct Constraints )
%%%%%%%%%%%%%%%%%% FLAGS for Outliers %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% -1 :    Not considered and Not added   ( Wrong Constraints/Outliers )
% -2 :    considered but Not independant ( Dependant constraints ) ( Wrong Constraints/Outliers )
% -3 :    considered and ready to be added ( Wrong Constraints/Outliers )
% -4 :    considered and already added ( Wrong Constraints/Outliers )
%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Initialization %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%d
ConNotConsidered = find(abs(N_All(:,4)) == 1);
% N_All
%%%%%%%%%%%%%%%%
if(size(ConNotConsidered,1) ==0)
    disp('All the constraints have been considered!');
    return;
end

Con_norm = Nonlinear_Con_3D_PoseSLAM_Norm( X0_SE, Ms_All(ConNotConsidered) );   

% sort constraints in ascending order by the norm of constraints
[Con_norm, Norm_SEQUENCE] = sort(Con_norm,'ascend'); 
ConNotConsidered = ConNotConsidered(Norm_SEQUENCE);

stopsign = false;
count = 0;

ConAdded = find(abs(N_All(:,4)) == 4);
NumAddedCons = size(ConAdded,1);

Dim = max(size(ConNotConsidered));

ConToAdd = 0;

firstvalue = true;

while (stopsign==false && (count+1)<=Dim)
    count = count+1;   
    Independant = CheckIndependantConstraint (N_All(ConAdded,:), N_All(ConNotConsidered(count),:));
    if(Independant)
        
        if(firstvalue)
            firstvalue = false;
            ConNormBase = Con_norm(count);
        end
        
        NumNewEdges = ComputeNumberNewEdges (N_All, N_All(ConNotConsidered(count),:));
        
        RootEdges = sqrt(NumNewEdges);
        
        if(NumNewEdges == 0) 
            NumNewEdges = 1;
            RootEdges = sqrt(NumNewEdges);
            disp('-----> Exceptions occured!');
            disp('-----> Number of New Edges of this constraints is 0');
            disp(['-----> Set NumNewEdges = ', num2str(NumNewEdges)]);
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if(1)
            disp(['  $:> ConNorm--NumNewEdges--AverageEdgeError-->    ' , ...
                num2str([Con_norm(count), NumNewEdges, ...
                Con_norm(count)/RootEdges, ...
                ])]);
            disp(['  $:> medianConNorm--medianEdgeAver-->    ' , ...
                num2str([median(ConsNorm), ...
                median(EdgeAver), ...
                ])]);            
            disp(['  $:> ConRatio--EdgeRatio-->    ' , ...
                num2str([Con_norm(count)/median(ConsNorm), ...
                (Con_norm(count)/RootEdges)/median(EdgeAver), ...
                ])]);
            if(N_All(ConNotConsidered(count),4) == -1)
                disp('*******************************************************************************************');
                disp('                                 An Outlier Constraint                                     ');
                disp(['LineIndex = ', num2str(ConNotConsidered(count)), ...
                    '   NormCons = ', num2str(Con_norm(count)),'   StepNum = ', num2str(NumNewEdges)]);
                disp('*******************************************************************************************');
            end
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        if(size(ConsNorm,1)==0)  % 
            
        %   disp('add first constaint');
        
            ConsNorm = [ConsNorm; Con_norm(count);];
            
            EdgeAver = [EdgeAver; Con_norm(count)/RootEdges];
      
            N_All(ConNotConsidered(count),4) = 3*sign(N_All(ConNotConsidered(count),4));
                       
            ConAdded = [ConAdded; ConNotConsidered(count)];
            
            NumAddedCons = NumAddedCons+1;
            
            ConToAdd = ConToAdd + 1;    
            
        elseif((Con_norm(count)/median(ConsNorm) <= ConsRatioThreshold) && ...
                ((Con_norm(count)/RootEdges)/median(EdgeAver) <= EdgeRatioThreshold) && ...
                (Con_norm(count)/ConNormBase <= ConsNormRelativeRatio) )
            
            ConsNorm = [ConsNorm; Con_norm(count);];
            
            EdgeAver = [EdgeAver; Con_norm(count)/RootEdges];
        
            N_All(ConNotConsidered(count),4) = 3*sign(N_All(ConNotConsidered(count),4));
            
            ConAdded = [ConAdded; ConNotConsidered(count)];
            
            NumAddedCons = NumAddedCons+1;        
           
            ConToAdd = ConToAdd + 1;
        elseif(ConToAdd == 0)
            if((Con_norm(count)/RootEdges)/median(EdgeAver) > EdgeRatioThreshold)
                disp('@ $EdgeRatioThreshold$ Exception =>    Edge average is Dangerous!');
                disp('@ Strategy =>    Try the rest of constraints!');
                
                while (stopsign==false && (count+1)<=Dim)
                    count = count+1;
                    ConNormBase = Con_norm(count);
                    Independant = CheckIndependantConstraint (N_All(ConAdded,:), N_All(ConNotConsidered(count),:));
                    
                    if(Independant)
                        
                        NumNewEdges = ComputeNumberNewEdges (N_All, N_All(ConNotConsidered(count),:));
                        
                        RootEdges = sqrt(NumNewEdges);
                        
                        if((Con_norm(count)/median(ConsNorm) <= ConsRatioThreshold) && ...
                                ((Con_norm(count)/RootEdges)/median(EdgeAver) <= EdgeRatioThreshold) )
                            %   disp('$$$$$$$$->detect big loop closure constraint!');
                            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                            if(1)
                                disp(['  $:> ConNorm--NumNewEdges--AverageEdgeError-->    ' , ...
                                    num2str([Con_norm(count), NumNewEdges, ...
                                    Con_norm(count)/RootEdges, ...
                                    ])]);
                                disp(['  $:> medianConNorm--medianEdgeAver-->    ' , ...
                                    num2str([median(ConsNorm), ...
                                    median(EdgeAver), ...
                                    ])]);
                                disp(['  $:> ConRatio--EdgeRatio-->    ' , ...
                                    num2str([Con_norm(count)/median(ConsNorm), ...
                                    (Con_norm(count)/RootEdges)/median(EdgeAver), ...
                                    ])]);
                                if(N_All(ConNotConsidered(count),4) == -1)
                                    disp('*******************************************************************************************');
                                    disp('                                 An Outlier Constraint                                     ');
                                    disp(['LineIndex = ', num2str(ConNotConsidered(count)), ...
                                        '   NormCons = ', num2str(Con_norm(count)),'   StepNum = ', num2str(NumNewEdges)]);
                                    disp('*******************************************************************************************');
                                end
                            end
                            %%%%%%%%%%%%%%%%%%%%%%%%%%%%
                            ConsNorm = [ConsNorm; Con_norm(count);];
                            
                            EdgeAver = [EdgeAver; Con_norm(count)/RootEdges];
                            
                            N_All(ConNotConsidered(count),4) = 3*sign(N_All(ConNotConsidered(count),4));
                            
                            ConAdded = [ConAdded; ConNotConsidered(count)];
                            
                            NumAddedCons = NumAddedCons+1;
                            
                            ConToAdd = ConToAdd + 1;
                            
                            stopsign = true;
                        end
                        
                    end
                end
           
                %firstvalue = true;
     
            elseif(((Con_norm(count)/RootEdges)/median(EdgeAver) < EdgeRatioThreshold/1.5) ...
                    && (Con_norm(count)/median(ConsNorm) > ConsRatioThreshold))
             %   disp('$$$$$$$$->Edge average is Safe!');
             %   disp('$$$$$$$$->current constraint is diagnosed as an potential big loop closure!');
             %   disp('$$$$$$$$->strategy: slack ConsRatioThreshold to current Con_norm/Cons_Average')
                ConsNorm = [ConsNorm; Con_norm(count);];
            
                EdgeAver = [EdgeAver; Con_norm(count)/RootEdges];
                
                ConsRatioThreshold = Con_norm(count)*NumAddedCons/Cons_Average;
                
                N_All(ConNotConsidered(count),4) = 3*sign(N_All(ConNotConsidered(count),4));
                
                ConAdded = [ConAdded; ConNotConsidered(count)];
                
                NumAddedCons = NumAddedCons+1;

                ConToAdd = ConToAdd + 1;
                
                stopsign = true;   % only add one constraint when ConsRatioThreshold is modified
            else
                stopsign = true;   % stop when rest of the constraints are outliers
            end
        else
            stopsign = true;
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % if(~stopsign)
        % disp(['NumAddedCons--ConNorm--NumEdges--NormRatio--StepRatio--RelaRatio -->    ' , ...
        %    num2str([NumAddedCons, Con_norm(count), NumEdges, ...
        %    Con_norm(count)*NumAddedCons/Cons_Average, ...
        %    Con_norm(count)*NumAddedCons/(NumEdges*Edge_Average), ...
        %    Con_norm(count)/ConNormBase])]);
        % end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    elseif(~Independant)  
        
        N_All(ConNotConsidered(count),4) = 2*sign(N_All(ConNotConsidered(count),4));   
        
    else
        disp('error');
        return;
    end  
end

end

% This function is used to Compute the number of new edges appeared in the constraints
% Cons_vertices (which is a row from N_trian format matrix)
function [ NumNewEdges ] = ComputeNumberNewEdges (N_trian, Cons_vertices ) 
% Initilize number of new edges with the total number of edges
NumNewEdges = Cons_vertices(2) - Cons_vertices(1) + 2;
    
if(size(N_trian,1) ==0)
    return;
end

ConsideredCons = N_trian(find(abs(N_trian(:,4))==4 ) ,[1,2,3,4]);

% Consider added constraints only

if(size(ConsideredCons,1) ==0)
    return;
end
%
start_node = Cons_vertices(1);
end_node = Cons_vertices(2);

OdoEdges([start_node+1 : end_node], 1) = sparse(1);
first_edge = 1;
second_edge = 1;

count = 1;
while(count <= size(ConsideredCons,1))
    if(ConsideredCons(count, 3)==Cons_vertices(3))
        if(ConsideredCons(count, 1)==start_node || ConsideredCons(count, 2)==start_node)
            first_edge = 0;
        end
        if(ConsideredCons(count, 1)==end_node || ConsideredCons(count, 2)==end_node)
            second_edge = 0;
        end
    end
    
    OdoEdges([ConsideredCons(count, 1)+1 : ConsideredCons(count, 2)], 1) = sparse(0);   
    
    count = count + 1;
end
NumNewEdges = sum(OdoEdges) + first_edge + second_edge;

%%%%%
end

% This function is usde to check if current constraint given by Cons_vertices (which is a row from N_trian format matrix) is independant
% from the constraints in N_trian or not
% If Independant, return 1
% If dependant, return 0;
function [Independant ] = CheckIndependantConstraint (N_trian, Cons_vertices ) 
    
if(size(N_trian,1) ==0)
    Independant = 1;
    return;
end

RelatedCons = N_trian(find(N_trian(:,3)==Cons_vertices(3)),:);

if(size(RelatedCons,1) ==0)
    Independant = 1;
    return;
end

Dim = max(max(RelatedCons(:,2)), Cons_vertices(2));

AdjacentMatrix = sparse((Dim+1),(Dim+1));

count = 1;

while(count<=size(RelatedCons,1))
  
AdjacentMatrix((RelatedCons(count,1)+1), (RelatedCons(count,2)+1)) = 1;

count = count + 1;

end

AdjacentMatrix((Cons_vertices(1)+1), (Cons_vertices(2)+1)) = 1;

AdjacentMatrix = AdjacentMatrix + AdjacentMatrix.';

ExistCycle = ExistCycleUndirectedGraph( AdjacentMatrix );

Independant = ~ExistCycle;

end


% This function use AdjacentMatrix of an undirected graph to check if there exist cycles in the graph
% ExistCycle : 1   | There exist circles in the graph
% ExistCycle : 0   | There are no circles in the graph
function [ ExistCycle ] = ExistCycleUndirectedGraph( AdjacentMatrix )

stopsign = 1;

while(stopsign > 0)
    
DimIn = size(AdjacentMatrix,1);

VertexDeg = sum(AdjacentMatrix);

CycleVertex = find(VertexDeg >=2);

AdjacentMatrix = AdjacentMatrix(CycleVertex, CycleVertex);

DimOut = size(AdjacentMatrix,1);

if(DimOut==DimIn || DimOut==0)
    stopsign =0;
    if(DimOut >0)
        ExistCycle = 1;
    else
        ExistCycle = 0;
    end
end

end
end

% This function is used to remove the outlier constraint from the candiated
function [ N_All ] = RemoveOutlierConstraint(X0_SE_ORIGINAL, FVAL_ORIGINAL, Data_SE, CovMatrix, Me, Ms, Me_All, Ms_All, N_All, ConToBeAdded, IterTimes, FvalGrowth, RecoveryRatioThreshold);
      
    disp('process identify outlier constraints within all the candidate constraints added this time');
    disp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');   
    data_dim = size(ConToBeAdded, 1);
         
    RatioCache = ones(data_dim, 1);
    
    con_size = size(Me, 2);

    outlier_found = false;
    
    if(con_size == 0)
        disp('Error: The number of correctly considered constraints is zero'); 
        return;
    end
    count = 1;
    %%%%%%%%%%%%%%%%%%%%%%% SELECCT CONSTRAINTS END %%%%%%%%%%%%%%%%%%%%%%%   
    while(count <= data_dim)
        Me(con_size+1) = Me_All (ConToBeAdded(count));
        Ms(con_size+1) = Ms_All (ConToBeAdded(count));
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        [ X0_SE_NEW, Fval_iSQP_NEW, iter ] = My_method_solve_3D_PoseSLAM( X0_SE_ORIGINAL, Data_SE, CovMatrix, Me, Ms);
        
        FvalChange = Fval_iSQP_NEW - FVAL_ORIGINAL;
        
        IterTimesNew = [IterTimes; iter];
        
        FvalGrowthNew = [FvalGrowth; FvalChange];
        
        RecoverRatio = (FvalChange/median(FvalGrowthNew)) * (iter/median(IterTimesNew));
        % SET FLAGES
        if( RecoverRatio > RecoveryRatioThreshold )            
            disp(['Detect an outlier constraint with ratio:  ', num2str(RecoverRatio)]);
            disp(['FvalRatio = ', num2str(FvalChange/median(FvalGrowthNew)), '    IterRatio = ', num2str(iter/median(IterTimesNew))  ]);
            disp(['original flag = ', num2str(N_All(ConToBeAdded(count),4)),  '    new flag = -2']);
            N_All(ConToBeAdded(count),4) = -2; % -2   % a detected outlier constraint   
            outlier_found = true;
        else
            disp(['This is an inlier constraint with ratio:  ', num2str(RecoverRatio)]);
            disp(['FvalRatio = ', num2str(FvalChange/median(FvalGrowthNew)), '    IterRatio = ', num2str(iter/median(IterTimesNew))  ]);
            disp(['original flag = ', num2str(N_All(ConToBeAdded(count),4)),  '    new flag = 1']);
            N_All(ConToBeAdded(count),4) = 1;  % an inlier constraint
        end
        
        RatioCache(count) = RecoverRatio;

        Me = Me(1 : con_size);
        Ms = Ms(1 : con_size);
        
        count = count + 1;
    end
    
    if(~outlier_found)
        disp('No outlier found! Possiblely current RecoveryRatioThreshold is too big!')
        [Y, Index] = max(RatioCache);
        disp(['Determine the largest ratio ',num2str(RatioCache(Index)), ' as an outlier']);
        N_All(ConToBeAdded(Index),4) = -2; % -2   % a detected outlier constraint
    end
    disp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
end