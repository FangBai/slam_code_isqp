clear all
close all
clc

num_outlier = 0;  % 10 % 20

choice = 3;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

DataFile = 'CSAIL_P';  % rec 3 % --- % 1, 2, 3   % 1 - same  %  2 - different  %  3 - slow convergence 
% DataFile = 'FRH_P';    % rec 2 % --- % 1, 2  % 1 - SQP correct  %  2 - same     
% DataFile = 'INTEL_P';   % rec 1 % 1, 2, 3, 4  %  1 - not converge  %  2 4 - SQP better solution and slow convergence % 3 - SQP better solution % 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CovChoice = 'Covariance_0.03';
 CovChoice = 'Covariance_0.05';
% CovChoice = 'CovarianceOdometry';
 

Directory = 'NoisyDataSet/LocalMinima_Odom/';
NoiseLevelStr = 'OM_0.05_0.05_0.1_LC_0.02_0.02_0.02';
data_file = [Directory, DataFile, '_Noisy_', NoiseLevelStr, '_', num2str(choice), '.g2o']



OutlierDirectory = 'NoisyDataSet/Minia_Outliers/CSAIL_P_Noisy/';
outlier_data_file = [OutlierDirectory, CovChoice, '/', DataFile, '_Noisy_', NoiseLevelStr, '_', ... 
    num2str(choice), '_Outliers_', num2str(num_outlier), '.g2o']

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(num_outlier == 0)
    InputFile = data_file;
else
    InputFile = outlier_data_file;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( InputFile );
%
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
%
CovMatrix = inv(CovMatrixInv);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

load(['Results/CSAIL_P_Noisy/Resutls', CovChoice, '/CSAIL_P_3/Outlier_', num2str(num_outlier), '/Result_iSQP.mat'])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Res_DCS, Obj_DCS, PR_DCS, Weight_DCS ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'DCS', 190, 200); % 190 CSAIL % 9 INTEL
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Xstate0 = FuncCreateXstateFromZstate_2D_PoseSLAM( Zstate );
[ Res_GN, Obj_GN ] = PoseSLAM_G2O_GN( Zstate, CovMatrixInv, Xstate0, 100, '2D' );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Process Zstate Flag for this dataset according to covariance
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
OM = [0.05; 0.05; 0.1];

LC = [0.02; 0.02; 0.02];

LC_Flag = (1/LC(1))^2

% if(strcmp(CovChoice, 'CovarianceOdometry'))
%     OLC = OM;
% elseif(strcmp(CovChoice, 'Covariance_0.05'))
%     OLC = [0.05; 0.05; 0.05];
% elseif(strcmp(CovChoice, 'Covariance_0.03'))
%     OLC = [0.03; 0.03; 0.03];
% else
%     disp('CovChoice error @ MAIN_PLOT_RESULT.m');
% end

count = 1;
while(count <= size(Zstate,1))
    if(Zstate(count, 2) - Zstate(count, 3) > 1)  % loop-closure  
        if(abs(CovMatrixInv(count, count) - LC_Flag) > 1)
           Zstate([count; count+1; count+2], 4) = - ones(3,1); 
        end        
    end    
    count = count + 3;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


NumOutlier = size(find(Zstate(:,4)==-1),1)/3


PoseStr = 'b.--';
InlierStr = 'g.--';
OutlierStr = 'r.--';


PlotPoseEdgePoseSLAM2D( Res_DCS, Zstate, PoseStr, InlierStr, OutlierStr, 1 );
PlotPoseEdgePoseSLAM2D( Res_iSQP, Zstate, PoseStr, InlierStr, OutlierStr, 2 );
PlotPoseEdgePoseSLAM2D( Res_GN, Zstate, PoseStr, InlierStr, OutlierStr, 3 );


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%


num_outlier = 10;  % 10 % 20

choice = 3;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

DataFile = 'CSAIL_P';  % rec 3 % --- % 1, 2, 3   % 1 - same  %  2 - different  %  3 - slow convergence 
% DataFile = 'FRH_P';    % rec 2 % --- % 1, 2  % 1 - SQP correct  %  2 - same     
% DataFile = 'INTEL_P';   % rec 1 % 1, 2, 3, 4  %  1 - not converge  %  2 4 - SQP better solution and slow convergence % 3 - SQP better solution % 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Directory = 'NoisyDataSet/LocalMinima_Odom/';
NoiseLevelStr = 'OM_0.05_0.05_0.1_LC_0.02_0.02_0.02';
data_file = [Directory, DataFile, '_Noisy_', NoiseLevelStr, '_', num2str(choice), '.g2o']



OutlierDirectory = 'NoisyDataSet/Minia_Outliers/CSAIL_P_Noisy/';
outlier_data_file = [OutlierDirectory, CovChoice, '/', DataFile, '_Noisy_', NoiseLevelStr, '_', ... 
    num2str(choice), '_Outliers_', num2str(num_outlier), '.g2o']

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(num_outlier == 0)
    InputFile = data_file; 
else
    InputFile = outlier_data_file;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( InputFile );
%
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
%
CovMatrix = inv(CovMatrixInv);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

load(['Results/CSAIL_P_Noisy/Resutls', CovChoice, '/CSAIL_P_3/Outlier_', num2str(num_outlier), '/Result_iSQP.mat'])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Res_DCS, Obj_DCS, PR_DCS, Weight_DCS ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'DCS', 190, 200); % 190 CSAIL % 9 INTEL
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Xstate0 = FuncCreateXstateFromZstate_2D_PoseSLAM( Zstate );
[ Res_GN, Obj_GN ] = PoseSLAM_G2O_GN( Zstate, CovMatrixInv, Xstate0, 100, '2D' );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Process Zstate Flag for this dataset according to covariance
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
OM = [0.05; 0.05; 0.1];

LC = [0.02; 0.02; 0.02];

LC_Flag = (1/LC(1))^2

% if(strcmp(CovChoice, 'CovarianceOdometry'))
%     OLC = OM;
% elseif(strcmp(CovChoice, 'Covariance_0.05'))
%     OLC = [0.05; 0.05; 0.05];
% elseif(strcmp(CovChoice, 'Covariance_0.03'))
%     OLC = [0.03; 0.03; 0.03];
% else
%     disp('CovChoice error @ MAIN_PLOT_RESULT.m');
% end

count = 1;
while(count <= size(Zstate,1))
    if(Zstate(count, 2) - Zstate(count, 3) > 1)  % loop-closure  
        if(abs(CovMatrixInv(count, count) - LC_Flag) > 1)
           Zstate([count; count+1; count+2], 4) = - ones(3,1); 
        end        
    end    
    count = count + 3;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


NumOutlier = size(find(Zstate(:,4)==-1),1)/3


PoseStr = 'b.--';
InlierStr = 'g.--';
OutlierStr = 'r.--';


PlotPoseEdgePoseSLAM2D( Res_DCS, Zstate, PoseStr, InlierStr, OutlierStr, 4 );
PlotPoseEdgePoseSLAM2D( Res_iSQP, Zstate, PoseStr, InlierStr, OutlierStr, 5 );
PlotPoseEdgePoseSLAM2D( Res_GN, Zstate, PoseStr, InlierStr, OutlierStr, 6 );

FigureDirectory = ['Figures/', DataFile, '_Noisy/', CovChoice, '/'];
mkdir(FigureDirectory);

savefig([1, 2, 3], [FigureDirectory, 'FigureInlier']); 
savefig([4, 5, 6], [FigureDirectory, 'FigureOutlier']); 



