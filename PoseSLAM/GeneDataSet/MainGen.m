clear all
close all
clc

% torofile = '/home/user/PoseSLAM/GeneDataSet/CSAIL_P_toro.graph';
% g2ofile = '/home/user/PoseSLAM/GeneDataSet/CSAIL_P.g2o'
% 
% torofile = '/home/user/PoseSLAM/GeneDataSet/FRH_P_toro.graph';
% g2ofile = '/home/user/PoseSLAM/GeneDataSet/FRH_P.g2o'
% 
% torofile = '/home/user/PoseSLAM/GeneDataSet/FR079_P_toro.graph';
% g2ofile = '/home/user/PoseSLAM/GeneDataSet/FR079_P.g2o';
% 
% torofile = '/home/user/PoseSLAM/GeneDataSet/INTEL_P_toro.graph';
% g2ofile = '/home/user/PoseSLAM/GeneDataSet/INTEL_P.g2o';

% torofile = '/home/user/PoseSLAM/GeneDataSet/killian-court.txt';
% g2ofile = '/home/user/PoseSLAM/GeneDataSet/killian_court.g2o';

torofile = '/home/user/PoseSLAM/GeneDataSet/input_MITb_toro.graph';
g2ofile = '/home/user/PoseSLAM/GeneDataSet/input_MITb_tora.g2o';


[ Zstate, CovMatrixInv ] = ConvertTOROFormatToZstatePS2D(  torofile );

[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );

[ Xstate0 ]  = FuncCreateXstateFromZstate_2D_PoseSLAM( Zstate );

CreateG2OFormatByZstatePS2D (g2ofile, Zstate, CovMatrixInv, Xstate0 );

full(CovMatrixInv([1:9], [1:9]))

