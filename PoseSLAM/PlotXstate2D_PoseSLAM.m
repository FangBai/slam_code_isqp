% This function is used to plot Xstate given in Euclidean space
% str control the line and color used to plot the pose
function [ h1 ] = PlotXstate2D_PoseSLAM( Xstate_2D, str )

count = 1;
X_pose = [0];
Y_pose = [0];


% extract pose and feature coordinate
% pose coordinates are saved in X_pose, Y_pose
% feature coordiantes are saved in X_feature, Y_feature
while (count<=size(Xstate_2D,1))
        X_pose = [X_pose Xstate_2D(count,1)];
        Y_pose = [Y_pose Xstate_2D(count+1,1)];
        count = count + 3;     
end

figure;
% plot pose 
hold on
h1 = plot(X_pose,Y_pose, str);
hold off
end

