% This Function is used to compute precision and recall statistics
function [ PrecisionRecall ] = PrecisionRecallStatistics( Zstate, N_All )

Zstate = Zstate([1:6:end], :);
Total_Data = size(Zstate, 1);

Zstate_Inlier = Zstate(find(Zstate(:,4) > 0), :);
Zstate_Outlier = Zstate(find(Zstate(:,4) < 0), :);

Total_Odometry = size(Zstate_Inlier(find(Zstate_Inlier(:,2) - Zstate_Inlier(:,3) == 1), :), 1);    %%%%%


Zstate_Inlier = Zstate_Inlier(find(Zstate_Inlier(:,2) - Zstate_Inlier(:,3) > 1), :);

Total_Inlier = size(Zstate_Inlier, 1);
Total_Outlier = size(Zstate_Outlier, 1);


Zstate_Inlier(:,4) = zeros(Total_Inlier, 1);
Zstate_Outlier(:,4) = zeros(Total_Outlier, 1);

count = 1;
Num_Cons = size(N_All, 1);

while(count <= Num_Cons)
    
    if ( N_All(count, 4) == -4 )  % Outlier but added
        
        tmp_count = 1;
        
        while(tmp_count <= Total_Outlier)
            
            if ( Zstate_Outlier(tmp_count,2) == N_All(count,3) )
                
                if( Zstate_Outlier(tmp_count,3) == N_All(count,1) )
                    
                    Zstate_Outlier(tmp_count,4) =1;
                    
                elseif( Zstate_Outlier(tmp_count,3) == N_All(count,2) )
                    
                    Zstate_Outlier(tmp_count,4) =1;
                    
                end
            end
            
            tmp_count = tmp_count + 1;
            
        end
        
    end
    
    if( N_All(count, 4) == 4)   % Inlier added
       
        tmp_count = 1;
        
        while(tmp_count <= Total_Inlier)
            
            if ( Zstate_Inlier(tmp_count,2) == N_All(count,3) )
                
                if( Zstate_Inlier(tmp_count,3) == N_All(count,1) )
                    
                    Zstate_Inlier(tmp_count,4) =1;
                    
                elseif( Zstate_Inlier(tmp_count,3) == N_All(count,2) )
                    
                    Zstate_Inlier(tmp_count,4) =1;
                    
                end
            end
            
            tmp_count = tmp_count + 1;
            
        end
        
    end
    
    count = count + 1;
    
end

OutlierAdded = sum(Zstate_Outlier(:,4));
InlierAdded = sum(Zstate_Inlier(:,4));


OutlierNotAdded = Total_Outlier - OutlierAdded;
InlierNotAdded = Total_Inlier - InlierAdded;

PrecisionRecall.Total_Odometry = Total_Odometry;
PrecisionRecall.Total_Inlier = Total_Inlier;
PrecisionRecall.Total_Outlier = Total_Outlier;

PrecisionRecall.TP = InlierAdded; 
PrecisionRecall.TN = OutlierNotAdded;
PrecisionRecall.FP = OutlierAdded;
PrecisionRecall.FN = InlierNotAdded;

PrecisionRecall.precision = PrecisionRecall.TP/(PrecisionRecall.TP + PrecisionRecall.FP);
PrecisionRecall.recall = PrecisionRecall.TP/(PrecisionRecall.TP + PrecisionRecall.FN);

end

