%
function [] = Main_MC_Simulation (TotalMC_Trials, num_outlier, DataFile)


% clear all
% close all
% clc

% TotalMC_Trials = 50;
% 
% num_outlier = 20;  %20 % 10 % 5 % 2 % 1
% 
% DataFile = 'MITb';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

groupsize = 1;

Niko = false;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

data_file = ['DataSet/' DataFile,'.g2o'];

ResultDir = ['Results/', DataFile, '/', num2str(num_outlier), '/' ];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load('Benchmark/MITb_benchmark.mat');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
add_outliers = 'DataSet/generateDataset.py';
data_file_outlier = 'Data_G2O/tmp_Outlier.g2o';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

MC_count = 1;

while(MC_count <= TotalMC_Trials)


if(Niko)
string_cmd2 = [add_outliers, ' -i ', data_file, ' -o ', data_file_outlier, ...
               ' -n ', num2str(num_outlier), ' -g ', num2str(groupsize), ' -s'];
[result2_cmd, info2_cmd] = system(string_cmd2);
%
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( data_file_outlier );
%
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
CovMatrix = inv(CovMatrixInv);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
else
    
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( data_file );
%
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
%
[Zstate, CovMatrixInv] = AddOutliersPoseSLAM( Zstate, CovMatrixInv, num_outlier, 0 );
%
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
CovMatrix = inv(CovMatrixInv);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tic
[Res_iSQP, Obj_iSQP, PR_iSQP, N_All ] = PoseSLAM_iSQP_2D ( Zstate, CovMatrix);
toc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Res_DCS, Obj_DCS, PR_DCS, Weight_DCS ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'DCS', 660, 200); % 660
PlotXstate2D_PoseSLAM( Res_DCS, 'b.--' );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Res_Cauchy, Obj_Cauchy, PR_Cauchy, Weight_Cauchy ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'Cauchy', 1.0, 200); 
PlotXstate2D_PoseSLAM( Res_Cauchy, 'g.--' );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Res_Cauchy_GN, Obj_Cauchy_GN ] = PoseSLAM_G2O_GN( Zstate, CovMatrixInv, Res_Cauchy, 100, '2D' );
PlotXstate2D_PoseSLAM( Res_Cauchy_GN, 'c.--' );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[ RMSE_iSQP ] = CompareXstateResult( Res_MITb, Res_iSQP);
[ RMSE_DCS ] = CompareXstateResult( Res_MITb, Res_DCS);
[ RMSE_Cauchy] = CompareXstateResult( Res_MITb, Res_Cauchy);
[ RMSE_Cauchy_GN ] = CompareXstateResult( Res_MITb, Res_Cauchy_GN);

disp(['Objective : MITb - iSQP - DCS - Cauchy - Cauchy+GN ']);

disp([num2str([Obj_MITb, Obj_iSQP, Obj_DCS, Obj_Cauchy, Obj_Cauchy_GN])]);

disp(['RMSE : isqp - dcs - cauchy - cauchy_gn =>   ', num2str([RMSE_iSQP, ...
                                                            RMSE_DCS, ...
                                                            RMSE_Cauchy, ...
                                                            RMSE_Cauchy_GN])]);

[ success_iSQP ] = Success_Decision_Strategy1( PR_iSQP );
[ success_DCS ] = Success_Decision_Strategy1( PR_DCS );
[ success_Cauchy ] = Success_Decision_Strategy1( PR_Cauchy );


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

FileStr = [ResultDir, DataFile, '_', num2str(num_outlier), '_', num2str(MC_count) ];

iSQP.Res = Res_iSQP;
iSQP.Obj = Obj_iSQP;
iSQP.PR = PR_iSQP;
iSQP.RMSE = RMSE_iSQP;
iSQP.success = success_iSQP;


DCS.Res = Res_DCS;
DCS.Obj = Obj_DCS;
DCS.PR = PR_DCS;
DCS.RMSE = RMSE_DCS;
DCS.success = success_DCS;


Cauchy.Res = Res_Cauchy;
Cauchy.Obj = Obj_Cauchy;
Cauchy.PR = PR_Cauchy;
Cauchy.RMSE = RMSE_Cauchy;
Cauchy.success = success_Cauchy;


save(FileStr, 'iSQP', 'DCS', 'Cauchy' );


MC_count = MC_count + 1;

close all
clc

end

end




