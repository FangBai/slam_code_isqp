%
% function: (1) add noise to true odometry
%           (2) produce observation -- this is based on the true 
%      location of the current robot location
%     and beacons location. They are in the map coordinate system.
%

function DoSimulation(loop)


global Params;
global Store;
global Truth;


%disp(' *** entering DoSimulation');

%% get odometry data

xr_previous = Truth.robot(1,loop);
yr_previous = Truth.robot(2,loop);
phir_previous = Truth.robot(3,loop);

xr_current = Truth.robot(1,loop+1);
yr_current = Truth.robot(2,loop+1);
phir_current = Truth.robot(3,loop+1);

% relative robot pose -- odometry without noises
xr_local =(xr_current - xr_previous)*cos(phir_previous) +  (yr_current- yr_previous )*sin(phir_previous);
yr_local= -(xr_current  - xr_previous)*sin(phir_previous) +  (yr_current- yr_previous )*cos(phir_previous);
phir_local= phir_current- phir_previous ;

%randn for dx with boundary
rand_dx = Params.sig_dx*randn;
if rand_dx>Params.sig_dx*Params.SensorNoiseLimit
    rand_dx = Params.sig_dx*Params.SensorNoiseLimit;
elseif rand_dx<-Params.sig_dx*Params.SensorNoiseLimit
    rand_dx = -Params.sig_dx*Params.SensorNoiseLimit;
end
%randn for dy with boundary
rand_dy = Params.sig_dy*randn;
if rand_dy>Params.sig_dy*Params.SensorNoiseLimit
    rand_dy = Params.sig_dy*Params.SensorNoiseLimit;
elseif rand_dy<-Params.sig_dy*Params.SensorNoiseLimit
    rand_dy = -Params.sig_dy*Params.SensorNoiseLimit;
end

%randn for phi with boundary
rand_phi=Params.sig_phi*randn;
if rand_phi>Params.sig_phi*Params.SensorNoiseLimit
    rand_phi = Params.sig_phi*Params.SensorNoiseLimit;
elseif rand_phi<-Params.sig_phi*Params.SensorNoiseLimit
    rand_phi = -Params.sig_phi*Params.SensorNoiseLimit;
end

%add randn noise in xr,yr,phi
xr=xr_local+rand_dx;
yr=yr_local+rand_dy;
phir=phir_local+rand_phi;
%% store the odometry data
Store.Zstate = [Store.Zstate;
    xr,  loop, loop-1, 1;
    yr,  loop, loop-1, 1;
    phir,  loop, loop-1, 1];



return;

