% This function is used to filter out duplicate edges in the dataset
function [ ZstateF, CovMatrixInvF ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv )

% Decide 2D or 3D from the dataset
% Assuming odometry edges are good (not outliers)

% For 2D dataSet, StepSize = 3
% For 3D dataSet, StepSize = 6
StepSize = size(find((Zstate(:,2)==2) & (Zstate(:,3)==1)), 1);
%

if(StepSize~=6 && StepSize~=3)
    StepSize
    disp('*********************data error!************************')
    return;
end

ZstateF = [];
CovMatrixInvF = sparse(0);

count = 1;
currentpose = 0;
MaxPose = max(Zstate(:,3));

while(currentpose <= MaxPose)

    PoseLine = find(Zstate(:,3) == currentpose);
    
    PoseIndex = PoseLine(1:StepSize:size(PoseLine,1)); 
    Flag = Zstate(PoseIndex, 4);
    
    dim = size(PoseIndex,1);
    k = 2;
    
    clear Select;
    Select =[];
    
    while(k <= dim)  
        if(Zstate(PoseIndex(k),2)~=Zstate(PoseIndex(k-1),2))
            
            Select = [Select; k-1];
            k = k + 1;
        else
            disp('remove duplicate edges!')
            tmp = k + 1;
            while( tmp<=dim && Zstate(PoseIndex(tmp),2)==Zstate(PoseIndex(k),2) )
                disp('reomve duplicate edges!')
                tmp =tmp + 1;
            end
          
            Dup = k-1 : tmp-1;

            findFlag = find(Flag(Dup,1)==1, 1, 'first');
            if(size(findFlag,1) == 0)
                Select = [Select; Dup(1)];
            else
                Select = [Select; Dup(findFlag)];
            end
            
            k = tmp + 1;
            
        end
        
    end 
    if(k == dim+1)
        Select = [Select; k-1];
    end
    
    
    PoseIndex = PoseIndex(Select);
        
    PoseLine = [];
    for j=1:size(PoseIndex,1)
        PoseLine = [PoseLine; (PoseIndex(j):(PoseIndex(j)+StepSize-1)).'];
    end
           
    DataNum = size(PoseLine, 1); 
    

    ZstateF([count:(count+DataNum-1)], :)= Zstate(PoseLine,:);
    CovMatrixInvF([count:(count+DataNum-1)], [count:(count+DataNum-1)]) = CovMatrixInv(PoseLine,PoseLine);

    count = count + DataNum;
    currentpose = currentpose + 1;
    
end
