function [Res_iSQP_2D, Obj_iSQP_2D, PrecisionRecall, N_All, ProcessInfo] = PoseSLAM_iSQP_2D( Zstate_2D, CovMatrix_2D)

[Zstate_EU, CovMatrix] = ConvertDataSetFrom2DTo3D ( Zstate_2D, CovMatrix_2D);

[Res_iSQP, Fval_iSQP, PrecisionRecall, N_All, ProcessInfo] = PoseSLAM_iSQP_3D ( Zstate_EU, CovMatrix);

[Res_iSQP_2D] = ConvertXstateFrom3DTo2D (Res_iSQP);

%%%%%%

InlierIndex = find(Zstate_2D(:,4) > 0);

Zstate_Inlier = Zstate_2D(InlierIndex, :);

CovMatrix_Inlier = CovMatrix_2D(InlierIndex, InlierIndex);

[ Obj_iSQP_2D ] = Obj_Pose_2D( Zstate_Inlier, inv(CovMatrix_Inlier), Res_iSQP_2D );

end
%%%%%%
function [Zstate_3D, CovMatrix_3D] = ConvertDataSetFrom2DTo3D ( Zstate_2D, CovMatrix_2D)

count3 = 1;
count2 = 1;

Zstate_3D = [];
CovMatrix_3D = sparse(0);

DataDim = size(Zstate_2D, 1);

while (count2 < DataDim)
    
    Zstate_3D([count3, count3+1, count3+5], [1,2,3,4]) = ... 
        Zstate_2D([count2, count2+1, count2+2], [1,2,3,4]);    
    
    Zstate_3D([count3+2, count3+3, count3+4], [1]) = [0; 0; 0];
    
    Zstate_3D([count3+2, count3+3, count3+4], [2,3,4]) = ...
        Zstate_2D([count2, count2+1, count2+2], [2,3,4]);
    
    CovMatrix_3D([count3, count3+1, count3+5], [count3, count3+1, count3+5]) = ...
        CovMatrix_2D([count2, count2+1, count2+2], [count2, count2+1, count2+2]);
    
    CovMatrix_3D([count3+2, count3+3, count3+4], [count3+2, count3+3, count3+4]) = speye(3);
    
    count3 = count3 + 6;
    count2 = count2 + 3;
        
end
end

%%%%%%%%%%%
function [Xstate_2D] = ConvertXstateFrom3DTo2D (Xstate_3D)

count3 = 1;
count2 = 1;

DataDim = size(Xstate_3D, 1);

while (count3 < DataDim)
 
    Xstate_2D([count2, count2+1, count2+2], [1,2]) = ...
        Xstate_3D([count3, count3+1, count3+5], [1,2]);
    
    count3 = count3 + 6;
    count2 = count2 + 3;
    
end
end