clear all
close all
clc

NewSphere = true;
NewOutlier = true;
%%%%%%%%%%%%%%%%% set noise level of simulation sphere %%%%%%%%%%%%%%%%%
noiseTran = 0.6;
noiseRota = 0.05;
nodesPerLevel = 10;
laps = 10;
radius = 10;

num_outlier = 50;  %20 % 10 % 5
groupsize = 1;
Niko = false;

%%%%%%%%%

SET_PATH_G2O

%%%%%%%%%

data_file = 'Data_G2O/MySphere.g2o';
data_file_outlier = 'Data_G2O/MySphere_Outlier.g2o';
input_file = 'Data_G2O/MySphere_Outlier_before.g2o';
output_file = 'Data_G2O/MySphere_Outlier_after.g2o';
summary_file = 'Data_G2O/MySphere_Outlier_summary.g2o';
add_outliers = 'Data_G2O/generateDataset.py';

%%% simulate sphere
noise =[ ' -noiseTranslation "', num2str(noiseTran),';',num2str(noiseTran),';',num2str(noiseTran)  ...
    '" -noiseRotation "', num2str(noiseRota),';',num2str(noiseRota),';',num2str(noiseRota),'"'];
para = [' -nodesPerLevel ', num2str(nodesPerLevel), ' -laps ', num2str(laps), ' -radius ',num2str(radius) ];
%
if(NewSphere)
string_cmd = [g2o_path,'create_sphere',' -o ', data_file, para , noise];
[resut_cmd info_cmd ] = system(string_cmd);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Now add outliers to the dataset, and solve again with iSQP and G2O
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% add outliers to the dataset
if(Niko)
string_cmd2 = [add_outliers, ' -i ', data_file, ' -o ', data_file_outlier, ...
               ' -n ', num2str(num_outlier), ' -g ', num2str(groupsize), ' -s'];
[result2_cmd, info2_cmd] = system(string_cmd2);
%
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS3D( data_file_outlier );
%
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
CovMatrix = inv(CovMatrixInv);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
else
    
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS3D( data_file );
%
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
%
[Zstate, CovMatrixInv] = AddOutliersPoseSLAM( Zstate, CovMatrixInv, num_outlier, 0 );
%
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
CovMatrix = inv(CovMatrixInv);
end

% tic
% [Res_SQP, Fval_SQP ] = PoseSLAM_SQP_3D ( Zstate, CovMatrix);
% PlotXstate3D_PoseSLAM( Res_SQP, 'k.--' );
% pause(0.05)
% toc

tic
[Res_SQP, Fval_SQP, N_All ] = PoseSLAM_iSQP_3D ( Zstate, CovMatrix);
%PlotXstate3D_PoseSLAM( Res_SQP, 'r.--' );
toc

[ Res_GN_DCS ] = PoseSLAM_G2O_GN_3D( Zstate, CovMatrixInv, 'DCS', 10, 200); % 10 %1
PlotXstate3D_PoseSLAM(  Res_GN_DCS, 'b.--' );

[ Res_GN_Cauchy ] = PoseSLAM_G2O_GN_3D( Zstate, CovMatrixInv, 'Cauchy', 1.0, 200);
PlotXstate3D_PoseSLAM( Res_GN_Cauchy, 'g.--' );




