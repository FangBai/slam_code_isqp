% This function is used to linearzie the objective from SE(3) to Euclidean
% data : the data preserved in SE(3) space
% init : the initial value (linearization point) in SE(3) space
% CovMatrix : the original covariance matrix
% Linear objective function is
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%   min  || J^{-1} *X - eta ||_P^2  %%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%     The objective can be written into the form as below       %%
%%%%%%%%%%%%%%%%%%%   min  || X - w ||_Q^2  %%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Q is the new covariance matrix for the linear objective function
% w is the constant vector describing the increments of the data
function [ J, eta ] = Linear_Obj_3D_PoseSLAM(data, init )

[ eta ] = Update_EUbySE_3D_PoseSLAM(data, init );

DataDim = max(size(eta));

EUcount = 1;

J = sparse(0);

while (EUcount < DataDim )
 
    J([EUcount, EUcount+1, EUcount+2], [EUcount, EUcount+1, EUcount+2]) = eye(3);
    J([EUcount+3, EUcount+4, EUcount+5], [EUcount+3, EUcount+4, EUcount+5]) = JacobianRightHandSO3( - eta([EUcount+3, EUcount+4, EUcount+5], 1) );

    EUcount = EUcount + 6;

end

end