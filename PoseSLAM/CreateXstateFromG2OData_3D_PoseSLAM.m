function [ Xstate ] = CreateXstateFromG2OData_3D_PoseSLAM( File_Path_Name )
threshold = 1e-10;
Zstate = [];
CovMatrixInv = sparse(0);
count = 1;

fid = fopen(File_Path_Name,'r');
g2oLine = fgetl(fid);

while g2oLine(1) ~=-1
    lineStr = textscan(g2oLine, '%s');
    
    if strcmp(lineStr{1}{1},'VERTEX_SE3:QUAT')
        
        PoseIndex = str2double(lineStr{1}{2});

        q_x = str2double(lineStr{1}{6});
        q_y = str2double(lineStr{1}{7});
        q_z = str2double(lineStr{1}{8});
        q_w = str2double(lineStr{1}{9}); 

        angle = 2*acos(q_w);
        if(angle<threshold)
            axis = [0;0;1];
        else
            axis = [q_x; q_y; q_z]/sin(angle/2);
        end
        if ( angle > pi)
            angle = 2*pi - angle;
            axis = -axis;
        end
        if(pi-angle<1e-10)
           if(axis(3)<0)
           axis = -axis;
           end    
        end
        AngleAxis = angle*axis;
        
        
        Xstate([count:count+5], [1,2]) = ... 
            [str2double(lineStr{1}{3}), PoseIndex;
             str2double(lineStr{1}{4}), PoseIndex;
             str2double(lineStr{1}{5}), PoseIndex;
             AngleAxis(1), PoseIndex;
             AngleAxis(2), PoseIndex;
             AngleAxis(3), PoseIndex; ];
                         
        count = count + 6;       
    end
    g2oLine = fgetl(fid);
end
fclose(fid);

end

