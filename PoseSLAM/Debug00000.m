clear all
close all
clc

choice = 3;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 DataFile = 'CSAIL_P';  % rec 3 % --- % 1, 2, 3   % 1 - same  %  2 - different  %  3 - slow convergence 
% DataFile = 'FRH_P';    % rec 2 % --- % 1, 2  % 1 - SQP correct  %  2 - same     
% DataFile = 'INTEL_P';   % rec 1 % 1, 2, 3, 4  %  1 - not converge  %  2 4 - SQP better solution and slow convergence % 3 - SQP better solution % 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
mkdir(['Results/', DataFile]);
delete(['Results/', DataFile, '/ConsoleOutput.txt']);
diary(['Results/', DataFile, '/ConsoleOutput.txt']);
diary on
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Directory = 'NoisyDataSet/LocalMinima_Odom/';
NoiseLevelStr = 'OM_0.05_0.05_0.1_LC_0.02_0.02_0.02';
data_file = [Directory, DataFile, '_Noisy_', NoiseLevelStr, '_', num2str(choice), '.g2o']

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( data_file );
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
CovMatrix = inv(CovMatrixInv);
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[Res_SQP, Obj_SQP ] = PoseSLAM_SQP_2D ( Zstate, CovMatrix);
PlotXstate2D_PoseSLAM( Res_SQP, 'r.--' );


Xstate0 = FuncCreateXstateFromZstate_2D_PoseSLAM( Zstate );
[ Res_GN, Obj_GN ] = PoseSLAM_G2O_GN( Zstate, CovMatrixInv, Xstate0, 100, '2D' );
PlotXstate2D_PoseSLAM( Res_GN, 'b.--' );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%[ Res_DCS, Obj_DCS, PR_DCS, Weight_DCS ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'DCS', 10.0, 200); % 660
[ Res_Cauchy, Obj_Cauchy, PR_Cauchy, Weight_Cauchy ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'Cauchy', 1.0, 200);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Res_Cauchy_GN, Obj_Cauchy_GN ] = PoseSLAM_G2O_GN( Zstate, CovMatrixInv, Res_Cauchy, 100, '2D' );
PlotXstate2D_PoseSLAM( Res_Cauchy_GN, 'g.--' );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

pause(1)

[Res_iSQP, Obj_iSQP, PR_iSQP, N_All ] = PoseSLAM_iSQP_2D ( Zstate, CovMatrix);
Obj_iSQP

disp(['Objective : SQP - GN - Cauchy - Cauchy+GN =>   ', num2str([Obj_SQP, Obj_GN, Obj_Cauchy, Obj_Cauchy_GN])]);

diary off;

h = [1, 2, 3, 100, 102, 105, 107];

savefig(h, ['Results/', DataFile, '/Figures']);







