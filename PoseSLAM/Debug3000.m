clear all
close all
clc

choice = 11;   %  11,

load(['MITb_Data/MITb10_', num2str(choice), '.mat']);

CovMatrix = inv(CovMatrixInv);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load('Benchmark/MITb_benchmark.mat');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[Res_iSQP, Obj_iSQP, PR_iSQP, N_All ] = PoseSLAM_iSQP_2D ( Zstate, CovMatrix);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Res_DCS, Obj_DCS, PR_DCS, Weight_DCS ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'DCS', 660, 200); % 660
PlotXstate2D_PoseSLAM( Res_DCS, 'b.--' );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Res_Cauchy, Obj_Cauchy, PR_Cauchy, Weight_Cauchy ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'Cauchy', 1.0, 200); 
PlotXstate2D_PoseSLAM( Res_Cauchy, 'g.--' );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Res_Cauchy_GN, Obj_Cauchy_GN ] = PoseSLAM_G2O_GN( Zstate, CovMatrixInv, Res_Cauchy, 100, '2D' );
PlotXstate2D_PoseSLAM( Res_Cauchy_GN, 'c.--' );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[ RMSE_isqp ] = CompareXstateResult( Res_MITb, Res_iSQP);
[ RMSE_dcs ] = CompareXstateResult( Res_MITb, Res_DCS);
[ RMSE_cauchy] = CompareXstateResult( Res_MITb, Res_Cauchy);
[ RMSE_cauchy_gn ] = CompareXstateResult( Res_MITb, Res_Cauchy_GN);

disp(['Objective : MITb - iSQP - DCS - Cauchy - Cauchy+GN =>   ', num2str([Obj_MITb, Obj_iSQP, Obj_DCS, Obj_Cauchy, Obj_Cauchy_GN])]);

disp(['RMSE : isqp - dcs - cauchy - cauchy_gn =>   ', num2str([RMSE_isqp, ...
                                                            RMSE_dcs, ...
                                                            RMSE_cauchy, ...
                                                            RMSE_cauchy_gn])]);
PR_iSQP
PR_DCS
PR_Cauchy

[ success_iSQP ] = Success_Decision_Strategy1( PR_iSQP )
[ success_DCS ] = Success_Decision_Strategy1( PR_DCS )
[ success_Cauchy ] = Success_Decision_Strategy1( PR_Cauchy )

OutlierConsAdded = [find(N_All(:,4)==-4), N_All(find(N_All(:,4)==-4),:)]
CorrectConsNotAdded = [find(N_All(:,4)==1), N_All(find(N_All(:,4)==1),:)]


% save MITb_Data/MITb10_16 Zstate CovMatrixInv

