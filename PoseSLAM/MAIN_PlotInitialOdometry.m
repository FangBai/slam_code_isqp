clear all
close all
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(1)
    choice = 3;
    DataFile = 'CSAIL_P';  % rec 3 % --- % 1, 2, 3   % 1 - same  %  2 - different  %  3 - slow convergence
    Directory = 'NoisyDataSet/LocalMinima_Odom/';
    NoiseLevelStr = 'OM_0.05_0.05_0.1_LC_0.02_0.02_0.02';
    InputFile = [Directory, DataFile, '_Noisy_', NoiseLevelStr, '_', num2str(choice), '.g2o']
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(0)
    DataFile = 'MITb';
    InputFile = ['DataSet/', DataFile, '.g2o'];
end
if(0)
    DataFile = 'intel';
    InputFile = ['DataSet/', DataFile, '.g2o'];
end
if(0)
    DataFile = 'M3500';
    InputFile = ['DataSet/', DataFile, '.g2o'];
end




[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( InputFile );
%
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
%
CovMatrix = inv(CovMatrixInv);
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Xstate0 = FuncCreateXstateFromZstate_2D_PoseSLAM( Zstate );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
PoseStr = 'b.--';
InlierStr = 'g.--';
OutlierStr = 'r.--';

PlotPoseEdgePoseSLAM2D( Xstate0, Zstate, PoseStr, InlierStr, OutlierStr, 1 );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
mkdir(['Results/InitialOdometry/']);

save(['Results/InitialOdometry/', DataFile], 'Xstate0', 'Zstate');
savefig(['Results/InitialOdometry/', DataFile]);


