% This function describe nonlinear constraints CEQ(X) = 0
% X   the variable/argument of constraints given in SE(3) space
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% M_s    the cell structure got from GetTriangle3D_PoseSLAM
%      M_s reflects indices for relative poses described in SE(3) group/space
%      M_s{i}: the i-th triangle
%      M_s{i}: num_odometry [odom1 odm2...domK] OdomToEndPose1 OdomToEndPose2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ CEQ_Quality, CEQ_Norm_Posi, CEQ_Norm_Rota ] = Nonlinear_Con_3D_PoseSLAM_Quality_All( Ms_All, Me_All, N_All, X0_SE, Var_EuInc, BenchMark)

ConNotConsidered = find(abs(N_All(:,4)) == 1);
ConConsidered = find(abs(N_All(:,4)) == 4);

N_new = N_All;
N_old = N_All(ConConsidered, :);


num_trian = size(Ms_All,2);
cur_trian = 1;

CEQ = zeros(6,1);

CEQ_Quality = zeros(num_trian,1);

CEQ_Norm_Posi = zeros(num_trian,1);

CEQ_Norm_Rota = zeros(num_trian,1); 

while(cur_trian<=num_trian)
    num_odom = Ms_All{cur_trian}(1);
    %%%%%%%%% odometry 1...k  
    count = 1;
    while(count<= num_odom)
        od(count)= Ms_All{cur_trian}(count+1);
        count = count + 1;
    end
    %%%%%%%%%observation 1
    OdomToEndPose1=Ms_All{cur_trian}(num_odom+2);
    %%%%%%%%% observation 2
    OdomToEndPose2=Ms_All{cur_trian}(num_odom+3);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    cur_odom = num_odom;
    R_cur_odom = [X0_SE(od(cur_odom)+3), X0_SE(od(cur_odom)+4), X0_SE(od(cur_odom)+5);
                  X0_SE(od(cur_odom)+6), X0_SE(od(cur_odom)+7), X0_SE(od(cur_odom)+8);
                  X0_SE(od(cur_odom)+9), X0_SE(od(cur_odom)+10), X0_SE(od(cur_odom)+11);];
    
    F = [X0_SE(od(cur_odom));X0_SE(od(cur_odom)+1);X0_SE(od(cur_odom)+2)] + ...
                R_cur_odom*[X0_SE(OdomToEndPose2);X0_SE(OdomToEndPose2+1);X0_SE(OdomToEndPose2+2)];
            
    Rota = [X0_SE(OdomToEndPose2+3), X0_SE(OdomToEndPose2+4), X0_SE(OdomToEndPose2+5);
            X0_SE(OdomToEndPose2+6), X0_SE(OdomToEndPose2+7), X0_SE(OdomToEndPose2+8);
            X0_SE(OdomToEndPose2+9), X0_SE(OdomToEndPose2+10), X0_SE(OdomToEndPose2+11);];
    
    Rota = R_cur_odom*Rota; 
    
    while( cur_odom~=1)
        cur_odom = cur_odom -1;
        R_cur_odom = [X0_SE(od(cur_odom)+3), X0_SE(od(cur_odom)+4), X0_SE(od(cur_odom)+5);
                      X0_SE(od(cur_odom)+6), X0_SE(od(cur_odom)+7), X0_SE(od(cur_odom)+8);
                      X0_SE(od(cur_odom)+9), X0_SE(od(cur_odom)+10), X0_SE(od(cur_odom)+11);];
                  
        F = [X0_SE(od(cur_odom));X0_SE(od(cur_odom)+1);X0_SE(od(cur_odom)+2)] + R_cur_odom*F;  
        Rota = R_cur_odom*Rota; 
    end
            
    CEQ([1:3],1) = F - [X0_SE(OdomToEndPose1);X0_SE(OdomToEndPose1+1);X0_SE(OdomToEndPose1+2)];
    
    R_cur_odom = [X0_SE(OdomToEndPose1+3), X0_SE(OdomToEndPose1+4), X0_SE(OdomToEndPose1+5);
                  X0_SE(OdomToEndPose1+6), X0_SE(OdomToEndPose1+7), X0_SE(OdomToEndPose1+8);
                  X0_SE(OdomToEndPose1+9), X0_SE(OdomToEndPose1+10), X0_SE(OdomToEndPose1+11);];

    
    CEQ([4:6],1) = Log_R2a((Rota.') * R_cur_odom);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    if( strcmp(BenchMark, 'Heuristic') )
              
        NormPosi = norm(CEQ([1:3],1));
        
        NormRota = norm(CEQ([4:6],1));
        
        CEQ_Norm_Posi(cur_trian, 1) = NormPosi;
        
        CEQ_Norm_Rota(cur_trian, 1) = NormRota;             
        
        % combine posi_error and rota_error together
        CEQ_Norm(cur_trian, 1) = norm([NormPosi, (NormRota/pi)*180*0.1]);  %
        
        NumEdges = N_new(cur_trian,2) - N_new(cur_trian,1) + 2;
        
        EdgeWeight = log(NumEdges);
        
        CEQ_Quality(cur_trian, 1) = CEQ_Norm(cur_trian, 1)/EdgeWeight;        
        
    elseif( strcmp(BenchMark, 'Probabilistic') )
                
        [ A_new ] = Linear_Con_3D_PoseSLAM( X0_SE, Me_All(cur_trian), Ms_All(cur_trian) );
        
        Var_T = A_new([1:3], :) * Var_EuInc * A_new([1:3], :).';
        
        Var_R = A_new([4:6], :) * Var_EuInc * A_new([4:6], :).';
        
        CEQ_Quality(cur_trian, 1) = CEQ([1:3],1).' * inv(Var_T) * CEQ([1:3],1) + CEQ([4:6],1).' * inv(Var_R) * CEQ([4:6],1);        
        CEQ_Quality(cur_trian, 1) = abs(CEQ_Quality(cur_trian, 1));
    else
        disp('BenchMark string is wrong!');
        return;    
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    cur_trian = cur_trian + 1;

end
    
end


% This function is used to Compute the number of new edges appeared in the constraints
% Cons_vertices (which is a row from N_trian format matrix)
function [ NumNewEdges ] = NumberNewEdges (N_old, Cons_vertices ) 
% Initilize number of new edges with the total number of edges
NumNewEdges = Cons_vertices(2) - Cons_vertices(1) + 2;
    
% Consider added constraints only

if(size(N_old,1) ==0)
    return;
end
%
start_node = Cons_vertices(1);
end_node = Cons_vertices(2);

OdoEdges([start_node+1 : end_node], 1) = sparse(1);
first_edge = 1;
second_edge = 1;

count = 1;
while(count <= size(N_old,1))
    if(N_old(count, 3)==Cons_vertices(3))
        if(N_old(count, 1)==start_node || N_old(count, 2)==start_node)
            first_edge = 0;
        end
        if(N_old(count, 1)==end_node || N_old(count, 2)==end_node)
            second_edge = 0;
        end
    end
    
    OdoEdges([N_old(count, 1)+1 : N_old(count, 2)], 1) = sparse(0);   
    
    count = count + 1;
end
NumNewEdges = sum(OdoEdges) + first_edge + second_edge;

%%%%%
end


% This function gives the Logarithm mapping to tranform a rotation R to the minimal representation Theta
% The minimal representation is described by axis angle representation
% Theta is a vertical vector
function [ Theta ] = Log_R2a( R )
threshold = 1e-10;
angle = acos( (trace(R) -1 )/2 );

if (abs(angle) <threshold ) % 0
    axis = [1; 0; 0;];
elseif(abs(angle - pi) <threshold )  % pi
   % disp('reach pi');
    axis = - [sign(R(2,3))*sqrt((1+R(1,1))/2); sign(R(1,3))*sqrt((1+R(2,2))/2); sign(R(1,2))*sqrt((1+R(3,3))/2) ];
else
    axis = [ R(3,2)-R(2,3);
             R(1,3)-R(3,1);
             R(2,1)-R(1,2); ];
    axis = axis/(2*sin(angle));
end

Theta = angle*axis;
[ Theta ] = WrapX( Theta );
end
% Wrap a arbitrary axis angle rotation into a rotation at interval [0, pi]
function [ X_v ] = WrapX( X_vec )
if (norm(X_vec)== 0)
    X_v = X_vec;
    return; 
end
angle = norm(X_vec);
axis  = X_vec/angle;
while (angle >= 2*pi)
    angle = angle - 2*pi;
end
while ( angle > pi)
    angle = 2*pi - angle;
    axis = -axis;  
end
if(pi-angle<1e-10)
   if(axis(3)<0)
       axis = -axis;
   end    
end

X_v = angle*axis;
end



