% This function is used to extract information to continue the algorithm
% from Zstate given in euclidean space.
% Variable_Indices :   odometry observation sequence of data/variables
% Data_SE :   vertical data vector in SE(3) space
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% M_e    the cell structure got from Get_triangle_3D
%      M_e reflects indices for odometries and observation in variables
%      described in Euclidean space
%      M_e{i}: the i-th triangle
%      M_e{i}: num_odmometry od1 od2..odn ob1 ob2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% M_s    the cell structure got from Get_triangle_3D
%      M_s reflects indices for odometries and observation in variables
%      described in SE(3) group/space
%      M_s{i}: the i-th triangle
%      M_s{i}: num_odmometry od1 od2..odn ob1 ob2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% N_trian = [pose1 pose2 feature]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ Data_SE, M_e, M_s, N_trian ] = ExtractDataSetInfo_PoseSLAM_iSQP( Zstate_EU )

[ Zstate_SE ] =  ZstateFromEuclideanToSE3_PoseSLAM( Zstate_EU );

[ M_e, N_trian ] = GetTriangle3D_PoseSLAM_All( Zstate_EU );

[ M_s ] = GetTriangle3D_PoseSLAM_All( Zstate_SE );

[ Data_SE ] = Zstate_SE(:,1);

if (max(size(M_e)) ~= max(size(M_s)))
    disp('Error: Different constrait numbers in SE(3) and Euclidean space!');
    return;
end
