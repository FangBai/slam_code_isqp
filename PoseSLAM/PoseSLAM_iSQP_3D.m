% This function is used to solve SLAM incrementally by SQP, which is robust to outliers
function [Res_iSQP, Obj_iSQP, PrecisionRecall, N_All, ProcessInfo] = PoseSLAM_iSQP_3D ( Zstate_EU, CovMatrix)
%%%%%%%%%%%%%%%%%%%%%%%%%%% CONS INFO DISPLAY %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
DISPLAY_CONS_INFO = true;
DISPLAY_RECOVERY_INFO = true;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 BenchMark = 'Heuristic';  
% BenchMark = 'Probabilistic';
%%%%%%%%%%%%%%%%%%%%%%%%%%%% SET FIGURE PLOT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
DISPLAY_ODOMETRY = true;    %
DISPLAY_CONS_QUALITY = false;
DISPLAY_ITER_RATIO = false;
DISPLAY_FVAL_RATIO = false;
DISPLAY_RECOVERY_RATIO = true;
DISPLAY_FVAL_GROWTH_HISTORY = true;
DISPLAY_CONS_QUALITY_HISTORY  = true;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%% SET RECOVERY RATIO THRESHOLD %%%%%%%%%%%%%%%%%%%%%%%%
% This value reflects the change of objective and fluctuation of iterations
RecoveryRatioThreshold = 10.0;
%%%%%%%%%%%%%%%%%%%%%%% TUNE PARAMETERS HERE%%%%%%%%%%%%%%%%%%%%%%%%%%%
ConsQualityRatioThreshold = 10.0;  % Average Edge Error Ratio
MaxConsNumPerStep = 1; % Control the number of constraints added each time
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Extract information
[ Data_SE, Me_All, Ms_All, N_All ] = ExtractDataSetInfo_PoseSLAM_iSQP( Zstate_EU );
%[  Edge_Index, Edges_Vertex ] = Edge_Related_Constraints( N_All, Zstate_EU, '3D');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%disp(['Number of Edges -> ', num2str(size(Zstate_EU,1)/6) ]);
%disp(['Number of Triangles - > ', num2str(size(Me_All,2))]);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The Last colum of N_All is the flag representing the constraint status
%%%%%%%%%%%%%%%%%% FLAGS for Correct Loop Closure %%%%%%%%%%%%%%%%%%%%%%%%%
% 1 :    Not considered and Not added   ( Correct Constraints )
% 2 :    considered but Not independant ( Dependant constraints ) ( Correct Constraints )
% 3 :    considered and ready to be added ( Correct Constraints )
% 4 :    considered and already added ( Correct Constraints )
%%%%%%%%%%%%%%%%%% FLAGS for Outliers %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% -1 :    Not considered and Not added   ( Wrong Constraints/Outliers )
% -2 :    considered but Not independant ( Dependant constraints ) ( Wrong Constraints/Outliers )
% -3 :    considered and ready to be added ( Wrong Constraints/Outliers )
% -4 :    considered and already added ( Wrong Constraints/Outliers )
%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Initialization %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Con_TotalAmount = size(N_All,1);
con_count = 1;  
%set initial value with data
X0_SE = Data_SE;
%stop sign
stopsign = 1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
NumEdgesHistory = [];
NumNewEdgesHistory = [];
ConsQualityHistory = [];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
FvalGrowth = [];
IterTimes = [];
RATIO_FVAL = [];
RATIO_ITER = [];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Fval_iSQP = 0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%ConNormInit = Nonlinear_Con_3D_PoseSLAM_Norm( X0_SE, Ms_All);
%%%%%%%%%%%%%%%%%%%%%%%%% FOR DISPLAY FIGURES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
firstIteration = true;
FLAG_CONS = sign(N_All(:,4));
%EDGES_CONS = N_All(:,2) - N_All(:,1) + 2 * ones(size(N_All,1),1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Var_EuInc = CovMatrix;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
while(stopsign>0)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    disp('==========================================================================================='); 
    if(DISPLAY_CONS_INFO)
        disp('  $:> NumAllEdges -|-ConPosi--ConRota-|-ConsQuality--medianConsQuality-|-QualityRatio--ConsCount -|-OutlierFlag');
    end
    %%%%%%%%%%%%%%%%%%%%% SELECCT CONSTRAINTS TO ADD %%%%%%%%%%%%%%%%%%%%%% 
    tb_chooseConstraints = cputime;
    
    [N_All, ConsQualityHistory ] = DetermineConToAddByNormAndIndependancy(Ms_All, Me_All, N_All, X0_SE, ...
        Var_EuInc, BenchMark, ConsQualityHistory, ConsQualityRatioThreshold, MaxConsNumPerStep, DISPLAY_CONS_INFO);

    ConToBeAdded = find(abs(N_All(:,4)) == 3);   % FLAG = 3 OR -3
       
    con_num = size(ConToBeAdded, 1);
    
    %%%%%%%%%######################################################%%%%%%%%
    for i = 1 : con_num
    NumEdges = N_All(ConToBeAdded(i), 2) - N_All(ConToBeAdded(i), 1) + 2;   
    
    NumNewEdges = NumberNewEdges (N_All(find(abs(N_All(:,4)) == 4),:), N_All(ConToBeAdded(i),:));
    
    NumEdgesHistory = [NumEdgesHistory; NumEdges];
    
    NumNewEdgesHistory = [NumNewEdgesHistory; NumNewEdges];
    end
    %%%%%%%%%######################################################%%%%%%%%    
    
    te_chooseConstraints = cputime;
    t_chooseConstraints = te_chooseConstraints - tb_chooseConstraints;
    %%%%%%%%%%%%%%%%%%%%%%% SELECCT CONSTRAINTS END %%%%%%%%%%%%%%%%%%%%%%%   
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % stop criteria
    if(con_num>0) 
        % Add selected constraints into the problem
        tb_Mcell = cputime;
        Me(con_count : (con_count+con_num-1)) = Me_All (ConToBeAdded);  
        Ms(con_count : (con_count+con_num-1)) = Ms_All (ConToBeAdded);
        con_count = con_count + con_num;
        te_Mcell = cputime;
        t_Mcell = te_Mcell - tb_Mcell;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%% plot constraint value %%%%%%%%%%%%%%%%%%%%%
        if(DISPLAY_CONS_QUALITY_HISTORY)
            figure(107)
            clf
            hold on
            plot([1:size(ConsQualityHistory,1)], ConsQualityHistory, 'ms-');
            title('Constraint Quality History')
            pause(0.01);
            hold off                  
        end
        %
        if(DISPLAY_CONS_QUALITY)
            tb_plotCons = cputime;
            [ Con_Quality ] = Nonlinear_Con_3D_PoseSLAM_Quality_All( Ms_All, Me_All, N_All, X0_SE, Var_EuInc, BenchMark);  

            figure(101)
            clf
            hold on
            for count = 1 : size(Con_Quality, 1)
                if(FLAG_CONS(count) > 0)
                    if(N_All(count, 4) == 1)
                        plot(count, Con_Quality(count), 'rd');
                    elseif(N_All(count, 4) == 2)
                        plot(count, Con_Quality(count), 'rx');
                    elseif(N_All(count, 4) == 3)
                        plot(count, Con_Quality(count), 'r+');
                    elseif(N_All(count, 4) == 4)
                        plot(count, Con_Quality(count), 'ro');
                    else
                        disp('$$$$$$$$$$$$$$$$$$$$$$$ data error! $$$$$$$$$$$$$$$$$$$$$$$');
                        disp('The FLAG SET for N Triangle is incorrect!')
                        disp([ 'FLAG IS  ', num2str(N_All(count, 4)), '    FLAG_CONS  ', num2str(FLAG_CONS(count)) ]);
                        % return;
                    end
                elseif(FLAG_CONS(count) < 0)
                    if(N_All(count, 4) == -1)
                        plot(count, Con_Quality(count), 'bd');
                    elseif(N_All(count, 4) == -2)
                        plot(count, Con_Quality(count), 'bx');
                    elseif(N_All(count, 4) == -3)
                        plot(count, Con_Quality(count), 'b+');
                    elseif(N_All(count, 4) == -4)
                        plot(count, Con_Quality(count), 'bo');
                    else
                        disp('$$$$$$$$$$$$$$$$$$$$$$$ data error! $$$$$$$$$$$$$$$$$$$$$$$');
                        disp('The FLAG SET for N Triangle is incorrect!')
                        disp([ 'FLAG IS  ', num2str(N_All(count, 4)), '    FLAG_CONS  ', num2str(FLAG_CONS(count)) ]);
                        %  return;
                    end
                else
                    disp('Flag error in N triangle');
                    return;
                end
            end
            %  plot(EDGES_CONS, 'y*--');
            title('Constraints Quality')
            hold off
            pause(0.01);
            te_plotCons = cputime;
            t_plotCons = te_plotCons - tb_plotCons;
            %   disp(['  Time to plot constraint norm = ', num2str(t_plotCons) ]);
        end
       
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%% CONTINUTE THE CODE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        tb_solveSubproblem  = cputime;
        
        N_All(ConToBeAdded,4) = 4*sign(N_All(ConToBeAdded,4));  % set FLAG to 4 OR -4  
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
        X0_SE_ORIGINAL = X0_SE;
        FVAL_ORIGINAL = Fval_iSQP;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
        [ X0_SE, Fval_iSQP, iter, Var_EuInc ] = My_method_solve_3D_PoseSLAM( X0_SE, Data_SE, CovMatrix, Me, Ms, BenchMark); 
        % -----------------------------------------------------------------

        FvalChange = (Fval_iSQP-FVAL_ORIGINAL)/con_num;       % Objective growth averaged by number of constraints
        
        % -----------------------------------------------------------------
        IterTimes = [IterTimes; iter];
        FvalGrowth = [FvalGrowth; FvalChange];
        
        te_solveSubproblem  = cputime;
        t_solveSubproblem  = te_solveSubproblem - tb_solveSubproblem;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %Display adding constraints process
        Constraint_added = max(size(Ms));
        disp(['=>> ConsAdd/ConsTotal=', num2str(Constraint_added), '/', num2str(Con_TotalAmount), ...
            '    ConsNum = ', num2str(con_num) ]);    
        disp(['+>> Timing:  ChooseConstraint = ', num2str(t_chooseConstraints), ... 
            '    IncreaseMcell = ', num2str(t_Mcell), ...
            '    SoveSubproblem = ', num2str(t_solveSubproblem) ]);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%% plot intermediate result %%%%%%%%%%%%%%%%%%%%          
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % For display figures
        RATIO_ITER = [RATIO_ITER; iter/median(IterTimes)];
        RATIO_FVAL = [RATIO_FVAL; FvalChange/median(FvalGrowth)];
        
        if(DISPLAY_FVAL_GROWTH_HISTORY)
            figure(102)
            clf
            hold on
            plot([1:size(FvalGrowth,1)], FvalGrowth, 'ms-');
            title('Objective Function Growth History')
            pause(0.01);
            hold off
        end        
        
        
        if(DISPLAY_ITER_RATIO)
            figure(103)
            clf
            hold on
            plot([1:size(RATIO_ITER,1)], RATIO_ITER, 'k.-');
            title('Iterations Ratio: iter/median')
            pause(0.01);
            hold off
        end
        
        if(DISPLAY_FVAL_RATIO)
            figure(104)
            clf
            hold on
            plot([1:size(RATIO_FVAL,1)], RATIO_FVAL, 'ms-');
            title('Objective Function Growth Ratio: Growth/median')
            pause(0.01);
            hold off
        end
        
        if(DISPLAY_RECOVERY_RATIO)
            TMP = RATIO_FVAL .* RATIO_ITER;
            figure(105)
            clf
            hold on
            plot([1:size(TMP,1)], TMP, 'ms-');
            title('Ratio .* times')
            pause(0.01);
            hold off
        end
        
        if(DISPLAY_ODOMETRY)
            tb_plotOdometry = cputime;
            figure(100)
            clf
            hold on
            [ Zstate_SE ] =  ZstateFromEuclideanToSE3_PoseSLAM( Zstate_EU );
            Zstate_SE(:,1) = X0_SE;
            [ Xstate_EU ]  = FuncCreateXstateFromZstate_SE_3D_PoseSLAM( Zstate_SE );
            count = 1;
            X_pose = [0];
            Y_pose = [0];
            Z_pose = [0];
            % extract pose and feature coordinate
            % pose coordinates are saved in X_pose, Y_pose
            % feature coordiantes are saved in X_feature, Y_feature
            while (count<=size(Xstate_EU,1))
                X_pose = [X_pose Xstate_EU(count,1)];
                Y_pose = [Y_pose Xstate_EU(count+1,1)];
                Z_pose = [Z_pose Xstate_EU(count+2,1)];
                count = count + 6;
            end
            % plot pose and feature
            plot3(X_pose,Y_pose,Z_pose, 'r.--');
            title('iSQP Trajectory')
            pause(0.01);
            hold off
            te_plotOdometry = cputime;
            t_plotOdometry = te_plotOdometry - tb_plotOdometry;
            %    disp(['  Time to plot odometry = ', num2str(t_plotOdometry) ]);
            %%%%
        end
        %%%%%%%%%%%%%%%%%%%%%%%%% FINISHED FIGURE %%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        disp(['+>> FvalChange = ', num2str(FvalChange),  ...
            '    median(FvalGrowth) = ', num2str(median(FvalGrowth)), ...
            '    Iter = ', num2str(iter), ...
            '    median(IterTimes) = ', num2str(median(IterTimes)), ...
            ]);
        disp(['+>> Recovery Ratio (ObjeGrowth * Iteration) = ', ...
            num2str((FvalChange/median(FvalGrowth)) * (iter/median(IterTimes))), ...
            '    ObjectiveGrowth = ', num2str(FvalChange/median(FvalGrowth)), ...
            '    IterFluctuation = ', num2str(iter/median(IterTimes)), ...
            ]);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%% DETECTION AND RECOVERY %%%%%%%%%%%%%%%%%%%%%
%%% @ Recovery %%%%       
        if(strcmp(BenchMark, 'Heuristic'))
            RecoveryCriteria = (FvalChange/median(FvalGrowth)) * (iter/median(IterTimes));
        elseif(strcmp(BenchMark, 'Probabilistic'))
            RecoveryCriteria = (FvalChange/median(FvalGrowth));
        else
            disp(['Error @ BenchMark string at RecoveryCriteria chosen']);
        end

        if( RecoveryCriteria > RecoveryRatioThreshold && FvalChange > 1.0 )
            disp('-------------------------------------------------------------------------------------------');
            disp('An outlier constraint detected!');
            disp('Start recovery proccess to romove outliers and reset variables');
            
            tb_recovery = cputime;
            % reset varibales
            X0_SE = X0_SE_ORIGINAL;
            Fval_iSQP = FVAL_ORIGINAL;
            
            con_count = con_count - con_num;
            
            IterTimes = IterTimes(1 : end-1);
            FvalGrowth = FvalGrowth(1 : end-1);
            
            ConsQualityHistory = ConsQualityHistory(1 : end-1);
            
            if(con_count == 1)
                Me = cell(0);
                Ms = cell(0);
            else
                Me = Me(1 : (con_count-1));
                Ms = Ms(1 : (con_count-1));
            end
            
            % reset FLAGS for related constraints in N_All
            [ N_All ] = RemoveOutlierConstraint(X0_SE_ORIGINAL, FVAL_ORIGINAL, Data_SE, CovMatrix, Me, Ms, Me_All, Ms_All, N_All, ConToBeAdded, IterTimes, FvalGrowth, RecoveryRatioThreshold, ...
                DISPLAY_RECOVERY_INFO, BenchMark);
            
            RATIO_FVAL = RATIO_FVAL(1 : end-1);
            RATIO_ITER = RATIO_ITER(1 : end-1);
            
            te_recovery = cputime;
            t_recovery = te_recovery - tb_recovery;
            disp([ 'Timing of Recovery = ', num2str(t_recovery) ]);
            disp('-------------------------------------------------------------------------------------------');
        end
        %%%%%%%%%%%%%%%%% FINISHED DETECTION AND RECOVERY %%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if(firstIteration)
            firstIteration = false;
             % disp('Please press any key to continue');
             % pause; % PAUSE AT FIRST ITERATION TO ARRANGE FIGURES
        end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    else   % no constraint to be added
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % constraint
        if(DISPLAY_CONS_QUALITY)
            tb_plotCons = cputime;
            [ Con_Quality ] = Nonlinear_Con_3D_PoseSLAM_Quality_All( Ms_All, Me_All, N_All, X0_SE, Var_EuInc, BenchMark);
            
            figure(101)
            clf
            hold on
            for count = 1 : size(Con_Quality, 1)
                if(FLAG_CONS(count) > 0)
                    if(N_All(count, 4) == 1)
                        plot(count, Con_Quality(count), 'rd');
                    elseif(N_All(count, 4) == 2)
                        plot(count, Con_Quality(count), 'rx');
                    elseif(N_All(count, 4) == 3)
                        plot(count, Con_Quality(count), 'r+');
                    elseif(N_All(count, 4) == 4)
                        plot(count, Con_Quality(count), 'ro');
                    else
                        disp('$$$$$$$$$$$$$$$$$$$$$$$ data error! $$$$$$$$$$$$$$$$$$$$$$$');
                        disp('The FLAG SET for N Triangle is incorrect!')
                        disp([ 'FLAG IS  ', num2str(N_All(count, 4)), '    FLAG_CONS  ', num2str(FLAG_CONS(count)) ]);
                        % return;
                    end
                elseif(FLAG_CONS(count) < 0)
                    if(N_All(count, 4) == -1)
                        plot(count, Con_Quality(count), 'bd');
                    elseif(N_All(count, 4) == -2)
                        plot(count, Con_Quality(count), 'bx');
                    elseif(N_All(count, 4) == -3)
                        plot(count, Con_Quality(count), 'b+');
                    elseif(N_All(count, 4) == -4)
                        plot(count, Con_Quality(count), 'bo');
                    else
                        disp('$$$$$$$$$$$$$$$$$$$$$$$ data error! $$$$$$$$$$$$$$$$$$$$$$$');
                        disp('The FLAG SET for N Triangle is incorrect!')
                        disp([ 'FLAG IS  ', num2str(N_All(count, 4)), '    FLAG_CONS  ', num2str(FLAG_CONS(count)) ]);
                        %  return;
                    end
                else
                    disp('Flag error in N triangle');
                    return;
                end
            end
            %  plot(EDGES_CONS, 'y*--');
            title('Constraints Quality')
            hold off
            pause(0.01);
            te_plotCons = cputime;
            t_plotCons = te_plotCons - tb_plotCons;
            %   disp(['  Time to plot constraint norm = ', num2str(t_plotCons) ]);
        end
        % odometry
        if(DISPLAY_ODOMETRY)
            tb_plotOdometry = cputime;
            figure(100)
            clf
            hold on
            [ Zstate_SE ] =  ZstateFromEuclideanToSE3_PoseSLAM( Zstate_EU );
            Zstate_SE(:,1) = X0_SE;
            [ Xstate_EU ]  = FuncCreateXstateFromZstate_SE_3D_PoseSLAM( Zstate_SE );
            count = 1;
            X_pose = [0];
            Y_pose = [0];
            Z_pose = [0];
            % extract pose and feature coordinate
            % pose coordinates are saved in X_pose, Y_pose
            % feature coordiantes are saved in X_feature, Y_feature
            while (count<=size(Xstate_EU,1))
                X_pose = [X_pose Xstate_EU(count,1)];
                Y_pose = [Y_pose Xstate_EU(count+1,1)];
                Z_pose = [Z_pose Xstate_EU(count+2,1)];
                count = count + 6;
            end
            % plot pose and feature
            plot3(X_pose,Y_pose,Z_pose, 'r.--');
            title('iSQP Trajectory')
            pause(0.01);
            hold off
            te_plotOdometry = cputime;
            t_plotOdometry = te_plotOdometry - tb_plotOdometry;
            %    disp(['  Time to plot odometry = ', num2str(t_plotOdometry) ]);
            %%%%
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%                    FINISH ALGORITHM                         %%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        disp('END:   STOP INCREMENTAL SQP ALGORITHM                                  ');
        stopsign = 0;
    end
    
    disp('===========================================================================================');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% N_All
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%save FvalIter/FvalIter_ IterTimes FvalGrowth
[ PrecisionRecall ] = PrecisionRecallStatistics( Zstate_EU, N_All );

% get Xstate form of solution
Zstate_SE = ZstateFromEuclideanToSE3_PoseSLAM( Zstate_EU );
Zstate_SE(:,1) = X0_SE;
Res_iSQP  = FuncCreateXstateFromZstate_SE_3D_PoseSLAM( Zstate_SE );   

%%%%%%%% Compute objective without outliers %%%%%%%%%%%%%%%%%%%%%%

InlierIndex = find(Zstate_EU(:,4) > 0);

Zstate_Inlier = Zstate_EU(InlierIndex, :);

CovMatrix_Inlier = CovMatrix(InlierIndex, InlierIndex);

[ Obj_iSQP ] = Obj_Pose_3D( Zstate_Inlier, inv(CovMatrix_Inlier), Res_iSQP );


ProcessInfo.ConsQualityHistory = ConsQualityHistory;
ProcessInfo.FvalGrowth = FvalGrowth;
ProcessInfo.NumEdgesHistory = NumEdgesHistory;
ProcessInfo.NumNewEdgesHistory = NumNewEdgesHistory;
ProcessInfo.IterTimes = IterTimes;

end

% This function is used to decide the constraints to be added by norm of constraints and
% independancies with constraints that have already been added
function [N_All, ConsQualityHistory] = DetermineConToAddByNormAndIndependancy(Ms_All, Me_All, N_All, X0_SE, ...
    Var_EuInc, BenchMark, ConsQualityHistory, ConsQualityRatioThreshold, MaxConsNumPerStep, DISPLAY_CONS_INFO)


tb_functionChooseCons = cputime;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The Last colum of N_All is the flag representing the constraint status
%%%%%%%%%%%%%%%%%% FLAGS for Correct Loop Closure %%%%%%%%%%%%%%%%%%%%%%
% 1 :    Not considered and Not added   ( Correct Constraints )
% 2 :    considered but Not independant ( Dependant constraints ) ( Correct Constraints )
% 3 :    considered and ready to be added ( Correct Constraints )
% 4 :    considered and already added ( Correct Constraints )
%%%%%%%%%%%%%%%%%% FLAGS for Outliers %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% -1 :    Not considered and Not added   ( Wrong Constraints/Outliers )
% -2 :    considered but Not independant ( Dependant constraints ) ( Wrong Constraints/Outliers )
% -3 :    considered and ready to be added ( Wrong Constraints/Outliers )
% -4 :    considered and already added ( Wrong Constraints/Outliers )
%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Initialization %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%d
ConNotConsidered = find(abs(N_All(:,4)) == 1);
% N_All
%%%%%%%%%%%%%%%%
if(size(ConNotConsidered,1) ==0)
    disp('END @: All the constraints have been considered!');
    return;
end

tb_ComputeConsQuality = cputime;

[ Con_Quality, Con_Norm_Posi, Con_Norm_Rota ] = Nonlinear_Con_3D_PoseSLAM_Quality( Ms_All, Me_All, N_All, X0_SE, Var_EuInc, BenchMark);  

te_ComputeConsQuality = cputime;
t_ComputeConsQuality = te_ComputeConsQuality - tb_ComputeConsQuality;

% sort constraints in ascending order by the norm of constraints
[Con_Quality, CONS_SEQUENCE] = sort(Con_Quality,'ascend');  % descend
ConNotConsidered = ConNotConsidered(CONS_SEQUENCE);

Con_Norm_Posi = Con_Norm_Posi(CONS_SEQUENCE);
Con_Norm_Rota = Con_Norm_Rota(CONS_SEQUENCE);

%%%%%%%%%%%%%%%%%%%%%%%%%%%% New Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [ An, bn ] = Linear_Con_3D_PoseSLAM( X0_SE, Me_All(ConNotConsidered), Ms_All(ConNotConsidered) );
% 
% [ CEQ ] = Nonlinear_Con_3D_PoseSLAM( X, M_s );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

stopsign = false;
count = 0;

ConAdded = find(abs(N_All(:,4)) == 4);

Dim = max(size(ConNotConsidered));

ConToAdd = 0;

ExistIndependentConstraint = false;

while (stopsign==false && (count+1)<=Dim && ConToAdd < MaxConsNumPerStep)
    count = count+1;   % Increase Index %
    
    tb_CheckIndependency = cputime;
    Independant = CheckIndependantConstraint (N_All(ConAdded,:), N_All(ConNotConsidered(count),:));
    te_CheckIndependency = cputime;
    t_CheckIndependency = te_CheckIndependency - tb_CheckIndependency;
    
    if(Independant)        
        
        ExistIndependentConstraint = true;
        
        NumEdges = N_All(ConNotConsidered(count), 2) - N_All(ConNotConsidered(count), 1) + 2;     
        
        if(DISPLAY_CONS_INFO)
            if(N_All(ConNotConsidered(count),4) == -1)
                disp(['    :>', num2str([NumEdges]), '  ||  ', ...
                    num2str([ Con_Norm_Posi(count), Con_Norm_Rota(count) ]), '  ||  ', ...
                    num2str([ Con_Quality(count), median(ConsQualityHistory)]), '  ||  ', ...
                    num2str([ Con_Quality(count)/median(ConsQualityHistory), ConToAdd+1]), ...
                    '  ||   YES  #**#']);
            elseif(N_All(ConNotConsidered(count),4) == 1)
                disp(['    :>', num2str([NumEdges]), '  ||  ', ...
                    num2str([ Con_Norm_Posi(count), Con_Norm_Rota(count) ]), '  ||  ', ...
                    num2str([ Con_Quality(count), median(ConsQualityHistory)]), '  ||  ', ...
                    num2str([ Con_Quality(count)/median(ConsQualityHistory), ConToAdd+1]), ...
                    '  ||   NO']);
            else
                disp('Data Error! FLAG FOR UNCONISDERED CONSTRAINT IS INCORRECT');
                return;
            end
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%
                
        if(size(ConsQualityHistory,1) <= 3)     % first SEVERAL constraint
            
        %   disp('add first constaint');
        
            ConsQualityHistory = [ConsQualityHistory; Con_Quality(count)];
      
            N_All(ConNotConsidered(count),4) = 3*sign(N_All(ConNotConsidered(count),4));
                       
            ConAdded = [ConAdded; ConNotConsidered(count)];
            
            ConToAdd = ConToAdd + 1;

            
        elseif( Con_Quality(count)/median(ConsQualityHistory) <= ConsQualityRatioThreshold  )
            
            ConsQualityHistory = [ConsQualityHistory; Con_Quality(count)];
        
            N_All(ConNotConsidered(count),4) = 3*sign(N_All(ConNotConsidered(count),4));
            
            ConAdded = [ConAdded; ConNotConsidered(count)];     
           
            ConToAdd = ConToAdd + 1;
            
        else
            
            disp('END @: No constraints satisfy EdgeRatioThreshold');
            stopsign = true;

        end
        
    elseif(~Independant)  
        
        N_All(ConNotConsidered(count),4) = 2*sign(N_All(ConNotConsidered(count),4));   
        
    else
        disp('error');
        return;
    end  
end

if(~ExistIndependentConstraint)
    disp('END @: All the constraints have been considered!');
end    
    
te_functionChooseCons = cputime;

t_functionChooseCons = te_functionChooseCons - tb_functionChooseCons;

disp(['Time function choose Constraint = ', num2str(t_functionChooseCons), ...
    '    Time Compute Constraint Quality = ', num2str(t_ComputeConsQuality), ...
    '    Time Check Independency = ', num2str(t_CheckIndependency) ]);

end

% This function is usde to check if current constraint given by Cons_vertices (which is a row from N_trian format matrix) is independant
% from the constraints in N_trian or not
% If Independant, return 1
% If dependant, return 0;
function [Independant ] = CheckIndependantConstraint (N_trian, Cons_vertices ) 
    
if(size(N_trian,1) ==0)
    Independant = 1;
    return;
end

RelatedCons = N_trian(find(N_trian(:,3)==Cons_vertices(3)),:);

if(size(RelatedCons,1) ==0)
    Independant = 1;
    return;
end

Dim = max(max(RelatedCons(:,2)), Cons_vertices(2));

AdjacentMatrix = sparse((Dim+1),(Dim+1));

count = 1;

while(count<=size(RelatedCons,1))
  
AdjacentMatrix((RelatedCons(count,1)+1), (RelatedCons(count,2)+1)) = 1;

count = count + 1;

end

AdjacentMatrix((Cons_vertices(1)+1), (Cons_vertices(2)+1)) = 1;

AdjacentMatrix = AdjacentMatrix + AdjacentMatrix.';

ExistCycle = ExistCycleUndirectedGraph( AdjacentMatrix );

Independant = ~ExistCycle;

end


% This function use AdjacentMatrix of an undirected graph to check if there exist cycles in the graph
% ExistCycle : 1   | There exist circles in the graph
% ExistCycle : 0   | There are no circles in the graph
function [ ExistCycle ] = ExistCycleUndirectedGraph( AdjacentMatrix )

stopsign = 1;

while(stopsign > 0)
    
DimIn = size(AdjacentMatrix,1);

VertexDeg = sum(AdjacentMatrix);

CycleVertex = find(VertexDeg >=2);

AdjacentMatrix = AdjacentMatrix(CycleVertex, CycleVertex);

DimOut = size(AdjacentMatrix,1);

if(DimOut==DimIn || DimOut==0)
    stopsign =0;
    if(DimOut >0)
        ExistCycle = 1;
    else
        ExistCycle = 0;
    end
end

end
end

% This function is used to remove the outlier constraint from the candiated
function [ N_All ] = RemoveOutlierConstraint(X0_SE_ORIGINAL, FVAL_ORIGINAL, Data_SE, CovMatrix, Me, Ms, Me_All, Ms_All, N_All, ConToBeAdded, IterTimes, FvalGrowth, RecoveryRatioThreshold, ...
    DISPLAY_RECOVERY_INFO, BenchMark);
    if(DISPLAY_RECOVERY_INFO)
    disp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');  
    disp('process identify outlier constraints within all the candidate constraints added this time');
    end
    data_dim = size(ConToBeAdded, 1);
         
    RatioCache = ones(data_dim, 1);
    
    con_size = size(Me, 2);

    outlier_found = false;
    
    if(con_size == 0)
        disp('Error: The number of correctly considered constraints is zero'); 
        return;
    end
    count = 1;
    %%%%%%%%%%%%%%%%%%%%%%% SELECCT CONSTRAINTS END %%%%%%%%%%%%%%%%%%%%%%%   
    while(count <= data_dim)
        Me(con_size+1) = Me_All (ConToBeAdded(count));
        Ms(con_size+1) = Ms_All (ConToBeAdded(count));
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        [ X0_SE_NEW, Fval_iSQP_NEW, iter ] = My_method_solve_3D_PoseSLAM( X0_SE_ORIGINAL, Data_SE, CovMatrix, Me, Ms, BenchMark);
        
        FvalChange = Fval_iSQP_NEW - FVAL_ORIGINAL;
        
        IterTimesNew = [IterTimes; iter];
        
        FvalGrowthNew = [FvalGrowth; FvalChange];
        
        RecoverRatio = (FvalChange/median(FvalGrowthNew)) * (iter/median(IterTimesNew));
        % SET FLAGES
        
        if( RecoverRatio > RecoveryRatioThreshold )   
            if(DISPLAY_RECOVERY_INFO)
            disp(['Detect an outlier constraint with ratio:  ', num2str(RecoverRatio)]);
            disp(['FvalRatio = ', num2str(FvalChange/median(FvalGrowthNew)), '    IterRatio = ', num2str(iter/median(IterTimesNew))  ]);
            disp(['original flag = ', num2str(N_All(ConToBeAdded(count),4)),  '    new flag = -2']);
            end
            N_All(ConToBeAdded(count),4) = -2; % -2   % a detected outlier constraint   
            outlier_found = true;
        else
            if(DISPLAY_RECOVERY_INFO)
            disp(['This is an inlier constraint with ratio:  ', num2str(RecoverRatio)]);
            disp(['FvalRatio = ', num2str(FvalChange/median(FvalGrowthNew)), '    IterRatio = ', num2str(iter/median(IterTimesNew))  ]);
            disp(['original flag = ', num2str(N_All(ConToBeAdded(count),4)),  '    new flag = 1']);
            end
            N_All(ConToBeAdded(count),4) = 1;  % an inlier constraint
        end

        RatioCache(count) = RecoverRatio;

        Me = Me(1 : con_size);
        Ms = Ms(1 : con_size);
        
        count = count + 1;
    end
    
    if(~outlier_found)
        if(DISPLAY_RECOVERY_INFO)
        disp('No outlier found! Possiblely current RecoveryRatioThreshold is too big!')
        end
        [Y, Index] = max(RatioCache);
        if(DISPLAY_RECOVERY_INFO)
        disp(['Determine the largest ratio ',num2str(RatioCache(Index)), ' as an outlier']);
        end
        N_All(ConToBeAdded(Index),4) = -2; % -2   % a detected outlier constraint
    end
    if(DISPLAY_RECOVERY_INFO)
    disp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    end
end




% This function is used to Compute the number of new edges appeared in the constraints
% Cons_vertices (which is a row from N_trian format matrix)
function [ NumNewEdges ] = NumberNewEdges (N_old, Cons_vertices ) 
% Initilize number of new edges with the total number of edges
NumNewEdges = Cons_vertices(2) - Cons_vertices(1) + 2;
    
% Consider added constraints only

if(size(N_old,1) ==0)
    return;
end
%
start_node = Cons_vertices(1);
end_node = Cons_vertices(2);

OdoEdges([start_node+1 : end_node], 1) = sparse(1);
first_edge = 1;
second_edge = 1;

count = 1;
while(count <= size(N_old,1))
    if(N_old(count, 3)==Cons_vertices(3))
        if(N_old(count, 1)==start_node || N_old(count, 2)==start_node)
            first_edge = 0;
        end
        if(N_old(count, 1)==end_node || N_old(count, 2)==end_node)
            second_edge = 0;
        end
    end
    
    OdoEdges([N_old(count, 1)+1 : N_old(count, 2)], 1) = sparse(0);   
    
    count = count + 1;
end
NumNewEdges = sum(OdoEdges) + first_edge + second_edge;

%%%%%
end
