function [ Xstate_2D ]  = FuncCreateXstateFromZstate_2D_PoseSLAM( Zstate_2D )

Zcount = 1;
Xcount = 1;
Xstate_2D = [];
CurrentPosi = [0; 0];
CurrentRota = eye(2);

Zstate_2D = Zstate_2D(find((Zstate_2D(:,2) - Zstate_2D(:,3)) == 1), :);
DataDim = size(Zstate_2D,1);

while ( Zcount <= DataDim )

    CurrentPosi = CurrentRota*Zstate_2D([Zcount, Zcount+1], 1) + CurrentPosi;
    CurrentRota = CurrentRota*R( Zstate_2D([Zcount+2], 1));  
        
    Xstate_2D([Xcount, Xcount+1], 1) = CurrentPosi;
    Xstate_2D([Xcount+2], 1) = R2a(CurrentRota);
        
    Xstate_2D([Xcount : Xcount+2],[2]) = Zstate_2D( [Zcount : Zcount+2],[2]); 
    Xcount = Xcount + 3;
    Zcount = Zcount + 3;    

end
end
% Get rotation matrix A with respect to angle phi
function [A] = R(phi)
A=[cos(phi),-sin(phi);sin(phi),cos(phi)];
end
% Get angle phi from rotation matrix R
function [phi] = R2a(R)
phi = atan2(R(2,1), R(1,1));
end