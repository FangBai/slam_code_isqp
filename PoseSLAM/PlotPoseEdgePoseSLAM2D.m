%
function [ ] = PlotPoseEdgePoseSLAM2D( Xstate, Zstate, PoseStr, InlierStr, OutlierStr, Fid )

Zstate = Zstate([1:3:end], :);

for i = 1 : size(Zstate, 1)
    
    Flag = Zstate(i, 4);
    
    StartPose = Zstate(i, 3);
    EndPose = Zstate(i, 2);
    
    if(EndPose-StartPose > 1)
        LoopClosure = true;
    else
        LoopClosure = false;
    end
    
    if(LoopClosure && Flag == 1)      
               
        if(StartPose == 0)
            StartPosition = [0; 0;];
        else
            StartPosition = Xstate([3*StartPose-2, 3*StartPose-1], 1);
        end
        if(EndPose == 0)
            EndPosition = [0; 0;];
        else
            EndPosition = Xstate([3*EndPose-2, 3*EndPose-1], 1);
        end
        
        X = [StartPosition(1), EndPosition(1)];
        Y = [StartPosition(2), EndPosition(2)];
               
        figure(Fid)
        % plot edge
        hold on
        plot(X, Y, InlierStr);
        hold off
        
    elseif(LoopClosure && Flag == -1)
        if(StartPose == 0)
            StartPosition = [0; 0;];
        else
            StartPosition = Xstate([3*StartPose-2, 3*StartPose-1], 1);
        end
        if(EndPose == 0)
            EndPosition = [0; 0;];
        else
            EndPosition = Xstate([3*EndPose-2, 3*EndPose-1], 1);
        end
        
        X = [StartPosition(1), EndPosition(1)];
        Y = [StartPosition(2), EndPosition(2)];
               
        figure(Fid)
        % plot edge
        hold on
        plot(X, Y, OutlierStr);
        hold off        
       
    end
    
end

%%
count = 1;
X_pose = [0];
Y_pose = [0];

% extract pose and feature coordinate
% pose coordinates are saved in X_pose, Y_pose
% feature coordiantes are saved in X_feature, Y_feature
while (count<=size(Xstate,1))
        X_pose = [X_pose Xstate(count,1)];
        Y_pose = [Y_pose Xstate(count+1,1)];
        count = count + 3;     
end

figure(Fid);
% plot pose 
hold on
plot(X_pose,Y_pose, PoseStr);
hold off

axis off


end



