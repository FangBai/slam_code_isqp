% Get triangles that describe edges/peaks of constraints from data
% M_trian = [num_odometry [odom1 odm2...domK] OdomToEndPose1 OdomToEndPose2]  Namely edges
% N_trian = [StartPose1 StartPose2 EndPose Flag]  Namely peaks
% The Last colum of N_All is the flag representing the constraint status
%%%%%%%%%%%%%%%%%% FLAGS for Correct Loop Closure %%%%%%%%%%%%%%%%%%%%%%
% 1 :    Not considered and Not added   ( Correct Constraints )
% 2 :    considered but Not independant ( Dependant constraints ) ( Correct Constraints )
% 3 :    considered and ready to be added ( Correct Constraints )
% 4 :    considered and already added ( Correct Constraints )
%%%%%%%%%%%%%%%%%% FLAGS for Outliers %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% -1 :    Not considered and Not added   ( Wrong Constraints/Outliers )
% -2 :    considered but Not independant ( Dependant constraints ) ( Wrong Constraints/Outliers )
% -3 :    considered and ready to be added ( Wrong Constraints/Outliers )
% -4 :    considered and already added ( Wrong Constraints/Outliers )

%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Initialization %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ M_trian, N_trian ] = GetTriangle3D_PoseSLAM_All( Zstate )
M_trian=cell(0);
N_trian=[];
count_tria=0;
EndPose=1;

EdgeCons = 1;

EndPoseNum = max(Zstate(:,2));

if(Zstate(1,2)==Zstate(12,2) && Zstate(1,3)==Zstate(12,3))
    stepsize = 12;   
elseif(Zstate(1,2)==Zstate(6,2) && Zstate(1,3)==Zstate(6,3))
    stepsize = 6;
else
    disp('data error!');
    return;
end


OdometryLine = find((Zstate(:,2) - Zstate(:,3)) == 1);
OdometryIndex = OdometryLine([1:stepsize:size(OdometryLine,1)], :);

while (EndPose <= EndPoseNum)
   
    EndPoseLine = find(Zstate(:,2) == EndPose);
    EndPoseIndex = EndPoseLine([1:stepsize:size(EndPoseLine,1)], :);
    
    TriangleNum = size(EndPoseIndex,1) - 1;
    
    if(TriangleNum == -1)
        EndPose = EndPose + 1;
    elseif (TriangleNum == 0)
        EndPose = EndPose + 1;
    elseif( TriangleNum >=1 )
        count_1 = 1;
        while(count_1 < size(EndPoseIndex,1))   % count1 = 1 :  size(EndPoseIndex)-1
            count_2 = count_1 + 1;
            while(count_2 <= size(EndPoseIndex,1))
                           
                count_tria = count_tria + 1;
                   %
               if(Zstate(EndPoseIndex(count_1),4)==1 && Zstate(EndPoseIndex(count_2),4)==1)    % All edges are good
                    N_trian(count_tria, [1:4]) = [Zstate(EndPoseIndex(count_1), 3), Zstate(EndPoseIndex(count_2), 3), EndPose, 1];      
               else         % at least one outlier edge
                    N_trian(count_tria, [1:4]) = [Zstate(EndPoseIndex(count_1), 3), Zstate(EndPoseIndex(count_2), 3), EndPose, -1];           
               end
                   %
                num_odom = Zstate(EndPoseIndex(count_2), 3) - Zstate(EndPoseIndex(count_1), 3);
                M_trian{count_tria}(1) = num_odom;
            
                for i =1: num_odom
                    M_trian{count_tria}(i+1) = OdometryIndex(Zstate(EndPoseIndex(count_1),3)+i);             
                end

                M_trian{count_tria}(num_odom+2) = EndPoseIndex(count_1);
                M_trian{count_tria}(num_odom+3) = EndPoseIndex(count_2);               
      
                count_2 = count_2 + 1;
            end
  
            count_1 = count_1 + 1;
        end
        EndPose = EndPose + 1;
    end

end


end