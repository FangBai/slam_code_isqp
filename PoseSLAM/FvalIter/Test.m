clear all
close all
clc

choice = 3;

load(['FvalIter', num2str(choice), '.mat']);

FvalGrowth = FvalGrowth - [0; FvalGrowth(1 : size(FvalGrowth, 1)-1)]; % OBJ increments

for i = 1 : size(IterTimes, 1)

    MEAN_ITER(i, 1) = mean(IterTimes(1:i)); 
    
    VAR_ITER(i, 1) = var(IterTimes(1:i));
   

end

for i = 1 : size(FvalGrowth, 1)

    MEAN_FVAL(i, 1) = mean(FvalGrowth(1:i));
    
    VAR_FVAL(i, 1) = var(FvalGrowth(1:i));
   
end

format shortG

FVAL = [FvalGrowth, gradient(FvalGrowth), MEAN_FVAL, VAR_FVAL] 

ITER = [IterTimes, gradient(IterTimes), MEAN_ITER, VAR_ITER]


