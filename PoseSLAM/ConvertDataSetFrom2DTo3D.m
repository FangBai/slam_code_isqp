function [Zstate_3D, CovMatrix_3D] = ConvertDataSetFrom2DTo3D ( Zstate_2D, CovMatrix_2D)

count3 = 1;
count2 = 1;

Zstate_3D = [];
CovMatrix_3D = sparse(0);

DataDim = size(Zstate_2D, 1);

while (count2 < DataDim)
    
    Zstate_3D([count3, count3+1, count3+5], [1,2,3,4]) = ... 
        Zstate_2D([count2, count2+1, count2+2], [1,2,3,4]);    
    
    Zstate_3D([count3+2, count3+3, count3+4], [1]) = [0; 0; 0];
    
    Zstate_3D([count3+2, count3+3, count3+4], [2,3,4]) = ...
        Zstate_2D([count2, count2+1, count2+2], [2,3,4]);
    
    CovMatrix_3D([count3, count3+1, count3+5], [count3, count3+1, count3+5]) = ...
        CovMatrix_2D([count2, count2+1, count2+2], [count2, count2+1, count2+2]);
    
    CovMatrix_3D([count3+2, count3+3, count3+4], [count3+2, count3+3, count3+4]) = speye(3);
    
    count3 = count3 + 6;
    count2 = count2 + 3;
        
end

end

