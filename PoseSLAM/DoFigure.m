%

% 12.3, zhan

function DoFigure

global Truth;

% disp(' *** entering DoFigure');

figure
xlabel('X(m)');
ylabel('Y(m)');

axis ([-10, 130, -10, 130])    % size of the map

axis square

plot(Truth.Beacons(:,2),Truth.Beacons(:,3),'b.');
hold on

plot(Truth.robot(1,:), Truth.robot(2,:), 'ro');  % use Store.RobotTrue to draw robot path 



return;

