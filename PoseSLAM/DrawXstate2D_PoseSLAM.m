%This function is used to plot Xstate saved in 2D euclidean space
function [ ] = DrawXstate2D_PoseSLAM( Xstate )
% set up parameters here
RobotSize = 0.4;
Tranparency = 0.3;
PoseColor = 'red';
FeatureColorShape = 'ko';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
vorigin = [ 0 0.2*RobotSize;
            0 -0.2*RobotSize;
            RobotSize 0;];
faces = [1 2 3];

count = 1;
DataDim = size(Xstate, 1);
figure;
hold on;
axis square;

while (count < DataDim)
    
    PoseX = Xstate(count,1);
    PoseY = Xstate(count+1,1);
    phi = Xstate(count+2,1);
    Posi = [PoseX PoseY;
            PoseX PoseY;
            PoseX PoseY;];

    vertices = R(phi)*vorigin.';
    vertices = vertices.' + Posi;

    patch('Faces', faces, 'Vertices', vertices, 'FaceColor', PoseColor, 'FaceAlpha', Tranparency);       
    count = count + 3;

end
hold off;
end
% Get rotation matrix A with respect to angle phi
function [A] = R(phi)
A=[cos(phi),-sin(phi);sin(phi),cos(phi)];
end
