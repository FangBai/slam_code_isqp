clc
clear all
close all

Zstate = [ 
    
           3.00 5 0;
           3.00 5 0;
           2.00 5 0;
           2.00 5 0;
           2.00 5 0;           
           2.00 5 0;
           
           3.00 1 0;
           3.00 1 0;
           2.00 1 0;
           2.00 1 0;
           2.00 1 0;           
           2.00 1 0; 
           
           3.00 2 1;
           3.00 2 1;
           2.00 2 1;
           2.00 2 1;
           2.00 2 1;           
           2.00 2 1;

           3.00 5 2;
           3.00 5 2;
           3.00 5 2;
           2.00 5 2;
           2.00 5 2;
           2.00 5 2;
%
           3.00 5 2;
           3.00 5 2;
           3.00 5 2;
           2.00 5 2;
           2.00 5 2;
           2.00 5 2;
%           
           3.00 3 2;
           3.00 3 2;
           3.00 3 2;
           2.00 3 2;
           2.00 3 2;
           2.00 3 2;
           
           3.00 4 3;
           3.00 4 3;
           3.00 4 3;
           2.00 4 3;
           2.00 4 3;
           2.00 4 3;
           
           3.00 5 4;
           3.00 5 4;
           2.00 5 4;
           2.00 5 4;
           2.00 5 4;           
           2.00 5 4;           
  ];  

[ Zstate_SE ] = ZstateFromEuclideanToSE3_PoseSLAM( Zstate);
tic
[ M_s, N_s ] = GetTriangle3D_PoseSLAM_All( Zstate_SE );
toc

X0 = Zstate_SE(:,1);
% tic
% [ CEQ_Norm ] = Nonlinear_Con_3D_PoseSLAM_Norm( X0, M_s );
% toc
for i =1:size(M_s,2)
    M_s{i}
end
N_s



A= sparse(30000,30000);

A(98, 30000) = 1;
% A(98, 10050) = 1;
A(10050, 20000) = 1;
A(98, 1000) = 1;
A(1000, 10050) = 1;
A(20000, 30000) = 1;

A(N_s(1,1)+1, N_s(1,2)+1) = 1;

A = A+A.'
tic
[ ExistCycle ] = ExistCycleUndirectedGraph( A )
toc

A = [ 0 0 1 0;
      0 0 1 1;
      0 0 0 0;
      0 0 0 0;];
A = A+A.';
tic
[ ExistCycle ] = ExistCycleUndirectedGraph( A )
toc



