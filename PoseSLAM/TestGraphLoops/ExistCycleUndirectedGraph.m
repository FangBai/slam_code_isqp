% This function use AdjacentMatrix of an undirected graph to check if there exist cycles in the graph
% ExistCycle : 1   | There exist circles in the graph
% ExistCycle : 0   | There are no circles in the graph
function [ ExistCycle ] = ExistCycleUndirectedGraph( AdjacentMatrix )

stopsign = 1;

while(stopsign > 0)

DimIn = size(AdjacentMatrix,1);

VertexDeg = sum(AdjacentMatrix);

CycleVertex = find(VertexDeg >=2);

AdjacentMatrix = AdjacentMatrix(CycleVertex, CycleVertex);

DimOut = size(AdjacentMatrix,1);

if(DimOut==DimIn || DimOut==0)
    stopsign =0;
    if(DimOut >0)
        ExistCycle = 1;
    else
        ExistCycle = 0;
    end
end

end
