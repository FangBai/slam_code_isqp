% This function is used to transform the data matrix Zstate from Euclidean
% space to SE(3) space.
function [ Zstate_SE ] = ZstateFromEuclideanToSE3_PoseSLAM( Zstate_EU )

EUcount = 1;
SEcount = 1;

DataDim = size(Zstate_EU, 1);

while (EUcount < DataDim)
        
    Zstate_SE([SEcount:(SEcount+2)],[1,2,3]) = Zstate_EU([EUcount:(EUcount+2)],[1,2,3]);
    
    R_cur = Exp_a2R(Zstate_EU([EUcount+3, EUcount+4, EUcount+5],1) );
    Zstate_SE([SEcount+3 : SEcount+11],1) = [ R_cur(1,1); R_cur(1,2); R_cur(1,3);
                                              R_cur(2,1); R_cur(2,2); R_cur(2,3);
                                              R_cur(3,1); R_cur(3,2); R_cur(3,3); ];
    Zstate_SE([SEcount+3 : SEcount+5],[2,3]) = Zstate_EU([EUcount, EUcount+1, EUcount+2],[2,3]);
    Zstate_SE([SEcount+6 : SEcount+8],[2,3]) = Zstate_EU([EUcount, EUcount+1, EUcount+2],[2,3]);
    Zstate_SE([SEcount+9 : SEcount+11],[2,3]) = Zstate_EU([EUcount, EUcount+1, EUcount+2],[2,3]);
    
    SEcount = SEcount + 12;
    EUcount = EUcount + 6;
end

end

