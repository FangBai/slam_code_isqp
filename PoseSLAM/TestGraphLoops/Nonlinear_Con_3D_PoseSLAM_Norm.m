% This function describe nonlinear constraints CEQ(X) = 0
% X   the variable/argument of constraints given in SE(3) space
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% M_s    the cell structure got from GetTriangle3D_PoseSLAM
%      M_s reflects indices for relative poses described in SE(3) group/space
%      M_s{i}: the i-th triangle
%      M_s{i}: num_odometry [odom1 odm2...domK] OdomToEndPose1 OdomToEndPose2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ CEQ_Norm ] = Nonlinear_Con_3D_PoseSLAM_Norm( X, M_s )

num_trian = size(M_s,2);
cur_trian = 1;
CEQ = zeros(12,1);
CEQ_Norm = zeros(num_trian,1);


while(cur_trian<=num_trian)
    num_odom = M_s{cur_trian}(1);
    %%%%%%%%% odometry 1...k  
    count = 1;
    while(count<= num_odom)
        od(count)= M_s{cur_trian}(count+1);
        count = count + 1;
    end
    %%%%%%%%%observation 1
    OdomToEndPose1=M_s{cur_trian}(num_odom+2);
    %%%%%%%%% observation 2
    OdomToEndPose2=M_s{cur_trian}(num_odom+3);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    cur_odom = num_odom;
    R_cur_odom = [X(od(cur_odom)+3), X(od(cur_odom)+4), X(od(cur_odom)+5);
                  X(od(cur_odom)+6), X(od(cur_odom)+7), X(od(cur_odom)+8);
                  X(od(cur_odom)+9), X(od(cur_odom)+10), X(od(cur_odom)+11);];
    
    F = [X(od(cur_odom));X(od(cur_odom)+1);X(od(cur_odom)+2)] + ...
                R_cur_odom*[X(OdomToEndPose2);X(OdomToEndPose2+1);X(OdomToEndPose2+2)];
            
    Rota = [X(OdomToEndPose2+3), X(OdomToEndPose2+4), X(OdomToEndPose2+5);
            X(OdomToEndPose2+6), X(OdomToEndPose2+7), X(OdomToEndPose2+8);
            X(OdomToEndPose2+9), X(OdomToEndPose2+10), X(OdomToEndPose2+11);];
    
    Rota = R_cur_odom*Rota; 
    
    while( cur_odom~=1)
        cur_odom = cur_odom -1;
        R_cur_odom = [X(od(cur_odom)+3), X(od(cur_odom)+4), X(od(cur_odom)+5);
                      X(od(cur_odom)+6), X(od(cur_odom)+7), X(od(cur_odom)+8);
                      X(od(cur_odom)+9), X(od(cur_odom)+10), X(od(cur_odom)+11);];
                  
        F = [X(od(cur_odom));X(od(cur_odom)+1);X(od(cur_odom)+2)] + R_cur_odom*F;  
        Rota = R_cur_odom*Rota; 
    end
            
    CEQ([1:3],1) = F - [X(OdomToEndPose1);X(OdomToEndPose1+1);X(OdomToEndPose1+2)];
    
    R_cur_odom = [X(OdomToEndPose1+3), X(OdomToEndPose1+4), X(OdomToEndPose1+5);
                  X(OdomToEndPose1+6), X(OdomToEndPose1+7), X(OdomToEndPose1+8);
                  X(OdomToEndPose1+9), X(OdomToEndPose1+10), X(OdomToEndPose1+11);];
              
    Rota = Rota - R_cur_odom;
    
    CEQ([4:12],1) = [Rota(1,1); Rota(1,2); Rota(1,3);
                     Rota(2,1); Rota(2,2); Rota(2,3);
                     Rota(3,1); Rota(3,2); Rota(3,3);];
                                              
    CEQ_Norm(cur_trian,1) = norm(CEQ);
    
    cur_trian = cur_trian + 1;

end

end



