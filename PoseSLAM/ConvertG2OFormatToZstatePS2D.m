% This function is used to convert 2D pose SLAM dataset in g2o format to Zstate format
% Zstate Colum:  Data EndingPose StartingPose
% Zstate Row: x, y, phi
function [ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D(  File_Path_Name )

Zstate = [];
CovMatrixInv = sparse(0);
count = 1;

fid = fopen(File_Path_Name,'r');
g2oLine = fgetl(fid);

while g2oLine(1) ~=-1
    lineStr = textscan(g2oLine, '%s');
    
    if strcmp(lineStr{1}{1},'EDGE_SE2')
        
        StartingPose = str2double(lineStr{1}{2});
        EndingPose = str2double(lineStr{1}{3});
        
        AngleRota = str2double(lineStr{1}{6});
        
        if(EndingPose>StartingPose)
            
        Zstate([count:count+2], [1,2,3,4]) = ... 
            [str2double(lineStr{1}{4}), EndingPose, StartingPose, 1;
             str2double(lineStr{1}{5}), EndingPose, StartingPose, 1;
             AngleRota, EndingPose, StartingPose, 1; ];
       
        elseif(EndingPose<StartingPose)   

        NewTrans = -R(str2double(lineStr{1}{6})).'*[str2double(lineStr{1}{4}); str2double(lineStr{1}{5});];    
        
        AngleRota = -AngleRota;
        
        Zstate([count:count+2], [1,2,3,4]) = ... 
            [NewTrans(1), StartingPose, EndingPose, 1;
             NewTrans(2), StartingPose, EndingPose, 1;
             AngleRota, StartingPose, EndingPose, 1; ];               
            
        else
            disp('Duplicate edges in the graph!');
            return;
        end
            
            
        CMI = ... 
            [str2double(lineStr{1}{7}), str2double(lineStr{1}{8}), str2double(lineStr{1}{9});
             str2double(lineStr{1}{8}), str2double(lineStr{1}{10}), str2double(lineStr{1}{11});
             str2double(lineStr{1}{9}), str2double(lineStr{1}{11}), str2double(lineStr{1}{12}); ];
         
        %%%%%%%%%%%%%%%%%% Covariance SE2 and Euclidean Mapping %%%%%%%%%%%%%%%%%
        Rz = blkdiag(R(AngleRota), 1);
        CovMatrixInv([count:count+2], [count:count+2]) = Rz * CMI * Rz.';
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
        
        count = count + 3;       
    
    elseif strcmp(lineStr{1}{1},'EDGE_SE2_SWITCHABLE')
        
        StartingPose = str2double(lineStr{1}{2});
        EndingPose = str2double(lineStr{1}{3});
        
        AngleRota = str2double(lineStr{1}{7});
        
        if(EndingPose>StartingPose)
            
        Zstate([count:count+2], [1,2,3,4]) = ... 
            [str2double(lineStr{1}{5}), EndingPose, StartingPose, -1;
             str2double(lineStr{1}{6}), EndingPose, StartingPose, -1;
             AngleRota, EndingPose, StartingPose, -1; ];
       
        elseif(EndingPose<StartingPose)   

        NewTrans = -R(str2double(lineStr{1}{7})).'*[str2double(lineStr{1}{5}); str2double(lineStr{1}{6});];    
        
        AngleRota = -AngleRota;
        
        Zstate([count:count+2], [1,2,3,4]) = ... 
            [NewTrans(1), StartingPose, EndingPose, -1;
             NewTrans(2), StartingPose, EndingPose, -1;
             AngleRota, StartingPose, EndingPose, -1; ];               
            
        else
            disp('Duplicate edges in the graph!');
            return;
        end
            
            
        CMI = ... 
            [str2double(lineStr{1}{8}), str2double(lineStr{1}{9}), str2double(lineStr{1}{10});
             str2double(lineStr{1}{9}), str2double(lineStr{1}{11}), str2double(lineStr{1}{12});
             str2double(lineStr{1}{10}), str2double(lineStr{1}{12}), str2double(lineStr{1}{13}); ];
        
        %%%%%%%%%%%%%%%%%% Covariance SE2 and Euclidean Mapping %%%%%%%%%%%%%%%%%
        Rz = blkdiag(R(AngleRota), 1);
        CovMatrixInv([count:count+2], [count:count+2]) = Rz * CMI * Rz.';
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
        
        count = count + 3;       
    end
        
        
    g2oLine = fgetl(fid);
end
fclose(fid);

end
% Get rotation matrix A with respect to angle phi
function [A] = R(phi)
A=[cos(phi),-sin(phi);sin(phi),cos(phi)];
end

