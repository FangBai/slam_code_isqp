function [ success ] = Success_Decision_Strategy1( PrecisionRecall )


InlierAdded = PrecisionRecall.TP; 
OutlierNotAdded = PrecisionRecall.TN;

OutlierAdded = PrecisionRecall.FP;
InlierNotAdded = PrecisionRecall.FN;



success = true;

if(OutlierAdded > 0 || InlierNotAdded > 0)
    success = false;
end

end

