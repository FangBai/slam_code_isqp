% This function is used to compute the objective for 3D Pose SLAM 
function [ Obj ] = Obj_Pose_3D( Zstate_EU, CovMatrixInv, Xstate0 )

count = 1;
Dim = size(Zstate_EU, 1);
Obj = 0;

while (count < Dim)

InfoMatrix = CovMatrixInv([count: count+5], [count: count+5]);

Data = Zstate_EU([count: count+5], 1);

end_pose = Zstate_EU(count, 2);

start_pose = Zstate_EU(count, 3);

if(start_pose == 0)
    start_vec = zeros(6,1);
else
    start_vec = Xstate0([(start_pose*6-5) : start_pose*6], 1);
end

end_vec = Xstate0([(end_pose*6-5) : end_pose*6], 1);

X_vec([1:3],1) = Exp_a2R(start_vec([4:6] ,1)).' * (end_vec([1:3], 1) - start_vec([1:3], 1));

X_Error([1:3],1) = X_vec([1:3],1) - Data([1:3],1);

X_Error([4:6],1) = Log_R2a( Exp_a2R(Data([4:6] ,1)).' *  Exp_a2R(start_vec([4:6] ,1)).' *  Exp_a2R(end_vec([4:6] ,1)) );

Obj = Obj + X_Error.' * InfoMatrix * X_Error;

count = count + 6;

end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ Theta ] = Log_R2a( R )
threshold = 1e-10;
angle = acos( (trace(R) -1 )/2 );

if (abs(angle) <threshold ) % 0
    axis = [1; 0; 0;];
elseif(abs(angle - pi) <threshold )  % pi
   % disp('reach pi');
    axis = - [sign(R(2,3))*sqrt((1+R(1,1))/2); sign(R(1,3))*sqrt((1+R(2,2))/2); sign(R(1,2))*sqrt((1+R(3,3))/2) ];
else
    axis = [ R(3,2)-R(2,3);
             R(1,3)-R(3,1);
             R(2,1)-R(1,2); ];
    axis = axis/(2*sin(angle));
end

Theta = angle*axis;
[ Theta ] = WrapX( Theta );
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Wrap a arbitrary axis angle rotation into a rotation at interval [0, pi]
function [ X_v ] = WrapX( X_vec )
if (norm(X_vec)== 0)
    X_v = X_vec;
    return; 
end
angle = norm(X_vec);
axis  = X_vec/angle;
while (angle >= 2*pi)
    angle = angle - 2*pi;
end
while ( angle > pi)
    angle = 2*pi - angle;
    axis = -axis;  
end
if(pi-angle<1e-10)
   if(axis(3)<0)
       axis = -axis;
   end    
end

X_v = angle*axis;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function is used to transform a vector into a rotation matrix by exponential mapping
function [ RotationMatrixExp ] = Exp_a2R( X_vec )

threshold = 1e-10;

theta = norm(X_vec);

if (theta < threshold)
    RotationMatrixExp = eye(3);
else
    RotationMatrixExp = eye(3) + SkewSem(X_vec)*sin(theta)/theta + ...
        (SkewSem(X_vec)^2)*(1 - cos(theta))/(theta^2);    
end

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% transform a 3-dimensional vector into a skew-semmetric marix
function [ SkewSemmetricMatrix ] = SkewSem( X_vec )
SkewSemmetricMatrix = [ 0          -X_vec(3)     X_vec(2);
                        X_vec(3)    0           -X_vec(1);
                       -X_vec(2)    X_vec(1)     0       ;];
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




