function [ J ] = Jacobian_AxisAngle2QuaternionNumerical( AxisAngle )

d = 1e-10;

t = @(x,y,z) norm([x;y;z]);

q_w = @(x,y,z) cos(t(x,y,z)/2);
q_x = @(x,y,z) x/t(x,y,z)*sin(t(x,y,z)/2);
q_y = @(x,y,z) y/t(x,y,z)*sin(t(x,y,z)/2);
q_z = @(x,y,z) z/t(x,y,z)*sin(t(x,y,z)/2);

x = AxisAngle(1);
y = AxisAngle(2);
z = AxisAngle(3);

J = ...
[ (q_w(x+d,y,z)-q_w(x,y,z))/d , (q_w(x,y+d,z)-q_w(x,y,z))/d,  (q_w(x,y,z+d)-q_w(x,y,z))/d;
  (q_x(x+d,y,z)-q_x(x,y,z))/d , (q_x(x,y+d,z)-q_x(x,y,z))/d,  (q_x(x,y,z+d)-q_x(x,y,z))/d;
  (q_y(x+d,y,z)-q_y(x,y,z))/d , (q_y(x,y+d,z)-q_y(x,y,z))/d,  (q_y(x,y,z+d)-q_y(x,y,z))/d;
  (q_z(x+d,y,z)-q_z(x,y,z))/d , (q_z(x,y+d,z)-q_z(x,y,z))/d,  (q_z(x,y,z+d)-q_z(x,y,z))/d;];


end

