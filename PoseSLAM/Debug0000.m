clear all
close all
clc

data_file = 'DataSet/MITb.g2o';
% data_file = 'G2O/input_MITb_g2o.g2o';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( data_file );
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
CovMatrix = inv(CovMatrixInv);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[Res_SQP, Obj_SQP ] = PoseSLAM_SQP_2D ( Zstate, CovMatrix);
PlotXstate2D_PoseSLAM( Res_SQP, 'c.--' );


[Res_iSQP, Obj_iSQP, PR_iSQP, N_All ] = PoseSLAM_iSQP_2D ( Zstate, CovMatrix);

PlotXstate2D_PoseSLAM( Res_iSQP, 'g.--' );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Res_DCS, Obj_DCS, PR_DCS, Weight_DCS ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'DCS', 660, 200); % 660
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Res_Cauchy, Obj_Cauchy, PR_Cauchy, Weight_Cauchy ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'Cauchy', 1.0, 200); 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Res_Cauchy_GN, Obj_Cauchy_GN ] = PoseSLAM_G2O_GN( Zstate, CovMatrixInv, Res_Cauchy, 100, '2D' );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[ isqp_dcs ] = CompareXstateResult( Res_iSQP, Res_DCS);
[ isqp_cauchy] = CompareXstateResult( Res_iSQP, Res_Cauchy);
[ isqp_dcs_gn ] = CompareXstateResult( Res_iSQP, Res_Cauchy_GN);

% [ Weight_DCS(:,1), Weight_Cauchy ]

disp(['Objective : iSQP - DCS - Cauchy - Cauchy+GN =>   ', num2str([Obj_iSQP, Obj_DCS, Obj_Cauchy, Obj_Cauchy_GN])]);

disp(['RMSE : dcs - cauchy - dcs_gn =>   ', num2str([isqp_dcs, isqp_cauchy, isqp_dcs_gn])]);

PR_DCS
PR_Cauchy

[ success_iSQP ] = Success_Decision_Strategy1( PR_iSQP )
[ success_DCS ] = Success_Decision_Strategy1( PR_DCS )
[ success_Cauchy ] = Success_Decision_Strategy2( PR_Cauchy )
