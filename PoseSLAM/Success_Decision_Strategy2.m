function [ success ] = Success_Decision_Strategy2( PrecisionRecall )

InlierAdded = PrecisionRecall.TP; 
OutlierNotAdded = PrecisionRecall.TN;

OutlierAdded = PrecisionRecall.FP;
InlierNotAdded = PrecisionRecall.FN;

success = true;

if(OutlierAdded > 0)
    success = false;
end

end

