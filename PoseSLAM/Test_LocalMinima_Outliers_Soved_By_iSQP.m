clear all
close all
clc

% rng('shuffle');

% DataFile = 'FRH_P';   % 280
% DataFile = 'INTEL_P';  % 10
% DataFile = 'ringCity';     % 9-10




%data_file = [ 'NoisyDataSet/Minima_Outliers/FRH_P_Noisy_0.05_0.05_0.05_200.g2o' ];
data_file = [ 'NoisyDataSet/Minima_Outliers/INTEL_P_Noisy_0.05_0.05_0.05_100.g2o' ];
%data_file = [ 'NoisyDataSet/Minima_Outliers/ringCity_Noisy_0.05_0.05_0.05_100.g2o' ];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( data_file );
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Res_Cauchy ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'Cauchy', 1.0, 100); 
PlotXstate2D_PoseSLAM( Res_Cauchy, 'g.--' );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Res_DCS ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'DCS', 280.0, 100);  
PlotXstate2D_PoseSLAM( Res_DCS, 'b.--' );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
CovMatrix = inv(CovMatrixInv);
tic
[Res_iSQP, Obj_iSQP, PR_iSQP, N_All ] = PoseSLAM_iSQP_2D ( Zstate, CovMatrix);
toc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


