function [ Edge_Index, Edges_Vertex ] = Edge_Related_Constraints( N_All, Zstate, DataDim )

if(strcmp(DataDim, '2D'))
    StepSize = 3;
elseif(strcmp(DataDim, '3D'))
    StepSize = 6;
else
    disp('--------------- data error ---------------------');
end


num_count = 1;

loop_closure = find(Zstate(:,2) - Zstate(:,3) > 1);
loop_closure = loop_closure([1: StepSize: size(loop_closure, 1)], 1);

for edge = 1 : size(loop_closure, 1)
    edge_end = Zstate(loop_closure(edge), 2);
    edge_start = Zstate(loop_closure(edge), 3);
    
    cons = [];
    cons_index = [];
    
    for count = 1 : size(N_All, 1)     
       if(N_All(count,3) == edge_end)
           if((N_All(count,1) == edge_start) || (N_All(count,2) == edge_start))
               cons = [cons; N_All(count, :)];                
               cons_index = [cons_index; count];
           end     
       end
    end
    
    if(size(cons,1) == 0)   
        disp('related constraints not found')    
        return;     
    else
        
        Edge_Index{num_count} = cons_index;
        
        Edges_Vertex(num_count, [1,2]) = [edge_start, edge_end];
        
        num_count = num_count + 1;
        
    end
end

end

