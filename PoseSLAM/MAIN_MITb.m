clear all
close all
clc

TotalMC_Trials = 100;

DataFile = 'MITb';

num_outlier = [20; 15; 10; 5; 2; 1; 0;];  %20 % 10 % 5 % 2 % 1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% rmdir(['Results/', DataFile], 's')

count = 1;
while(count <= size(num_outlier,1));
mkdir(['Results/', DataFile, '/', num2str(num_outlier(count)) ])
count = count + 1;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



count = 1;

while(count <= size(num_outlier,1));

Main_MC_Simulation (TotalMC_Trials, num_outlier(count), DataFile)

Main_StatisticalAnalysis (TotalMC_Trials, num_outlier(count), DataFile)

count = count + 1;

end


clc
close all

count = 1;

while(count <= size(num_outlier,1));

load(['Results/', DataFile, '/', DataFile, '_', num2str(num_outlier(count)),]);

iSQP_Statistics
DCS_Statistics
Cauchy_Statistics

count = count + 1;

end

