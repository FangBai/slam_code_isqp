% This function is used to acquire Euclidean difference of two SE(3) variables
% X_SE1 represents the current variable value in SE(3) before update
% X_SE2 represents the variable value after update
% X_EU represents the difference in Euclidean space
% X_EU = X_SE1 - X_SE2 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ X_EU ] = Update_EUbySE_3D_PoseSLAM_Test(X_SE1, X_SE2 )

SEcount = 1;
EUcount = 1;

DataDim1 = max(size(X_SE1));
DataDim2 = max(size(X_SE2));

if(DataDim1 ~= DataDim2)
    disp('data error!');
    return;
elseif(DataDim1 == DataDim2)
    DataDim = DataDim1;
end

while (SEcount <= DataDim )

    Posi_SE1 = X_SE1([SEcount, SEcount+1, SEcount+2]);
    Posi_SE2 = X_SE2([SEcount, SEcount+1, SEcount+2]);
    Rota_SE1 = [X_SE1([SEcount+3, SEcount+6, SEcount+9]) X_SE1([SEcount+4, SEcount+7, SEcount+10]) X_SE1([SEcount+5, SEcount+8, SEcount+11])];
    Rota_SE2 = [X_SE2([SEcount+3, SEcount+6, SEcount+9]) X_SE2([SEcount+4, SEcount+7, SEcount+10]) X_SE2([SEcount+5, SEcount+8, SEcount+11])];
    
    X_EU([EUcount, EUcount+1, EUcount+2],1) = Posi_SE1 - Posi_SE2;
    X_EU([EUcount+3, EUcount+4, EUcount+5],1) = Log_R2a((Rota_SE1)*(Rota_SE2'));    
    
    SEcount = SEcount + 12;
    EUcount = EUcount + 6;

end