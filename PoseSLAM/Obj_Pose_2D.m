% This function is used to compute the objective for 3D Pose SLAM 
function [ Obj ] = Obj_Pose_2D( Zstate_EU, CovMatrixInv, Xstate0 )

count = 1;
Dim = size(Zstate_EU, 1);
Obj = 0;

while (count < Dim)

InfoMatrix = CovMatrixInv([count: count+2], [count: count+2]);

Data = Zstate_EU([count: count+2], 1);

end_pose = Zstate_EU(count, 2);

start_pose = Zstate_EU(count, 3);

if(start_pose == 0)
    start_vec = zeros(3,1);
else
    start_vec = Xstate0([(start_pose*3-2) : start_pose*3], 1);
end

end_vec = Xstate0([(end_pose*3-2) : end_pose*3], 1);

X_vec([1,2],1) = R(start_vec([3] ,1)).' * (end_vec([1,2], 1) - start_vec([1,2], 1));

X_Error([1,2],1) = X_vec([1,2],1) - Data([1,2],1);

X_Error([3],1) = R2a( R(Data([3] ,1)).' *  R(start_vec([3] ,1)).' *  R(end_vec([3] ,1)));

Obj = Obj + X_Error.' * InfoMatrix * X_Error;

% if(X_Error.' * InfoMatrix * X_Error > 1000)
%    disp(['count = ', num2str([count, X_Error.' * InfoMatrix * X_Error, ]) ]);
%    disp('InfoMatrix');
%    full(InfoMatrix)
%    disp('X_Error, X_vec, Data');
%    [[X_Error], [X_vec;0], Data]
% end

count = count + 3;

end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get rotation matrix A with respect to angle phi
function [A] = R(phi)
A=[cos(phi),-sin(phi);sin(phi),cos(phi)];
end

% Get angle phi from rotation matrix R
function [phi] = R2a(R)
phi = atan2(R(2,1), R(1,1));
end




