% Get triangles that describe edges/peaks of constraints from data
% M_trian = [num_odometry [odom1 odm2...domK] OdomToEndPose1 OdomToEndPose2]  Namely edges
% N_trian = [StartPose1 StartPose2 EndPose]  Namely peaks
function [ M_trian, N_trian ] = GetTriangle3D_PoseSLAM( Zstate )

M_trian=cell(0);
N_trian=[];
count_tria=0;
EndPose=1;

EndPoseNum = max(Zstate(:,2));

if(Zstate(1,2)==Zstate(12,2) && Zstate(1,3)==Zstate(12,3))
    stepsize = 12;   
elseif(Zstate(1,2)==Zstate(6,2) && Zstate(1,3)==Zstate(6,3))
    stepsize = 6;
else
    disp('data error!');
    return;
end


OdometryLine = find((Zstate(:,2) - Zstate(:,3)) == 1);
OdometryIndex = OdometryLine([1:stepsize:size(OdometryLine,1)], :);

while (EndPose <= EndPoseNum)
   
    EndPoseLine = find(Zstate(:,2) == EndPose);
    EndPoseIndex = EndPoseLine([1:stepsize:size(EndPoseLine,1)], :);
    
    TriangleNum = size(EndPoseIndex,1) - 1;
    
    if(TriangleNum == -1)
        EndPose = EndPose + 1;
    elseif (TriangleNum == 0)
        EndPose = EndPose + 1;
    elseif( TriangleNum >=1 )
        count = 1;
        while(count < size(EndPoseIndex,1))
            count_tria = count_tria + 1;
            N_trian(count_tria, [1,2,3]) = [Zstate(EndPoseIndex(count), 3), Zstate(EndPoseIndex(count+1), 3), EndPose];
            
            num_odom = Zstate(EndPoseIndex(count+1), 3) - Zstate(EndPoseIndex(count), 3);
            M_trian{count_tria}(1) = num_odom;
            
            for i =1: num_odom
                M_trian{count_tria}(i+1) = OdometryIndex(Zstate(EndPoseIndex(count),3)+i);             
            end

            M_trian{count_tria}(num_odom+2) = EndPoseIndex(count);
            M_trian{count_tria}(num_odom+3) = EndPoseIndex(count+1);
            
            count = count + 1;
        end
        EndPose = EndPose + 1;
    end

end


end

