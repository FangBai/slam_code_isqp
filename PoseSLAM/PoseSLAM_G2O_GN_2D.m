%
function [ Res_G2O, Obj_G2O, PrecisionRecall, Weight ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, kernel, kernelWidth, max_iteration)

% max_iteration = 500;
% kernel = 'DCS';   % 'Cauchy', 'DCS', 'Fair', 'GemanMcClure', 'Huber', 'PseudoHuber', 'Saturated', 'Tukey', 'Welsch',   g2o -listRobustKernels
% kernelWidth = 10;  %  DCS = 10


%%%%%%%%%

SET_PATH_G2O

%%%%%%%%%

input_file = 'Data_G2O/tmp.g2o';
output_file = 'Data_G2O/tmp_out.g2o';
summary_file = 'Data_G2O/tmp_summary.g2o';

delete Data_G2O/tmp_summary.g2o

Xstate0 = FuncCreateXstateFromZstate_2D_PoseSLAM( Zstate );
CreateG2OFormatByZstatePS2D (input_file, Zstate, CovMatrixInv, Xstate0);

% process g2o
string_cmd = [g2o_path,'g2o ', '-i ', num2str(max_iteration),' -summary ', summary_file, ... 
    ' -robustKernel ', kernel, ' -robustKernelWidth ', num2str(kernelWidth), ...
    ' -solver gn_var_eigen',' -o ', output_file, ' ', input_file];

[result_cmd, info_cmd ] = system(string_cmd);

[ Res_G2O ] = CreateXstateFromG2OData_2D_PoseSLAM( output_file );

Res_G2O = Res_G2O ([4: size(Res_G2O)],:);

Weight = RobustFunctionWeight(Zstate, CovMatrixInv, Res_G2O, kernel, kernelWidth, '2D' );

[ PrecisionRecall ] = PrecisionRecallStatistics_G2O_M_estimator( Weight );

%%%%%%%%%

InlierIndex = find(Zstate(:,4) > 0);

Zstate_Inlier = Zstate(InlierIndex, :);

CovMatrixInv_Inlier = CovMatrixInv(InlierIndex, InlierIndex);

[ Obj_G2O ] = Obj_Pose_2D( Zstate_Inlier, CovMatrixInv_Inlier, Res_G2O );


end

