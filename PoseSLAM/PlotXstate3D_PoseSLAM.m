% This function is used to plot Xstate given in Euclidean space
% str control the line and color used to plot the pose
function [ h1 ] = PlotXstate3D_PoseSLAM( Xstate_EU, str )

count = 1;
X_pose = [0];
Y_pose = [0];
Z_pose = [0];

% extract pose and feature coordinate
% pose coordinates are saved in X_pose, Y_pose
% feature coordiantes are saved in X_feature, Y_feature
while (count<=size(Xstate_EU,1))
        X_pose = [X_pose Xstate_EU(count,1)];
        Y_pose = [Y_pose Xstate_EU(count+1,1)];
        Z_pose = [Z_pose Xstate_EU(count+2,1)];
        count = count + 6;     
end

figure;
% plot pose and feature
hold on
h1 = plot3(X_pose,Y_pose,Z_pose, str);
hold off
end

