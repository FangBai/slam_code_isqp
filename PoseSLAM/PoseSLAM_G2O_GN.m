%
% str = '2D'   OR   str = '3D'
function [ Res_G2O, Obj_G2O ] = PoseSLAM_G2O_GN( Zstate, CovMatrixInv, Xstate0, max_iteration, str )
% max_iteration = 500;


%%%%%%%%%

% InlierIndex = find(Zstate(:,4) > 0);
% 
% Zstate = Zstate(InlierIndex, :);
% 
% CovMatrixInv = CovMatrixInv(InlierIndex, InlierIndex);

%%%%%%%%%

SET_PATH_G2O

%%%%%%%%%

input_file = 'Data_G2O/tmp.g2o';
output_file = 'Data_G2O/tmp_out.g2o';
summary_file = 'Data_G2O/tmp_summary.g2o';

delete Data_G2O/tmp_summary.g2o

if(strcmp(str, '3D'))
    CreateG2OFormatByZstatePS3D (input_file, Zstate, CovMatrixInv, Xstate0);
elseif(strcmp(str, '2D'))
    CreateG2OFormatByZstatePS2D (input_file, Zstate, CovMatrixInv, Xstate0);
else
    disp('Dataset type string is not correct');
end

% process g2o
string_cmd = [g2o_path,'g2o ', '-i ', num2str(max_iteration),' -summary ', summary_file, ... 
    ' -solver gn_var_eigen',' -o ', output_file, ' ', input_file];

[result_cmd, info_cmd ] = system(string_cmd);



if(strcmp(str, '3D'))
    [ Res_G2O ] = CreateXstateFromG2OData_3D_PoseSLAM( output_file );
    Res_G2O = Res_G2O ([7: size(Res_G2O)],:);
    [ Obj_G2O ] = Obj_Pose_3D( Zstate, CovMatrixInv, Res_G2O );
elseif(strcmp(str, '2D'))
    [ Res_G2O ] = CreateXstateFromG2OData_2D_PoseSLAM( output_file );
    Res_G2O = Res_G2O ([4: size(Res_G2O)],:);
    [ Obj_G2O ] = Obj_Pose_2D( Zstate, CovMatrixInv, Res_G2O );
end


end

