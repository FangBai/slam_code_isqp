clear all
close all
clc

num_outlier = 300;  %20 % 10 % 5
groupsize = 1;
Niko = false;

add_outliers = 'DataSet/generateDataset.py';
data_file_outlier = 'Data_G2O/tmp_Outlier.g2o';

datafile = 'M3500';
DataFile = ['DataSet/', datafile, '.g2o'];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ResultFolder = ['Results/', datafile, '/Outlier_', num2str(num_outlier), '/'];
mkdir(ResultFolder);
delete([ResultFolder, 'ConsoleOutput.txt']);
diary([ResultFolder, 'ConsoleOutput.txt']);
diary on

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Now add outliers to the dataset, and solve again with iSQP and G2O
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% add outliers to the dataset
if(Niko)
string_cmd2 = [add_outliers, ' -i ', DataFile, ' -o ', data_file_outlier, ...
               ' -n ', num2str(num_outlier), ' -g ', num2str(groupsize), ' -s'];
[result2_cmd, info2_cmd] = system(string_cmd2);
%
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( data_file_outlier );
%
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
CovMatrix = inv(CovMatrixInv);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
else
    
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( DataFile );
%
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
%
[Zstate, CovMatrixInv] = AddOutliersPoseSLAM( Zstate, CovMatrixInv, num_outlier, 0 );
%
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
CovMatrix = inv(CovMatrixInv);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tic
[Res_iSQP, Obj_iSQP, PR_iSQP, N_All, ProcessInfo ] = PoseSLAM_iSQP_2D ( Zstate, CovMatrix);
toc


[ success_iSQP ] = Success_Decision_Strategy1( PR_iSQP )


diary off

save([ResultFolder, 'Result_iSQP.mat'], 'Res_iSQP', 'Obj_iSQP', 'PR_iSQP', 'N_All', 'ProcessInfo');
save([ResultFolder, 'DataZstate.mat'], 'Zstate', 'CovMatrix');


Xstate0 = FuncCreateXstateFromZstate_2D_PoseSLAM( Zstate );
CreateG2OFormatByZstatePS2D ([ResultFolder, datafile, '_Outlier_', num2str(num_outlier), '.g2o'], Zstate, CovMatrixInv, Xstate0);

