function [ Nodes, Edges, LpEdges, LpConstraints ] = ObtainDatasetInfo( Zstate, str )

if(strcmp(str, '2D'))
    StepSize = 3;
elseif(strcmp(str, '3D'))
    StepSize = 6;
else
    disp(['dataset type str error @ ObtainDatasetInfo']);
end

Dim = size(Zstate, 1);


if(strcmp(str, '2D'))
    [Zstate_EU, ~] = ConvertDataSetFrom2DTo3D ( Zstate, speye(Dim));
    Xstate1 = FuncCreateXstateFromZstate_2D_PoseSLAM( Zstate );
end

Xstate2 = FuncCreateXstateFromZstate_EU_3D_PoseSLAM( Zstate_EU );

[ ~, N_trian ] = GetTriangle3D_PoseSLAM( Zstate_EU );




Nodes1 = size(Xstate1, 1)/3;
Nodes2 = size(Xstate2, 1)/6;

Edges1 = size(Zstate, 1)/3;
Edges2 = size(Zstate_EU, 1)/6;


if(Nodes1 == Nodes2)
    Nodes = Nodes1;
else
    disp(['Nodes num error @ ObtainDatasetInfo']);
end



if(Edges1 == Edges2)
    Edges = Edges1;
else
    disp(['Edges num error @ ObtainDatasetInfo']);
end


LpEdges1 = size(find(Zstate(:,2) - Zstate(:,3) > 1),1)/3;
LpEdges2 = size(find(Zstate_EU(:,2) - Zstate_EU(:,3) > 1),1)/6;

if(LpEdges1 == LpEdges2)
    LpEdges = LpEdges1;
else
    disp(['LpEdges num error @ ObtainDatasetInfo']);
end


LpConstraints = size(N_trian, 1);




end

