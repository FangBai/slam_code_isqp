clear all
close all
clc

rng('shuffle');

 DataFile = 'FRH_P';   % 280
% DataFile = 'INTEL_P';  % 10
% DataFile = 'ringCity';     % 9-10

% mkdir NoisyDataSet


data_file = [ 'NoisyDataSet/Minima_odom/', DataFile, '_Noisy_0.05_0.05_0.05.g2o' ];

num_outlier = 200;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( data_file );

[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );

[ Zstate, CovMatrixInv ] = AddOutliersPoseSLAM( Zstate, CovMatrixInv, num_outlier, 0 );

[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Res_Cauchy ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'Cauchy', 1.0, 100); 
PlotXstate2D_PoseSLAM( Res_Cauchy, 'g.--' );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Res_DCS ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'DCS', 280.0, 100);  
PlotXstate2D_PoseSLAM( Res_DCS, 'b.--' );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [ Xstate0 ] = FuncCreateXstateFromZstate_2D_PoseSLAM( Zstate );
% [ Res_GN, Obj_GN ] = PoseSLAM_G2O_GN( Zstate, CovMatrixInv, Xstate0, 100, '2D' );
% PlotXstate2D_PoseSLAM( Res_GN, 'r.--' );


