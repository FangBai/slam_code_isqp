% This function gives the Logarithm mapping to tranform a rotation R to the minimal representation Theta
% The minimal representation is described by axis angle representation
% Theta is a vertical vector
function [ Theta ] = Log_R2a( R )
threshold = 1e-10;
angle = acos( (trace(R) -1 )/2 );

if (abs(angle) <threshold ) % 0
    axis = [1; 0; 0;];
elseif(abs(angle - pi) <threshold )  % pi
   % disp('reach pi');
    axis = - [sign(R(2,3))*sqrt((1+R(1,1))/2); sign(R(1,3))*sqrt((1+R(2,2))/2); sign(R(1,2))*sqrt((1+R(3,3))/2) ];
else
    axis = [ R(3,2)-R(2,3);
             R(1,3)-R(3,1);
             R(2,1)-R(1,2); ];
    axis = axis/(2*sin(angle));
end

Theta = angle*axis;
[ Theta ] = WrapX( Theta );
end

