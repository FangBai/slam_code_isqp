%
function [] = Main_StatisticalAnalysis (TotalMC_Trials, num_outlier, DataFile)


% clear all
% close all
% clc

% TotalMC_Trials = 50;
% 
% num_outlier = 20;  %20 % 10 % 5 % 2 % 1
% 
% DataFile = 'MITb';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ResultDir = ['Results/', DataFile, '/', num2str(num_outlier), '/' ];

MC_count = 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

iSQP_Statistics.RMSE_aver = 0;
iSQP_Statistics.Obj_aver = 0;
iSQP_Statistics.Precision_aver = 0;
iSQP_Statistics.Recall_aver = 0;
iSQP_Statistics.success_rate = 0;

DCS_Statistics.RMSE_aver = 0;
DCS_Statistics.Obj_aver = 0;
DCS_Statistics.Precision_aver = 0;
DCS_Statistics.Recall_aver = 0;
DCS_Statistics.success_rate = 0;

Cauchy_Statistics.RMSE_aver = 0;
Cauchy_Statistics.Obj_aver = 0;
Cauchy_Statistics.Precision_aver = 0;
Cauchy_Statistics.Recall_aver = 0;
Cauchy_Statistics.success_rate = 0;


%%%%%%

while(MC_count <= TotalMC_Trials)

FileStr = [ResultDir, DataFile, '_', num2str(num_outlier), '_', num2str(MC_count) ];

load(FileStr, 'iSQP', 'DCS', 'Cauchy');

iSQP_Statistics.RMSE_aver = iSQP_Statistics.RMSE_aver + iSQP.RMSE;
iSQP_Statistics.Obj_aver = iSQP_Statistics.Obj_aver + iSQP.Obj;
iSQP_Statistics.Precision_aver = iSQP_Statistics.Precision_aver + iSQP.PR.precision;
iSQP_Statistics.Recall_aver = iSQP_Statistics.Recall_aver + iSQP.PR.recall;
iSQP_Statistics.success_rate = iSQP_Statistics.success_rate + iSQP.success;

DCS_Statistics.RMSE_aver = DCS_Statistics.RMSE_aver + DCS.RMSE;
DCS_Statistics.Obj_aver = DCS_Statistics.Obj_aver + DCS.Obj;
DCS_Statistics.Precision_aver = DCS_Statistics.Precision_aver + DCS.PR.precision;
DCS_Statistics.Recall_aver = DCS_Statistics.Recall_aver + DCS.PR.recall;
DCS_Statistics.success_rate = DCS_Statistics.success_rate + DCS.success;

Cauchy_Statistics.RMSE_aver = Cauchy_Statistics.RMSE_aver + Cauchy.RMSE;
Cauchy_Statistics.Obj_aver = Cauchy_Statistics.Obj_aver + Cauchy.Obj;
Cauchy_Statistics.Precision_aver = Cauchy_Statistics.Precision_aver + Cauchy.PR.precision;
Cauchy_Statistics.Recall_aver = Cauchy_Statistics.Recall_aver + Cauchy.PR.recall;
Cauchy_Statistics.success_rate = Cauchy_Statistics.success_rate + Cauchy.success;

MC_count = MC_count + 1;

end

iSQP_Statistics.RMSE_aver = iSQP_Statistics.RMSE_aver/TotalMC_Trials;
iSQP_Statistics.Obj_aver = iSQP_Statistics.Obj_aver/TotalMC_Trials;
iSQP_Statistics.Precision_aver = iSQP_Statistics.Precision_aver/TotalMC_Trials;
iSQP_Statistics.Recall_aver = iSQP_Statistics.Recall_aver/TotalMC_Trials;
iSQP_Statistics.success_rate = iSQP_Statistics.success_rate/TotalMC_Trials;

DCS_Statistics.RMSE_aver = DCS_Statistics.RMSE_aver/TotalMC_Trials;
DCS_Statistics.Obj_aver = DCS_Statistics.Obj_aver/TotalMC_Trials;
DCS_Statistics.Precision_aver = DCS_Statistics.Precision_aver/TotalMC_Trials;
DCS_Statistics.Recall_aver = DCS_Statistics.Recall_aver/TotalMC_Trials;
DCS_Statistics.success_rate = DCS_Statistics.success_rate/TotalMC_Trials;

Cauchy_Statistics.RMSE_aver = Cauchy_Statistics.RMSE_aver/TotalMC_Trials;
Cauchy_Statistics.Obj_aver = Cauchy_Statistics.Obj_aver/TotalMC_Trials;
Cauchy_Statistics.Precision_aver = Cauchy_Statistics.Precision_aver/TotalMC_Trials;
Cauchy_Statistics.Recall_aver = Cauchy_Statistics.Recall_aver/TotalMC_Trials;
Cauchy_Statistics.success_rate = Cauchy_Statistics.success_rate/TotalMC_Trials;


FileStr = [ResultDir, '../', DataFile, '_', num2str(num_outlier),];

save(FileStr, 'iSQP_Statistics', 'DCS_Statistics', 'Cauchy_Statistics');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% iSQP_Statistics
% DCS_Statistics
% Cauchy_Statistics

end