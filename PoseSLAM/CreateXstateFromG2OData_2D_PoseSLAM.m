function [ Xstate ] = CreateXstateFromG2OData_2D_PoseSLAM( File_Path_Name )
threshold = 1e-10;
Zstate = [];
CovMatrixInv = sparse(0);
count = 1;

fid = fopen(File_Path_Name,'r');
g2oLine = fgetl(fid);

while g2oLine(1) ~=-1
    lineStr = textscan(g2oLine, '%s');
    
    if strcmp(lineStr{1}{1},'VERTEX_SE2')
        
        PoseIndex = str2double(lineStr{1}{2});

        Xstate([count:count+2], [1,2]) = ... 
            [str2double(lineStr{1}{3}), PoseIndex;
             str2double(lineStr{1}{4}), PoseIndex;
             str2double(lineStr{1}{5}), PoseIndex;];
                         
        count = count + 3;       
    end
    g2oLine = fgetl(fid);
end
fclose(fid);

end