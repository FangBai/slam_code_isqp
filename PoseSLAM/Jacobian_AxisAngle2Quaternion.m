function [ J ] = Jacobian_AxisAngle2Quaternion( AxisAngle )

threshold = 1e-15;
t = norm(AxisAngle);
c = cos(t/2);
s = sin(t/2);

if(t<threshold)
    J = [ 0 0 0;
          1/2 0 0;
          0 1/2 0;
          0 0 1/2;];
else
    J = [ -s/(2*t)*AxisAngle(1),  -s/(2*t)*AxisAngle(2),   -s/(2*t)*AxisAngle(3);
          c/(2*t^2)*AxisAngle(1)^2+s/(t^3)*(AxisAngle(2)^2+AxisAngle(3)^2),  (c/(2*t^2)-s/(t^3))*AxisAngle(1)*AxisAngle(2),  (c/(2*t^2)-s/(t^3))*AxisAngle(1)*AxisAngle(3);
          (c/(2*t^2)-s/(t^3))*AxisAngle(1)*AxisAngle(2),  c/(2*t^2)*AxisAngle(2)^2+s/(t^3)*(AxisAngle(1)^2+AxisAngle(3)^2),  (c/(2*t^2)-s/(t^3))*AxisAngle(2)*AxisAngle(3);
          (c/(2*t^2)-s/(t^3))*AxisAngle(1)*AxisAngle(3),  (c/(2*t^2)-s/(t^3))*AxisAngle(2)*AxisAngle(3),   c/(2*t^2)*AxisAngle(3)^2+s/(t^3)*(AxisAngle(1)^2+AxisAngle(2)^2);];
end

end

