close all
clear
clc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%% Noise variance %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
X_Noise = 0.01;
Y_Noise = 0.01;
Phi_Noise = 1/180 * pi * 1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%% Anonymous Functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
R = @(phi) [cos(phi),-sin(phi);sin(phi),cos(phi)];
R2a = @(R) atan2(R(2,1), R(1,1));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% POSES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Pose([1 2 3], 1) = [0; 0; 0];
Pose([1 2 3], 2) = [1; 0; 0];
Pose([1 2 3], 3) = [2; 0; pi/4];
Pose([1 2 3], 4) = [2+sqrt(2)/2; sqrt(2)/2; pi/2];
Pose([1 2 3], 5) = [2+sqrt(2)/2; 1+sqrt(2)/2; 3*pi/4];
Pose([1 2 3], 6) = [2; 1+2*sqrt(2)/2; pi];
Pose([1 2 3], 7) = [1; 1+2*sqrt(2)/2; pi];
Pose([1 2 3], 8) = [0; 1+2*sqrt(2)/2; -3*pi/4];
Pose([1 2 3], 9) = [-sqrt(2)/2; 1+sqrt(2)/2; -pi/2];
Pose([1 2 3], 10) = [-sqrt(2)/2; sqrt(2)/2; -pi/4];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% EDGES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
inlier_edges = [1 2; % 1
                3 4; % 2
                4 5; % 3
                5 6; % 4
                6 7; % 5
                7 8; % 6
                8 9; % 7
               1 10; % 8    % 1 10 % 10 1
                2 6; % 9
                3 6; % 10
                3 8; % 11
                1 8;
                9 10;]; % 12
% ----------------------------------------------------------------------- %      
outlier_edges = [2 3; % 1    
                 1 7; % 2
                    ]; % 3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Triangles : M %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
All_Traingle_Edges = {
    %1 - 2 - 6 - 7 - 8 - (1-8)
    [inlier_edges(1); inlier_edges(9); inlier_edges(5); inlier_edges(6); inlier_edges(12) ],
    %3 - 4 - 5 - 6 - (3-6)
    [inlier_edges(2); inlier_edges(3); inlier_edges(4); inlier_edges(10)],
    %3 - 4 - 5 - 6 - 7 - 8 - (3-8)  // Not independant
    % [inlier_edges(2); inlier_edges(3); inlier_edges(4); inlier_edges(5); inlier_edges(6); inlier_edges(11) ],
    %3 - 6 - 7 - 8 - (3-8)
    [inlier_edges(10); inlier_edges(5); inlier_edges(6); inlier_edges(11) ],
    %
    %
    [inlier_edges(1); outlier_edges(1); inlier_edges(11); inlier_edges(12) ], % 
    
    [inlier_edges(1); outlier_edges(1); inlier_edges(10); inlier_edges(5); inlier_edges(6); inlier_edges(12)], %
    
    [inlier_edges(1); outlier_edges(1); inlier_edges(2); inlier_edges(3); inlier_edges(4); inlier_edges(5); inlier_edges(6); inlier_edges(12)], %
                      
                      
                     };




%%%%%%%%%%%%%%%%%%%%%%%%%% plot poses and edges %%%%%%%%%%%%%%%%%%%%%%%%%%%
%
pose_x = [Pose(1,:)];
pose_y = [Pose(2,:)];
plot(pose_x, pose_y, 'ro');
axis([ -1, 3, -0.3, 2.7]);
hold on
%
for i = 1:size(inlier_edges,1)
    edge_x = [Pose(1, inlier_edges(i,1)), Pose(1, inlier_edges(i,2))];
    edge_y = [Pose(2, inlier_edges(i,1)), Pose(2, inlier_edges(i,2))];
    plot(edge_x, edge_y, 'r.-');
end
%
for i = 1:size(outlier_edges,1)
    edge_x = [Pose(1, outlier_edges(i,1)), Pose(1, outlier_edges(i,2))];
    edge_y = [Pose(2, outlier_edges(i,1)), Pose(2, outlier_edges(i,2))];
    plot(edge_x, edge_y, 'g.-');       
end
%
hold off
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%% Compute inlier edges %%%%%%%%%%%%%%%%%%%%%%%%%%%
Zstate = [];
for i = 1:size(inlier_edges,1)
    start_pose = Pose([1 2 3], inlier_edges(i,1));
    end_pose = Pose([1 2 3], inlier_edges(i,2));
% ----------------------------------------------------------------------- %    
    Data_xy = R(start_pose(3,1)).' * (end_pose([1 2],1) - start_pose([1 2],1));    
    Data_phi = R2a(R(start_pose(3,1)).' * R(end_pose(3,1)));
% ----------------------------------------------------------------------- %
    Data_xy = Data_xy + [X_Noise*randn; Y_Noise*randn];
    Data_phi = Data_phi + Phi_Noise*randn;
% ----------------------------------------------------------------------- %    
    Zstate = [Zstate; [[Data_xy; Data_phi], inlier_edges(i,2)*ones(3,1)-1, inlier_edges(i,1)*ones(3,1)-1, 1*ones(3,1)] ];   
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%% Generate outlier edges %%%%%%%%%%%%%%%%%%%%%%%%%%            
for i = 1:size(outlier_edges,1)
    start_pose = Pose([1 2 3], outlier_edges(i,1));
    end_pose = Pose([1 2 3], outlier_edges(i,2));
% ----------------------------------------------------------------------- %       
    Data_xy = [5*rand; 5*rand; ];       
    Data_phi = 2*pi*rand;
% ----------------------------------------------------------------------- %       
    while (Data_phi > pi)
		Data_phi = Data_phi - 2 * pi;
	end;
	while (Data_phi <= -pi)
		Data_phi = Data_phi + 2 * pi;
	end; 
% ----------------------------------------------------------------------- %  
    Zstate = [Zstate; [[Data_xy; Data_phi], outlier_edges(i,2)*ones(3,1)-1, outlier_edges(i,1)*ones(3,1)-1, -1*ones(3,1)] ];   
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%% Constraint Edges : M %%%%%%%%%%%%%%%%%%%%%%%%%%%
save SimpleExample Zstate



