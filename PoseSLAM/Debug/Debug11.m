clear all
close all
clc

iSQP_times = 0;
DCS_times = 0;
Cauchy_times = 0;

for trial = 1:50
    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
NewSphere = 1;
NewOutlier = 1;
%%%%%%%%%%%%%%%%% set noise level of simulation sphere %%%%%%%%%%%%%%%%%
noiseTran = 1;     % 0.1         % 6     0.6   1.2   1.8
noiseRota =0.06;     % 0.001  18    %  0.3   0.03  0.06  0.09
nodesPerLevel = 30;  % 50     
laps = 30;           % 50
radius = 30;         % 100
load('GroundTruth15by15.mat')
GroundTruth = GroundTruth([7: size(GroundTruth, 1)],1);
% %%%%%%%%%%%%%%%%%%%%%%%%%% set G2O parameters %%%%%%%%%%%%%%%%%%%%%%%%%%
% max_iteration = 500;    
% kernel = 'DCS';   % 'Cauchy', 'DCS', 'Fair', 'GemanMcClure', 'Huber', 'PseudoHuber', 'Saturated', 'Tukey', 'Welsch',   g2o -listRobustKernels
% kernelWidth = 10;  %  DCS = 10
% %%%%%%%%%%%%%%%%%%%%%%%%% set number of outliers %%%%%%%%%%%%%%%%%%%%%%%
num_outlier = 50;  %300   %    50   100   200   350   500
groupsize = 1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Set dataset parth
g2o_path = '/home/user/github/g2o/bin/'; 
add_outliers = '/home/user/PoseSLAM/Data_G2O/generateDataset.py';
data_file = '/home/user/PoseSLAM/Data_G2O/MySphere.g2o';
data_file_outlier = '/home/user/PoseSLAM/Data_G2O/MySphere_Outlier.g2o';
%%% simulate sphere
noise =[ ' -noiseTranslation "', num2str(noiseTran),';',num2str(noiseTran),';',num2str(noiseTran)  ...
    '" -noiseRotation "', num2str(noiseRota),';',num2str(noiseRota),';',num2str(noiseRota),'"'];
para = [' -nodesPerLevel ', num2str(nodesPerLevel), ' -laps ', num2str(laps), ' -radius ',num2str(radius) ];
if(NewSphere)
string_cmd = [g2o_path,'create_sphere',' -o ', data_file, para , noise];
[resut_cmd, info_cmd ] = system(string_cmd);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Solve the dataset with SQP Algorithm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(0)
    [ Zstate0, CovMatrixInv0 ] = ConvertG2OFormatToZstatePS3D( data_file );
    [ Zstate0, CovMatrixInv0 ] = ArrangeDataSequence( Zstate0, CovMatrixInv0 );
    CovMatrix0 = inv(CovMatrixInv0);
    [Res_SQP, Fval_SQP ] = PoseSLAM_SQP_3D ( Zstate0, CovMatrix0);
    PlotXstate3D_PoseSLAM( Res_SQP, 'm.--' );
end

if(0)
Xstate0 = FuncCreateXstateFromZstate_EU_3D_PoseSLAM( Zstate0 );
CreateG2OFormatByZstatePS3D (data_file, Zstate0, CovMatrixInv0, Xstate0);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Now add outliers to the dataset, and solve again with iSQP and G2O
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% add outliers to the dataset
if(NewOutlier)
string_cmd2 = [add_outliers, ' -i ', data_file, ' -o ', data_file_outlier, ...
               ' -n ', num2str(num_outlier), ' -g ', num2str(groupsize), ' -s'];
[result2_cmd, info2_cmd] = system(string_cmd2);
end

% [DataSetType ] = AddOutliersPoseSLAM( Zstate, CovMatrixInv, NumRandomOutliers, NumInitialOutliers )
% CreateG2OFormatByZstatePS3D (data_file_outlier, Zstate, CovMatrixInv, Xstate0);

% create Zstate data
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS3D( data_file_outlier );
[ Zstate1, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
%size_of_Zstate_before = size(Zstate1,1)
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate1, CovMatrixInv );
%size_of_Zstate_after = size(Zstate,1)
CovMatrix = inv(CovMatrixInv);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot inital value
% Xstate0 = FuncCreateXstateFromZstate_EU_3D_PoseSLAM( Zstate );
% PlotXstate3D_PoseSLAM( Xstate0, 'k.--' );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% sove the dataset with outliers by iSQP 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% tic 
% [Res_iSQP, Fval_iSQP, N_All ] = PoseSLAM_iSQP_3D ( Zstate, CovMatrix);
% PlotXstate3D_PoseSLAM( Res_iSQP, 'r.--' );
% toc


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% solve the dataset with outliers by G2O
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% rewrite g2o format dataset
[ Res_GN_DCS ] = PoseSLAM_G2O_GN_3D( Zstate, CovMatrixInv, 'DCS', 10, 100);
PlotXstate3D_PoseSLAM(  Res_GN_DCS, 'b.--' );
[ Res_GN_Cauchy ] = PoseSLAM_G2O_GN_3D( Zstate, CovMatrixInv, 'Cauchy', 1.0, 100);
PlotXstate3D_PoseSLAM( Res_GN_Cauchy, 'g.--' );

[ Obj_init ] = Obj_Pose_3D( Zstate0, CovMatrixInv0, GroundTruth );
[ Obj_g2o_DCS ] = Obj_Pose_3D( Zstate0, CovMatrixInv0,  Res_GN_DCS );
[ Obj_g2o_Cauchy ] = Obj_Pose_3D( Zstate0, CovMatrixInv0,  Res_GN_Cauchy );
[ Obj_iSQP ] = Obj_Pose_3D( Zstate0, CovMatrixInv0, Res_iSQP );

% [ Obj_SQP ] = Obj_Pose_3D( Zstate0, CovMatrixInv0, Res_SQP );

if(Obj_iSQP <= Obj_init)
    iSQP_Success = 'success';
    iSQP_times = iSQP_times + 1;
else
    iSQP_Success = 'failed';
end

if(Obj_g2o_DCS <= Obj_init)
    DCS_Success = 'success';
    DCS_times = DCS_times + 1;
else
    DCS_Success = 'failed';
end

if(Obj_g2o_Cauchy <= Obj_init)
    Cauchy_Success = 'success';
    Cauchy_times = Cauchy_times + 1;
else
    Cauchy_Success = 'failed';
end

disp(['trial = ', num2str(trial), ':   iSQP = ', iSQP_Success, '  DCS = ', DCS_Success, '  Cauchy = ', Cauchy_Success]);
% 
% 
% OutlierConsAdded = [find(N_All(:,4)==-4), N_All(find(N_All(:,4)==-4),:)]
% CorrectConsNotAdded = [find(N_All(:,4)==1), N_All(find(N_All(:,4)==1),:)]
pause(5)
end

iSQP_times
DCS_times
Cauchy_times


