clear all
close all
clc

% load('CovMatrixInvSphereBigNoise.mat')
% load('CovMatrixSphereBigNoise.mat')
% load('ZstateSphereBigNoise.mat')

% 
load('ZstateParking_Garage.mat');
load('CovMatrixInvParking_Garage.mat');
load('CovMatrixParking_Garage.mat')

PoseNum = 500;
DataSize = find(Zstate(:,3) == PoseNum, 1, 'last');
Zstate = Zstate([1:DataSize], :);
index = find(Zstate(:,2) < PoseNum+2);
Zstate = Zstate(index, :);

Xstate0 = FuncCreateXstateFromZstate_EU_3D_PoseSLAM( Zstate );
DrawXstate3D_PoseSLAM( Xstate0)
% CovMatrixInv = CovMatrixInv(index, index);
% CovMatrix = CovMatrix(index, index);

CovMatrixInv = speye(size(index,1));
CovMatrix = speye(size(index,1));

% using identity matrix
if(0)
   DataDim = size(Zstate,1);
   CovMatrixInv = speye(DataDim);
   CovMatrix = speye(DataDim);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tic
[Res_SQP, Fval_SQP] = PoseSLAM_SQP_3D ( Zstate, CovMatrix);
toc

DrawXstate3D_PoseSLAM( Res_SQP)

% Set g2o parth
g2o_path = '/home/user/github/g2o/bin/'; 
% Set file name and path
input_file = '/home/user/PoseSLAM/Data_G2O/tmp.g2o';
output_file = '/home/user/PoseSLAM/Data_G2O/tmp_out.g2o';
summary_file = '/home/user/PoseSLAM/Data_G2O/tmp_summary.g2o';


File_Path_Name = input_file;
Xstate0 = FuncCreateXstateFromZstate_EU_3D_PoseSLAM( Zstate );
CreateG2OFormatByZstatePS3D (File_Path_Name, Zstate, CovMatrixInv, Res_SQP );

% Set max_iteration
max_iteration = 100;

% process g2o
string_cmd = [g2o_path,'g2o ', '-i ', num2str(max_iteration),' -summary ', summary_file,' -solver gn_var_eigen',' -o ', output_file, ' ', input_file];

[resut_cmd info_cmd ] = system(string_cmd);

[ Res_G2O ] = CreateXstateFromG2OData_3D_PoseSLAM( output_file );

Res_G2O = Res_G2O ([7: size(Res_G2O)],:);

[Res_G2O(:,1)-Res_SQP(:,1), Res_G2O(:,1), Res_SQP ]


erro = max (abs(Res_G2O - Res_SQP))
Fval_SQP

DrawXstate3D_PoseSLAM( Res_G2O)



