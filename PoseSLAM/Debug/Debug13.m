clear all
close all
clc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Set dataset parth
g2o_path = '/home/user/github/g2o/bin/'; 
data_file = '/home/user/PoseSLAM/DataSet/MITb.g2o';
%data_file = '/home/user/PoseSLAM/DataSet/intel.g2o';

add_outliers = '/home/user/PoseSLAM/DataSet/generateDataset.py';
% Set file name and path
input_file = '/home/user/PoseSLAM/Data_G2O/tmp.g2o';
output_file = '/home/user/PoseSLAM/Data_G2O/tmp_out.g2o';
summary_file = '/home/user/PoseSLAM/Data_G2O/tmp_summary.g2o';

%%%%%%%%%%%%%%%%%%%%%%%%%% set G2O parameters %%%%%%%%%%%%%%%%%%%%%%%%%%
max_iteration = 100;
kernel = 'DCS';   % 'Cauchy', 'DCS', 'Fair', 'GemanMcClure', 'Huber', 'PseudoHuber', 'Saturated', 'Tukey', 'Welsch',   g2o -listRobustKernels
kernelWidth = 1000;
%%%%%%%%%%%%%%%%%%%%%%%%% set number of outliers %%%%%%%%%%%%%%%%%%%%%%%
num_outlier =0;

data_file_outlier = ['/home/user/PoseSLAM/DataSet/MITb_', num2str(num_outlier), '.g2o'];
string_cmd = [add_outliers, ' -i ', data_file, ' -o ', data_file_outlier, ' -n ', num2str(num_outlier), ' -s'];
[resut_cmd info_cmd ] = system(string_cmd);


[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( data_file_outlier );
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
CovMatrix = inv(CovMatrixInv);
[Res_iSQP, Fval_iSQP ] = PoseSLAM_iSQP_2D ( Zstate, CovMatrix);
PlotXstate2D_PoseSLAM( Res_iSQP, 'r.--' );

% tic
% [Res_SQP, Fval_SQP] = PoseSLAM_SQP_2D ( Zstate, CovMatrix);
% toc
% 
% DrawXstate2D_PoseSLAM( Res_SQP)
% % Set g2o parth



[ Xstate0 ]  = FuncCreateXstateFromZstate_2D_PoseSLAM( Zstate);
CreateG2OFormatByZstatePS2D (data_file_outlier, Zstate, CovMatrixInv, Xstate0 );
% Set max_iteration
max_iteration = 100;

% 
% % process g2o
% string_cmd = [g2o_path,'g2o ', '-i ', num2str(max_iteration),' -summary ', summary_file, ... 
%     ' -solver gn_var_eigen',' -o ', output_file, ' ', data_file_outlier];
% [resut_cmd info_cmd ] = system(string_cmd);
% [ Res_G2O ] = CreateXstateFromG2OData_2D_PoseSLAM( output_file );
% Res_G2O = Res_G2O ([4: size(Res_G2O)],:);
% PlotXstate2D_PoseSLAM( Res_G2O ,'g.');


% process g2o
string_cmd = [g2o_path,'g2o ', '-i ', num2str(max_iteration),' -summary ', summary_file, ... 
    ' -robustKernel ', kernel, ' -robustKernelWidth ', num2str(kernelWidth), ...
    ' -solver gn_var_eigen',' -o ', output_file, ' ', data_file_outlier];
[resut_cmd info_cmd ] = system(string_cmd);

[ Res_G2O ] = CreateXstateFromG2OData_2D_PoseSLAM( output_file );
Res_G2O = Res_G2O ([4: size(Res_G2O)],:);
PlotXstate2D_PoseSLAM( Res_G2O ,'b.');
