clear all
close all
clc

num_outlier = 0;  %20 % 10 % 5


%data_file = 'G2O/ringCity.g2o';
%data_file = 'G2O/ring.g2o';


data_file = 'G2O/INTEL_P.g2o';


[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( data_file );
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
%
% 0 -3499;
if(0)
%StartPose = 1000;
EndPose = 2800;    % Edges = 4332;  Triangles = 1531
%EndPose = 2700;    % Edges = 4153;  Triangles = 1452
%EndPose = 2600;    % Edges = 4006;  Triangles = 1405
%EndPose = 2595;    % Edges = 4000;  Triangles = 1404

DataSize = find(Zstate(:,3) == EndPose, 1, 'last');
Zstate = Zstate([1:DataSize], :);
index = find(Zstate(:,2) < EndPose+2);
Zstate = Zstate(index, :);
CovMatrixInv = CovMatrixInv(index, index);
end
CovMatrix = inv(CovMatrixInv);

T1 = cputime;
[Res_SQP, Fval_SQP] = PoseSLAM_SQP_2D ( Zstate, CovMatrix);
T = cputime - T1
PlotXstate2D_PoseSLAM( Res_SQP, 'k.--');
pause(0.01);
%
% [ Xstate0 ]  = FuncCreateXstateFromZstate_2D_PoseSLAM( Zstate);
% PlotXstate2D_PoseSLAM( Xstate0);
% 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Now add outliers to the dataset, and solve again with iSQP and G2O
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% add outliers to the dataset
[Zstate, CovMatrixInv] = AddOutliersPoseSLAM( Zstate, CovMatrixInv, num_outlier, 0 );
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
CovMatrix = inv(CovMatrixInv);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic
[Res_iSQP, Fval_iSQP] = PoseSLAM_iSQP_2D ( Zstate, CovMatrix);
PlotXstate2D_PoseSLAM( Res_iSQP, 'r.--')
pause(0.01);
toc

%
[ Res_GN_DCS ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'DCS', 6.0, 1000); % 6.0
PlotXstate2D_PoseSLAM(  Res_GN_DCS, 'b.--' );
pause(0.01);
% 
 [ Res_GN_Cauchy ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'Cauchy', 1.0, 200);
PlotXstate2D_PoseSLAM( Res_GN_Cauchy, 'g.--' );
pause(0.01);




