clear all
close all
clc

% load('CovMatrixInvParking_Garage.mat')
% tic
% CovMatrix = inv(CovMatrixInv);
% toc
% save CovMatrixParking_Garage CovMatrix

% clear all
% Zstate= Zstate(1:4038, :);

% load('CovMatrixInvSphereBigNoise.mat')
% tic
% CovMatrix = inv(CovMatrixInv);
% toc
% save CovMatrixSphereBigNoise CovMatrix


% load('ZstateParking_Garage.mat')
load('ZstateSphereBigNoise.mat')
load CovMatrixSphereBigNoise CovMatrix
% Zstate= Zstate(1:4038, :);
% CovMatrix = CovMatrix(1:4038, 1:4038);

Zstate_EU = Zstate;

tic
[ M_e, N_e ]  = GetTriangle3D_PoseSLAM( Zstate_EU );
toc

tic
[ Zstate_SE ] =  ZstateFromEuclideanToSE3_PoseSLAM( Zstate_EU );
toc

tic
[ M_s, N_s ] = GetTriangle3D_PoseSLAM( Zstate_SE );
toc

[ Data_SE ] = Zstate_SE(:,1);




X0 = Data_SE;

tic
[ Con_value ] = Nonlinear_Con_3D_PoseSLAM( X0, M_s );
toc

con_norm = max(abs(Con_value))

tic
[ J, eta ] = Linear_Obj_3D_PoseSLAM(Data_SE, X0);
toc

tic
[ A, b ] = Linear_Con_3D_PoseSLAM( X0, M_e, M_s );
toc

[ Increment_EU,  F_value] = Equilibrium_Direct_Method_3D(J, eta, CovMatrix, A ,b);


[ X0 ] = Update_SEbyEU_3D_PoseSLAM(X0, Increment_EU );


[ Con_value ] = Nonlinear_Con_3D_PoseSLAM( X0, M_s );