clear all
close all
clc

data_file = '/home/user/PoseSLAM/DataSet/M3500.g2o';

[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( data_file );
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
CovMatrix = inv(CovMatrixInv);

    
PoseNum = 1000;
DataSize = find(Zstate(:,3) == PoseNum, 1, 'last');
Zstate = Zstate([1:DataSize], :);
index = find(Zstate(:,2) < PoseNum+2);
Zstate = Zstate(index, :);

CovMatrixInv = CovMatrixInv(index, index);
CovMatrix = CovMatrix(index, index);

% using identity matrix
if(0)
   DataDim = size(Zstate,1);
   CovMatrixInv = speye(DataDim);
   CovMatrix = speye(DataDim);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


[ Xstate0 ]  = FuncCreateXstateFromZstate_2D_PoseSLAM( Zstate);
DrawXstate2D_PoseSLAM( Xstate0);

tic
[Res_SQP, Fval_SQP] = PoseSLAM_SQP_2D ( Zstate, CovMatrix);
toc

DrawXstate2D_PoseSLAM( Res_SQP)
% Set g2o parth
g2o_path = '/home/user/github/g2o/bin/'; 
% Set file name and path
input_file = '/home/user/PoseSLAM/Data_G2O/tmp.g2o';
output_file = '/home/user/PoseSLAM/Data_G2O/tmp_out.g2o';
summary_file = '/home/user/PoseSLAM/Data_G2O/tmp_summary.g2o';


File_Path_Name = input_file;
[ Xstate0 ]  = FuncCreateXstateFromZstate_2D_PoseSLAM( Zstate);
CreateG2OFormatByZstatePS2D (File_Path_Name, Zstate, CovMatrixInv, Xstate0 );
% Set max_iteration
max_iteration = 100;

% process g2o
string_cmd = [g2o_path,'g2o ', '-i ', num2str(max_iteration),' -summary ', summary_file,' -solver gn_var_eigen',' -o ', output_file, ' ', input_file];

[resut_cmd info_cmd ] = system(string_cmd);

[ Res_G2O ] = CreateXstateFromG2OData_2D_PoseSLAM( output_file );

Res_G2O = Res_G2O ([4: size(Res_G2O)],:);

format long;


[Res_G2O(:,1)-Res_SQP(:,1), Res_G2O(:,1), Res_SQP ]
DrawXstate2D_PoseSLAM( Res_G2O)

erro = max (abs(Res_G2O - Res_SQP))
Fval_SQP




