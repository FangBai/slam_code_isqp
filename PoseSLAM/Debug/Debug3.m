clear all
close all
clc
%load('ZstateParking_Garage.mat')
load('ZstateSphereBigNoise.mat')




Zstate_EU = Zstate;

[ Zstate_SE ] = ZstateFromEuclideanToSE3_PoseSLAM( Zstate_EU );
[ Zstate_EU ] = ZstateFromSE3ToEuclidean_PoseSLAM( Zstate_SE );


format long

err = max(abs(Zstate - Zstate_EU))

line = find(abs(Zstate - Zstate_EU)>1e-7)
[Zstate(line,1) Zstate_EU(line,1)]

[ Xstate_1 ]  = FuncCreateXstateFromZstate_EU_3D_PoseSLAM( Zstate_EU );
[ Xstate_2 ]  = FuncCreateXstateFromZstate_SE_3D_PoseSLAM( Zstate_SE );
[ Xstate_3 ]  = FuncCreateXstateFromZstate_EU_3D_PoseSLAM( Zstate );

erro1 = max(abs(Xstate_1 - Xstate_2))
erro2 = max(abs(Xstate_3 - Xstate_2))
erro3 = max(abs(Xstate_3 - Xstate_1))
