close all
clear all
clc

num_outlier = 0;  %20 % 10 % 5

data_file = 'G2O/sphere_bignoise_vertex3.g2o';
%data_file = 'G2O/parking-garage.g2o';
%
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS3D( data_file );
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
%[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
%
CovMatrix = inv(CovMatrixInv);

T1 = cputime
[Res_SQP, Fval_SQP] = PoseSLAM_SQP_3D ( Zstate, CovMatrix);
T = cputime - T1
PlotXstate3D_PoseSLAM( Res_SQP, 'k.--');
pause(0.01);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Now add outliers to the dataset, and solve again with iSQP and G2O
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% add outliers to the dataset
[Zstate, CovMatrixInv] = AddOutliersPoseSLAM( Zstate, CovMatrixInv, num_outlier, 0 );
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
CovMatrix = inv(CovMatrixInv);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% tic
% [Res_iSQP, Fval_iSQP] = PoseSLAM_iSQP_3D ( Zstate, CovMatrix);
% PlotXstate3D_PoseSLAM( Res_iSQP, 'r.--')
% pause(0.01);
% toc

%
[ Res_GN_DCS ] = PoseSLAM_G2O_GN_3D( Zstate, CovMatrixInv, 'DCS', 6.0, 1000); % 6.0
PlotXstate3D_PoseSLAM(  Res_GN_DCS, 'b.--' );
pause(0.01);
% 
[ Res_GN_Cauchy ] = PoseSLAM_G2O_GN_3D( Zstate, CovMatrixInv, 'Cauchy', 1.0, 200);
PlotXstate3D_PoseSLAM( Res_GN_Cauchy, 'g.--' );
pause(0.01);




