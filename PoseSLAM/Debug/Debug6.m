clear all
close all;
clc

% 
%  load('ZstateSphereBigNoise.mat')
%  load('CovMatrixInvSphereBigNoise.mat')
%  load CovMatrixSphereBigNoise CovMatrix
% n = 10938
% n = 14976


load('ZstateParking_Garage.mat')
load('CovMatrixParking_Garage.mat')


% n= 19782;
% Zstate = Zstate([1:n],:);
% CovMatrix = CovMatrix([1:n], [1:n]);

[ Xstate0 ]  = FuncCreateXstateFromZstate_EU_3D_PoseSLAM( Zstate);
PlotXstate3D_PoseSLAM( Xstate0, 'b.');
DrawXstate3D_PoseSLAM( Xstate0 );


[Res_SQP, Fval_SQP] = PoseSLAM_SQP_3D ( Zstate, CovMatrix);

PlotXstate3D_PoseSLAM( Res_SQP, 'r.')
DrawXstate3D_PoseSLAM( Res_SQP );

Fval_SQP

