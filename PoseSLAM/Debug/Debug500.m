
clear all
close all
clc
SimpleExample;
clear all

g2o_path = '/home/user/github/g2o/bin/'; 
data_file = '/home/user/PoseSLAM/Data_G2O/SimpleExample.g2o';

load('SimpleExample.mat')
CovMatrixInv = speye( size(Zstate,1) );
CovMatrix = inv(CovMatrixInv);
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );

Xstate0 = FuncCreateXstateFromZstate_2D_PoseSLAM( Zstate );
CreateG2OFormatByZstatePS2D (data_file, Zstate, CovMatrixInv, Xstate0);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( data_file );
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
CovMatrix = inv(CovMatrixInv);

Xstate0 = FuncCreateXstateFromZstate_2D_PoseSLAM( Zstate );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% tic
% [Res_SQP, Fval_SQP, N_All ] = PoseSLAM_iSQP_2D ( Zstate0, CovMatrix0);
% PlotXstate3D_PoseSLAM( Res_SQP, 'm.--' );
% toc

[ Res_GN_DCS ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'DCS', 1.0, 300);
PlotXstate2D_PoseSLAM(  Res_GN_DCS, 'b.--' );

[ Res_GN_Cauchy ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'Cauchy', 1.0, 100);
PlotXstate2D_PoseSLAM( Res_GN_Cauchy, 'g.--' );







