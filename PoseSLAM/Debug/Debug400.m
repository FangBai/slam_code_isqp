clear all
close all
clc

num_outlier = 50;  %300   %    50   100   200   350   500
groupsize = 1;


g2o_path = '/home/user/github/g2o/bin/'; 
add_outliers = '/home/user/PoseSLAM/DataSet/generateDataset.py';
data_file_outlier = '/home/user/PoseSLAM/Data_G2O/MySphere_Outlier.g2o';
data_file = '/home/user/PoseSLAM/G2O/input_M3500b_g2o.g2o';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Now add outliers to the dataset, and solve again with iSQP and G2O
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% add outliers to the dataset
string_cmd2 = [add_outliers, ' -i ', data_file, ' -o ', data_file_outlier, ...
               ' -n ', num2str(num_outlier), ' -g ', num2str(groupsize), ' -s'];
[result2_cmd, info2_cmd] = system(string_cmd2);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% create Zstate data
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( data_file_outlier );
[ Zstate1, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
%size_of_Zstate_before = size(Zstate1,1)
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate1, CovMatrixInv );
%size_of_Zstate_after = size(Zstate,1)
CovMatrix = inv(CovMatrixInv);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% tic
% [Res_SQP, Fval_SQP, N_All ] = PoseSLAM_iSQP_2D ( Zstate, CovMatrix);
% PlotXstate3D_PoseSLAM( Res_SQP, 'r.--' );
% toc

[ Res_GN_DCS ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'DCS', 8.0, 100);
PlotXstate2D_PoseSLAM(  Res_GN_DCS, 'b.--' );

% [ Res_GN_Cauchy ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'Cauchy', 1.0, 100);
% PlotXstate2D_PoseSLAM( Res_GN_Cauchy, 'g.--' );

% OutlierConsAdded = [find(N_All(:,4)==-4), N_All(find(N_All(:,4)==-4),:)]
% CorrectConsNotAdded = [find(N_All(:,4)==1), N_All(find(N_All(:,4)==1),:)]



