clear all
close all
clc


noiseTran = 0.1;
noiseRota =0.01;
nodesPerLevel = 10;
laps = 10;
radius = 10;

% Set g2o parth
g2o_path = '/home/user/github/g2o/bin/'; 
input_file = '/home/user/PoseSLAM/Data_G2O/MySphere.g2o';
output_file = '/home/user/PoseSLAM/Data_G2O/MySphere_after.g2o';
summary_file = '/home/user/PoseSLAM/Data_G2O/MySphere_summary.g2o';

noise =[ ' -noiseTranslation "', num2str(noiseTran),';',num2str(noiseTran),';',num2str(noiseTran)  ...
    '" -noiseRotation "', num2str(noiseRota),';',num2str(noiseRota),';',num2str(noiseRota),'"'];
para = [' -nodesPerLevel ', num2str(nodesPerLevel), ' -laps ', num2str(laps), ' -radius ',num2str(radius) ];
% simulate sphere
string_cmd = [g2o_path,'create_sphere',' -o ', input_file, para , noise];
[resut_cmd info_cmd ] = system(string_cmd);
% create Zstate data
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS3D( input_file );
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
CovMatrix = inv(CovMatrixInv);

% using identity matrix
if(0)
   DataDim = size(Zstate,1);
   CovMatrixInv = speye(DataDim);
   CovMatrix = speye(DataDim);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% plot inital value
Xstate0 = FuncCreateXstateFromZstate_EU_3D_PoseSLAM( Zstate );
PlotXstate3D_PoseSLAM( Xstate0, 'k.--' );
% SQP optimization
tic 
[Res_SQP, Fval_SQP ] = PoseSLAM_SQP_3D ( Zstate, CovMatrix);
PlotXstate3D_PoseSLAM( Res_SQP, 'r.--' );
toc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
File_Path_Name = input_file;
Xstate0 = FuncCreateXstateFromZstate_EU_3D_PoseSLAM( Zstate );
CreateG2OFormatByZstatePS3D (File_Path_Name, Zstate, CovMatrixInv, Xstate0);
% Set max_iteration
max_iteration = 100;


% process g2o
string_cmd = [g2o_path,'g2o ', '-i ', num2str(max_iteration),' -summary ', summary_file,' -solver gn_var_eigen',' -o ', output_file, ' ', input_file];

[resut_cmd info_cmd ] = system(string_cmd);

[ Res_G2O ] = CreateXstateFromG2OData_3D_PoseSLAM( output_file );
PlotXstate3D_PoseSLAM( Res_G2O, 'b.--' );

Res_G2O = Res_G2O ([7: size(Res_G2O)],:);

[Res_G2O(:,1)-Res_SQP(:,1), Res_G2O(:,1), Res_SQP ]


erro = max (abs(Res_G2O - Res_SQP))

Fval_SQP

