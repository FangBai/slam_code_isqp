clear all
close all
clc

g2o_path = '/home/user/github/g2o/bin/'; 
add_outliers = '/home/user/PoseSLAM/DataSet/generateDataset.py';
data_file = '/home/user/PoseSLAM/Data_G2O/INTEL_P.g2o';


[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( data_file);
[ Zstate1, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
%size_of_Zstate_before = size(Zstate1,1)
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate1, CovMatrixInv );
%size_of_Zstate_after = size(Zstate,1)

CovMatrix = (CovMatrixInv);

[Zstate_EU, CovMatrix] = ConvertDataSetFrom2DTo3D ( Zstate, CovMatrix);

[ Data_SE, Me_All, Ms_All, N_All ] = ExtractDataSetInfo_PoseSLAM_iSQP( Zstate_EU );

[ Edge_Cons, Edge_Index, Edges_Vertex ] = Edge_Related_Constraints( N_All, Zstate_EU, '3D');


i = 15

Edge = [Edges_Vertex(i, 1), Edges_Vertex(i, 2)]
RelateCons = N_All(Edge_Index{i}, :)
Cons = N_All(Edge_Index{i}(1), :)



if(Cons(1,3) - Cons(1,1) > 1)
    Edge_Cons{Cons(1,1), Cons(1,3)}
elseif(Cons(1,3) - Cons(1,2) > 1)
    Edge_Cons{Cons(1,2), Cons(1,3)}
end
    











