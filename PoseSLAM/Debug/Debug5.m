
clear all
close all;
clc


load('ZstateManhattan3500.mat')
load('CovMatrixManhattan3500.mat')

load('ZstateIntel.mat')
load('CovMatrixIntel.mat')

[Zstate_EU, CovMatrix] = ConvertDataSetFrom2DTo3D ( Zstate, CovMatrix);

[ Data_SE, M_e, M_s, N ] = ExtractDataSetInfo_PoseSLAM( Zstate_EU );

X0_SE = Data_SE;

[ X_value_SE, Fval_SQP_2D ] = My_method_solve_3D_PoseSLAM( X0_SE, Data_SE, CovMatrix, M_e, M_s);

% get Xstate form of solution
[ Zstate_SE ] = ZstateFromEuclideanToSE3_PoseSLAM( Zstate_EU );
Zstate_SE(:,1) = X_value_SE;
Xstate_3D  = FuncCreateXstateFromZstate_SE_3D_PoseSLAM( Zstate_SE );    

[Res_SQP_2D] = ConvertXstateFrom3DTo2D (Xstate_3D);



PlotXstate2D_PoseSLAM( Res_SQP_2D, 'r.' );
DrawXstate2D_PoseSLAM( Res_SQP_2D );

Fval_SQP_2D

              

