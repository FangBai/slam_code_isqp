clc
close all

NumRandomOutliers = 50;
NumInitialOutliers = 0;

g2o_path = '/home/user/github/g2o/bin/'; 
data_file = '/home/user/PoseSLAM/Data_G2O/MySphere.g2o';
data_file_outlier = '/home/user/PoseSLAM/Data_G2O/MySphere_Outlier.g2o';

[ Zstate0, CovMatrixInv0 ] = ConvertG2OFormatToZstatePS3D( data_file );
[ Zstate0, CovMatrixInv0 ] = ArrangeDataSequence( Zstate0, CovMatrixInv0 );
[ Zstate0, CovMatrixInv0 ] = FilterOutDuplicateEdges( Zstate0, CovMatrixInv0 );
CovMatrix0 = inv(CovMatrixInv0);


Xstate0 = FuncCreateXstateFromZstate_EU_3D_PoseSLAM( Zstate0 );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Now add outliers to the dataset, and solve again with iSQP and G2O
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% add outliers to the dataset
[ Zstate, CovMatrixInv ] = AddOutliersPoseSLAM( Zstate0, CovMatrixInv0, NumRandomOutliers, NumInitialOutliers );
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );

%Zstate
%size_of_Zstate_before = size(Zstate,1)
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
%size_of_Zstate_after = size(Zstate,1)
CovMatrix = inv(CovMatrixInv);



