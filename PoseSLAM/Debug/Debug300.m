clear all
close all
clc

num_outlier = 10;  %300   %    50   100   200   350   500
groupsize = 5;


g2o_path = '/home/user/github/g2o/bin/'; 
add_outliers = '/home/user/PoseSLAM/DataSet/generateDataset.py';
data_file_outlier = '/home/user/PoseSLAM/Data_G2O/MySphere_Outlier.g2o';
data_file = '/home/user/PoseSLAM/Data_G2O/MySphere.g2o';

MainLoop;

load Zstate_Simu_256 Zstate


CovMatrixInv = speye( size(Zstate,1) );
CovMatrix = inv(CovMatrixInv);

Xstate0 = FuncCreateXstateFromZstate_2D_PoseSLAM( Zstate );
CreateG2OFormatByZstatePS2D (data_file, Zstate, CovMatrixInv, Xstate0);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Solve the dataset with SQP Algorithm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( data_file );
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
CovMatrix = inv(CovMatrixInv);

Xstate0 = FuncCreateXstateFromZstate_2D_PoseSLAM( Zstate );
PlotXstate2D_PoseSLAM( Xstate0, 'k.--' );
axis([-10, 20, -5, 25])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Solve the dataset with SQP Algorithm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(1)
[Res_SQP, Fval_SQP ] = PoseSLAM_SQP_2D ( Zstate, CovMatrix);
PlotXstate2D_PoseSLAM( Res_SQP, 'm.--' );
end
axis([-10, 20, -5, 25])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Now add outliers to the dataset, and solve again with iSQP and G2O
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% add outliers to the dataset
string_cmd2 = [add_outliers, ' -i ', data_file, ' -o ', data_file_outlier, ...
               ' -n ', num2str(num_outlier), ' -g ', num2str(groupsize), ' -s'];
[result2_cmd, info2_cmd] = system(string_cmd2);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% create Zstate data
% [ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( data_file_outlier );
% % [ Zstate1, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
% %size_of_Zstate_before = size(Zstate1,1)
% [ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate1, CovMatrixInv );
% %size_of_Zstate_after = size(Zstate,1)
% CovMatrix = inv(CovMatrixInv);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic
[Res_SQP, Fval_SQP, N_All ] = PoseSLAM_iSQP_2D ( Zstate, CovMatrix);
PlotXstate2D_PoseSLAM( Res_SQP, 'r.--' );
axis([-10, 20, -5, 25])
toc

[ Res_GN_DCS ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'DCS', 1.0, 100);
PlotXstate2D_PoseSLAM(  Res_GN_DCS, 'b.--' );
axis([-10, 20, -5, 25])

[ Res_GN_Cauchy ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'Cauchy', 1.0, 100);
PlotXstate2D_PoseSLAM( Res_GN_Cauchy, 'g.--' );
axis([-10, 20, -5, 25])

OutlierConsAdded = [find(N_All(:,4)==-4), N_All(find(N_All(:,4)==-4),:)]
CorrectConsNotAdded = [find(N_All(:,4)==1), N_All(find(N_All(:,4)==1),:)]



