
num_trian1 = size(M_e,2);
num_trian2 = size(M_s,2);
if(num_trian1 ~= num_trian2)
    disp('M_e, M_s have different amount of constraints!');
    return
end
num_trian = num_trian1;
cur_trian = 1;
%define sparse matrix A
A = sparse(1,1);

while(cur_trian<=num_trian)
%%%%%%%%%%% read information from cell M_e, M_s
num_odom1 = M_e{cur_trian}(1);
num_odom2 = M_s{cur_trian}(1);
if(num_odom1 ~= num_odom2)
    disp(['For constraint with index ', num2str(cur_trian),', edge numbers in M_e, M_s are different!']);
    return
end
num_odom = num_odom1;
%odometry indices
for i=1:num_odom
    ode(i)= M_e{cur_trian}(i+1);
    ods(i)= M_s{cur_trian}(i+1);
end
%observation 1 index
obe1=M_e{cur_trian}(num_odom+2);
obs1=M_s{cur_trian}(num_odom+2);
%observation 2 index
obe2=M_e{cur_trian}(num_odom+3);
obs2=M_s{cur_trian}(num_odom+3);
%%%%%%%%% read information complete %%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%set coefficient for odometry position
A([6*cur_trian-5, 6*cur_trian-4, 6*cur_trian-3], [ode(1), ode(1)+1, ode(1)+2]) = - eye(3);  
for i=2:num_odom
    R_cur = [ X0(ods(i-1)+3), X0(ods(i-1)+4), X0(ods(i-1)+5);
              X0(ods(i-1)+6), X0(ods(i-1)+7), X0(ods(i-1)+8);
              X0(ods(i-1)+9), X0(ods(i-1)+10), X0(ods(i-1)+11);];
    A([6*cur_trian-5, 6*cur_trian-4, 6*cur_trian-3], [ode(i), ode(i)+1, ode(i)+2]) = ...
        A([6*cur_trian-5, 6*cur_trian-4, 6*cur_trian-3], [ode(i-1), ode(i-1)+1, ode(i-1)+2])*R_cur;
end

%set coefficient for observation
A([6*cur_trian-5, 6*cur_trian-4, 6*cur_trian-3], [obe1, obe1+1, obe1+2]) = eye(3);  
    R_cur = [ X0(ods(num_odom)+3), X0(ods(num_odom)+4), X0(ods(num_odom)+5);
              X0(ods(num_odom)+6), X0(ods(num_odom)+7), X0(ods(num_odom)+8);
              X0(ods(num_odom)+9), X0(ods(num_odom)+10), X0(ods(num_odom)+11);];   
A([6*cur_trian-5, 6*cur_trian-4, 6*cur_trian-3], [obe2, obe2+1, obe2+2]) = ...
    A([6*cur_trian-5, 6*cur_trian-4, 6*cur_trian-3], [ode(num_odom), ode(num_odom)+1, ode(num_odom)+2])*R_cur;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%set coefficient for odometry orientation/ minimal representation in Euclidean
    X_cur = [ X0(obs2); X0(obs2+1); X0(obs2+2)];
A([6*cur_trian-5, 6*cur_trian-4, 6*cur_trian-3], [ode(num_odom)+3]) = X_cur; 
% compute sum(Di) recursively %%%%%%%%%%%%%%
for i= (num_odom-1): -1 :1
    X_cur = [ X0(ods(i+1)); X0(ods(i+1)+1); X0(ods(i+1)+2)];  
    R_cur = [ X0(ods(i+1)+3), X0(ods(i+1)+4), X0(ods(i+1)+5);
              X0(ods(i+1)+6), X0(ods(i+1)+7), X0(ods(i+1)+8);
              X0(ods(i+1)+9), X0(ods(i+1)+10), X0(ods(i+1)+11);];
A([6*cur_trian-5, 6*cur_trian-4, 6*cur_trian-3], [ode(i)+3]) = ... 
    R_cur * A([6*cur_trian-5, 6*cur_trian-4, 6*cur_trian-3], [ode(i+1)+3]) +  X_cur;
end
% compute Ci*sum(Di)
for i=1:(num_odom-1)
    A([6*cur_trian-5, 6*cur_trian-4, 6*cur_trian-3], [ode(i)+3, ode(i)+4, ode(i)+5]) = ...
        - A([6*cur_trian-5, 6*cur_trian-4, 6*cur_trian-3], [ode(i+1), ode(i+1)+1, ode(i+1)+2]) ... 
        * SkewSem(A([6*cur_trian-5, 6*cur_trian-4, 6*cur_trian-3], [ode(i)+3]));
end
A([6*cur_trian-5, 6*cur_trian-4, 6*cur_trian-3], [ode(num_odom)+3, ode(num_odom)+4, ode(num_odom)+5]) = ...
        - A([6*cur_trian-5, 6*cur_trian-4, 6*cur_trian-3], [obe2, obe2+1, obe2+2]) ... 
        * SkewSem(A([6*cur_trian-5, 6*cur_trian-4, 6*cur_trian-3], [ode(num_odom)+3]));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%compute vector b
b([6*cur_trian-5, 6*cur_trian-4, 6*cur_trian-3],1) = zeros(3,1);
for i=1:num_odom
    X_cur = [X0(ods(i)); X0(ods(i)+1); X0(ods(i)+2)];
    b([6*cur_trian-5, 6*cur_trian-4, 6*cur_trian-3],1) = b([6*cur_trian-5, 6*cur_trian-4, 6*cur_trian-3],1) - ...
        A([6*cur_trian-5, 6*cur_trian-4, 6*cur_trian-3], [ode(i), ode(i)+1, ode(i)+2])* X_cur;
end
    X_cur = [X0(obs2); X0(obs2+1); X0(obs2+2)];
b([6*cur_trian-5, 6*cur_trian-4, 6*cur_trian-3],1) = b([6*cur_trian-5, 6*cur_trian-4, 6*cur_trian-3],1) - ...
    A([6*cur_trian-5, 6*cur_trian-4, 6*cur_trian-3], [obe2, obe2+1, obe2+2])* X_cur;
    X_cur = [X0(obs1); X0(obs1+1); X0(obs1+2)];
b([6*cur_trian-5, 6*cur_trian-4, 6*cur_trian-3],1) = b([6*cur_trian-5, 6*cur_trian-4, 6*cur_trian-3],1) - X_cur;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% linearize rotational constraints
A([6*cur_trian-2, 6*cur_trian-1, 6*cur_trian],[obe1+3, obe1+4, obe1+5]) = -eye(3);
A([6*cur_trian-2, 6*cur_trian-1, 6*cur_trian],[obe2+3, obe2+4, obe2+5]) = eye(3);


R_cur = [ X0(obs2+3), X0(obs2+4), X0(obs2+5);
          X0(obs2+6), X0(obs2+7), X0(obs2+8);
          X0(obs2+9), X0(obs2+10), X0(obs2+11);];
A([6*cur_trian-2, 6*cur_trian-1, 6*cur_trian], [ode(num_odom)+3, ode(num_odom)+4, ode(num_odom)+5]) = R_cur.';

for i= (num_odom-1): -1 :1
    R_cur = [ X0(ods(i+1)+3), X0(ods(i+1)+4), X0(ods(i+1)+5);
              X0(ods(i+1)+6), X0(ods(i+1)+7), X0(ods(i+1)+8);
              X0(ods(i+1)+9), X0(ods(i+1)+10), X0(ods(i+1)+11);];
    A([6*cur_trian-2, 6*cur_trian-1, 6*cur_trian], [ode(i)+3, ode(i)+4, ode(i)+5]) = ... 
        A([6*cur_trian-2, 6*cur_trian-1, 6*cur_trian], [ode(i+1)+3, ode(i+1)+4, ode(i+1)+5]) * R_cur.';
end
R_cur = [ X0(ods(1)+3), X0(ods(1)+4), X0(ods(1)+5);
          X0(ods(1)+6), X0(ods(1)+7), X0(ods(1)+8);
          X0(ods(1)+9), X0(ods(1)+10), X0(ods(1)+11);];

Rota = A([6*cur_trian-2, 6*cur_trian-1, 6*cur_trian], [ode(1)+3, ode(1)+4, ode(1)+5]) * R_cur.';
R_cur = [ X0(obs1+3), X0(obs1+4), X0(obs1+5);
          X0(obs1+6), X0(obs1+7), X0(obs1+8);
          X0(obs1+9), X0(obs1+10), X0(obs1+11);];
      
Rota = Rota * R_cur;
eta = Log_R2a(Rota.');

b([6*cur_trian-2, 6*cur_trian-1, 6*cur_trian],1) = - JacobianRightHandSO3( eta ) * eta;
      
    cur_trian = cur_trian + 1;
end


