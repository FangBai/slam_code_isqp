clear all
close all
clc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
NewSphere = 1;
NewOutlier = 1;
%%%%%%%%%%%%%%%%% set noise level of simulation sphere %%%%%%%%%%%%%%%%%
noiseTran = 0.0000000000001;
noiseRota = 0.00000000000001;
nodesPerLevel = 15;
laps = 15;
radius = 15;
%%%%%%%%%%%%%%%%%%%%%%%%%% set G2O parameters %%%%%%%%%%%%%%%%%%%%%%%%%%
max_iteration = 500;
kernel = 'DCS';   % 'Cauchy', 'DCS', 'Fair', 'GemanMcClure', 'Huber', 'PseudoHuber', 'Saturated', 'Tukey', 'Welsch',   g2o -listRobustKernels
kernelWidth = 10;
%%%%%%%%%%%%%%%%%%%%%%%%% set number of outliers %%%%%%%%%%%%%%%%%%%%%%%
num_outlier = 500;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Set dataset parth
g2o_path = '/home/user/github/g2o/bin/'; 

data_file = '/home/user/PoseSLAM/Data_G2O/MySphere.g2o';
data_file_outlier = '/home/user/PoseSLAM/Data_G2O/MySphere_Outlier.g2o';
input_file = '/home/user/PoseSLAM/Data_G2O/MySphere_Outlier_before.g2o';
output_file = '/home/user/PoseSLAM/Data_G2O/MySphere_Outlier_after.g2o';
summary_file = '/home/user/PoseSLAM/Data_G2O/MySphere_Outlier_summary.g2o';
add_outliers = '/home/user/PoseSLAM/Data_G2O/generateDataset.py';

%%% simulate sphere
noise =[ ' -noiseTranslation "', num2str(noiseTran),';',num2str(noiseTran),';',num2str(noiseTran)  ...
    '" -noiseRotation "', num2str(noiseRota),';',num2str(noiseRota),';',num2str(noiseRota),'"'];
para = [' -nodesPerLevel ', num2str(nodesPerLevel), ' -laps ', num2str(laps), ' -radius ',num2str(radius) ];
%
if(NewSphere)
string_cmd = [g2o_path,'create_sphere',' -o ', data_file, para , noise];
[resut_cmd info_cmd ] = system(string_cmd);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Solve the dataset with SQP Algorithm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



if(1)
    [ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS3D( data_file );
    [ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
    CovMatrixInv = speye(size(Zstate, 1));
    CovMatrix = inv(CovMatrixInv);
        
    Xstate0 = FuncCreateXstateFromZstate_EU_3D_PoseSLAM( Zstate );
    [ Obj_iSQP ] = Obj_Pose_3D( Zstate, CovMatrixInv, Xstate0 )

    [Res_SQP, Fval_SQP ] = PoseSLAM_SQP_3D ( Zstate, CovMatrix);
    PlotXstate3D_PoseSLAM( Res_SQP, 'm.--' );
end

GroundTruth = [zeros(6,2); Res_SQP]

Obj_iSQP = Obj_Pose_3D( Zstate, CovMatrixInv, Res_SQP)
Fval_SQP

save GroundTruth15by15 GroundTruth



