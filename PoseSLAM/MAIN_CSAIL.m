clear all
close all
clc

num_outlier = 0;  % 10 % 20

choice = 3;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

DataFile = 'CSAIL_P';  % rec 3 % --- % 1, 2, 3   % 1 - same  %  2 - different  %  3 - slow convergence 
% DataFile = 'FRH_P';    % rec 2 % --- % 1, 2  % 1 - SQP correct  %  2 - same     
% DataFile = 'INTEL_P';   % rec 1 % 1, 2, 3, 4  %  1 - not converge  %  2 4 - SQP better solution and slow convergence % 3 - SQP better solution % 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ResultFolder = ['Results/', DataFile, '_', num2str(choice), '/Outlier_', num2str(num_outlier), '/'];
mkdir(ResultFolder);
delete([ResultFolder, 'ConsoleOutput.txt']);
diary([ResultFolder, 'ConsoleOutput.txt']);
diary on

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Directory = 'NoisyDataSet/LocalMinima_Odom/';
NoiseLevelStr = 'OM_0.05_0.05_0.1_LC_0.02_0.02_0.02';
data_file = [Directory, DataFile, '_Noisy_', NoiseLevelStr, '_', num2str(choice), '.g2o']


OutlierDirectory = 'NoisyDataSet/Minia_Outliers/';
outlier_data_file = [OutlierDirectory, DataFile, '_Noisy_', NoiseLevelStr, '_', ... 
    num2str(choice), '_Outliers_', num2str(num_outlier), '.g2o']
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(num_outlier == 0)
    InputFile = data_file;
else
    InputFile = outlier_data_file;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( InputFile );
%
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
%
CovMatrix = inv(CovMatrixInv);
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tic
[Res_iSQP, Obj_iSQP, PR_iSQP, N_All, ProcessInfo ] = PoseSLAM_iSQP_2D ( Zstate, CovMatrix);
PlotXstate2D_PoseSLAM( Res_iSQP, 'r.--' );
toc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Res_DCS, Obj_DCS, PR_DCS, Weight_DCS ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'DCS', 190, 200); % 190 CSAIL % 9 INTEL
PlotXstate2D_PoseSLAM( Res_DCS, 'b.--' );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Res_Cauchy, Obj_Cauchy, PR_Cauchy, Weight_Cauchy ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'Cauchy', 1.0, 200); 
PlotXstate2D_PoseSLAM( Res_Cauchy, 'g.--' );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Xstate0 = FuncCreateXstateFromZstate_2D_PoseSLAM( Zstate );
[ Res_GN, Obj_GN ] = PoseSLAM_G2O_GN( Zstate, CovMatrixInv, Res_Cauchy, 100, '2D' );
PlotXstate2D_PoseSLAM( Res_GN, 'c.--' );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Obj_GN

Obj_DCS

Obj_Cauchy

diary off


save([ResultFolder, 'Result_iSQP.mat'], 'Res_iSQP', 'Obj_iSQP', 'PR_iSQP', 'N_All', 'ProcessInfo');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% ################################################################## %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clear all
close all
clc

num_outlier = 20;  % 10 % 20

choice = 3;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

DataFile = 'CSAIL_P';  % rec 3 % --- % 1, 2, 3   % 1 - same  %  2 - different  %  3 - slow convergence 
% DataFile = 'FRH_P';    % rec 2 % --- % 1, 2  % 1 - SQP correct  %  2 - same     
% DataFile = 'INTEL_P';   % rec 1 % 1, 2, 3, 4  %  1 - not converge  %  2 4 - SQP better solution and slow convergence % 3 - SQP better solution % 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ResultFolder = ['Results/', DataFile, '_', num2str(choice), '/Outlier_', num2str(num_outlier), '/'];
mkdir(ResultFolder);
delete([ResultFolder, 'ConsoleOutput.txt']);
diary([ResultFolder, 'ConsoleOutput.txt']);
diary on

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Directory = 'NoisyDataSet/LocalMinima_Odom/';
NoiseLevelStr = 'OM_0.05_0.05_0.1_LC_0.02_0.02_0.02';
data_file = [Directory, DataFile, '_Noisy_', NoiseLevelStr, '_', num2str(choice), '.g2o']


OutlierDirectory = 'NoisyDataSet/Minia_Outliers/';
outlier_data_file = [OutlierDirectory, DataFile, '_Noisy_', NoiseLevelStr, '_', ... 
    num2str(choice), '_Outliers_', num2str(num_outlier), '.g2o']
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(num_outlier == 0)
    InputFile = data_file;
else
    InputFile = outlier_data_file;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( InputFile );
%
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
%
CovMatrix = inv(CovMatrixInv);
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tic
[Res_iSQP, Obj_iSQP, PR_iSQP, N_All, ProcessInfo ] = PoseSLAM_iSQP_2D ( Zstate, CovMatrix);
PlotXstate2D_PoseSLAM( Res_iSQP, 'r.--' );
toc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Res_DCS, Obj_DCS, PR_DCS, Weight_DCS ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'DCS', 190, 200); % 190 CSAIL % 9 INTEL
PlotXstate2D_PoseSLAM( Res_DCS, 'b.--' );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Res_Cauchy, Obj_Cauchy, PR_Cauchy, Weight_Cauchy ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'Cauchy', 1.0, 200); 
PlotXstate2D_PoseSLAM( Res_Cauchy, 'g.--' );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Xstate0 = FuncCreateXstateFromZstate_2D_PoseSLAM( Zstate );
[ Res_GN, Obj_GN ] = PoseSLAM_G2O_GN( Zstate, CovMatrixInv, Res_Cauchy, 100, '2D' );
PlotXstate2D_PoseSLAM( Res_GN, 'c.--' );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Obj_GN

Obj_DCS

Obj_Cauchy

diary off


save([ResultFolder, 'Result_iSQP.mat'], 'Res_iSQP', 'Obj_iSQP', 'PR_iSQP', 'N_All', 'ProcessInfo');








