%
%
function [Res_SQP_2D, Fval_SQP] = PoseSLAM_SQP_2D( Zstate_2D, CovMatrix_2D)
%CovMatrix_2D = inv(CovMatrixInv_2D);

[Zstate_EU, CovMatrix] = ConvertDataSetFrom2DTo3D ( Zstate_2D, CovMatrix_2D);

[Res_SQP, Fval_SQP] = PoseSLAM_SQP_3D ( Zstate_EU, CovMatrix);

% [ Obj_SQP_3D ] = Obj_Pose_3D( Zstate_EU, inv(CovMatrix), Res_SQP )

[Res_SQP_2D] = ConvertXstateFrom3DTo2D (Res_SQP);

end

%%%%%%%%%%%%
function [Zstate_3D, CovMatrix_3D] = ConvertDataSetFrom2DTo3D ( Zstate_2D, CovMatrix_2D)

count3 = 1;
count2 = 1;

Zstate_3D = [];
CovMatrix_3D = sparse(0);

DataDim = size(Zstate_2D, 1);

while (count2 < DataDim)
    
    Zstate_3D([count3, count3+1, count3+5], [1,2,3,4]) = ... 
        Zstate_2D([count2, count2+1, count2+2], [1,2,3,4]);    
    
    Zstate_3D([count3+2, count3+3, count3+4], [1]) = [0; 0; 0];
    
    Zstate_3D([count3+2, count3+3, count3+4], [2,3,4]) = ...
        Zstate_2D([count2, count2+1, count2+2], [2,3,4]);
    
    CovMatrix_3D([count3, count3+1, count3+5], [count3, count3+1, count3+5]) = ...
        CovMatrix_2D([count2, count2+1, count2+2], [count2, count2+1, count2+2]);
    
    CovMatrix_3D([count3+2, count3+3, count3+4], [count3+2, count3+3, count3+4]) = speye(3);
    
    count3 = count3 + 6;
    count2 = count2 + 3;
        
end
end


%%%%%%%%%%%
function [Xstate_2D] = ConvertXstateFrom3DTo2D (Xstate_3D)

count3 = 1;
count2 = 1;

DataDim = size(Xstate_3D, 1);

while (count3 < DataDim)
 
    Xstate_2D([count2, count2+1, count2+2], [1,2]) = ...
        Xstate_3D([count3, count3+1, count3+5], [1,2]);
    
    count3 = count3 + 6;
    count2 = count2 + 3;
    
end
end