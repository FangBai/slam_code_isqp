%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [  ] = AddNoiseToG2OFilePoseSLAM (input_file, output_file, NoiseLevelOdom, NoiseLevelLoopClosure, DataType_str)

if(strcmp(DataType_str, '3D')) 
    
    [ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS3D( input_file );
    [ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
    [ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
    
    [ Xstate0 ] = FuncCreateXstateFromZstate_EU_3D_PoseSLAM( Zstate );
    [ Res ] = PoseSLAM_G2O_GN( Zstate, CovMatrixInv, Xstate0, 100, '3D' );
    [ Zstate, CovMatrixInv ] = AddNoiseToZstatePoseSLAM( Zstate, CovMatrixInv, Res, NoiseLevelOdom, NoiseLevelLoopClosure, '', '3D' );

    [ Xstate0 ] = FuncCreateXstateFromZstate_EU_3D_PoseSLAM( Zstate );
    CreateG2OFormatByZstatePS3D (output_file, Zstate, CovMatrixInv, Xstate0);
 
elseif(strcmp(DataType_str, '2D'))
    
    [ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( input_file );
    [ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
    [ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
    
    [ Xstate0 ] = FuncCreateXstateFromZstate_2D_PoseSLAM( Zstate );
    [ Res ] = PoseSLAM_G2O_GN( Zstate, CovMatrixInv, Xstate0, 100, '2D' );
    [ Zstate, CovMatrixInv ] = AddNoiseToZstatePoseSLAM( Zstate, CovMatrixInv, Res, NoiseLevelOdom, NoiseLevelLoopClosure, '', '2D' );

    [ Xstate0 ] = FuncCreateXstateFromZstate_2D_PoseSLAM( Zstate );
    CreateG2OFormatByZstatePS2D (output_file, Zstate, CovMatrixInv, Xstate0);
    
else
    disp('Dataset type string is not correct');
    return;
end

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ Zstate, CovMatrixInv ] = AddNoiseToZstatePoseSLAM( Zstate, CovMatrixInv, GroundTruth, NoiseLevelOdom, NoiseLevelLoopClosure, Mode, DataType_str )



count = 1;

SIGMA_BOUND = 2.0;

if(size(NoiseLevelOdom,2) > size(NoiseLevelOdom,1))
    NoiseLevelOdom = NoiseLevelOdom.';
end

if(size(NoiseLevelLoopClosure,2) > size(NoiseLevelLoopClosure,1))
    NoiseLevelLoopClosure = NoiseLevelLoopClosure.';
end

SIGMA_OM = diag(NoiseLevelOdom);

SIGMA_LC = diag(NoiseLevelLoopClosure);

Dim = size(Zstate, 1);

Cov_i = zeros(Dim,1);
Cov_j = zeros(Dim,1);
Cov_v = zeros(Dim,1);

while(count <= Dim)
    
    pose_start = Zstate(count, 3);
    pose_end   = Zstate(count, 2);
    
    if(abs(pose_end - pose_start) > 1)    % loop closure
        Noise = NoiseLevelLoopClosure;  
        SIGMA = SIGMA_LC;        
    else                                  % odometry
        Noise = NoiseLevelOdom;        
        SIGMA = SIGMA_OM;
    end
    
    
    if(strcmp(DataType_str, '3D'))
        
        end_vec = GroundTruth([6*pose_end-5 : 6*pose_end], 1);
        
        if(pose_start == 0)
            start_vec = [0;0;0;0;0;0;];
        else
            start_vec = GroundTruth([6*pose_start-5 : 6*pose_start], 1);
        end
        
        %
        Data_vec([1:3], 1) = Exp_a2R(start_vec([4:6] ,1)).' * (end_vec([1:3], 1) - start_vec([1:3], 1));
        
        Data_vec([4:6], 1) = Log_R2a( Exp_a2R(start_vec([4:6] ,1)).' *  Exp_a2R(end_vec([4:6] ,1)) );
        %
        
        %
        
        if(strcmp(Mode, 'additive'))
            Data_vec = Zstate([count : count+5], 1);
        end
    
        % Add noise to Data_vec

        for i = 1 : 6
            Rand_vec(i, 1) = randn;           
            while(abs(Rand_vec(i,1)) > SIGMA_BOUND)
                Rand_vec(i, 1) = randn;
            end
        end
        
        [ Data_vec([4:6],1) ] = AnlgeAxis2EulerAngle ( Data_vec([4:6],1) );
        
        Data_vec = Data_vec + SIGMA * Rand_vec;
        
        [ Data_vec([4:6],1) ] = EulerAngle2AnlgeAxis ( Data_vec([4:6],1) );
        
        %
        Zstate([count : count+5], 1) = Data_vec;
        
        Cov_i([count : count+5], 1) = [count : count+5].';
        Cov_j([count : count+5], 1) = [count : count+5].';
        Cov_v([count : count+5], 1) = [Noise].^2;
        
        count = count + 6;
        
    elseif(strcmp(DataType_str, '2D'))
        
        end_vec = GroundTruth([3*pose_end-2 : 3*pose_end], 1);
        
        if(pose_start == 0)
            start_vec = [0;0;0;];
        else
            start_vec = GroundTruth([3*pose_start-2 : 3*pose_start], 1);
        end
        
        %
        Data_vec([1,2], 1) = R(start_vec([3],1)).' * (end_vec([1,2],1) - start_vec([1,2] ,1));
        
        Data_vec([3], 1) = R2a(R(start_vec([3] ,1)).' *  R(end_vec([3] ,1)));
        %      
        
        %
        
        if(strcmp(Mode, 'additive'))
            Data_vec = Zstate([count : count+2], 1);
        end        

        % Add noise to Data_vec
       
        for i = 1 : 3
            Rand_vec(i, 1) = randn;            
            while(abs(Rand_vec(i,1)) > SIGMA_BOUND)
                Rand_vec(i, 1) = randn;
            end
        end
        
        Data_vec = Data_vec + SIGMA * Rand_vec;
        
        %
        Zstate([count : count+2], 1) = Data_vec;       
        
        Cov_i([count : count+2], 1) = [count : count+2].';
        Cov_j([count : count+2], 1) = [count : count+2].';
        Cov_v([count : count+2], 1) = [Noise].^2;
        
        count = count + 3;
        
    else
        disp('Dataset type string is not correct');
        return;
    end
    
end

Cov = sparse(Cov_i, Cov_j, Cov_v);

if(strcmp(Mode, 'additive'))
    CovMatrixInv = inv(inv(CovMatrixInv) + Cov);
else
    CovMatrixInv = inv(Cov);
end

end




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get rotation matrix A with respect to angle phi
function [A] = R(phi)
A=[cos(phi),-sin(phi);sin(phi),cos(phi)];
end

% Get angle phi from rotation matrix R
function [phi] = R2a(R)
phi = atan2(R(2,1), R(1,1));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ Theta ] = Log_R2a( R )
threshold = 1e-10;
angle = acos( (trace(R) -1 )/2 );

if (abs(angle) <threshold ) % 0
    axis = [1; 0; 0;];
elseif(abs(angle - pi) <threshold )  % pi
   % disp('reach pi');
    axis = - [sign(R(2,3))*sqrt((1+R(1,1))/2); sign(R(1,3))*sqrt((1+R(2,2))/2); sign(R(1,2))*sqrt((1+R(3,3))/2) ];
else
    axis = [ R(3,2)-R(2,3);
             R(1,3)-R(3,1);
             R(2,1)-R(1,2); ];
    axis = axis/(2*sin(angle));
end

Theta = angle*axis;
[ Theta ] = WrapX( Theta );
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Wrap a arbitrary axis angle rotation into a rotation at interval [0, pi]
function [ X_v ] = WrapX( X_vec )
if (norm(X_vec)== 0)
    X_v = X_vec;
    return; 
end
angle = norm(X_vec);
axis  = X_vec/angle;
while (angle >= 2*pi)
    angle = angle - 2*pi;
end
while ( angle > pi)
    angle = 2*pi - angle;
    axis = -axis;  
end
if(pi-angle<1e-10)
   if(axis(3)<0)
       axis = -axis;
   end    
end

X_v = angle*axis;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function is used to transform a vector into a rotation matrix by exponential mapping
function [ RotationMatrixExp ] = Exp_a2R( X_vec )

threshold = 1e-10;

theta = norm(X_vec);

if (theta < threshold)
    RotationMatrixExp = eye(3);
else
    RotationMatrixExp = eye(3) + SkewSem(X_vec)*sin(theta)/theta + ...
        (SkewSem(X_vec)^2)*(1 - cos(theta))/(theta^2);    
end

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% transform a 3-dimensional vector into a skew-semmetric marix
function [ SkewSemmetricMatrix ] = SkewSem( X_vec )
SkewSemmetricMatrix = [ 0          -X_vec(3)     X_vec(2);
                        X_vec(3)    0           -X_vec(1);
                       -X_vec(2)    X_vec(1)     0       ;];
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This function is used to convert AngleAxis representation to EulerAngles
% EulerAngle [heading; attitude; bank;] or [yaw; pitch; roll;]
% EulerAngle Rotated by [ y-axis, z-axis, x-axis ]
function [ EulerAngle ] = AnlgeAxis2EulerAngle( AngleAxis )

threshold = 1e-10;

angle = norm(AngleAxis);

if (angle < threshold)
    axis = [1; 0; 0;];
else
    axis = AngleAxis/angle;
end

x = axis(1);
y = axis(2);
z = axis(3);

pitch= asin(x*y*(1 - cos(angle)) + z*sin(angle));

if ( abs(pitch - pi/2) < threshold )
    yaw = 2*atan2(x*sin(angle/2), cos(angle/2));
    roll = 0;
elseif ( abs(pitch + pi/2) < threshold )
    yaw = -2*atan2(x*sin(angle/2), cos(angle/2));
    roll = 0;
else
    yaw = atan2(y*sin(angle)- x*z*(1 - cos(angle)) ,1 - (y^2 + z^2 )*(1 - cos(angle)));
    roll = atan2(x*sin(angle)-y*z*(1 - cos(angle)) ,1 - (x^2 + z^2)*(1 - cos(angle)));   
end

EulerAngle = [yaw; pitch; roll;];

end

% This function is used to convert EulerAngles to AngleAxis representation
% EulerAngle [heading; attitude; bank;] or [yaw; pitch; roll;]
% EulerAngle Rotated by [ y-axis, z-axis, x-axis ]
function [ AngleAxis ] = EulerAngle2AnlgeAxis ( EulerAngle )

threshold = 1e-10;

yaw = EulerAngle(1);
pitch = EulerAngle(2);
roll = EulerAngle(3);

c1 = cos(yaw/2);
c2 = cos(pitch/2);
c3 = cos(roll/2);
s1 = sin(yaw/2);
s2 = sin(pitch/2);
s3 = sin(roll/2);

angle = 2*acos(c1*c2*c3 - s1*s2*s3);

x = s1*s2*c3 + c1*c2*s3;
y = s1*c2*c3 + c1*s2*s3;
z = c1*s2*c3 - s1*c2*s3;

axis = [x; y; z;];

normalizer = sqrt(x^2 + y^2 + z^2);

if (normalizer < threshold)
   axis = [1; 0; 0;]; 
else
   axis = axis/normalizer;
end

AngleAxis = WrapX(angle*axis);

end
