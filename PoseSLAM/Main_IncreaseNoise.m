
clear all
close all
clc
% rng('shuffle');
% DataFile = 'CSAIL_P';
% DataFile = 'FR079_P';
% DataFile = 'FRH_P';
 DataFile = 'INTEL_P';
% DataFile = 'ringCity';
% DataFile = 'ring';


% mkdir NoisyDataSet

NewFile = true;

NoiseLevelOdom = [0.05; 0.05; 0.10];   % odometry
NoiseLevelLoopClosure = [0.02; 0.02; 0.02];    % loop closure

input_file = [ 'DataSet/', DataFile, '.g2o' ];
output_file = [ 'NoisyDataSet/', DataFile, '_Noisy_OM_', ...
                num2str(NoiseLevelOdom(1)), '_', ...
                num2str(NoiseLevelOdom(2)), '_', ...
                num2str(NoiseLevelOdom(3)), '_LC_', ... 
                num2str(NoiseLevelLoopClosure(1)), '_', ...
                num2str(NoiseLevelLoopClosure(2)), '_', ...
                num2str(NoiseLevelLoopClosure(3)), ...                 
                '.g2o' ];

if(NewFile)
AddNoiseToG2OFilePoseSLAM (input_file, output_file, NoiseLevelOdom, NoiseLevelLoopClosure, '2D');
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( output_file );
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
[ Xstate0 ] = FuncCreateXstateFromZstate_2D_PoseSLAM( Zstate );

[ Res_GN, Obj_GN ] = PoseSLAM_G2O_GN( Zstate, CovMatrixInv, Xstate0, 200, '2D' );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Res_Cauchy ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'Cauchy', 1.0, 100); 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Res_DCS ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'DCS', 10.0, 100); 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
PlotXstate2D_PoseSLAM( Xstate0, 'k.--' );
PlotXstate2D_PoseSLAM( Res_Cauchy, 'g.--' );
PlotXstate2D_PoseSLAM( Res_DCS, 'b.--' );
PlotXstate2D_PoseSLAM( Res_GN, 'r.--' );
Obj_GN
PlotXstate2D_PoseSLAM( Xstate0, 'k.--' );