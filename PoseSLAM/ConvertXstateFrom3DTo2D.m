function [Xstate_2D] = ConvertXstateFrom3DTo2D (Xstate_3D)

count3 = 1;
count2 = 1;

DataDim = size(Xstate_3D, 1);

while (count3 < DataDim)
 
    Xstate_2D([count2, count2+1, count2+2], [1,2]) = ...
        Xstate_3D([count3, count3+1, count3+5], [1,2]);
    
    count3 = count3 + 6;
    count2 = count2 + 3;
    
end

end