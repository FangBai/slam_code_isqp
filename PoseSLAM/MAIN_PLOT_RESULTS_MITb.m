clear all
close all
clc


datafile = 'MITb';
DataFile = ['DataSet/', datafile, '.g2o'];


[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( DataFile );
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
CovMatrix = inv(CovMatrixInv);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[Res_iSQP, Obj_iSQP, PR_iSQP, N_All ] = PoseSLAM_iSQP_2D ( Zstate, CovMatrix);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Res_DCS, Obj_DCS, PR_DCS, Weight_DCS ] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'DCS', 100, 200); % 660
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Xstate0 = FuncCreateXstateFromZstate_2D_PoseSLAM( Zstate );
[ Res_GN, Obj_GN ] = PoseSLAM_G2O_GN( Zstate, CovMatrixInv, Xstate0, 200, '2D' );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


PoseStr = 'b.--';
InlierStr = 'g.--';
OutlierStr = 'r.--';


PlotPoseEdgePoseSLAM2D( Res_DCS, Zstate, PoseStr, InlierStr, OutlierStr, 1 );
PlotPoseEdgePoseSLAM2D( Res_iSQP, Zstate, PoseStr, InlierStr, OutlierStr, 2 );
PlotPoseEdgePoseSLAM2D( Res_GN, Zstate, PoseStr, InlierStr, OutlierStr, 3 );

FigureDirectory = ['Figures/', datafile, '/'];
mkdir(FigureDirectory);

savefig([1, 2, 3], [FigureDirectory, 'FigureInlier']); 




