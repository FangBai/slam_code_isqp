clear all
close all
clc

% tic
% File_Path_Name = 'input_MITb_g2o.g2o';
% [ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( File_Path_Name );
% [ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
% save ZstateMITb Zstate;
% save CovMatrixInvMITb CovMatrixInv;
% CovMatrix = inv(CovMatrixInv);
% save CovMatrixMITb CovMatrix;
% toc
% 
% 
% 
% tic
% File_Path_Name = 'intel.g2o';
% [ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( File_Path_Name );
% [ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
% save ZstateIntel Zstate;
% save CovMatrixInvIntel CovMatrixInv;
% CovMatrix = inv(CovMatrixInv);
% save CovMatrixIntel CovMatrix;
% toc
% 
% clear all
% tic
% File_Path_Name = 'input_M3500_g2o.g2o';
% [ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( File_Path_Name );
% [ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
% save ZstateManhattan3500 Zstate;
% save CovMatrixInvManhattan3500 CovMatrixInv;
% CovMatrix = inv(CovMatrixInv);
% save CovMatrixManhattan3500 CovMatrix;
% toc
% 
clear all
tic;
File_Path_Name = 'parking-garage.g2o';
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS3D(  File_Path_Name );
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
CovMatrix = inv(CovMatrixInv);
save ZstateParking_Garage Zstate CovMatrixInv CovMatrix;
toc
% 
% clear all
% tic;
% File_Path_Name = 'sphere_bignoise_vertex3.g2o';
% [ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS3D(  File_Path_Name );
% [ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
% save ZstateSphereBigNoise Zstate;
% save CovMatrixInvSphereBigNoise CovMatrixInv;
% CovMatrix = inv(CovMatrixInv);
% save CovMatrixSphereBigNoise CovMatrix;
% toc
% 
