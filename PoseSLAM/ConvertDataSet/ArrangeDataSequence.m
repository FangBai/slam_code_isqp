% This function is used to arrange the order of the edges for both 2D and 3D pose SLAM Zstate format dataset
% The starting poses are arranged in the ascending order
% The ending poses are arranged in the descending order
function [ ZstateS, CovMatrixInvS ] = ArrangeDataSequence( Zstate, CovMatrixInv )

ZstateS = [];
CovMatrixInvS = sparse(0);
currentpose = 0;
count = 1;

MaxPose = max(Zstate(:,3));

while(currentpose <= MaxPose)
    
    PoseLine = find(Zstate(:,3) == currentpose);
    DataNum = size(PoseLine,1);
    Zstate(PoseLine,:);
    EndPose = Zstate(PoseLine,2);
    [PoseOrder, IndexOrder] = sort(EndPose, 'descend');
    PoseLine = PoseLine(IndexOrder);

    ZstateS([count:(count+DataNum-1)], [1,2,3])= Zstate(PoseLine,:);
    CovMatrixInvS([count:(count+DataNum-1)], [count:(count+DataNum-1)]) = CovMatrixInv(PoseLine,PoseLine);

    count = count + DataNum;
    currentpose = currentpose + 1;
    
end