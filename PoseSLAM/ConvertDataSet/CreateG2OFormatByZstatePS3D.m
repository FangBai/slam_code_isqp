function [  ] = CreateG2OFormatByZstatePS3D (File_Path_Name, Zstate, CovMatrixInv, Xstate0 )

% data format for each type
format_pose = 'VERTEX_SE3:QUAT %d %f %f %f %f %f %f %f\n';
format_odometry = 'EDGE_SE3:QUAT %d %d %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n';

% create and open the file
fid = fopen(File_Path_Name, 'w');

% write pose 0 into the file
fprintf(fid, format_pose, 0, 0, 0, 0, 0, 0, 0, 1);

% write vertices (pose and feature) into the file
num_vertex = size(Xstate0,1);

count = 1;
while (count< num_vertex)
    Posi = [Xstate0(count,1); Xstate0(count+1,1); Xstate0(count+2,1)];
    Rota = aa2q( [Xstate0(count+3,1); Xstate0(count+4,1); Xstate0(count+5,1)]);
    fprintf(fid, format_pose, Xstate0(count,2), Posi(1), Posi(2), Posi(3), Rota(2), Rota(3), Rota(4), Rota(1));
    count = count + 6;
end

% write odometry into the file
num_odom = size(Zstate,1);
count = 1;
while(count< num_odom)
    Posi = [Zstate(count,1); Zstate(count+1,1); Zstate(count+2,1)];
    Rota = aa2q( [Zstate(count+3,1); Zstate(count+4,1); Zstate(count+5,1)]);    
    cov = CovMatrixInv([count: (count+5)], [count: (count+5)]);
    cov = full(cov);
    fprintf(fid, format_odometry, Zstate(count,3), Zstate(count,2), Posi(1), Posi(2), Posi(3), Rota(2), Rota(3), Rota(4), Rota(1), ... 
        cov(1,1), cov(1,2), cov(1,3), cov(1,4), cov(1,5), cov(1,6), cov(2,2), cov(2,3), cov(2,4), cov(2,5), cov(2,6), ...
        cov(3,3), cov(3,4), cov(3,5), cov(3,6), cov(4,4), cov(4,5), cov(4,6), cov(5,5), cov(5,6), cov(6,6));
    count = count + 6;
end

%close file
fclose(fid);


end


% tranform axis angle representation to quaternion
% the axis angle representation must be presented by vertical vector
function [ quaternion ] = aa2q( X_vec )

if (norm(X_vec) ==0)
    q_w = 1;
    q_v = [0; 0; 0];
else
    q_w = cos(norm(X_vec)/2);
    q_v = X_vec*sin(norm(X_vec)/2)/norm(X_vec);
end

quaternion = [q_w; q_v];

end


