% This function is used to convert 3D pose SLAM dataset in g2o format to Zstate format
% Zstate Colum:  Data EndingPose StartingPose
% Zstate Row: Position, AngleAxis
% Position: x, y, z
% AngleAxis: angle*axis = [aax; aay; aaz];
function [ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS3D(  File_Path_Name )

threshold = 1e-10;
Zstate = [];
CovMatrixInv = sparse(0);
count = 1;

fid = fopen(File_Path_Name,'r');
g2oLine = fgetl(fid);

while g2oLine(1) ~=-1
    lineStr = textscan(g2oLine, '%s');
    
    if strcmp(lineStr{1}{1},'EDGE_SE3:QUAT')
        
        StartingPose = str2double(lineStr{1}{2});
        EndingPose = str2double(lineStr{1}{3});
        
        q_x = str2double(lineStr{1}{7});
        q_y = str2double(lineStr{1}{8});
        q_z = str2double(lineStr{1}{9});
        q_w = str2double(lineStr{1}{10}); 

        angle = 2*acos(q_w);
        if(angle<threshold)
            axis = [0;0;1];
        else
            axis = [q_x; q_y; q_z]/sin(angle/2);
        end
        if ( angle > pi)
            angle = 2*pi - angle;
            axis = -axis;
        end
        AngleAxis = angle*axis;

        Zstate([count:count+5], [1,2,3]) = ... 
            [str2double(lineStr{1}{4}), EndingPose, StartingPose;
             str2double(lineStr{1}{5}), EndingPose, StartingPose;
             str2double(lineStr{1}{6}), EndingPose, StartingPose;
             AngleAxis(1), EndingPose, StartingPose;
             AngleAxis(2), EndingPose, StartingPose;
             AngleAxis(3), EndingPose, StartingPose; ];
              
        CovMatrixInv([count:count+5], [count:count+5]) = ... 
            [str2double(lineStr{1}{11}), str2double(lineStr{1}{12}), str2double(lineStr{1}{13}), str2double(lineStr{1}{14}), str2double(lineStr{1}{15}), str2double(lineStr{1}{16});
             str2double(lineStr{1}{12}), str2double(lineStr{1}{17}), str2double(lineStr{1}{18}), str2double(lineStr{1}{19}), str2double(lineStr{1}{20}), str2double(lineStr{1}{21});
             str2double(lineStr{1}{13}), str2double(lineStr{1}{18}), str2double(lineStr{1}{22}), str2double(lineStr{1}{23}), str2double(lineStr{1}{24}), str2double(lineStr{1}{25});
             str2double(lineStr{1}{14}), str2double(lineStr{1}{19}), str2double(lineStr{1}{23}), str2double(lineStr{1}{26}), str2double(lineStr{1}{27}), str2double(lineStr{1}{28});
             str2double(lineStr{1}{15}), str2double(lineStr{1}{20}), str2double(lineStr{1}{24}), str2double(lineStr{1}{27}), str2double(lineStr{1}{29}), str2double(lineStr{1}{30});
             str2double(lineStr{1}{16}), str2double(lineStr{1}{21}), str2double(lineStr{1}{25}), str2double(lineStr{1}{28}), str2double(lineStr{1}{30}), str2double(lineStr{1}{31});];              
        count = count + 6;       
    end
    g2oLine = fgetl(fid);
end
fclose(fid);

end
