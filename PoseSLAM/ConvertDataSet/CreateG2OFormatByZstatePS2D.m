function [  ] = CreateG2OFormatByZstatePS2D (File_Path_Name, Zstate, CovMatrixInv, Xstate0 )
%File_Path_Name = [FileZstate([7:(size(FileZstate,2) -4)]),'.g2o'];
% data format for each type
format_pose = 'VERTEX_SE2 %d %f %f %f\n';
format_odometry = 'EDGE_SE2 %d %d %f %f %f %f %f %f %f %f %f\n';

% create and open the file
fid = fopen(File_Path_Name, 'w');

% write pose 0 into the file
fprintf(fid, format_pose, 0, 0, 0, 0);

% write vertices (pose and feature) into the file
num_vertex = size(Xstate0,1);

count = 1;
while (count< num_vertex)
    Posi = [Xstate0(count,1); Xstate0(count+1,1);];
    Angle = Xstate0(count+2,1);
    fprintf(fid, format_pose, Xstate0(count,2), Posi(1), Posi(2), Angle);
    count = count + 3;
end

% write odometry into the file
num_odom = size(Zstate,1);
count = 1;
while(count< num_odom)
    Posi = [Zstate(count,1); Zstate(count+1,1);];
    Angle = Zstate(count+2,1);
    cov = CovMatrixInv([count: (count+2)], [count: (count+2)]);
    cov = full(cov);
    fprintf(fid, format_odometry, Zstate(count,3), Zstate(count,2), Posi(1), Posi(2), Angle, ... 
        cov(1,1), cov(1,2), cov(1,3), cov(2,2), cov(2,3) ,cov(3,3) );
    count = count + 3;
end
%close file
fclose(fid);