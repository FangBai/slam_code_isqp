function [ Xstate_EU ]  = FuncCreateXstateFromZstate_EU_3D_PoseSLAM( Zstate_EU )

Zcount = 1;
Xcount = 1;
Xstate_EU = [];
CurrentPosi = [0; 0; 0];
CurrentRota = eye(3);

Zstate_EU = Zstate_EU(find((Zstate_EU(:,2) - Zstate_EU(:,3)) == 1), :);
DataDim = size(Zstate_EU,1);

while ( Zcount <= DataDim )

    CurrentPosi = CurrentRota*Zstate_EU([Zcount, Zcount+1, Zcount+2], 1) + CurrentPosi;
    CurrentRota = CurrentRota*Exp_a2R( Zstate_EU([Zcount+3, Zcount+4, Zcount+5], 1));  
        
    Xstate_EU([Xcount, Xcount+1, Xcount+2], 1) = CurrentPosi;
    Xstate_EU([Xcount+3, Xcount+4, Xcount+5], 1) = Log_R2a(CurrentRota);
        
    Xstate_EU([Xcount : Xcount+5],[2]) = Zstate_EU( [Zcount : Zcount+5],[2]); 
    Xcount = Xcount + 6;
    Zcount = Zcount + 6;    

end
end
% This function is used to transform a vector into a rotation matrix by exponential mapping
function [ RotationMatrixExp ] = Exp_a2R( X_vec )

threshold = 1e-10;

theta = norm(X_vec);

if (theta < threshold)
    RotationMatrixExp = eye(3);
else
    RotationMatrixExp = eye(3) + SkewSem(X_vec)*sin(theta)/theta + ...
        (SkewSem(X_vec)^2)*(1 - cos(theta))/(theta^2);    
end

end
% This function gives the Logarithm mapping to tranform a rotation R to the minimal representation Theta
% The minimal representation is described by axis angle representation
% Theta is a vertical vector
function [ Theta ] = Log_R2a( R )
threshold = 1e-10;
angle = acos( (trace(R) -1 )/2 );

if (abs(angle) <threshold ) % 0
    axis = [1; 0; 0;];
elseif(abs(angle - pi) <threshold )  % pi
   % disp('reach pi');
    axis = - [sign(R(2,3))*sqrt((1+R(1,1))/2); sign(R(1,3))*sqrt((1+R(2,2))/2); sign(R(1,2))*sqrt((1+R(3,3))/2) ];
else
    axis = [ R(3,2)-R(2,3);
             R(1,3)-R(3,1);
             R(2,1)-R(1,2); ];
    axis = axis/(2*sin(angle));
end

Theta = angle*axis;
[ Theta ] = WrapX( Theta );
end
% transform a 3-dimensional vector into a skew-semmetric marix
function [ SkewSemmetricMatrix ] = SkewSem( X_vec )
SkewSemmetricMatrix = [ 0          -X_vec(3)     X_vec(2);
                        X_vec(3)    0           -X_vec(1);
                       -X_vec(2)    X_vec(1)     0       ;];
end
% Wrap a arbitrary axis angle rotation into a rotation at interval [0, pi]
function [ X_v ] = WrapX( X_vec )
if (norm(X_vec)== 0)
    X_v = X_vec;
    return; 
end
angle = norm(X_vec);
axis  = X_vec/angle;
while (angle >= 2*pi)
    angle = angle - 2*pi;
end
while ( angle > pi)
    angle = 2*pi - angle;
    axis = -axis;  
end
X_v = angle*axis;
end


