clear all
close all
clc

tic
load('ZstateParking_Garage.mat');
load('CovMatrixInvParking_Garage.mat');
File_Path_Name = 'garage.g2o';
Xstate0 = FuncCreateXstateFromZstate_EU_3D_PoseSLAM( Zstate );
CreateG2OFormatByZstatePS3D (File_Path_Name, Zstate, CovMatrixInv, Xstate0 );
toc

tic
load('ZstateManhattan3500.mat');
load('CovMatrixInvManhattan3500.mat');
File_Path_Name = 'M3500.g2o';
Xstate0  = FuncCreateXstateFromZstate_2D_PoseSLAM( Zstate );
CreateG2OFormatByZstatePS2D (File_Path_Name, Zstate, CovMatrixInv, Xstate0 )
toc

