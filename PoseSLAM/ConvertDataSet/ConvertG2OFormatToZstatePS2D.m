% This function is used to convert 2D pose SLAM dataset in g2o format to Zstate format
% Zstate Colum:  Data EndingPose StartingPose
% Zstate Row: x, y, phi
function [ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D(  File_Path_Name )

Zstate = [];
CovMatrixInv = sparse(0);
count = 1;

fid = fopen(File_Path_Name,'r');
g2oLine = fgetl(fid);

while g2oLine(1) ~=-1
    lineStr = textscan(g2oLine, '%s');
    
    if strcmp(lineStr{1}{1},'EDGE_SE2')
        
        StartingPose = str2double(lineStr{1}{2});
        EndingPose = str2double(lineStr{1}{3});
       
        Zstate([count:count+2], [1,2,3]) = ... 
            [str2double(lineStr{1}{4}), EndingPose, StartingPose;
             str2double(lineStr{1}{5}), EndingPose, StartingPose;
             str2double(lineStr{1}{6}), EndingPose, StartingPose; ];
              
        CovMatrixInv([count:count+2], [count:count+2]) = ... 
            [str2double(lineStr{1}{7}), str2double(lineStr{1}{8}), str2double(lineStr{1}{9});
             str2double(lineStr{1}{8}), str2double(lineStr{1}{10}), str2double(lineStr{1}{11});
             str2double(lineStr{1}{9}), str2double(lineStr{1}{11}), str2double(lineStr{1}{12}); ];
            
        count = count + 3;       
    end
    g2oLine = fgetl(fid);
end
fclose(fid);

end


