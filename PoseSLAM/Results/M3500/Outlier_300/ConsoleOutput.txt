meet: odometry edges
meet: end_pose   start_pose   518  517
===========================================================================================
  $:> NumAllEdges -|-ConPosi--ConRota-|-ConsQuality--medianConsQuality-|-QualityRatio--ConsCount -|-OutlierFlag
    :>5  ||  0.0031148    0.000558  ||  0.002773354           NaN  ||  NaN    1  ||   NO
Time function choose Constraint = 6.95    Time Compute Constraint Quality = 6.93    Time Check Independency = 0
=>> ConsAdd/ConsTotal=1/3186    ConsNum = 1
+>> Timing:  ChooseConstraint = 7.01    IncreaseMcell = 0    SoveSubproblem = 2.55
+>> FvalChange = 8.9775e-05    median(FvalGrowth) = 8.9775e-05    Iter = 2    median(IterTimes) = 2
+>> Recovery Ratio (ObjeGrowth * Iteration) = 1    ObjectiveGrowth = 1    IterFluctuation = 1
===========================================================================================
===========================================================================================
  $:> NumAllEdges -|-ConPosi--ConRota-|-ConsQuality--medianConsQuality-|-QualityRatio--ConsCount -|-OutlierFlag
    :>34  ||  0.01279  0.00067869  ||  0.0037908   0.0027734  ||  1.3669           1  ||   NO
Time function choose Constraint = 7.6    Time Compute Constraint Quality = 7.56    Time Check Independency = 0.04
=>> ConsAdd/ConsTotal=2/3186    ConsNum = 1
+>> Timing:  ChooseConstraint = 7.6    IncreaseMcell = 0    SoveSubproblem = 3.49
+>> FvalChange = 5.687e-05    median(FvalGrowth) = 7.3323e-05    Iter = 3    median(IterTimes) = 2.5
+>> Recovery Ratio (ObjeGrowth * Iteration) = 0.93073    ObjectiveGrowth = 0.77561    IterFluctuation = 1.2
===========================================================================================
===========================================================================================
  $:> NumAllEdges -|-ConPosi--ConRota-|-ConsQuality--medianConsQuality-|-QualityRatio--ConsCount -|-OutlierFlag
    :>6  ||  0.01637    0.001126  ||  0.0098202   0.0032821  ||  2.9921           1  ||   NO
Time function choose Constraint = 6.92    Time Compute Constraint Quality = 6.92    Time Check Independency = 0
=>> ConsAdd/ConsTotal=3/3186    ConsNum = 1
+>> Timing:  ChooseConstraint = 6.92    IncreaseMcell = 0    SoveSubproblem = 3.13
+>> FvalChange = 0.0016138    median(FvalGrowth) = 8.9775e-05    Iter = 3    median(IterTimes) = 3
+>> Recovery Ratio (ObjeGrowth * Iteration) = 17.9757    ObjectiveGrowth = 17.9757    IterFluctuation = 1
===========================================================================================
===========================================================================================
  $:> NumAllEdges -|-ConPosi--ConRota-|-ConsQuality--medianConsQuality-|-QualityRatio--ConsCount -|-OutlierFlag
{Operation terminated by user during <a href="matlab:matlab.internal.language.introspective.errorDocCallback('Nonlinear_Con_3D_PoseSLAM_Quality', '/home/user/Documents/SLAM_CODE_iSQP/PoseSLAM/Nonlinear_Con_3D_PoseSLAM_Quality.m', 80)" style="font-weight:bold">Nonlinear_Con_3D_PoseSLAM_Quality</a> (<a href="matlab: opentoline('/home/user/Documents/SLAM_CODE_iSQP/PoseSLAM/Nonlinear_Con_3D_PoseSLAM_Quality.m',80,0)">line 80</a>)


In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('PoseSLAM_iSQP_3D>DetermineConToAddByNormAndIndependancy', '/home/user/Documents/SLAM_CODE_iSQP/PoseSLAM/PoseSLAM_iSQP_3D.m', 514)" style="font-weight:bold">PoseSLAM_iSQP_3D>DetermineConToAddByNormAndIndependancy</a> (<a href="matlab: opentoline('/home/user/Documents/SLAM_CODE_iSQP/PoseSLAM/PoseSLAM_iSQP_3D.m',514,0)">line 514</a>)
[ Con_Quality, Con_Norm_Posi, Con_Norm_Rota ] = Nonlinear_Con_3D_PoseSLAM_Quality( Ms_All, Me_All, N_All, X0_SE,
Var_EuInc, BenchMark);


In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('PoseSLAM_iSQP_3D', '/home/user/Documents/SLAM_CODE_iSQP/PoseSLAM/PoseSLAM_iSQP_3D.m', 80)" style="font-weight:bold">PoseSLAM_iSQP_3D</a> (<a href="matlab: opentoline('/home/user/Documents/SLAM_CODE_iSQP/PoseSLAM/PoseSLAM_iSQP_3D.m',80,0)">line 80</a>)
    [N_All, ConsQualityHistory ] = DetermineConToAddByNormAndIndependancy(Ms_All, Me_All, N_All, X0_SE, ...


In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('PoseSLAM_iSQP_2D', '/home/user/Documents/SLAM_CODE_iSQP/PoseSLAM/PoseSLAM_iSQP_2D.m', 5)" style="font-weight:bold">PoseSLAM_iSQP_2D</a> (<a href="matlab: opentoline('/home/user/Documents/SLAM_CODE_iSQP/PoseSLAM/PoseSLAM_iSQP_2D.m',5,0)">line 5</a>)
[Res_iSQP, Fval_iSQP, PrecisionRecall, N_All, ProcessInfo] = PoseSLAM_iSQP_3D ( Zstate_EU, CovMatrix);


In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('MAIN_MAHANTTAN', '/home/user/Documents/SLAM_CODE_iSQP/PoseSLAM/MAIN_MAHANTTAN.m', 56)" style="font-weight:bold">MAIN_MAHANTTAN</a> (<a href="matlab: opentoline('/home/user/Documents/SLAM_CODE_iSQP/PoseSLAM/MAIN_MAHANTTAN.m',56,0)">line 56</a>)
[Res_iSQP, Obj_iSQP, PR_iSQP, N_All, ProcessInfo ] = PoseSLAM_iSQP_2D ( Zstate, CovMatrix);
} 
