clear all
close all
clc

num_outlier = 0;  % 10 % 20

choice = 3;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

DataFile = 'CSAIL_P';  % rec 3 % --- % 1, 2, 3   % 1 - same  %  2 - different  %  3 - s
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 CovChoice = 'Covariance_0.03';
% CovChoice = 'Covariance_0.05';
% CovChoice = 'CovarianceOdometry';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ResultFolder = ['Results/', DataFile, '_Noisy/Resutls', CovChoice, '/', ... 
    DataFile, '_', num2str(choice),'/Outlier_', num2str(num_outlier), '/'];
mkdir(ResultFolder);
delete([ResultFolder, 'ConsoleOutput.txt']);
diary([ResultFolder, 'ConsoleOutput.txt']);
diary on

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


Directory = 'NoisyDataSet/LocalMinima_Odom/';
NoiseLevelStr = 'OM_0.05_0.05_0.1_LC_0.02_0.02_0.02';
data_file = [Directory, DataFile, '_Noisy_', NoiseLevelStr, '_', num2str(choice), '.g2o']



OutlierDirectory = 'NoisyDataSet/Minia_Outliers/CSAIL_P_Noisy/';
outlier_data_file = [OutlierDirectory, CovChoice, '/', DataFile, '_Noisy_', NoiseLevelStr, '_', ... 
    num2str(choice), '_Outliers_', num2str(num_outlier), '.g2o']

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(num_outlier == 0)
    InputFile = data_file;
else
    InputFile = outlier_data_file;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( InputFile );
%
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
%
CovMatrix = inv(CovMatrixInv);


tic
[Res_iSQP, Obj_iSQP, PR_iSQP, N_All, ProcessInfo ] = PoseSLAM_iSQP_2D ( Zstate, CovMatrix);
PlotXstate2D_PoseSLAM( Res_iSQP, 'r.--' );
toc


diary off;
save([ResultFolder, 'Result_iSQP.mat'], 'Res_iSQP', 'Obj_iSQP', 'PR_iSQP', 'N_All', 'ProcessInfo');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%
clear all
close all
clc

num_outlier = 0;  % 10 % 20

choice = 3;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

DataFile = 'CSAIL_P';  % rec 3 % --- % 1, 2, 3   % 1 - same  %  2 - different  %  3 - s
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CovChoice = 'Covariance_0.03';
 CovChoice = 'Covariance_0.05';
% CovChoice = 'CovarianceOdometry';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ResultFolder = ['Results/', DataFile, '_Noisy/Resutls', CovChoice, '/', ... 
    DataFile, '_', num2str(choice),'/Outlier_', num2str(num_outlier), '/'];
mkdir(ResultFolder);
delete([ResultFolder, 'ConsoleOutput.txt']);
diary([ResultFolder, 'ConsoleOutput.txt']);
diary on

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


Directory = 'NoisyDataSet/LocalMinima_Odom/';
NoiseLevelStr = 'OM_0.05_0.05_0.1_LC_0.02_0.02_0.02';
data_file = [Directory, DataFile, '_Noisy_', NoiseLevelStr, '_', num2str(choice), '.g2o']



OutlierDirectory = 'NoisyDataSet/Minia_Outliers/CSAIL_P_Noisy/';
outlier_data_file = [OutlierDirectory, CovChoice, '/', DataFile, '_Noisy_', NoiseLevelStr, '_', ... 
    num2str(choice), '_Outliers_', num2str(num_outlier), '.g2o']

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(num_outlier == 0)
    InputFile = data_file;
else
    InputFile = outlier_data_file;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( InputFile );
%
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
%
CovMatrix = inv(CovMatrixInv);


tic
[Res_iSQP, Obj_iSQP, PR_iSQP, N_All, ProcessInfo ] = PoseSLAM_iSQP_2D ( Zstate, CovMatrix);
PlotXstate2D_PoseSLAM( Res_iSQP, 'r.--' );
toc


diary off;
save([ResultFolder, 'Result_iSQP.mat'], 'Res_iSQP', 'Obj_iSQP', 'PR_iSQP', 'N_All', 'ProcessInfo');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%


clear all
close all
clc

num_outlier = 0;  % 10 % 20

choice = 3;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

DataFile = 'CSAIL_P';  % rec 3 % --- % 1, 2, 3   % 1 - same  %  2 - different  %  3 - s
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CovChoice = 'Covariance_0.03';
% CovChoice = 'Covariance_0.05';
 CovChoice = 'CovarianceOdometry';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ResultFolder = ['Results/', DataFile, '_Noisy/Resutls', CovChoice, '/', ... 
    DataFile, '_', num2str(choice),'/Outlier_', num2str(num_outlier), '/'];
mkdir(ResultFolder);
delete([ResultFolder, 'ConsoleOutput.txt']);
diary([ResultFolder, 'ConsoleOutput.txt']);
diary on

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


Directory = 'NoisyDataSet/LocalMinima_Odom/';
NoiseLevelStr = 'OM_0.05_0.05_0.1_LC_0.02_0.02_0.02';
data_file = [Directory, DataFile, '_Noisy_', NoiseLevelStr, '_', num2str(choice), '.g2o']



OutlierDirectory = 'NoisyDataSet/Minia_Outliers/CSAIL_P_Noisy/';
outlier_data_file = [OutlierDirectory, CovChoice, '/', DataFile, '_Noisy_', NoiseLevelStr, '_', ... 
    num2str(choice), '_Outliers_', num2str(num_outlier), '.g2o']

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(num_outlier == 0)
    InputFile = data_file;
else
    InputFile = outlier_data_file;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( InputFile );
%
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
%
CovMatrix = inv(CovMatrixInv);


tic
[Res_iSQP, Obj_iSQP, PR_iSQP, N_All, ProcessInfo ] = PoseSLAM_iSQP_2D ( Zstate, CovMatrix);
PlotXstate2D_PoseSLAM( Res_iSQP, 'r.--' );
toc


diary off;
save([ResultFolder, 'Result_iSQP.mat'], 'Res_iSQP', 'Obj_iSQP', 'PR_iSQP', 'N_All', 'ProcessInfo');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




%%

clear all
close all
clc

num_outlier = 10;  % 10 % 20

choice = 3;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

DataFile = 'CSAIL_P';  % rec 3 % --- % 1, 2, 3   % 1 - same  %  2 - different  %  3 - s
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 CovChoice = 'Covariance_0.03';
% CovChoice = 'Covariance_0.05';
% CovChoice = 'CovarianceOdometry';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ResultFolder = ['Results/', DataFile, '_Noisy/Resutls', CovChoice, '/', ... 
    DataFile, '_', num2str(choice),'/Outlier_', num2str(num_outlier), '/'];
mkdir(ResultFolder);
delete([ResultFolder, 'ConsoleOutput.txt']);
diary([ResultFolder, 'ConsoleOutput.txt']);
diary on

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


Directory = 'NoisyDataSet/LocalMinima_Odom/';
NoiseLevelStr = 'OM_0.05_0.05_0.1_LC_0.02_0.02_0.02';
data_file = [Directory, DataFile, '_Noisy_', NoiseLevelStr, '_', num2str(choice), '.g2o']



OutlierDirectory = 'NoisyDataSet/Minia_Outliers/CSAIL_P_Noisy/';
outlier_data_file = [OutlierDirectory, CovChoice, '/', DataFile, '_Noisy_', NoiseLevelStr, '_', ... 
    num2str(choice), '_Outliers_', num2str(num_outlier), '.g2o']

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(num_outlier == 0)
    InputFile = data_file;
else
    InputFile = outlier_data_file;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( InputFile );
%
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
%
CovMatrix = inv(CovMatrixInv);


tic
[Res_iSQP, Obj_iSQP, PR_iSQP, N_All, ProcessInfo ] = PoseSLAM_iSQP_2D ( Zstate, CovMatrix);
PlotXstate2D_PoseSLAM( Res_iSQP, 'r.--' );
toc


diary off;
save([ResultFolder, 'Result_iSQP.mat'], 'Res_iSQP', 'Obj_iSQP', 'PR_iSQP', 'N_All', 'ProcessInfo');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%

clear all
close all
clc

num_outlier = 10;  % 10 % 20

choice = 3;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

DataFile = 'CSAIL_P';  % rec 3 % --- % 1, 2, 3   % 1 - same  %  2 - different  %  3 - s
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CovChoice = 'Covariance_0.03';
 CovChoice = 'Covariance_0.05';
% CovChoice = 'CovarianceOdometry';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ResultFolder = ['Results/', DataFile, '_Noisy/Resutls', CovChoice, '/', ... 
    DataFile, '_', num2str(choice),'/Outlier_', num2str(num_outlier), '/'];
mkdir(ResultFolder);
delete([ResultFolder, 'ConsoleOutput.txt']);
diary([ResultFolder, 'ConsoleOutput.txt']);
diary on

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


Directory = 'NoisyDataSet/LocalMinima_Odom/';
NoiseLevelStr = 'OM_0.05_0.05_0.1_LC_0.02_0.02_0.02';
data_file = [Directory, DataFile, '_Noisy_', NoiseLevelStr, '_', num2str(choice), '.g2o']



OutlierDirectory = 'NoisyDataSet/Minia_Outliers/CSAIL_P_Noisy/';
outlier_data_file = [OutlierDirectory, CovChoice, '/', DataFile, '_Noisy_', NoiseLevelStr, '_', ... 
    num2str(choice), '_Outliers_', num2str(num_outlier), '.g2o']

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(num_outlier == 0)
    InputFile = data_file;
else
    InputFile = outlier_data_file;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( InputFile );
%
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
%
CovMatrix = inv(CovMatrixInv);


tic
[Res_iSQP, Obj_iSQP, PR_iSQP, N_All, ProcessInfo ] = PoseSLAM_iSQP_2D ( Zstate, CovMatrix);
PlotXstate2D_PoseSLAM( Res_iSQP, 'r.--' );
toc


diary off;
save([ResultFolder, 'Result_iSQP.mat'], 'Res_iSQP', 'Obj_iSQP', 'PR_iSQP', 'N_All', 'ProcessInfo');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%


clear all
close all
clc

num_outlier = 10;  % 10 % 20

choice = 3;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

DataFile = 'CSAIL_P';  % rec 3 % --- % 1, 2, 3   % 1 - same  %  2 - different  %  3 - s
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CovChoice = 'Covariance_0.03';
% CovChoice = 'Covariance_0.05';
 CovChoice = 'CovarianceOdometry';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ResultFolder = ['Results/', DataFile, '_Noisy/Resutls', CovChoice, '/', ... 
    DataFile, '_', num2str(choice),'/Outlier_', num2str(num_outlier), '/'];
mkdir(ResultFolder);
delete([ResultFolder, 'ConsoleOutput.txt']);
diary([ResultFolder, 'ConsoleOutput.txt']);
diary on

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


Directory = 'NoisyDataSet/LocalMinima_Odom/';
NoiseLevelStr = 'OM_0.05_0.05_0.1_LC_0.02_0.02_0.02';
data_file = [Directory, DataFile, '_Noisy_', NoiseLevelStr, '_', num2str(choice), '.g2o']



OutlierDirectory = 'NoisyDataSet/Minia_Outliers/CSAIL_P_Noisy/';
outlier_data_file = [OutlierDirectory, CovChoice, '/', DataFile, '_Noisy_', NoiseLevelStr, '_', ... 
    num2str(choice), '_Outliers_', num2str(num_outlier), '.g2o']

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(num_outlier == 0)
    InputFile = data_file;
else
    InputFile = outlier_data_file;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( InputFile );
%
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
%
CovMatrix = inv(CovMatrixInv);


tic
[Res_iSQP, Obj_iSQP, PR_iSQP, N_All, ProcessInfo ] = PoseSLAM_iSQP_2D ( Zstate, CovMatrix);
PlotXstate2D_PoseSLAM( Res_iSQP, 'r.--' );
toc


diary off;
save([ResultFolder, 'Result_iSQP.mat'], 'Res_iSQP', 'Obj_iSQP', 'PR_iSQP', 'N_All', 'ProcessInfo');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%








