clear all
close all
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(0)
    choice = 3;
    DataFile = 'CSAIL_P';  % rec 3 % --- % 1, 2, 3   % 1 - same  %  2 - different  %  3 - slow convergence
    Directory = 'NoisyDataSet/LocalMinima_Odom/';
    NoiseLevelStr = 'OM_0.05_0.05_0.1_LC_0.02_0.02_0.02';
    InputFile = [Directory, DataFile, '_Noisy_', NoiseLevelStr, '_', num2str(choice), '.g2o']
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(0)
    DataFile = 'MITb';
    InputFile = ['DataSet/', DataFile, '.g2o'];
end
if(0)
    DataFile = 'intel';
    InputFile = ['DataSet/', DataFile, '.g2o'];
end
if(1)
    DataFile = 'M3500';
    InputFile = ['DataSet/', DataFile, '.g2o'];
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[ Zstate, CovMatrixInv ] = ConvertG2OFormatToZstatePS2D( InputFile );
%
[ Zstate, CovMatrixInv ] = ArrangeDataSequence( Zstate, CovMatrixInv );
[ Zstate, CovMatrixInv ] = FilterOutDuplicateEdges( Zstate, CovMatrixInv );
%
CovMatrix = inv(CovMatrixInv);
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[ Nodes, Edges, LpEdges, LpConstraints ] = ObtainDatasetInfo( Zstate, '2D' )

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mkdir(['Results/DatasetInfo/']);

save(['Results/DatasetInfo/', DataFile], 'Nodes', 'Edges', 'LpEdges', 'LpConstraints');


