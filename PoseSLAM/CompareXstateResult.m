% This function is used to compare two results preserved in PoseSLAM Xstate
% The difference is returned in ErrorXstate
function [ RMSE, ErrorXstate ] = CompareXstateResult( Result1, Result2 )

    if(size(Result1,1) ~= size(Result2,1))
        disp('data error in CompareXstateResult');
        return;
    end

    if(Result1(1,2) == Result1(6,2))
        StepSize1 = 6;
    elseif(Result1(1,2) == Result1(3,2))
        StepSize1 = 3;
    else
        disp('data error in CompareXstateResult');
        return;
    end

    if(Result2(1,2) == Result2(6,2))
        StepSize2 = 6;
    elseif(Result2(1,2) == Result2(3,2))
        StepSize2 = 3;
    else
        disp('data error in CompareXstateResult');
        return;
    end
    
    if(StepSize1 ~= StepSize2)
        disp('data error in CompareXstateResult');
        return;
    end
    
    StepSize = StepSize1;
    
    DataDim = size(Result1,1);
    
    count = 1;
    
    ErrorXstate = [zeros(DataDim, 1), Result1(:,2)];
    
    while(count <= DataDim)
        
        if(StepSize == 6)  % 3D case
            
            ErrorXstate([count : (count+2)], 1) = Result1([count : (count+2)], 1) - Result2([count : (count+2)], 1);
            
            ErrorXstate([(count+3) : (count+5)], 1) = Log_R2a(Exp_a2R(Result2([(count+3) : (count+5)], 1)).' * Exp_a2R(Result1([(count+3) : (count+5)], 1)));
            
            count = count + 6;
        end
        
        
        if(StepSize == 3)  % 2D case
            
            ErrorXstate([count : (count+1)], 1) = Result1([count : (count+1)], 1) - Result2([count : (count+1)], 1);
           
            ErrorXstate([count+2], 1) = R2a( R(Result2([count+2], 1)).' * R(Result1([count+2], 1)));

            count = count + 3;
        end
  
    end
    
    RMSE = sqrt(ErrorXstate(:,1).'*ErrorXstate(:,1)/DataDim);  

end


% This function is used to transform a vector into a rotation matrix by exponential mapping
function [ RotationMatrixExp ] = Exp_a2R( X_vec )

threshold = 1e-10;

theta = norm(X_vec);

if (theta < threshold)
    RotationMatrixExp = eye(3);
else
    RotationMatrixExp = eye(3) + SkewSem(X_vec)*sin(theta)/theta + ...
        (SkewSem(X_vec)^2)*(1 - cos(theta))/(theta^2);    
end

end

% transform a 3-dimensional vector into a skew-semmetric marix
function [ SkewSemmetricMatrix ] = SkewSem( X_vec )
SkewSemmetricMatrix = [ 0          -X_vec(3)     X_vec(2);
                        X_vec(3)    0           -X_vec(1);
                       -X_vec(2)    X_vec(1)     0       ;];
end

% This function gives the Logarithm mapping to tranform a rotation R to the minimal representation Theta
% The minimal representation is described by axis angle representation
% Theta is a vertical vector
function [ Theta ] = Log_R2a( R )
threshold = 1e-10;
angle = acos( (trace(R) -1 )/2 );

if (abs(angle) <threshold ) % 0
    axis = [1; 0; 0;];
elseif(abs(angle - pi) <threshold )  % pi
   % disp('reach pi');
    axis = - [sign(R(2,3))*sqrt((1+R(1,1))/2); sign(R(1,3))*sqrt((1+R(2,2))/2); sign(R(1,2))*sqrt((1+R(3,3))/2) ];
else
    axis = [ R(3,2)-R(2,3);
             R(1,3)-R(3,1);
             R(2,1)-R(1,2); ];
    axis = axis/(2*sin(angle));
end

Theta = angle*axis;
[ Theta ] = WrapX( Theta );
end

% Wrap a arbitrary axis angle rotation into a rotation at interval [0, pi]
function [ X_v ] = WrapX( X_vec )
if (norm(X_vec)== 0)
    X_v = X_vec;
    return; 
end
angle = norm(X_vec);
axis  = X_vec/angle;
while (angle >= 2*pi)
    angle = angle - 2*pi;
end
while ( angle > pi)
    angle = 2*pi - angle;
    axis = -axis;  
end
if(pi-angle<1e-10)
   if(axis(3)<0)
       axis = -axis;
   end    
end

X_v = angle*axis;
end

% Get rotation matrix A with respect to angle phi
function [A] = R(phi)
A=[cos(phi),-sin(phi);sin(phi),cos(phi)];
end

% Get angle phi from rotation matrix R
function [phi] = R2a(R)
phi = atan2(R(2,1), R(1,1));
end