%%%This is a Gauss-Newton for processing 2 feature and 3 positions SLAM
%%%problem
clc;
clear all;
disp('Observation generation process');
num_P = 3;    %Number of positions
num_F = 2;    %Number of features

Xr0 = [0;0];%position0
Xr1 = [2;1];%position1
Xr2 = [3;2];%position1
Fi0 = 0;
Fi1 = pi/6;
Fi2 = pi/3;
Rr0 = [cos(Fi0),-sin(Fi0);sin(Fi0),cos(Fi0)]; %Relative angle between position0 and postion1
Rr1 = [cos(Fi1),-sin(Fi1);sin(Fi1),cos(Fi1)]; %Relative angle between position0 and postion1
Rr2 = [cos(Fi2),-sin(Fi2);sin(Fi2),cos(Fi2)]; %Relative angle between position0 and postion1
Xf1 = [1;3];
Xf2 = [1;4];

realdata_P = [Xr0;Xr1;Xr2];  %realdata of positions
realdata_Fi = [Fi1;Fi2];     %realdata of features
realdata_Rr = [Rr0;Rr1];     %realdata of rotations
realdata_Xf = [Xf1;Xf2];     %realdata of features

%No noises
disp('Odormetry without noises');
O = zeros(3*(num_P - 1),1);
O = [Rr0' * (Xr1 - Xr0);Fi1;Rr1' * (Xr2-Xr1);(Fi2-Fi1)];

% H_O1_X = [Rr0' * Xr1;Fi1]; %Corresponding function of ordometry(P1 to P2)
% H_O2_X = [Rr1' * Xr2;Fi2]; %Corresponding function of ordometry(P1 to P2)
% O1 = H_O1_X %Odometry between between position0 to p1
% O2 = H_O2_X %Odometry between between position1 to p2

disp('Observation without noises');
Z = zeros(2*num_F*num_P,1);
Z(1:4,1) = [Xf1;Xf2];    %position1 observation
Z(5:8,1) = [Rr1' * (Xf1 - Xr1);Rr1' * (Xf2 - Xr1)]; %position2 observation
Z(9:12,1) = [Rr2' * (Xf1 - Xr2);Rr2' * (Xf2 - Xr2)]; %position3 observation
% H_Z01_X = Xf1; %Corresponding function of observation0 to feature1
% H_Z02_X = Xf2; %Corresponding function of observation0 to feature2

% disp('Observation from position1 without noises');
% H_Z1_X = Rr0' * (Xf1 - Xr1); %Corresponding function of observation1
% Z_1 = H_Z1_X %%Ordometry from position1
bool_noise = input('Do you want to add noise to these observations?(1 for Yes/2 for No):');
while ((bool_noise ~= 1)&&(bool_noise ~= 2))
    bool_noise = input('Kidding? Type again. (1 for Yes/N2 for No');
end
if (bool_noise == 1)
    [m, n] = size(O);
    zero1 = zeros(m,n);
    O = O + normrnd(zero1, sqrt(0.01));
    [m, n] = size(Z);
    zero1 = zeros(m,n);
    Z = Z + normrnd(zero1, sqrt(0.01));
end

[num_Z,tmp] = size(Z);
[num_O,tmp] = size(O);

k=num_O/3;
n_ob=num_Z/(k+1);
P = n_ob+3;
zstate=zeros(k*7+n_ob,4);
for i=0:k
    zstate(i*P+1:i*P+n_ob,1) = Z(i*n_ob+1:i*n_ob+4,1);
    
    zstate(i*P+1:i*P+n_ob,2) = 2;
    
    for j=1:n_ob/2
        zstate(i*P+j*n_ob/2-1:i*P+j*n_ob/2,3) = j;
    end
    
    zstate(i*P+1:i*P+n_ob,4) = i;
    
    if i<k
        zstate(i*P+n_ob+1:i*P+n_ob+3,1) = O(i*3+1:i*3+3,1);
        zstate(i*P+n_ob+1:i*P+n_ob+3,2) = 1;
        zstate(i*P+n_ob+1:i*P+n_ob+3,3) = i+1;
        zstate(i*P+n_ob+1:i*P+n_ob+3,4) = i;
    end
end

save Zstate_TestData zstate;

%%%%
disp('Observation processed and saved.');