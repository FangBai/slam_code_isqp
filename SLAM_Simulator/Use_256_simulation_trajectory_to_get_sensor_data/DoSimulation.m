%
% function: (1) add noise to true odometry
%           (2) produce observation -- this is based on the true 
%      location of the current robot location
%     and beacons location. They are in the map coordinate system.
%

function DoSimulation(loop)


global Params;
global Store;
global Truth;


%disp(' *** entering DoSimulation');

%% get odometry data

xr_previous = Truth.robot(1,loop);
yr_previous = Truth.robot(2,loop);
phir_previous = Truth.robot(3,loop);

xr_current = Truth.robot(1,loop+1);
yr_current = Truth.robot(2,loop+1);
phir_current = Truth.robot(3,loop+1);

% relative robot pose -- odometry without noises
xr_local =(xr_current - xr_previous)*cos(phir_previous) +  (yr_current- yr_previous )*sin(phir_previous);
yr_local= -(xr_current  - xr_previous)*sin(phir_previous) +  (yr_current- yr_previous )*cos(phir_previous);
phir_local= phir_current- phir_previous ;

%randn for dx with boundary
rand_dx = Params.sig_dx*randn;
if rand_dx>Params.sig_dx*Params.SensorNoiseLimit
    rand_dx = Params.sig_dx*Params.SensorNoiseLimit;
elseif rand_dx<-Params.sig_dx*Params.SensorNoiseLimit
    rand_dx = -Params.sig_dx*Params.SensorNoiseLimit;
end
%randn for dy with boundary
rand_dy = Params.sig_dy*randn;
if rand_dy>Params.sig_dy*Params.SensorNoiseLimit
    rand_dy = Params.sig_dy*Params.SensorNoiseLimit;
elseif rand_dy<-Params.sig_dy*Params.SensorNoiseLimit
    rand_dy = -Params.sig_dy*Params.SensorNoiseLimit;
end

%randn for phi with boundary
rand_phi=Params.sig_phi*randn;
if rand_phi>Params.sig_phi*Params.SensorNoiseLimit
    rand_phi = Params.sig_phi*Params.SensorNoiseLimit;
elseif rand_phi<-Params.sig_phi*Params.SensorNoiseLimit
    rand_phi = -Params.sig_phi*Params.SensorNoiseLimit;
end

%add randn noise in xr,yr,phi
xr=xr_local+rand_dx;
yr=yr_local+rand_dy;
phir=phir_local+rand_phi;
%% store the odometry data
Store.Zstate = [Store.Zstate;
    xr, 1, loop, loop-1;
    yr, 1, loop, loop-1;
    phir, 1, loop, loop-1];

%% get observation data
% truth robot position come from stored data
Truth.X = Truth.robot(1,loop+1);
Truth.Y = Truth.robot(2,loop+1);
Truth.Phi = Truth.robot(3,loop+1);
% decide which beacons can been seen by robot
% and produce measurements

for i = 1:size(Truth.Beacons,1)
    range =  sqrt(   (Truth.Beacons(i,2)-Truth.X)^2  + (Truth.Beacons(i,3)-Truth.Y)^2  );
    
    if(range<= Params.SensorMaxDist &&  range>= Params.SensorMinDist) %
        % this beacon with index i can be seen by robot
        
        xf_global = Truth.Beacons(i,2);
        yf_global = Truth.Beacons(i,3);
        xf_local =(xf_global - Truth.X)*cos(Truth.Phi) +  (yf_global- Truth.Y)*sin(Truth.Phi);
        yf_local= -(xf_global  - Truth.X)*sin(Truth.Phi) +  (yf_global- Truth.Y)*cos(Truth.Phi);
        
        
        %randn for dx with boundary
        rand_dx = Params.sig_dx*randn;
        if rand_dx>Params.sig_dx*Params.SensorNoiseLimit
            rand_dx = Params.sig_dx*Params.SensorNoiseLimit;
        elseif rand_dx<-Params.sig_dx*Params.SensorNoiseLimit
            rand_dx = -Params.sig_dx*Params.SensorNoiseLimit;
        end
        %randn for dy with boundary
        rand_dy = Params.sig_dy*randn;
        if rand_dy>Params.sig_dy*Params.SensorNoiseLimit
            rand_dy = Params.sig_dy*Params.SensorNoiseLimit;
        elseif rand_dy<-Params.sig_dy*Params.SensorNoiseLimit
            rand_dy = -Params.sig_dy*Params.SensorNoiseLimit;
        end
        
        
        %add randn noise in xf and yf
        xf_local=xf_local+rand_dx;
        yf_local=yf_local+rand_dy;
        
        %% store observation data
        Store.Zstate = [Store.Zstate;
            xf_local, 2, i, loop;
            yf_local, 2, i, loop];
        
        
    end
end

return;

