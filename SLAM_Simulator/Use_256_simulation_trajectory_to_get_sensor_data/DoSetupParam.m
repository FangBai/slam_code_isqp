%
% function: set initial value for all stuff
%
% input:
%
%
%

function DoSetupParam

global Params;
global Store;
global Truth;


%randn('state',0)
randn('state',sum(100*clock))


%%% sensor range
Params.SensorMaxDist = 6;          % how far the sensor can see, original
Params.SensorMinDist = 0.1;  % added by Shoudong, remove the observation to very close beacons

% %%%%% 256 loop data small noise for Adizul
% Params.sigr = 0.01; % range 
% Params.sigtheta = (1*pi/180); % bearing


%% odometry and observation noise level
Params.sig_dx = 0.1*10;
Params.sig_dy = 0.1*10;
Params.sig_phi = 1/180*pi*10;

Params.SensorNoiseLimit = 2; 


%%% store obs data
Store.Zstate = [];    % store range and bearing, [beacon index, range, bearing]. each 3 columns are of one time
                            % here, estimation of the beacon in State is not stored 
return; 




