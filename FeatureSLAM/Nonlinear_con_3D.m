% This function describe nonlinear constraints CEQ(X) = 0
% X   the variable/argument of constraints given in SE(3) space
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% M_s    the cell structure got from Get_triangle_3D
%      M_s reflects indices for odometry and observation in variables
%      described in SE(3) group/space
%      M_s{i}: the i-th triangle
%      M_s{i}: num_odmometry od1 od2..odn ob1 ob2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ CEQ ] = Nonlinear_con_3D( X, M_s )

num_trian = size(M_s,2);
cur_trian = 1;

while(cur_trian<=num_trian)
    
num_odom = M_s{cur_trian}(1);
%%%%%%%%% odometry 1...k
for count=1:num_odom
    od(count)= M_s{cur_trian}(count+1);
end
%%%%%%%%%observation 1
ob1=M_s{cur_trian}(num_odom+2);
%%%%%%%%% observation 2
ob2=M_s{cur_trian}(num_odom+3);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cur_odom = num_odom;

R_cur_odom = [X(od(cur_odom)+3), X(od(cur_odom)+4), X(od(cur_odom)+5);
              X(od(cur_odom)+6), X(od(cur_odom)+7), X(od(cur_odom)+8);
              X(od(cur_odom)+9), X(od(cur_odom)+10), X(od(cur_odom)+11);];

F = [X(od(cur_odom));X(od(cur_odom)+1);X(od(cur_odom)+2)] + ...
       R_cur_odom ...
       *[X(ob2);X(ob2+1);X(ob2+2)];

while( cur_odom~=1)
    cur_odom = cur_odom -1;
    
    R_cur_odom = [X(od(cur_odom)+3), X(od(cur_odom)+4), X(od(cur_odom)+5);
              X(od(cur_odom)+6), X(od(cur_odom)+7), X(od(cur_odom)+8);
              X(od(cur_odom)+9), X(od(cur_odom)+10), X(od(cur_odom)+11);];

    F = [X(od(cur_odom));X(od(cur_odom)+1);X(od(cur_odom)+2)] + ...
       R_cur_odom ... 
       *F;
end

CEQ([3*cur_trian-2;3*cur_trian-1;3*cur_trian],1) = F - [X(ob1);X(ob1+1);X(ob1+2)];

cur_trian = cur_trian + 1;

end

% CEQ

end

