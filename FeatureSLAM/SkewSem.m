% transform a 3-dimensional vector into a skew-semmetric marix
function [ SkewSemmetricMatrix ] = SkewSem( X_vec )
SkewSemmetricMatrix = [ 0          -X_vec(3)     X_vec(2);
                        X_vec(3)    0           -X_vec(1);
                       -X_vec(2)    X_vec(1)     0       ;];
end

