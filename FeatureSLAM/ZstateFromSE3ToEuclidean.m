% This function is used to transform the data matrix Zstate from SE(3)
% space to Euclidean space.
function [ Zstate_EU, Variable_Indices ] = ZstateFromSE3ToEuclidean( Zstate_SE )

SEcount = 1;
EUcount = 1;
Vcount = 1;
DataDim = size(Zstate_SE,1);

while (SEcount < DataDim)   
    if(Zstate_SE(SEcount,2) == 1)
    
        Zstate_EU([EUcount, EUcount+1, EUcount+2],:) = Zstate_SE([SEcount, SEcount+1, SEcount+2],:);   

        R_cur = [Zstate_SE([SEcount+3, SEcount+6, SEcount+9],1) Zstate_SE([SEcount+4, SEcount+7, SEcount+10],1) Zstate_SE([SEcount+5, SEcount+8, SEcount+11],1)];
        
        Zstate_EU([EUcount+3, EUcount+4, EUcount+5],1) = Log_R2a( R_cur );
        Zstate_EU([EUcount+3, EUcount+4, EUcount+5],[2:4]) = Zstate_SE([SEcount, SEcount+1, SEcount+2],[2:4]); 
        
        Variable_Indices(Vcount,1) = 1;
        Vcount= Vcount + 1;
        SEcount = SEcount + 12;
        EUcount = EUcount + 6;
    elseif(Zstate_SE(SEcount,2) == 2)

        Zstate_EU([EUcount, EUcount+1, EUcount+2],:) = Zstate_SE([SEcount, SEcount+1, SEcount+2],:);       
   
        Variable_Indices(Vcount,1) = 2;
        Vcount= Vcount + 1;
        SEcount = SEcount + 3;
        EUcount = EUcount + 3;
    else
        disp('data error!');
        return;
    end

end

end
