function [Res_SQP, Fval_SQP, Iteration_count] = FeatureSLAM_SQP_3D ( Zstate_EU, CovMatrixInv)

CovMatrix = inv(CovMatrixInv);

[ Data_SE, Variable_Indices,  M_e,  M_s ] = ExtractDataSetInfo( Zstate_EU );

X0_SE = Data_SE;

[ X_value_SE, Fval_SQP, Iteration_count ] = My_method_solve_3D( X0_SE, Data_SE, CovMatrix, M_e, M_s, Variable_Indices, 'None');

% get Xstate form of solution
[ Zstate_SE ] = ZstateFromEuclideanToSE3( Zstate_EU );
Zstate_SE(:,1) = X_value_SE;
Res_SQP  = FuncCreateXstateFromZstate_SE_3D(Zstate_SE);    

end