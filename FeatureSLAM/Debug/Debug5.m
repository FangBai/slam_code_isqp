clc
clear all
close all

load('Zstate_Simu_256.mat')
[ Xstate2dold ]  = FuncCreateXstateFromZstate_2D(Zstate);
[ Xstate2d ]  = FuncCreateXstateFromZstate_2D(Zstate);
max( Xstate2dold  -  Xstate2d)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Zstate_EU ] = TransDataFrom2DTo3D( Zstate );  %
[ err ] = CompareXstate2DWith3D( Zstate, Zstate_EU );
max(err)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Xstate3d ]  = FuncCreateXstateFromZstate_EU_3D( Zstate_EU );  %
[ err ] = CompareXstate2DWith3D( Xstate2d, Xstate3d );
max(err)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Zstate_SE, Variable_Indices  ] = ZstateFromEuclideanToSE3( Zstate_EU );
[ Zstate_EUNEW ] = ZstateFromSE3ToEuclidean( Zstate_SE );
max([Zstate_EU - Zstate_EUNEW])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Xstate_EU ]  = FuncCreateXstateFromZstate_SE_3D( Zstate_SE );
max(Xstate3d - Xstate_EU)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
X0_SE = Zstate_SE(:,1);
X_EU = zeros(size(Zstate_EU,1),1);
[ X_SE ] = Update_SEbyEU_3D(X0_SE, X_EU, Variable_Indices );
Zstate_SE(:,1) = X_SE;
[ Xstate_EU ]  = FuncCreateXstateFromZstate_SE_3D( Zstate_SE );
max(Xstate3d - Xstate_EU)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ X_EU ] = Update_EUbySE_3D(X0_SE, X0_SE, Variable_Indices );
max(X_EU)
Zstate_EU(:,1) = X_EU;
max(Zstate_EU)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Zstate_EU(:,1) = zeros(size(Zstate,1),1);
[ Zstate_SE, Variable_Indices  ] = ZstateFromEuclideanToSE3( Zstate_EU )
X0_SE = Zstate_SE(:,1); % 0 SE3













