clear all
close all
clc

u = -[1/sqrt(3); 1/sqrt(3); 1/sqrt(3)];
theta = 17/3*pi;
X = u*theta
Y = -u *(2*pi -theta)
disp('after wrap minimal representation');
WrapX(X)
Z = WrapX(Y)

Exp = Exp_a2R(X)
q = aa2q(X);
Rq = R_q(q)


Exp = Exp_a2R(Y)
q = aa2q(Y);
Rq = R_q(q)

disp('rotation after wrap');
Exp = Exp_a2R(Z)
q = aa2q(Z);
Rq = R_q(q)

disp('2 Quaternions for same rotation');
q1 = aa2q(X)
q2 = aa2q(Y)
Rq1 = R_q(q1)
Rq2 = R_q(q2)

u = -[0/sqrt(5); 4/sqrt(5); 3/sqrt(5)];
theta = 17/3*pi;
X = u*theta
R = Exp_a2R(X)
WrapX = WrapX(X)
Log_R = Log_R2a(R)
R= eye(3)
Log_R = Log_R2a(R)

X_SE1 =[1;2;3;4;5;6;7;8;9;10;11;12];
SEcount = 1;
Rota_SE1 = [X_SE1([SEcount+3, SEcount+6, SEcount+9]) X_SE1([SEcount+4, SEcount+7, SEcount+10]) X_SE1([SEcount+5, SEcount+8, SEcount+11])]
 






