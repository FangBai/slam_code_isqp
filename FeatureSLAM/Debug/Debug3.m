clear all
close all
clc

load Zstate_DLR

[ N1, M1 ] = Get_triangle( Zstate );
[ M2, N2 ] = GetTriangle3D( Zstate );

max(N1 - N2)

for i = 1: max(size(M1))
e(i) = max(M1{i} - M2{i});
end
max(e(i))