% This function is used to convert AngleAxis representation to EulerAngles
% EulerAngle [heading; attitude; bank;] or [yaw; pitch; roll;]
% EulerAngle Rotated by [ y-axis, z-axis, x-axis ]
function [ EulerAngle ] = AnlgeAxis2EulerAngle( AngleAxis )

threshold = 1e-10;

angle = norm(AngleAxis);

if (angle < threshold)
    axis = [1; 0; 0;];
else
    axis = AngleAxis/angle;
end

x = axis(1);
y = axis(2);
z = axis(3);

pitch= asin(x*y*(1 - cos(angle)) + z*sin(angle));

if ( abs(pitch - pi/2) < threshold )
    yaw = 2*atan2(x*sin(angle/2), cos(angle/2));
    roll = 0;
elseif ( abs(pitch + pi/2) < threshold )
    yaw = -2*atan2(x*sin(angle/2), cos(angle/2));
    roll = 0;
else
    yaw = atan2(y*sin(angle)- x*z*(1 - cos(angle)) ,1 - (y^2 + z^2 )*(1 - cos(angle)));
    roll = atan2(x*sin(angle)-y*z*(1 - cos(angle)) ,1 - (x^2 + z^2)*(1 - cos(angle)));   
end

EulerAngle = [yaw; pitch; roll;];

end
