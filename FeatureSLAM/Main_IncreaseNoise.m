clear all
close all
clc

NoiseLevelOdom = [0.05, 0.05, 0.10];

NoiseLevelObser = [0.05, 0.05];

FileName = 'DataSet/DLR.g2o';

FileNameNoisy = [ 'DataSet/DLR_Noisy_OM_', ...
                   num2str(NoiseLevelOdom(1)), '_', ...
                   num2str(NoiseLevelOdom(2)), '_', ...
                   num2str(NoiseLevelOdom(3)), '_OB_', ... 
                   num2str(NoiseLevelObser(1)), '_', ...
                   num2str(NoiseLevelObser(2)), ...           
                  '.g2o' ];


load('DataSet/DLR.mat', 'Zstate', 'CovMatrixInv', 'GroundTruth');

% [ Xstate0 ]  = FuncCreateXstateFromZstate_2D( Zstate );
% 
% PlotXstate2D( Xstate0, 'r.:', 'b.');
% 
% [pose_index, feature_index] = CreateG2OFormatByZstateFeature2D (FileName, Zstate, CovMatrixInv, Xstate0 );

[ Zstate, CovMatrixInv ] = AddNoiseToZstateFeatureSLAM_2D( Zstate, CovMatrixInv, GroundTruth, NoiseLevelOdom, NoiseLevelObser, ' ' );

[ Xstate0 ]  = FuncCreateXstateFromZstate_2D( Zstate );

PlotXstate2D( Xstate0, 'r.:', 'b.');

[pose_index, feature_index] = CreateG2OFormatByZstateFeature2D (FileNameNoisy , Zstate, CovMatrixInv, Xstate0 );

% DCS tuning paramter 2



