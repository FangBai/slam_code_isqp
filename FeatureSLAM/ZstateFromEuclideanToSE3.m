% This function is used to transform the data matrix Zstate from Euclidean
% space to SE(3) space.
function [ Zstate_SE, Variable_Indices ] = ZstateFromEuclideanToSE3( Zstate_EU )

EUcount = 1;
SEcount = 1;
Vcount = 1;
DataDim = size(Zstate_EU,1);

while (EUcount < DataDim)   
    if(Zstate_EU(EUcount,2) == 1)
        
        Zstate_SE([SEcount, SEcount+1, SEcount+2],:) = Zstate_EU([EUcount, EUcount+1, EUcount+2],:);
        
        R_cur = Exp_a2R(Zstate_EU([EUcount+3, EUcount+4, EUcount+5],1) );
        Zstate_SE([SEcount+3 : SEcount+11],1) = [ R_cur(1,1); R_cur(1,2); R_cur(1,3);
                                              R_cur(2,1); R_cur(2,2); R_cur(2,3);
                                              R_cur(3,1); R_cur(3,2); R_cur(3,3); ];
        Zstate_SE([SEcount+3 : SEcount+5],[2:4]) = Zstate_EU([EUcount, EUcount+1, EUcount+2],[2:4]);
        Zstate_SE([SEcount+6 : SEcount+8],[2:4]) = Zstate_EU([EUcount, EUcount+1, EUcount+2],[2:4]);
        Zstate_SE([SEcount+9 : SEcount+11],[2:4]) = Zstate_EU([EUcount, EUcount+1, EUcount+2],[2:4]);
        
        Variable_Indices(Vcount,1) = 1;
        Vcount= Vcount + 1;
        EUcount = EUcount + 6;
        SEcount = SEcount + 12;
    elseif(Zstate_EU(EUcount,2) == 2)
          
        Zstate_SE([SEcount, SEcount+1, SEcount+2],:) = Zstate_EU([EUcount, EUcount+1, EUcount+2],:);
        
        Variable_Indices(Vcount,1) = 2;
        Vcount= Vcount + 1;
        EUcount = EUcount + 3;
        SEcount = SEcount + 3;
    else
        disp('data error!');
        return;
    end

end


end

