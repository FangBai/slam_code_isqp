% This function is used to plot Xstate given in Euclidean space
% pose_str control the line and color used to plot the pose
% feature_str control the line and color used to plot the feature
function [ h1, h2 ] = PlotXstate3D( Xstate_EU, pose_str, feature_str )

count = 1;
X_pose = [0];
Y_pose = [0];
Z_pose = [0];
X_feature = [];
Y_feature = [];
Z_feature = [];
% extract pose and feature coordinate
% pose coordinates are saved in X_pose, Y_pose
% feature coordiantes are saved in X_feature, Y_feature
while (count<=size(Xstate_EU,1))
    if(Xstate_EU(count,2)==1)
        X_pose = [X_pose Xstate_EU(count,1)];
        Y_pose = [Y_pose Xstate_EU(count+1,1)];
        Z_pose = [Z_pose Xstate_EU(count+2,1)];
     
        count = count + 6;     
    elseif(Xstate_EU(count,2)==2)     
        X_feature = [X_feature Xstate_EU(count,1)];
        Y_feature = [Y_feature Xstate_EU(count+1,1)];
        Z_feature = [Z_feature Xstate_EU(count+2,1)];    
        count = count + 3;
    end
end

figure;
% plot pose and feature
hold on
h1 = plot3(X_pose,Y_pose,Z_pose, pose_str);
h2 = plot3(X_feature,Y_feature,Z_feature, feature_str);
hold off
end

