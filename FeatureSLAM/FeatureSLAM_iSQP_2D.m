function [ Res_iSQP_2D, Fval_iSQP ] = FeatureSLAM_iSQP_2D( Zstate_2D, CovMatrixInv_2D )

[ Zstate_3D, CovMatrixInv_3D ] = TransDataFrom2DTo3D( Zstate_2D, CovMatrixInv_2D );

[Res_iSQP, Fval_iSQP] = FeatureSLAM_iSQP_3D ( Zstate_3D, CovMatrixInv_3D);

[Res_iSQP_2D] = TransformXstateFrom3DTo2D (Res_iSQP);

end

%This function is used to transform 2D dataset to 3D
function [ Zstate_3D, CovMatrixInv_3D ] = TransDataFrom2DTo3D( Zstate_2D, CovMatrixInv_2D )

count2D = 1;
count3D = 1;

CovMatrixInv_3D = sparse(0,0);

DataDim = size(Zstate_2D,1);

while (count2D < DataDim)
    
   if( Zstate_2D(count2D,2) == 1)
       
       Zstate_3D([count3D, count3D+1],:) = Zstate_2D([count2D, count2D+1],:);
       
       Zstate_3D([count3D+2 : count3D+4],1) = [0; 0; 0;];
       Zstate_3D([count3D+2 : count3D+4],[2,3,4]) = Zstate_2D([count2D, count2D+1, count2D+2],[2,3,4]);
       
       Zstate_3D([count3D+5],:) = Zstate_2D([count2D+2],:);
       
       CovMatrixInv_3D([count3D, count3D+1, count3D+5], [count3D, count3D+1, count3D+5]) = ...
           CovMatrixInv_2D([count2D, count2D+1, count2D+2], [count2D, count2D+1, count2D+2]);
       
       CovMatrixInv_3D([count3D+2, count3D+3, count3D+4], [count3D+2, count3D+3, count3D+4]) = speye(3);
       
       count3D = count3D + 6;
       count2D = count2D + 3;
       
   elseif( Zstate_2D(count2D,2) == 2)       
       
       Zstate_3D([count3D, count3D+1],:) = Zstate_2D([count2D, count2D+1],:);
       
       Zstate_3D([count3D+2],1) = [0];
       Zstate_3D([count3D+2],[2,3,4]) = Zstate_2D([count2D],[2,3,4]);

       CovMatrixInv_3D([count3D, count3D+1], [count3D, count3D+1]) = ...
           CovMatrixInv_2D([count2D, count2D+1], [count2D, count2D+1]);
       
       CovMatrixInv_3D([count3D+2], [count3D+2]) = 1;       
       
       count3D = count3D + 3;
       count2D = count2D + 2;
       
   else
       disp('data error!');
       return;
   end
end
end

%%%%%%%%%%%%%%%%%%%
% This function is used to tranform feature based SLAM 3D solution to 2D
function [Xstate_2D] = TransformXstateFrom3DTo2D (Xstate_3D)

count3D = 1;
count2D = 1;

DataDim = size(Xstate_3D, 1);

while (count3D < DataDim)
    
    if(Xstate_3D(count3D, 2) == 1)
        
        Xstate_2D([count2D, count2D+1, count2D+2], [1,2,3]) = ...
            Xstate_3D([count3D, count3D+1, count3D+5], [1,2,3]);
        
        count3D = count3D + 6;
        count2D = count2D + 3;
        
    elseif(Xstate_3D(count3D, 2) == 2)
        
        Xstate_2D([count2D, count2D+1], [1,2,3]) = ...
            Xstate_3D([count3D, count3D+1], [1,2,3]);
        
        count3D = count3D + 3;
        count2D = count2D + 2;
        
    end
    
end

end