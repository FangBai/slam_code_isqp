function [Zstate, CovMatrixInv] = AddOutliersPoseSLAM( Zstate, CovMatrixInv, NumRandomOutliers, NumInitialOutliers )

StepSize = size(find((Zstate(:,2)==2) & (Zstate(:,3)==1)), 1);
%
if(StepSize == 6)
    DataSetType = '3D';  % 3D
elseif(StepSize == 3)
    DataSetType = '2D';
elseif(StepSize~=6 && StepSize~=3)
    disp('*********************data error!************************')
    return;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
CovStart = find(Zstate(:,2)- Zstate(:,3) == 1, 1, 'first');     % odometry
% CovStart = find(Zstate(:,2)- Zstate(:,3) > 1, 1, 'first');    % loop-closure
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

CovarianceInv = CovMatrixInv([CovStart : CovStart+StepSize-1], [CovStart : CovStart+StepSize-1]);

if(strcmp(DataSetType, '3D'))
    
    [Res] = PoseSLAM_G2O_GN_3D( Zstate, CovMatrixInv, 'Cauchy', 1.0, 200);
    
    Xstate0 = FuncCreateXstateFromZstate_EU_3D_PoseSLAM( Zstate );
    maximum_index = max(Zstate(:,2));
    
    Dim = size(CovMatrixInv, 1);
    % add rand selected outliers
    for i=1: NumRandomOutliers
        
        [start_pose, end_pose]  = rand_edges(maximum_index);
         
        if(start_pose == 0)
%             disp('start_pose =0')
%             disp(['end_pose = ', num2str(end_pose)]) 
            start_vec = zeros(6,1);
        else
            start_vec = Res([(start_pose*6-5) : start_pose*6], 1);
        end     
        end_vec = Res([(end_pose*6-5) : end_pose*6], 1);
        
        X_vec([1:3],1) = Exp_a2R(start_vec([4:6] ,1)).' * (end_vec([1:3], 1) - start_vec([1:3], 1));
        
        X_vec([4:6],1) = Log_R2a( Exp_a2R(start_vec([4:6] ,1)).' *  Exp_a2R(end_vec([4:6] ,1)));
        
        [start_outlier, end_outlier]  = rand_edges(maximum_index);
        
        while((start_outlier==start_pose) && (end_outlier==end_pose))
            [start_outlier, end_outlier]  = rand_edges(maximum_index);   
            if((end_outlier-start_outlier)==1)
%               disp('see: odometry edges')
%               disp(['see: end_pose   start_pose   ', num2str(end_outlier), '  ', num2str(start_outlier)])
                if(end_outlier < maximum_index)
                    end_outlier = end_outlier + 1;
                elseif(start_outlier > 0)
                    start_outlier = start_outlier - 1;
                end
            end
        end
        if((end_outlier-start_outlier)==1)
%             disp('meet: odometry edges')
%             disp(['meet: end_pose   start_pose   ', num2str(end_outlier), '  ', num2str(start_outlier)])
            if(end_outlier < maximum_index)
                end_outlier = end_outlier + 1;
            elseif(start_outlier > 0)
                start_outlier = start_outlier - 1;
            end
        end   
        if((end_outlier-start_outlier)==1)    
            disp('add odometry edges')
            disp(['end_pose   start_pose   ', num2str(end_outlier), '  ', num2str(start_outlier)])         
        end
        
        OutlierEdges = [X_vec([1:6],1), end_outlier*ones(6,1), start_outlier*ones(6,1), -1*ones(6,1)];
        
        Zstate = [Zstate; OutlierEdges];
        
        CovMatrixInv([Dim+1 : Dim+StepSize], [Dim+1 : Dim+StepSize]) = CovarianceInv;
        
        Dim = Dim + StepSize;
        
    end
    
    for j=1: NumInitialOutliers
    
        [start_outlier, end_outlier]  = rand_edges(maximum_index);

        while(end_outlier - start_outlier < 20)    %%%%%
            [start_outlier, end_outlier]  = rand_edges(maximum_index);
        end        
        
        if(start_outlier == 0)
            start_vec = zeros(6,1);
        else
            start_vec = Xstate0([(start_outlier*6-5) : start_outlier*6], 1);
        end     
        end_vec = Xstate0([(end_outlier*6-5) : end_outlier*6], 1);
        
        X_vec([1:3],1) = Exp_a2R(start_vec([4:6] ,1)).' * (end_vec([1:3], 1) - start_vec([1:3], 1));
        
        X_vec([4:6],1) = Log_R2a( Exp_a2R(start_vec([4:6] ,1)).' *  Exp_a2R(end_vec([4:6] ,1)));
        
        OutlierEdges = [X_vec([1:6],1), end_outlier*ones(6,1), start_outlier*ones(6,1), -1*ones(6,1)];
        
        Zstate = [Zstate; OutlierEdges];
        
        CovMatrixInv([Dim+1 : Dim+StepSize], [Dim+1 : Dim+StepSize]) = CovarianceInv;
        
        Dim = Dim + StepSize;      
            
    end        
    
elseif(strcmp(DataSetType, '2D'))
    [Res] = PoseSLAM_G2O_GN_2D( Zstate, CovMatrixInv, 'Cauchy', 1.0, 200);
    Xstate0 = FuncCreateXstateFromZstate_2D_PoseSLAM( Zstate );
    maximum_index = max(Zstate(:,2));
    
    Dim = size(CovMatrixInv, 1);
    % add rand selected outliers
    for i=1: NumRandomOutliers
        
        [start_pose, end_pose]  = rand_edges(maximum_index);
         
        if(start_pose == 0)
            start_vec = zeros(6,1);
        else
            start_vec = Res([(start_pose*3-2) : start_pose*3], 1);
        end     
        end_vec = Res([(end_pose*3-2) : end_pose*3], 1);
        
        X_vec([1,2],1) = R(start_vec(3 ,1)).' * (end_vec([1,2], 1) - start_vec([1,2], 1));
        
        X_vec(3,1) = R2a( R(start_vec(3 ,1)).' *  R(end_vec(3 ,1)));
        
        [start_outlier, end_outlier]  = rand_edges(maximum_index);
        
        while((start_outlier==start_pose) && (end_outlier==end_pose))
            [start_outlier, end_outlier]  = rand_edges(maximum_index);   
            if((end_outlier-start_outlier)==1)
              disp('see: odometry edges')
              disp(['see: end_pose   start_pose   ', num2str(end_outlier), '  ', num2str(start_outlier)])
                if(end_outlier < maximum_index)
                    end_outlier = end_outlier + 1;
                elseif(start_outlier > 0)
                    start_outlier = start_outlier - 1;
                end
            end
        end
        if((end_outlier-start_outlier)==1)
            disp('meet: odometry edges')
            disp(['meet: end_pose   start_pose   ', num2str(end_outlier), '  ', num2str(start_outlier)])
            if(end_outlier < maximum_index)
                end_outlier = end_outlier + 1;
            elseif(start_outlier > 0)
                start_outlier = start_outlier - 1;
            end
        end   
        if((end_outlier-start_outlier)==1)    
            disp('add odometry edges')
            disp(['end_pose   start_pose   ', num2str(end_outlier), '  ', num2str(start_outlier)])         
        end
   
        OutlierEdges = [X_vec([1:3],1), end_outlier*ones(3,1), start_outlier*ones(3,1), -1*ones(3,1)];
        
        Zstate = [Zstate; OutlierEdges];
        
        CovMatrixInv([Dim+1 : Dim+StepSize], [Dim+1 : Dim+StepSize]) = CovarianceInv;
        
        Dim = Dim + StepSize;
        
    end
    
    for j=1: NumInitialOutliers
    
        [start_outlier, end_outlier]  = rand_edges(maximum_index);
        
        while(end_outlier - start_outlier < 20)   %%%%%
            [start_outlier, end_outlier]  = rand_edges(maximum_index);
        end
        
        if(start_pose == 0)
            start_vec = zeros(6,1);
        else
            start_vec = Xstate0([(start_outlier*3-2) : start_outlier*3], 1);
        end     
        end_vec = Xstate0([(end_outlier*3-2) : end_outlier*3], 1);
        
        X_vec([1,2],1) = R(start_vec(3 ,1)).' * (end_vec([1,2], 1) - start_vec([1,2], 1));
        
        X_vec(3,1) = R2a( R(start_vec(3 ,1)).' *  R(end_vec(3 ,1)));
        
        OutlierEdges = [X_vec([1:3],1), end_outlier*ones(3,1), start_outlier*ones(3,1), -1*ones(3,1)];
        
        Zstate = [Zstate; OutlierEdges];
        
        CovMatrixInv([Dim+1 : Dim+StepSize], [Dim+1 : Dim+StepSize]) = CovarianceInv;
        
        Dim = Dim + StepSize;    
            
    end      
            
else
    disp('****************running error *******************')
    return;
end

end
%%%
function  [start_pose, end_pose]  = rand_edges(maximum_index)

        end_pose = ceil(rand * maximum_index); % uniform distribution      
        start_pose = ceil(rand* maximum_index)-1;  % uniform distribution     

        while(end_pose == start_pose)          
            end_pose = ceil(rand * maximum_index); % uniform distribution      
            start_pose = ceil(rand* maximum_index)-1;  % uniform distribution           
        end

        
        if(end_pose < start_pose)
            tmp = start_pose;
            start_pose = end_pose;
            end_pose = tmp;
        end
        
        

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ Theta ] = Log_R2a( R )
threshold = 1e-10;
angle = acos( (trace(R) -1 )/2 );

if (abs(angle) <threshold ) % 0
    axis = [1; 0; 0;];
elseif(abs(angle - pi) <threshold )  % pi
   % disp('reach pi');
    axis = - [sign(R(2,3))*sqrt((1+R(1,1))/2); sign(R(1,3))*sqrt((1+R(2,2))/2); sign(R(1,2))*sqrt((1+R(3,3))/2) ];
else
    axis = [ R(3,2)-R(2,3);
             R(1,3)-R(3,1);
             R(2,1)-R(1,2); ];
    axis = axis/(2*sin(angle));
end

Theta = angle*axis;
[ Theta ] = WrapX( Theta );
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Wrap a arbitrary axis angle rotation into a rotation at interval [0, pi]
function [ X_v ] = WrapX( X_vec )
if (norm(X_vec)== 0)
    X_v = X_vec;
    return; 
end
angle = norm(X_vec);
axis  = X_vec/angle;
while (angle >= 2*pi)
    angle = angle - 2*pi;
end
while ( angle > pi)
    angle = 2*pi - angle;
    axis = -axis;  
end
if(pi-angle<1e-10)
   if(axis(3)<0)
       axis = -axis;
   end    
end

X_v = angle*axis;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function is used to transform a vector into a rotation matrix by exponential mapping
function [ RotationMatrixExp ] = Exp_a2R( X_vec )

threshold = 1e-10;

theta = norm(X_vec);

if (theta < threshold)
    RotationMatrixExp = eye(3);
else
    RotationMatrixExp = eye(3) + SkewSem(X_vec)*sin(theta)/theta + ...
        (SkewSem(X_vec)^2)*(1 - cos(theta))/(theta^2);    
end

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% transform a 3-dimensional vector into a skew-semmetric marix
function [ SkewSemmetricMatrix ] = SkewSem( X_vec )
SkewSemmetricMatrix = [ 0          -X_vec(3)     X_vec(2);
                        X_vec(3)    0           -X_vec(1);
                       -X_vec(2)    X_vec(1)     0       ;];
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get rotation matrix A with respect to angle phi
function [A] = R(phi)
A=[cos(phi),-sin(phi);sin(phi),cos(phi)];
end
% Get angle phi from rotation matrix R
function [phi] = R2a(R)
phi = atan2(R(2,1), R(1,1));
end