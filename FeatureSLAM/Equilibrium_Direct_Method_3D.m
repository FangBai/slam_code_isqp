% This function is used to compute the solution of linear system.
% P is the covariance matrix of the meausrement noise rather than CovMatrixInv (the inversion of covariance matrix)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%  min  || J^{-1} *X - eta ||_P^2   %%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%      st.  AX = b        %%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ X_value, F_value ] = Equilibrium_Direct_Method_3D(J, eta, P, A ,b)

%%%%%%%%%%%%% Solve Lagrange Equilibrium Equations %%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%% Direct Method %%%%%%%%%%%%%%%%%%%%%%%%%%%
J = sparse(J);
eta = sparse(eta);
P = sparse(P);
A = sparse(A);
b = sparse(b);


Q = J*P*(J.');
w = J*eta;

X_value =  w - Q*A.'*((A*Q*A.')\(A*w-b));

% detmat = det(A*P*A.');
% detinv = det(inv(A*P*A.'))

X_w = (J\X_value - eta);

F_value = X_w.'*(P\X_w);

% normX = norm(X_value)

end

