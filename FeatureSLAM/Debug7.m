clear all
close all
clc


load('Zstate_DLR.mat')
load('CovMatrixInv_DLR.mat')
if(0) 
   NumPose = 1000;
   DataDim = find(Zstate(:,4) == NumPose, 1, 'last');
   Zstate = Zstate([1:DataDim],:);
   CovMatrixInv = CovMatrixInv([1:DataDim], [1:DataDim]);
end

% load('Zstate_VicPark_6898_loops.mat')
% load('CovMatrixInv_VicPark_6898_loops.mat')

% load('Zstate_Simu_256.mat')
% CovMatrixInv = speye(size(Zstate,1));

[ Xstate0 ]  = FuncCreateXstateFromZstate_2D( Zstate );
PlotXstate2D( Xstate0, 'r.:', 'b.');
% DrawXstate2D( Xstate0 );

[Res_SQP, Fval_SQP] = FeatureSLAM_SQP_2D( Zstate, CovMatrixInv );

PlotXstate2D( Res_SQP, 'r.:', 'b.');
% DrawXstate2D( Res_SQP );

