function [pose_index feature_index] = CreateG2OFormatByZstateFeature2D (File_Path_Name, Zstate, CovMatrixInv, Xstate_0 )


% function to create G2O format data
% Observation and odometry data saved in Zstate
% Initial value for pose and feature saved in Xstate_0

% data format for each type
format_pose = 'VERTEX_SE2 %d %f %f %f\n';
format_feature = 'VERTEX_XY %d %f %f\n';
format_odometry = 'EDGE_SE2 %d %d %f %f %f %f %f %f %f %f %f\n';
format_observation = 'EDGE_SE2_XY %d %d %f %f %f %f %f\n';


% classify the data into pose feature odometry observation
data_pose = Xstate_0(find(Xstate_0(:,2)==1),:);
data_feature = Xstate_0(find(Xstate_0(:,2)==2),:);
od_index = find(Zstate(:,2)==1);
data_odometry = Zstate(od_index,:);
Cov_odometry = CovMatrixInv(od_index, od_index);
ob_index = find(Zstate(:,2)==2);
data_observation = Zstate(ob_index,:);
Cov_observation = CovMatrixInv(ob_index, ob_index);

% quantity of each data type
num_pose = size(data_pose,1)/3;
num_feature = size(data_feature,1)/2;
num_odometry = size(data_odometry,1)/3;
num_observation = size(data_observation,1)/2;



% assign new index to poses and features
% index = vertex index
% Pose_index(0) = 0
% feautre_index(1) = 1
feature_index = [];
pose_index  =[];
vertex_count = 1;
pose_count  = 0;
while( pose_count<=num_odometry)
current_pose_data = Zstate(find(Zstate(:,4)==pose_count),:);

count  = 1;
while (2*count-1<=size(current_pose_data,1))
if(current_pose_data(2*count-1,2) ==2)
    
    if(current_pose_data(2*count-1,3)> max(size(feature_index)))
% assign a vertex for new feature
feature_index(current_pose_data(2*count-1,3)) = vertex_count; 
vertex_count = vertex_count +1;    
    elseif(current_pose_data(2*count-1,3)< max(size(feature_index)) &&feature_index(current_pose_data(2*count-1,3))==0)
% assign a vertex for new feature
feature_index(current_pose_data(2*count-1,3)) = vertex_count; 
vertex_count = vertex_count +1;
    end
  count  = count + 1;
 
elseif(current_pose_data(2*count-1,2) ==1)
% assign a vertex for pose
pose_index(current_pose_data(2*count-1,3))=vertex_count;
vertex_count = vertex_count+1;
  count  = count + 2;

end
end
   pose_count = pose_count + 1;
end
%%%%%%%%%%%%%% vertex subscript set in pose_index feature_index


% create and open the file
fid = fopen(File_Path_Name, 'w');

% write pose 0 into the file
fprintf(fid, format_pose, 0, 0, 0, 0);

fprintf(fid, 'FIX 0\n');

% write vertices (pose and feature) into the file
num_vertex = max(max(pose_index),max(feature_index));

for count =1:num_vertex
    
 tmp = find(pose_index(1,:)==count);
 
 if(size(tmp,2))
   vertex_index = tmp;
   pose_tmp = find(data_pose(:,3)==vertex_index);
   tmp_x = data_pose(pose_tmp(1),1);
   tmp_y = data_pose(pose_tmp(2),1);
   tmp_phi = data_pose(pose_tmp(3),1);
   
  
   fprintf(fid, format_pose, pose_index(vertex_index), tmp_x, tmp_y, tmp_phi);  
   
 else
   vertex_index = find(feature_index(1,:)==count);  
 %  count
 %  vertex_index
   feature_tmp = find(data_feature(:,3)==vertex_index);
   
   tmp_x = data_feature(feature_tmp(1),1);
   tmp_y = data_feature(feature_tmp(2),1);
   
   fprintf(fid, format_feature, feature_index(vertex_index), tmp_x ,tmp_y);
   
 end
 
    
end


% write odometry into the file
for count =1:num_odometry

tmp_x = data_odometry(3*count-2,1);
tmp_y = data_odometry(3*count-1,1);
tmp_phi = data_odometry(3*count,1);

Cov_11 = full(Cov_odometry(3*count-2,3*count-2));
Cov_12 = full(Cov_odometry(3*count-2,3*count-1));
Cov_13 = full(Cov_odometry(3*count-2,3*count));
Cov_22 = full(Cov_odometry(3*count-1,3*count-1));
Cov_23 = full(Cov_odometry(3*count-1,3*count));
Cov_33 = full(Cov_odometry(3*count,3*count));

if(data_odometry(3*count,4)==0)
fprintf(fid, format_odometry, 0, pose_index(data_odometry(3*count,3)), tmp_x, tmp_y, tmp_phi, Cov_11, Cov_12, Cov_13, Cov_22, Cov_23, Cov_33);
else
fprintf(fid, format_odometry, pose_index(data_odometry(3*count,4)), pose_index(data_odometry(3*count,3)), tmp_x, tmp_y, tmp_phi, Cov_11, Cov_12, Cov_13, Cov_22, Cov_23, Cov_33);
end
end

% write observation into the file
for count =1:num_observation
    
tmp_x = data_observation(2*count-1,1);
tmp_y = data_observation(2*count,1);

Cov_11 = full(Cov_observation(2*count-1,2*count-1));
Cov_12 = full(Cov_observation(2*count-1,2*count));
Cov_22 = full(Cov_observation(2*count,2*count));


if(data_observation(2*count,4)==0)
fprintf(fid, format_observation, 0, feature_index(data_observation(2*count,3)), tmp_x, tmp_y, Cov_11, Cov_12, Cov_22); 
else
fprintf(fid, format_observation, pose_index(data_observation(2*count,4)), feature_index(data_observation(2*count,3)), tmp_x, tmp_y, Cov_11, Cov_12, Cov_22);
end
end
%close file
fclose(fid);


end

