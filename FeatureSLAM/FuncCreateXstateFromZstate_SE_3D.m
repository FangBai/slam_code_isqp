function [ Xstate_EU ]  = FuncCreateXstateFromZstate_SE_3D( Zstate_SE )

DataDim = size(Zstate_SE,1);
Zcount = 1;
Xcount = 1;

Xstate_EU = [];

CurrentPosi = [0; 0; 0];
CurrentRota = eye(3);

while ( Zcount <= DataDim )
    if( Zstate_SE(Zcount,2) == 1)
       
        R_cur =  [Zstate_SE([Zcount+3, Zcount+6, Zcount+9],1) Zstate_SE([Zcount+4, Zcount+7, Zcount+10],1) Zstate_SE([Zcount+5, Zcount+8, Zcount+11],1)];
        
        CurrentPosi = CurrentRota*Zstate_SE([Zcount, Zcount+1, Zcount+2], 1) + CurrentPosi;
        CurrentRota = CurrentRota*R_cur;  
        
        Xstate_EU([Xcount, Xcount+1, Xcount+2], 1) = CurrentPosi;
        Xstate_EU([Xcount+3, Xcount+4, Xcount+5], 1) = Log_R2a(CurrentRota);
        
        Xstate_EU([Xcount : Xcount+5],[2,3]) = Zstate_SE( [Zcount : Zcount+5],[2,3]); 
        Xcount = Xcount + 6;
        Zcount = Zcount + 12;    
        
    elseif( Zstate_SE(Zcount,2) == 2)
       
        if(CheckNewFeature (Xstate_EU, Zstate_SE(Zcount,3)) == 1)
            
            FeaturePosi = CurrentRota*Zstate_SE([Zcount, Zcount+1, Zcount+2], 1) + CurrentPosi;
            
            Xstate_EU([Xcount, Xcount+1, Xcount+2], 1) = FeaturePosi;
        
            Xstate_EU([Xcount : Xcount+2],[2,3]) = Zstate_SE( [Zcount : Zcount+2],[2,3]);
            Xcount = Xcount + 3;   
            
        end
        Zcount = Zcount + 3; 
        
    else
        disp('data error!');
        return;
    end
end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ TrueFalse ] = CheckNewFeature (Xstate, Feature)
TrueFalse = 1;
for i = 1 : size(Xstate,1)
    if(Xstate(i,2) == 2 && Xstate(i,3) == Feature)
        TrueFalse = 0;
        break;
    end 
end
end


