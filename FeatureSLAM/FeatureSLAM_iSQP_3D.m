function [Res_iSQP, Fval_iSQP] = FeatureSLAM_iSQP_3D( Zstate_EU, CovMatrixInv)


% BenchMark = 'Covariance';
BenchMark = 'NewEdge';



%disp('SQP method by adding constraints gradually is processing!')
CovMatrix = inv(CovMatrixInv);
[ Data_SE, Variable_Indices,  M_e_all,  M_s_all ] = ExtractDataSetInfo( Zstate_EU );
%%%%%%%%%%%%%%%%%%% Initialization %%%%%%%%%%%%%%%%%%%
M_s = cell(0);
M_e = cell(0);
con_count = 1;  
Con_amount = max(size(M_s_all));
%set initial value with data
X0_SE = Data_SE;

Fval_iSQP = 0;

while (con_count <= Con_amount)
    
   %%%%%%%%%%%%%%%%%%% SELECCT CONSTRAINTS  %%%%%%%%%%%%%%%%%%% 
   % select a small portion of constraints from N_all to N  
   % get the number of constraints to be added each time by the quantity of odometries
     Con_value = Nonlinear_con_3D(X0_SE, M_s_all );
     Con_value = abs(Con_value);
     for k = 1:Con_amount
     Con_norm(k) = sqrt(Con_value(3*k-2)^2 + Con_value(3*k-1)^2 + Con_value(3*k)^2);
     end
   % sort constraints in ascending order by the value of constraints
     [Con_norm, Cons_SEQUENCE] = sort(Con_norm,'ascend');

   % Get the quantity of constraints to be added into the problem each time
   % By the value of constraints
   % Get ratio between consecutive constraint value
     Mean = sum(Con_norm(con_count:Con_amount))/(Con_amount - con_count +1 );
     Con_ratio (con_count) = 1;     
     for k = con_count+1: Con_amount
          Con_ratio (k) = Con_norm(k)/Con_norm(con_count);
     end
   % Criteria to decide con_num     
     con_num = 1;    
     tmp = con_count+1;
     %Criteria
        while ( tmp <= Con_amount && Con_ratio(tmp) < 1.618 && Con_norm(tmp)< Mean )
           tmp = tmp + 1;
           con_num = con_num + 1;
        end
   % add selected constraints into the problem
   % the constraints left
     con_res = Con_amount - con_count + 1;
   % add constraints into N
     if(con_res < con_num)
        for k = 0:con_res-1 
        M_e{con_count + k} = M_e_all {Cons_SEQUENCE(con_count+k)};  
        M_s{con_count + k} = M_s_all {Cons_SEQUENCE(con_count+k)};         
        end        
     else
        for k = 0:con_num-1
        M_e{con_count + k} = M_e_all {Cons_SEQUENCE(con_count+k)};  
        M_s{con_count + k} = M_s_all {Cons_SEQUENCE(con_count+k)};   
        end
     end
   %%%%%%%%%%%%%%%%%%% SELECCT CONSTRAINTS END %%%%%%%%%%%%%%%%%%% 
    
    %Display adding constraints process
    Constraint_total = max(size(M_s_all));
    Constraint_added = max(size(M_s));
    str = ['Constraint_add/Constraint_total=', num2str(Constraint_added), '/', num2str(Constraint_total)];
    str = ['Constraint_add/Constraint_total=', num2str(Constraint_added), '/', num2str(Con_amount)];
    disp(str);    

   %%%%%SQP method to solve SLAM problem  %%%%%%%
    [ X_value_SE, Fval_iSQP, Iteration_count, Var_Increment_EU ] = My_method_solve_3D( X0_SE, Data_SE, CovMatrix, M_e, M_s, Variable_Indices, BenchMark);

    X0_SE = X_value_SE ;
    con_count = con_count + con_num;
    
end
% get Xstate form of solution
[ Zstate_SE ] = ZstateFromEuclideanToSE3( Zstate_EU );
Zstate_SE(:,1) = X_value_SE;
Res_iSQP  = FuncCreateXstateFromZstate_SE_3D(Zstate_SE); 

