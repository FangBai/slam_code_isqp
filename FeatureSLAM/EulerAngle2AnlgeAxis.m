% This function is used to convert EulerAngles to AngleAxis representation
% EulerAngle [heading; attitude; bank;] or [yaw; pitch; roll;]
% EulerAngle Rotated by [ y-axis, z-axis, x-axis ]
function [ AngleAxis ] = EulerAngle2AnlgeAxis ( EulerAngle )

threshold = 1e-10;

yaw = EulerAngle(1);
pitch = EulerAngle(2);
roll = EulerAngle(3);

c1 = cos(yaw/2);
c2 = cos(pitch/2);
c3 = cos(roll/2);
s1 = sin(yaw/2);
s2 = sin(pitch/2);
s3 = sin(roll/2);

angle = 2*acos(c1*c2*c3 - s1*s2*s3);

x = s1*s2*c3 + c1*c2*s3;
y = s1*c2*c3 + c1*s2*s3;
z = c1*s2*c3 - s1*c2*s3;

axis = [x; y; z;];

normalizer = sqrt(x^2 + y^2 + z^2);

if (normalizer < threshold)
   axis = [1; 0; 0;]; 
else
   axis = axis/normalizer;
end

AngleAxis = WrapX(angle*axis);

end

% Wrap a arbitrary axis angle rotation into a rotation at interval [0, pi]
function [ X_v ] = WrapX( X_vec )
if (norm(X_vec)== 0)
    X_v = X_vec;
    return; 
end
angle = norm(X_vec);
axis  = X_vec/angle;
while (angle >= 2*pi)
    angle = angle - 2*pi;
end
while ( angle > pi)
    angle = 2*pi - angle;
    axis = -axis;  
end
X_v = angle*axis;
end
