% This function is used to update variables in SE(3) by the increments in Euclidean space
% X0_SE represents the current variable value in SE(3) before update
% X_EU represents the increments in Euclidean space
% X_SE represents the variable value after update
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [ X_SE ] = Update_SEbyEU_3D(X0_SE, X_EU, Variable_Indices )

DataDim = max(size(Variable_Indices));
SEcount = 1;
EUcount = 1;
count = 1;

while (count <= DataDim )
if (Variable_Indices (count) == 1)
    Posi_SE = X0_SE([SEcount, SEcount+1, SEcount+2]);
    Rota_SE = [X0_SE([SEcount+3, SEcount+6, SEcount+9]) X0_SE([SEcount+4, SEcount+7, SEcount+10]) X0_SE([SEcount+5, SEcount+8, SEcount+11])];
    Posi_EU = X_EU([EUcount, EUcount+1, EUcount+2],1);
    Rota_EU = X_EU([EUcount+3, EUcount+4, EUcount+5],1);
    
    X_SE([SEcount, SEcount+1, SEcount+2],1) = Posi_SE + Posi_EU;   
    
    Rota_SE = Rota_SE * Exp_a2R(Rota_EU);
    X_SE([SEcount+3 : SEcount+11],1) = [Rota_SE(1,1); Rota_SE(1,2); Rota_SE(1,3);
                                        Rota_SE(2,1); Rota_SE(2,2); Rota_SE(2,3);
                                        Rota_SE(3,1); Rota_SE(3,2); Rota_SE(3,3);];

    SEcount = SEcount + 12;
    EUcount = EUcount + 6;
elseif (Variable_Indices (count) == 2)
    Posi_SE = X0_SE([SEcount, SEcount+1, SEcount+2]);
    Posi_EU = X_EU([EUcount, EUcount+1, EUcount+2],1);
    
    X_SE([SEcount, SEcount+1, SEcount+2],1) = Posi_SE + Posi_EU;
    
    SEcount = SEcount + 3;
    EUcount = EUcount + 3;    
else
    disp('data error!');
    return;
end
count = count + 1;
end
