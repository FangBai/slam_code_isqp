function [ Xstate ]  = FuncCreateXstateFromZstate_2D( Zstate_EU )

DataDim = size(Zstate_EU,1);
Zcount = 1;
Xcount = 1;

Xstate = [];

CurrentPosi = [0; 0];
CurrentRota = eye(2);

while ( Zcount <= DataDim )
    if( Zstate_EU(Zcount,2) == 1)
       
        CurrentPosi = CurrentRota*Zstate_EU([Zcount, Zcount+1], 1) + CurrentPosi;
        CurrentRota = CurrentRota*R( Zstate_EU([Zcount+2], 1));  
        
        Xstate([Xcount, Xcount+1], 1) = CurrentPosi;
        Xstate([Xcount+2], 1) = R2a(CurrentRota);
        
        Xstate([Xcount : Xcount+2],[2,3]) = Zstate_EU( [Zcount : Zcount+2],[2,3]); 
        Xcount = Xcount + 3;
        Zcount = Zcount + 3;    
        
    elseif( Zstate_EU(Zcount,2) == 2)
       
        if(CheckNewFeature (Xstate, Zstate_EU(Zcount,3)) == 1)
            
            FeaturePosi = CurrentRota*Zstate_EU([Zcount, Zcount+1], 1) + CurrentPosi;
            
            Xstate([Xcount, Xcount+1], 1) = FeaturePosi;
        
            Xstate([Xcount, Xcount+1],[2,3]) = Zstate_EU( [Zcount, Zcount+1],[2,3]);
            Xcount = Xcount + 2;   
            
        end
        Zcount = Zcount + 2; 
        
    else
        disp('data error!');
        return;
    end
end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ TrueFalse ] = CheckNewFeature (Xstate, Feature)
TrueFalse = 1;
for i = 1 : size(Xstate,1)
    if(Xstate(i,2) == 2 && Xstate(i,3) == Feature)
        TrueFalse = 0;
        break;
    end 
end
end
% Get rotation matrix A with respect to angle phi
function [A] = R(phi)
A=[cos(phi),-sin(phi);sin(phi),cos(phi)];
end
% Get angle phi from rotation matrix R
function [phi] = R2a(R)
phi = atan2(R(2,1), R(1,1));
end
