%right hand Jacobian of SO(3) space
%X_vec: minimal representation represented by angle-axis rotation
%J_r : right hand Jocobian with regard to X_vec
function [ J_r ] = JacobianRightHandSO3( X_vec )

if (norm(X_vec) ==0)
    J_r = eye(3);
else
    J_r = eye(3) - SkewSem(X_vec)*(1 - cos(norm(X_vec)))/((norm(X_vec))^2) + ...
        (SkewSem(X_vec)^2)*(norm(X_vec) - sin(norm(X_vec)))/((norm(X_vec))^3);    
end

end