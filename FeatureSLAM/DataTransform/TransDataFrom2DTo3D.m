%This function is used to transform 2D dataset to 3D
function [ Zstate_3D ] = TransDataFrom2DTo3D( Zstate_2D )

count2D = 1;
count3D = 1;
DataDim = size(Zstate_2D,1);

while (count2D < DataDim)
   if( Zstate_2D(count2D,2) == 1)
       Zstate_3D([count3D, count3D+1],:) = Zstate_2D([count2D, count2D+1],:);
       Zstate_3D([count3D+2 : count3D+4],1) = [0; 0; 0;];
       Zstate_3D([count3D+2 : count3D+4],[2,3,4]) = Zstate_2D([count2D, count2D+1, count2D+2],[2,3,4]);
       Zstate_3D([count3D+5],:) = Zstate_2D([count2D+2],:);
       count3D = count3D + 6;
       count2D = count2D + 3;
   elseif( Zstate_2D(count2D,2) == 2)       
       Zstate_3D([count3D, count3D+1],:) = Zstate_2D([count2D, count2D+1],:);
       Zstate_3D([count3D+2],1) = [0];
       Zstate_3D([count3D+2],[2,3,4]) = Zstate_2D([count2D],[2,3,4]);
       count3D = count3D + 3;
       count2D = count2D + 2;
   else
       disp('data error!');
       return;
   end
end
end

