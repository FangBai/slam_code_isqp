function [ Xstate_EU ]  = FuncCreateXstateFromZstate_EU_3D( Zstate_EU )

DataDim = size(Zstate_EU,1);
Zcount = 1;
Xcount = 1;

CurrentPosi = [0; 0; 0];
CurrentRota = eye(3);

while ( Zcount <= DataDim )
    if( Zstate_EU(Zcount,2) == 1)
       
        CurrentPosi = CurrentRota*Zstate_EU([Zcount, Zcount+1, Zcount+2], 1) + CurrentPosi;
        CurrentRota = CurrentRota*Exp_a2R( Zstate_EU([Zcount+3, Zcount+4, Zcount+5], 1));  
        
        Xstate_EU([Xcount, Xcount+1, Xcount+2], 1) = CurrentPosi;
        Xstate_EU([Xcount+3, Xcount+4, Xcount+5], 1) = Log_R2a(CurrentRota);
        
        Xstate_EU([Xcount : Xcount+5],[2,3]) = Zstate_EU( [Zcount : Zcount+5],[2,3]); 
        Xcount = Xcount + 6;
        Zcount = Zcount + 6;    
        
    elseif( Zstate_EU(Zcount,2) == 2)
       
        if(CheckNewFeature (Xstate_EU, Zstate_EU(Zcount,3)) == 1)
            
            FeaturePosi = CurrentRota*Zstate_EU([Zcount, Zcount+1, Zcount+2], 1) + CurrentPosi;
            
            Xstate_EU([Xcount, Xcount+1, Xcount+2], 1) = FeaturePosi;
        
            Xstate_EU([Xcount : Xcount+2],[2,3]) = Zstate_EU( [Zcount : Zcount+2],[2,3]);
            Xcount = Xcount + 3;   
            
        end
        Zcount = Zcount + 3; 
        
    else
        disp('data error!');
        return;
    end
end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ TrueFalse ] = CheckNewFeature (Xstate, Feature)
TrueFalse = 1;
for i = 1 : size(Xstate,1)
    if(Xstate(i,2) == 2 && Xstate(i,3) == Feature)
        TrueFalse = 0;
        break;
    end 
end
end
% transform a vector into a rotation matrix by exponential mapping
function [ RotationMatrixExp ] = Exp_a2R( X_vec )

if (norm(X_vec) ==0)
    RotationMatrixExp = eye(3);
else
    RotationMatrixExp = eye(3) + SkewSem(X_vec)*sin(norm(X_vec))/norm(X_vec) + ...
        (SkewSem(X_vec)^2)*(1 - cos(norm(X_vec)))/(norm(X_vec))^2;    
end
end
% This function gives the Logarithm mapping to tranform a rotation R to the minimal representation Theta
% The minimal representation is described by axis angle representation
% Theta is a vertical vector
function [ Theta ] = Log_R2a( R )

angle = acos( (trace(R) -1 )/2 );

if (angle ==0)
    axis = [1; 0; 0;];
else
    axis = [ R(3,2)-R(2,3);
             R(1,3)-R(3,1);
             R(2,1)-R(1,2); ];
    axis = axis/(2*sin(angle));
end

Theta = angle*axis;

end
% transform a 3-dimensional vector into a skew-semmetric marix
function [ SkewSemmetricMatrix ] = SkewSem( X_vec )
SkewSemmetricMatrix = [ 0          -X_vec(3)     X_vec(2);
                        X_vec(3)    0           -X_vec(1);
                       -X_vec(2)    X_vec(1)     0       ;];
end



