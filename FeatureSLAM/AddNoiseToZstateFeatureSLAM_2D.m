
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ Zstate, CovMatrixInv ] = AddNoiseToZstateFeatureSLAM_2D( Zstate, CovMatrixInv, GroundTruth, NoiseLevelOdom, NoiseLevelObser, Mode )


GroundTruthPose = GroundTruth(find(GroundTruth(:,2) == 1), :);
GroundTruthFeature = GroundTruth(find(GroundTruth(:,2) == 2), :);

[~,v] = sort(GroundTruthFeature(:,3), 'ascend');
GroundTruthFeature = GroundTruthFeature(v,:);

count = 1;

SIGMA_BOUND = 2.0;

if(size(NoiseLevelOdom,2) > size(NoiseLevelOdom,1))
    NoiseLevelOdom = NoiseLevelOdom.';
end

if(size(NoiseLevelObser,2) > size(NoiseLevelObser,1))
    NoiseLevelObser = NoiseLevelObser.';
end

SIGMA_OM = diag(NoiseLevelOdom);

SIGMA_OB = diag(NoiseLevelObser);

Dim = size(Zstate, 1);

Cov_i = zeros(Dim,1);
Cov_j = zeros(Dim,1);
Cov_v = zeros(Dim,1);

while(count <= Dim)
    
    vertex_start = Zstate(count, 4);
    vertex_end   = Zstate(count, 3);
    
    measure_type = Zstate(count, 2);
    
    if(measure_type == 2)    %     observation     
            
        FidLine = find(GroundTruthFeature(:,3) == vertex_end, 1, 'first');
        end_vec = GroundTruthFeature([FidLine, FidLine+1], 1);
        
        if(vertex_start == 0)
            start_vec = [0;0;0;];
        else
            start_vec = GroundTruthPose([3*vertex_start-2 : 3*vertex_start], 1);
        end
        
        %
        OB_Data_vec([1,2], 1) = R(start_vec([3],1)).' * (end_vec([1,2],1) - start_vec([1,2] ,1)); 
        
        %
        
        if(strcmp(Mode, 'additive'))
            OB_Data_vec = Zstate([count, count+1], 1);
        end        

        % Add noise to Data_vec
       
        for i = 1 : 2
            OB_Rand_vec(i, 1) = randn;            
            while(abs(OB_Rand_vec(i,1)) > SIGMA_BOUND)
                OB_Rand_vec(i, 1) = randn;
            end
        end
              
        OB_Data_vec = OB_Data_vec + SIGMA_OB * OB_Rand_vec;        
        
        Zstate([count, count+1], 1) = OB_Data_vec;       
        
        Cov_i([count, count+1], 1) = [count, count+1].';
        Cov_j([count, count+1], 1) = [count, count+1].';
        Cov_v([count, count+1], 1) = [NoiseLevelObser].^2;
        
        count = count + 2;
         
        
    elseif(measure_type == 1)   %   odometry     
          
        end_vec = GroundTruthPose([3*vertex_end-2 : 3*vertex_end], 1);
        
        if(vertex_start == 0)
            start_vec = [0;0;0;];
        else
            start_vec = GroundTruthPose([3*vertex_start-2 : 3*vertex_start], 1);
        end
        
        %
        OM_Data_vec([1,2], 1) = R(start_vec([3],1)).' * (end_vec([1,2],1) - start_vec([1,2] ,1));
        
        OM_Data_vec([3], 1) = R2a(R(start_vec([3] ,1)).' *  R(end_vec([3] ,1)));
        %      
        
        %
        
        if(strcmp(Mode, 'additive'))
            OM_Data_vec = Zstate([count : count+2], 1);
        end        

        % Add noise to Data_vec
       
        for i = 1 : 3
            OM_Rand_vec(i, 1) = randn;            
            while(abs(OM_Rand_vec(i,1)) > SIGMA_BOUND)
                OM_Rand_vec(i, 1) = randn;
            end
        end
        
        OM_Data_vec = OM_Data_vec + SIGMA_OM * OM_Rand_vec;
        
        %
        Zstate([count : count+2], 1) = OM_Data_vec;       
        
        Cov_i([count : count+2], 1) = [count : count+2].';
        Cov_j([count : count+2], 1) = [count : count+2].';
        Cov_v([count : count+2], 1) = [NoiseLevelOdom].^2;
        
        count = count + 3;
         
        
    else
        disp(['Error @ measurement type error in Zstate!']);
        return;
    end

end

Cov = sparse(Cov_i, Cov_j, Cov_v);

if(strcmp(Mode, 'additive'))
    CovMatrixInv = inv(inv(CovMatrixInv) + Cov);
else
    CovMatrixInv = inv(Cov);
end

end




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get rotation matrix A with respect to angle phi
function [A] = R(phi)
A=[cos(phi),-sin(phi);sin(phi),cos(phi)];
end

% Get angle phi from rotation matrix R
function [phi] = R2a(R)
phi = atan2(R(2,1), R(1,1));
end

