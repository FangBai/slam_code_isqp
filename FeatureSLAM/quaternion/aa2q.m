% tranform axis angle representation to quaternion
% the axis angle representation must be presented by vertical vector
function [ quaternion ] = aa2q( X_vec )

if (norm(X_vec) ==0)
    q_w = 1;
    q_v = [0; 0; 0];
else
    q_w = cos(norm(X_vec)/2);
    q_v = X_vec*sin(norm(X_vec)/2)/norm(X_vec);
end

quaternion = [q_w; q_v];

end

