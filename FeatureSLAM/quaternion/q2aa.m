function [ AngleAxis ] =  q2aa( quaternion )

threshold = 1e-10;

q_w = quaternion(1);
q_x = quaternion(2);
q_y = quaternion(3);
q_z = quaternion(4);

angle = 2*acos(q_w);

if(angle<threshold)
    axis = [1;0;0];
else
    axis = [q_x; q_y; q_z]/sin(angle/2);
end

AngleAxis = angle*axis;

end

