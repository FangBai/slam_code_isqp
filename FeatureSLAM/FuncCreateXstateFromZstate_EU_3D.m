function [ Xstate_EU ]  = FuncCreateXstateFromZstate_EU_3D( Zstate_EU )

DataDim = size(Zstate_EU,1);
Zcount = 1;
Xcount = 1;

Xstate_EU = [];

CurrentPosi = [0; 0; 0];
CurrentRota = eye(3);

while ( Zcount <= DataDim )
    if( Zstate_EU(Zcount,2) == 1)
       
        CurrentPosi = CurrentRota*Zstate_EU([Zcount, Zcount+1, Zcount+2], 1) + CurrentPosi;
        CurrentRota = CurrentRota*Exp_a2R( Zstate_EU([Zcount+3, Zcount+4, Zcount+5], 1));  
        
        Xstate_EU([Xcount, Xcount+1, Xcount+2], 1) = CurrentPosi;
        Xstate_EU([Xcount+3, Xcount+4, Xcount+5], 1) = Log_R2a(CurrentRota);
        
        Xstate_EU([Xcount : Xcount+5],[2,3]) = Zstate_EU( [Zcount : Zcount+5],[2,3]); 
        Xcount = Xcount + 6;
        Zcount = Zcount + 6;    
        
    elseif( Zstate_EU(Zcount,2) == 2)
       
        if(CheckNewFeature (Xstate_EU, Zstate_EU(Zcount,3)) == 1)
            
            FeaturePosi = CurrentRota*Zstate_EU([Zcount, Zcount+1, Zcount+2], 1) + CurrentPosi;
            
            Xstate_EU([Xcount, Xcount+1, Xcount+2], 1) = FeaturePosi;
        
            Xstate_EU([Xcount : Xcount+2],[2,3]) = Zstate_EU( [Zcount : Zcount+2],[2,3]);
            Xcount = Xcount + 3;   
            
        end
        Zcount = Zcount + 3; 
        
    else
        disp('data error!');
        return;
    end
end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ TrueFalse ] = CheckNewFeature (Xstate, Feature)
TrueFalse = 1;
for i = 1 : size(Xstate,1)
    if(Xstate(i,2) == 2 && Xstate(i,3) == Feature)
        TrueFalse = 0;
        break;
    end 
end
end


