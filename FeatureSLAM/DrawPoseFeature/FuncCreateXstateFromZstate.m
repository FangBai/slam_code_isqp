%% a small error is fixed by Shoudong in 2016
function Xstate = FuncCreateXstateFromZstate(Zstate)

%Creating Xstate
count=1;count2=1;count3=1;count4=1;
currentpos=[0,0,0];
savePos=currentpos;
%IDsaved(1)=-1;
IDsaved=[];
checkIDsaved=1;
while count<=size(Zstate,1)
    
    if(Zstate(count,2)==1)
        s = sin(currentpos(3));
        c = cos(currentpos(3));
        Xstate(count2,1)=currentpos(1)+Zstate(count,1)*c-Zstate(count+1,1)*s;
        Xstate(count2+1,1)=currentpos(2)+Zstate(count,1)*s+Zstate(count+1,1)*c;
        Xstate(count2+2,1)=wrap(currentpos(3)+Zstate(count+2,1));
        Xstate(count2:count2+2,2)=1;
        Xstate(count2:count2+2,3)=count4;
        count4=count4+1;
        
        currentpos=Xstate(count2:count2+2,1)'; % order changed by Shoudong
        savePos=currentpos; % order changed by Shoudong
        
        count=count+3;
        count2=count2+3;
    else
        if size(IDsaved,2)>0
            for a=1:size(IDsaved,2)
                if IDsaved(a)==Zstate(count,3)
                    checkIDsaved=0;
                    break;
                end
            end
        end
        
        if (checkIDsaved==1)
            s = sin(savePos(3));
            c = cos(savePos(3));
%             % pause
%             savePos
%             pause
            Xstate(count2,1)= savePos(1)+Zstate(count,1)*c-Zstate(count+1,1)*s;
            Xstate(count2+1,1)= savePos(2)+Zstate(count,1)*s+Zstate(count+1,1)*c;
            Xstate(count2:count2+1,2)=2;
            Xstate(count2:count2+1,3)=Zstate(count,3);
            count2=count2+2;
            IDsaved(count3)=Zstate(count,3);
            count3=count3+1;
        end
        checkIDsaved=1;
        count=count+2;
    end
    
end

end

function nu = wrap(alpha)

clear nu;
nu = alpha;

	while (nu > pi)
		nu = nu - 2 * pi;
	end;

	while (nu < -pi)
		nu = nu + 2 * pi;
	end;
end
