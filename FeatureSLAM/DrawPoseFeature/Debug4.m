clear all
close all
clc
 
load('Zstate_Simu_256.mat')
Xstate = FuncCreateXstateFromZstate(Zstate);

DrawXstate2D( Xstate)
PlotXstate2D( Xstate , 'ro--', 'k.');

XstateN = FuncCreateXstateFromZstate_2D(Zstate);

[Xstate XstateN];
DrawXstate2D( XstateN)
PlotXstate2D( XstateN , 'ro--', 'k.');

max(Xstate - XstateN)

