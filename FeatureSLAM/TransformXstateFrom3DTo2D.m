% This function is used to tranform feature based SLAM 3D solution to 2D
function [Xstate_2D] = TransformXstateFrom3DTo2D (Xstate_3D)

count3D = 1;
count2D = 1;

DataDim = size(Xstate_3D, 1);

while (count3D < DataDim)
    
    if(Xstate_3D(count3D, 2) == 1)
        
        Xstate_2D([count2D, count2D+1, count2D+2], [1,2,3]) = ...
            Xstate_3D([count3D, count3D+1, count3D+5], [1,2,3]);
        
        count3D = count3D + 6;
        count2D = count2D + 3;
        
    elseif(Xstate_3D(count3D, 2) == 2)
        
        Xstate_2D([count2D, count2D+1], [1,2,3]) = ...
            Xstate_3D([count3D, count3D+1], [1,2,3]);
        
        count3D = count3D + 3;
        count2D = count2D + 2;
        
    end
    
end

end