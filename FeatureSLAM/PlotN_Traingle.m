
function [ ] = PlotN_Traingle( N, Fid )

t1 = cputime;

vx = [];

vy = [];

Dim = size(N,1);

for i = 1 : Dim
    
    Odom1 = N(i, 1);
    
    Odom2 = N(i, 2);
    
    Odom = N(i, 1) : N(i, 2);
    
    vy = [vy; i*ones(size(Odom,2),1)];
    
    vx = [vx; Odom.'];   
    
end

LoopClosureNode = N(:,3);

figure(Fid)

clf

hold on

plot(vx, vy, '.');

plot(LoopClosureNode, [1 : Dim], '*');

hold off

title('N Triangle');

time_plot_N = cputime  - t1

pause(0.001)

end

