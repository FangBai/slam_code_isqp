% Get triangles that describe edges/peaks of constraints from odometry and observation data
% M_trian = [num_odometry [odom1 odm2...domK] observation1 observation2]  Namely edges
% N_trian = [pose1 pose2 feature]  Namely peaks
function [ M_trian, N_trian ] = GetTriangle3D( Zstate )

M_trian=cell(0);
N_trian=[];
count_tria=0;
cur_feature=1;

TriangleType2D_Euclidean = 3;
%TriangleType2D_SE2 = 6;
TriangleType3D_Euclidean = 6;
TriangleType3D_SE3 = 12;
LengthType2D = 2;
LengthType3D = 3;
% get all odometries
Odometry_data = find(Zstate(:,2)==1);
% check odometry data type
if (Zstate(Odometry_data(1), 4) == Zstate(Odometry_data(12), 4))
    TriangleType = TriangleType3D_SE3;
    LengthType = LengthType3D;
elseif (Zstate(Odometry_data(1), 4) == Zstate(Odometry_data(6), 4))
    TriangleType = TriangleType3D_Euclidean;
    LengthType = LengthType3D;
elseif (Zstate(Odometry_data(1), 4) == Zstate(Odometry_data(3), 4))
    TriangleType = TriangleType2D_Euclidean;
    LengthType = LengthType2D;
else
    disp('data error!');
    return;
end
% get the indices for odometry data
for n=1:size(Odometry_data,1)/TriangleType
    Odometry_line(n) = Odometry_data(TriangleType*(n-1)+1);
end
% all the feature indices
Max_index_feature=max(Zstate(find(Zstate(:,2)==2),3));
% try every feature if there are constraints related to it
% if yes, put the peaks into N_trian
% if yes, put the edges into M_trian by preserve the first line's index of edge in the data
while(cur_feature<=Max_index_feature)
fea_line = find(Zstate(:,2)==2 & Zstate(:,3)==cur_feature);
if(size(fea_line,1)>=2*LengthType)
       for i=1:LengthType:size(fea_line,1)
           if(i+LengthType<=size(fea_line,1))    
              count_tria = count_tria+1;
% get M_trian  
              N_trian(count_tria,1)= Zstate(fea_line(i),4);
              N_trian(count_tria,2)= Zstate(fea_line(i+LengthType),4);
              N_trian(count_tria,3)= cur_feature;
% get N_trian     
              num_odom = Zstate(fea_line(i+LengthType),4) - Zstate(fea_line(i),4);
              M_trian{count_tria}(1) = num_odom;
              for odo_count =1:num_odom
                 M_trian{count_tria}(odo_count+1) = Odometry_line(Zstate(fea_line(i),4)+odo_count);  
              end
              M_trian{count_tria}(num_odom+2) = fea_line(i);
              M_trian{count_tria}(num_odom+3) = fea_line(i+LengthType);               
           end
       end
       cur_feature = cur_feature+1;
elseif(size(fea_line,1)==LengthType)
       cur_feature = cur_feature+1;
elseif(size(fea_line,1)==0)
       cur_feature = cur_feature+1;
end
end
