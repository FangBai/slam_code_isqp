% This function is used to linearize constraints.
% A b  coefficient matrix and constant vector after linearization
% X0   the initial value of X given in SE(3) group/space
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% M_e    the cell structure got from Get_triangle_3D
%      M_e reflects indices for odometries and observation in variables
%      described in Euclidean space
%      M_e{i}: the i-th triangle
%      M_e{i}: num_odmometry od1 od2..odn ob1 ob2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% M_s    the cell structure got from Get_triangle_3D
%      M_s reflects indices for odometries and observation in variables
%      described in SE(3) group/space
%      M_s{i}: the i-th triangle
%      M_s{i}: num_odmometry od1 od2..odn ob1 ob2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ A, b ] = Linear_con_3D( X0, M_e, M_s, Variable_Indices )

count = 1;
VariableDim = 0;
while (count<= size(Variable_Indices ,1))
   if(Variable_Indices(count) ==1)
       VariableDim = VariableDim + 6;
       count = count+1;
   elseif(Variable_Indices(count) ==2)
       VariableDim = VariableDim + 3;
       count = count+1;
   end
end

num_trian1 = size(M_e,2);
num_trian2 = size(M_s,2);
if(num_trian1 ~= num_trian2)
    disp('M_e, M_s have different amount of constraints!');
    return
end
num_trian = num_trian1;
cur_trian = 1;
p_cur_trian = 1;
% define sparse matrix A
[NZMAX, NZMAX_LINE] = NumberCoefficientNonzeroElements ( M_e );

b = zeros(num_trian*3, 1);
R_cur = zeros(3,3);

% ID_i = [];
% ID_j = [];
% ID_v = [];

ID_i = size(NZMAX, 1);
ID_j = size(NZMAX, 1);
ID_v = size(NZMAX, 1);
ID_cnt = 1;

while(cur_trian<=num_trian)

%t1 = cputime;     
    
clear TmpA;
clear TmpB;     
TmpA = zeros(3, VariableDim);
TmpB = zeros(3,1);   

%%%%%%%%%%% read information from cell M_e, M_s
num_odom1 = M_e{cur_trian}(1);
num_odom2 = M_s{cur_trian}(1);
if(num_odom1 ~= num_odom2)
    disp(['For constraint with index ', num2str(cur_trian),', edge numbers in M_e, M_s are different!']);
    return
end
num_odom = num_odom1;
%odometry indices
for i=1:num_odom
    ode(i)= M_e{cur_trian}(i+1);
    ods(i)= M_s{cur_trian}(i+1);
end
%observation 1 index
obe1=M_e{cur_trian}(num_odom+2);
obs1=M_s{cur_trian}(num_odom+2);
%observation 2 index
obe2=M_e{cur_trian}(num_odom+3);
obs2=M_s{cur_trian}(num_odom+3);
%%%%%%%%% read information complete %%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%set coefficient for odometry position
TmpA(:, [ode(1), ode(1)+1, ode(1)+2]) = - eye(3);  
for i=2:num_odom
    R_cur = [ X0(ods(i-1)+3), X0(ods(i-1)+4), X0(ods(i-1)+5);
              X0(ods(i-1)+6), X0(ods(i-1)+7), X0(ods(i-1)+8);
              X0(ods(i-1)+9), X0(ods(i-1)+10), X0(ods(i-1)+11);];
    TmpA(:, [ode(i), ode(i)+1, ode(i)+2]) = ...
        TmpA(:, [ode(i-1), ode(i-1)+1, ode(i-1)+2])*R_cur;
end

%set coefficient for observation
TmpA(:, [obe1, obe1+1, obe1+2]) = eye(3);  
    R_cur = [ X0(ods(num_odom)+3), X0(ods(num_odom)+4), X0(ods(num_odom)+5);
              X0(ods(num_odom)+6), X0(ods(num_odom)+7), X0(ods(num_odom)+8);
              X0(ods(num_odom)+9), X0(ods(num_odom)+10), X0(ods(num_odom)+11);];   
TmpA(:, [obe2, obe2+1, obe2+2]) = ...
    TmpA(:, [ode(num_odom), ode(num_odom)+1, ode(num_odom)+2])*R_cur;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%set coefficient for odometry orientation/ minimal representation in Euclidean
    X_cur = [ X0(obs2); X0(obs2+1); X0(obs2+2)];
TmpA(:, [ode(num_odom)+3]) = X_cur; 
% compute sum(Di) recursively %%%%%%%%%%%%%%
for i= (num_odom-1): -1 :1
    X_cur = [ X0(ods(i+1)); X0(ods(i+1)+1); X0(ods(i+1)+2)];  
    R_cur = [ X0(ods(i+1)+3), X0(ods(i+1)+4), X0(ods(i+1)+5);
              X0(ods(i+1)+6), X0(ods(i+1)+7), X0(ods(i+1)+8);
              X0(ods(i+1)+9), X0(ods(i+1)+10), X0(ods(i+1)+11);];
TmpA(:, [ode(i)+3]) = ... 
    R_cur * TmpA(:, [ode(i+1)+3]) +  X_cur;
end
% compute Ci*sum(Di)
for i=1:(num_odom-1)
    TmpA(:, [ode(i)+3, ode(i)+4, ode(i)+5]) = ...
        - TmpA(:, [ode(i+1), ode(i+1)+1, ode(i+1)+2]) ... 
        * SkewSem(TmpA(:, [ode(i)+3]));
end
TmpA(:, [ode(num_odom)+3, ode(num_odom)+4, ode(num_odom)+5]) = ...
        - TmpA(:, [obe2, obe2+1, obe2+2]) ... 
        * SkewSem(TmpA(:, [ode(num_odom)+3]));
%%%%    
%tc1 = cputime;

% ########################################################################

[tmp_i, tmp_j, tmp_v] = find(TmpA);

tmp_i = tmp_i + 3*cur_trian - 3;

% ID_i = [ID_i; tmp_i];
% ID_j = [ID_j; tmp_j];
% ID_v = [ID_v; tmp_v];

ID_num = size(tmp_i, 1);

ID_i([ID_cnt : ID_cnt+ID_num-1], 1) = tmp_i;
ID_j([ID_cnt : ID_cnt+ID_num-1], 1) = tmp_j;
ID_v([ID_cnt : ID_cnt+ID_num-1], 1) = tmp_v;

ID_cnt = ID_cnt + ID_num;
% ########################################################################


%ts1 = cputime;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%compute vector b
TmpB = [0;0;0];
for i=1:num_odom
    X_cur = [X0(ods(i)); X0(ods(i)+1); X0(ods(i)+2)];
    TmpB = TmpB - TmpA(:, [ode(i), ode(i)+1, ode(i)+2])* X_cur;
end
    X_cur = [X0(obs2); X0(obs2+1); X0(obs2+2)];
TmpB = TmpB - TmpA(:, [obe2, obe2+1, obe2+2])* X_cur;
    X_cur = [X0(obs1); X0(obs1+1); X0(obs1+2)];
TmpB = TmpB - X_cur;
%%%%
b([3*cur_trian-2, 3*cur_trian-1, 3*cur_trian],1) = TmpB; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cur_trian = cur_trian + 1;

end



% NZMAX
% ID_i_size = size(ID_i)
% ID_j_size = size(ID_j)

UsedMemo_i = find(ID_i);
UsedMemo_j = find(ID_j);

if(size(UsedMemo_i,1) == size(UsedMemo_j,1))

A = sparse(ID_i(UsedMemo_i), ID_j(UsedMemo_j), ID_v(UsedMemo_i));

else
    disp('ID allocate error');
    UsedMemo_i
    UsedMemo_j
end

end

function [NZMAX, NZMAX_LINE] = NumberCoefficientNonzeroElements ( M_e )
num_trian = size(M_e,2);
NZMAX = 0;
NZMAX_LINE = 0;

for count = 1:num_trian
    
    num_odom = M_e{count}(1);    
    num_trans = 18*num_odom + 18;
    
    if(NZMAX_LINE < num_trans)
        NZMAX_LINE = num_trans;
    end
    
    NZMAX = NZMAX + num_trans;
    
end

end


