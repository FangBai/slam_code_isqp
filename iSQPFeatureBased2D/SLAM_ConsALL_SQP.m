%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Res_SQP_All, Fval_SQP_All] = SLAM_ConsALL_SQP ( Xinitial, Zstate, CovMatrixInv)
%disp('SQP method by adding all constraints at once is processing!')


P = inv(CovMatrixInv);
w = Zstate(:,1);
% Get constraints/ triangles from the data
[M N] = Get_triangle( Zstate);
% Get Angle rows
Ang_index = Get_Angle_Row(Zstate);
%%%%%%%%%%%%%%%%%%% Solve the probem by SQP method %%%%%%%%%%%%%%%
[X Fval_SQP_All] = My_method_solve(Xinitial, w, P, N, Ang_index);

%Con = Nonlinear_con(X, N );       %
%Con_value= max(abs(Con));          %
%%%%%%%%%%%%%%%%%%%%%%% Get optim_data ( optimum of data ) %%%%%%%%%%%%%%%%%%%
Optim_data=Zstate;
for i=1:size(Optim_data,1)
    Optim_data(i,1)=X(i);
end

%%%%%%%%%%%%%%%%%%%%%%%%%  Get poses and features  %%%%%%%%%%%%%%%%%%%%%%%%
% Get result from optim_data
Res_SQP_All  = FuncCreateXstateFromZstate(Optim_data);    
Res_SQP_All  = Wrap_result(Res_SQP_All);

%SQPAll = CreateZstateFromXstate ( Res_SQP_All, Zstate);   %
%Con = Nonlinear_con(X, N );       %
%Con_value= max(abs(Con))          %

end



