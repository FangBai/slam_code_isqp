function [A] = R(phi)
%%%%%%%  Get rotation matrix A with respect to angle phi %%%%%%%%%%%%%
A=[cos(phi),-sin(phi);sin(phi),cos(phi)];
end

