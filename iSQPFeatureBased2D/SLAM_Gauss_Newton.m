%
function [Res_Gauss_New, Fval_Gauss_New] = SLAM_Gauss_Newton ( Xstate_0, Zstate, CovMatrixInv)

disp('Gauss Newton method is processing!')

%%%%%%%%%%%  Solve problem with Gaussian Newton algorithm
%%%%%%%%%%%%%%%% Gauss_Newton_SLAM %%%%%%%%%%%%%%%%
time_start_LS = cputime;

% parameters for convergence
MinChi2Error = 1e-6;
MinDelta = 1e-6;
MinGradient = 1e-6;
Max_iteration = 100;


% initial state value
Xstate=Xstate_0;

% reason for converge
stop_sign = -1; % maximal iteration number reached, 1-gradient, 2-delta, 3-chi2error

stop = 0;

% begin LS iteration without LM

iteration = 1;

while stop<1 && iteration<Max_iteration

    % compute the F(X0) and Jacobian J0

    [FX0 J0] = FuncComputeJacobian_new(Zstate,Xstate);

   % compute the error

    [error, chi2_error] = FuncComputeError(FX0,Zstate,CovMatrixInv);

    %        pause(1)

    ATC = J0'*CovMatrixInv;
    ZLSS = ATC*error;
    % for checking convergence
    gradient = J0'*CovMatrixInv*error;

    InfoMatrix = ATC*J0;
    
    clear ATC J0 

    Xstate_delta=(InfoMatrix\ZLSS);

    
    
    clear ZLSS
  
    Xstate(:,1)=Xstate(:,1)+Xstate_delta;
   % if chi2_error<51.31 && chi2_error>51.30
%Xstate_delta
%chi2_error   %##
   % pause
  %  end

    if (norm(gradient,inf)<MinGradient)
        stop = 1;
        stop_sign = 1;
    elseif chi2_error<MinChi2Error
        stop = 1;
        stop_sign = 3;
    elseif norm(Xstate_delta,2)<MinDelta*norm(Xstate(:,1))
        stop = 1;
        stop_sign = 2;
    else
        stop = 0;
    end
    %save Xstate Xstate
    iteration = iteration+1;
    chi2_error_LS = chi2_error;
    if( iteration == Max_iteration)
        disp('-> Warning: Gauss Newton method has reach the maximum iteration. The result may not be accurate any more!')
    end 

    %            save Xstate Xstate
end


Xstate_LS=Xstate;
iteration_LS=iteration;
stop_sign_LS = stop_sign;
chi2_error_LS = chi2_error;
error_LS = error;

time_used_LS = cputime-time_start_LS;

%%%%%%%%%%%%%% Wrap the orientation in the SLAM result %%%%%%%%%
% Gauss Newton Result
Res_Gauss_New = Wrap_result(Xstate_LS);
% Gauss Newton Objective
Fval_Gauss_New = chi2_error_LS;

end

