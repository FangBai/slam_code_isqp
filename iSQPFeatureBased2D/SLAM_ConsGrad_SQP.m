function [Res_SQP_Gra, Fval_SQP_Gra] = SLAM_ConsGrad_SQP ( Xinitial, Zstate, CovMatrixInv)
%disp('SQP method by adding constraints gradually is processing!')
Cons_threshold = 1e-6;
P = inv(CovMatrixInv);
w = Zstate(:,1);
% Get constraints/ triangles from the data
[M_all N_all] = Get_triangle( Zstate);
% Get Angle rows
Ang_index = Get_Angle_Row(Zstate);
%%%%%%%%%%%%%%%%%%% Initialization %%%%%%%%%%%%%%%%%%%
N = cell(0);
con_count = 1;  
Con_amount = max(size(N_all));
%%%%%%%%%%%%%%% Compute C(X) and see whether Xinitial is an optimum
Con_value = Nonlinear_con(Xinitial, N_all ); 
Con_value_ini = max(abs(Con_value));
% indicate whether Xinitial is an optimum or not
Optim = 0; 
% 0 means it is not an optimum
% 1 means it is an optimum
if(Con_value_ini < Cons_threshold)
    % if Xinitial satisfy the constraints, then it is an optimum
    disp('-> Notice: Input Xinitial is an optimum solution!')
    Optim = 1;
    F_initial = (Xinitial - w).'*CovMatrixInv*(Xinitial - w);
    X_optim = Xinitial;
    F_optim = F_initial;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%% Adding Control parameters %%%%%%%%%%%%%%%%%%%%
FLUC_RANGE = 10;
TIMES = 100;
WINDOW_MIN = round(Con_amount/ TIMES);
WINDOW_MAX = round(WINDOW_MIN*10);
%%%%%%%%%%%%%%%%%% Different Ways to Add Constriants %%%%%%%%%%%%%%
Add_option =2; 
% 1 ----by number of odometries
% 2 ----by data quality
% 3 ----by odometry quantity, with certain con_num each time 
% 0 ----random sequence with certain con_num each time
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%
% Add_option  = 1
if(Add_option ==1)
% get odometries of constraints
for k = 1:Con_amount
    tmp(k) = N_all{k}(1);
end
% sort constraints in ascending order by the number of odometries
%[Odo_amount Cons_SEQUENCE] = sort(tmp,'ascend');
% sort constraints in descending order by the number of odometries
[Odo_amount Cons_SEQUENCE] = sort(tmp,'descend');
end
%%%%%%%%%%%%%%%%
% Add_option  = 2
if(Add_option ==2)

end
%%%%%%%%%%%%%%%%
% Add_option  = 3
if(Add_option ==3)
% get odometries of constraints
for k = 1:Con_amount
    tmp(k) = N_all{k}(1);
end
% sort constraints in ascending order by the number of odometries
%[Odo_amount Cons_SEQUENCE] = sort(tmp,'ascend');
% sort constraints in descending order by the number of odometries
[Odo_amount Cons_SEQUENCE] = sort(tmp,'descend');
con_num = 4;
end

%%%%%%%%%%%%%%%%
% Add_option  = 0
if(Add_option ==0)
% random sequence of constraints
Cons_SEQUENCE = randperm(Con_amount); 
con_num = 4;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% chang weight here
% weight = 1;

while (con_count <= Con_amount)
    

   %%%%%%%%%%%%%%%%%%% SELECCT CONSTRAINTS  %%%%%%%%%%%%%%%%%%% 
   % select a small portion of constraints from N_all to N  
    
   if(Add_option ==1) 
   % get the number of constraints to be added each time by the quantity of odometries
   % control the quantity of constraints added each time by con_num   
     con_num = 1;
     tmp = con_count;
        while ( tmp < Con_amount && Odo_amount(tmp) == Odo_amount(tmp + 1))
           tmp = tmp + 1;
           con_num = con_num + 1;
        end
   % con_num
   % constraints with same quantity of odometries are added into the problem
   end

   
   if(Add_option ==2) 
   % get constraints norm and SEQUENCE
     Con_value = Nonlinear_con(Xinitial, N_all );
     Con_value = abs(Con_value);
     for k = 1:Con_amount
     Con_norm(k) = sqrt(Con_value(2*k-1)^2 + Con_value(2*k)^2);
     end
   % sort constraints in ascending order by the value of constraints
      [Con_norm Cons_SEQUENCE] = sort(Con_norm,'ascend');
   % sort constraints in descending order by the value of constraints
   %  [Con_value Cons_SEQUENCE] = sort(Con_norm,'descend'); 
   
   end
   % 

   if(Add_option ==2) 
   % Get the quantity of constraints to be added into the problem each time
   % By the value of constraints
   % Get ratio between consecutive constraint value
     Mean = sum(Con_norm(con_count:Con_amount))/(Con_amount - con_count +1 );
     Con_ratio (con_count) = 1;     
     for k = con_count+1: Con_amount
          Con_ratio (k) = Con_norm(k)/Con_norm(con_count);
     end
   % Criteria to decide con_num     
     con_num = 1;    
     tmp = con_count+1;
     %Criteria
        while ( tmp <= Con_amount && Con_ratio(tmp) < 1.618 && Con_norm(tmp)< Mean )
           tmp = tmp + 1;
           con_num = con_num + 1;
        end

   end  
   
   % Additional laws for adding constraints
   % Window technqiues
   % Fluctuation techniques
   if (Add_option ==102) 
       % Fluctuation rate and stage
       fluctuation_rate = Con_norm(Con_amount)/Con_norm(con_count);
       fluctuation_stage = fluctuation_rate/FLUC_RANGE;
       fluctuation_stage = max(fluctuation_stage, 1);
       % Accelarate adding process
       Accelerator = round(WINDOW_MIN/fluctuation_stage);
       con_num = con_num + Accelerator;                
       % Maximum number of added constaints
       Decelerator = round(WINDOW_MAX/fluctuation_stage);
       Decelerator = max(Decelerator, 1);
       con_num = min(con_num, Decelerator);     
   % Assign constant/fixed number to con_num     
    con_num = 1;              
   end
    
%%%%%%%%%% FOR TESTING CODE  %%%%%%%%%%%%%%%%%%%%
 if(1)
 %  clc
  % Con_norm
 %  Cons_SEQUENCE
  [con_count con_num Con_amount];
  [Con_norm(con_count) Con_norm(Con_amount)];
   disp(['con_count con_num Con_amount ConNorm_min ConNorm_max MAX_MIN_ratio  ' , num2str(con_count), ...
       '  ' ,num2str(con_num),'  ', num2str(Con_amount), '  ', num2str(Con_norm(con_count)), '  ', ...
       num2str(Con_norm(Con_amount)),'  ', num2str(Con_norm(Con_amount)/Con_norm(con_count))])
 end
  % pause
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
   if(Add_option ==0 | Add_option ==1 | Add_option ==2 | Add_option ==3)
   % the constraints left
     con_res = Con_amount - con_count + 1;
   % add constraints into N
     if(con_res < con_num)
        for k = 0:con_res-1
        N{con_count + k} = N_all {Cons_SEQUENCE(con_count+k)};   
        end        
     else
        for k = 0:con_num-1
        N{con_count + k} = N_all {Cons_SEQUENCE(con_count+k)};   
        end
     end
   end
    
   %%%%%%%%%%%%%%%%%%% SELECCT CONSTRAINTS END %%%%%%%%%%%%%%%%%%% 
    
    %Display adding constraints process
    Constraint_total = max(size(N_all));
    Constraint_added = max(size(N));
    str = ['Constraint_add/Constraint_total=', num2str(Constraint_added), '/', num2str(Constraint_total)];
    str = ['Constraint_add/Constraint_total=', num2str(Constraint_added), '/', num2str(Con_amount)];
   % disp(str);    

   %%%%%SQP method to solve SLAM problem  %%%%%%%
    [X Fval_SQP_Gra] = My_method_solve(Xinitial, w, P, N, Ang_index);

  % Set Xinital using X computed
    % Add weight with covariance matrix
    % weight represent the relevance of new value and old vaules
    
  %  Xinitial = weight*CovMatrixInv*X + (1-weight)*CovMatrixInv*Xinitial;
    Xinitial = X ;
    con_count = con_count + con_num;
    
% If Xinitial is an optimum   
% check whether the solution computed is better than Xinitial or not   
  if(Optim && Fval_SQP_Gra> F_optim)
          X = X_optim;
          Fval_SQP_Gra = F_optim;
          disp('worse solution')
          break
  end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end

%Get optim_data ( optimum of data )
Optim_data=Zstate;  %get the same dimension
for i=1:size(Optim_data,1)
    Optim_data(i,1)=X(i);
end

% Get result from optim_data
Res_SQP_Gra = FuncCreateXstateFromZstate(Optim_data);    
Res_SQP_Gra = Wrap_result(Res_SQP_Gra);

 end




