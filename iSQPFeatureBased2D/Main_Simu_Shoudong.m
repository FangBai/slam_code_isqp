clc
clear all
close all

%%%%%%%%%%%%% Set number of trials here %%%%%%%%%%%%%%%%
Trial_num = 500;   % Total number of trials
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Trial_count = 1;
    
Same = 0;
SQP_All = 0;
SQP_Gra = 0;
G2O_GN = 0;
All_only = 0;
Gra_only = 0;
GN_only = 0;

Diff = 1;

while (Trial_count <= Trial_num)
    
t1=cputime;
%%%%%%%% Simulatioin %%%%%%%%%%%
MainLoop;
load Zstate_Simu_256 Zstate

%%%%%%%%%%%%%%%%%% Get data %%%%%%%%%%%%%%%%%%%%%%%%
% select Testdata dataset
Zstate = Wrap_result(Zstate);
% set covariance matrix by Identity matrix
Data_scale = size(Zstate,1);
CovMatrixInv = eye(Data_scale);
%%%%%%%%%%%%%%%%% Set initial value %%%%%%%%%%%%%%%%%
% set initial value with observation and odometry data
Xinitial = Zstate(:,1);
Xstate_0 = FuncCreateXstateFromZstate(Zstate);
ss = size(Xstate_0,1);

%%%%%%%%%%%%%%% Solve the problem by different methods %%%%%%%%%%%%
% solve by Gauss Newton 
[Res_G2O_GN, Fval_G2O_GN] = SLAM_G2O_GN ( Xstate_0, Zstate, CovMatrixInv);
% solve by SQP (adding constraints gradually)
[Res_SQP_Gra, Fval_SQP_Gra] = SLAM_ConsGrad_SQP ( Xinitial, Zstate, CovMatrixInv);
% solve by SQP (adding all constraints at once)
[Res_SQP_All, Fval_SQP_All] = SLAM_ConsALL_SQP ( Xinitial, Zstate, CovMatrixInv);
%%%%%%%%%%%%%%% Solutions and Objectives %%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%  end %%%%%%%%%%%%%%%%%%%%
[max(Res_G2O_GN(:,1) - Res_SQP_Gra(:,1)), max(Res_G2O_GN(:,1) - Res_SQP_All(:,1)),max(Res_SQP_Gra(:,1) - Res_SQP_All(:,1))]
[Fval_G2O_GN Fval_SQP_Gra Fval_SQP_All]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
threshold = 1e-2;

if ( abs(Fval_SQP_Gra - Fval_SQP_All) < threshold & abs(Fval_SQP_Gra - Fval_G2O_GN) < threshold & abs(Fval_SQP_All - Fval_G2O_GN)< threshold)
str = '------';
Same = Same + 1;
elseif (abs(Fval_SQP_Gra - Fval_SQP_All) < threshold)
    if(Fval_G2O_GN < Fval_SQP_Gra )
        str = 'G2O_GN  $';
        G2O_GN = G2O_GN + 1;
        GN_only = GN_only +1;
    else
        str = 'SQP_All_Gra--';     
        SQP_All = SQP_All + 1;    
        SQP_Gra = SQP_Gra + 1;       
    end
elseif ( abs(Fval_SQP_Gra - Fval_G2O_GN) < threshold)   
    if( Fval_SQP_All < Fval_SQP_Gra )
        str = 'SQP_All  #';   
        SQP_All = SQP_All + 1;
        All_only = All_only + 1;
    else
        str = 'Gra_GN--';     
        G2O_GN = G2O_GN + 1;      
        SQP_Gra = SQP_Gra + 1;       
    end    
elseif ( abs(Fval_SQP_All - Fval_G2O_GN)< threshold)    
    if( Fval_SQP_Gra < Fval_SQP_All )
        str = 'SQP_Grad  <+>';
        SQP_Gra = SQP_Gra + 1;
        Gra_only = Gra_only + 1; 
    else
        str = 'All_GN--';     
        G2O_GN = G2O_GN + 1;      
        SQP_All = SQP_All + 1;       
    end      
elseif(Fval_SQP_All < Fval_G2O_GN)
        if(Fval_SQP_Gra < Fval_SQP_All  )
           str = 'SQP_Grad  <+>';           
           SQP_Gra = SQP_Gra + 1;
           Gra_only = Gra_only + 1;           
        else
           str = 'SQP_All  #';   
           SQP_All = SQP_All + 1;
           All_only = All_only + 1;           
        end
elseif(Fval_G2O_GN < Fval_SQP_All)
        if(Fval_SQP_Gra < Fval_G2O_GN )
           str = 'SQP_Grad  <+>';
           SQP_Gra = SQP_Gra + 1;
           Gra_only = Gra_only + 1;
        else
           str = 'G2O_GN  $';
           G2O_GN = G2O_GN + 1; 
           GN_only = GN_only +1;         
        end        
end

        if(1)
            
if ( abs(Fval_SQP_Gra - Fval_SQP_All) > threshold | abs(Fval_SQP_Gra - Fval_G2O_GN) > threshold | abs(Fval_SQP_All - Fval_G2O_GN) > threshold)       
 %%%%%%%%%%%%%%%%%%%%%% Plot the result %%%%%%%%%%%%%%%%%%%%%%%%%
figure(Diff)
hold on
title('Comparsion of feature and pose positions', 'fontsize',15);
xlabel('x axis/coordinate','fontsize', 12);
ylabel('y axis/coordinate','fontsize', 12);
[h1 h2] = Plot_Xstate(Res_SQP_Gra,'rv:','r.');
[h3 h4] = Plot_Xstate(Res_SQP_All,'gd:','gx');
[h5 h6] = Plot_Xstate(Res_G2O_GN,'ys:','y+');
hold off
legend([h1 h3 h5 h2 h4 h6],'SQP-Gra-Pose', 'SQP-All-Pose', 'G2O-GN-Pose', 'SQP-Gra-Feature', 'SQP-All-Feature', 'G2O-GN-Feature', 'Location','Best');
pause(1)       
        
      load ResSimuSh Res Fva   
       
       Res(Diff).g2o = Res_G2O_GN;
       Res(Diff).gra = Res_SQP_Gra;
       Res(Diff).all = Res_SQP_All;            
       Fva(Diff).g2o = Fval_G2O_GN;
       Fva(Diff).gra = Fval_SQP_Gra;
       Fva(Diff).all = Fval_SQP_All;

       save ResSimuSh Res Fva
       Diff = Diff +1;
end
        end


t2 = cputime;
Time_Elapsed =t2 -t1;

disp(['Trial->', num2str(Trial_count),':  ','The best method is -> ', str, '    Time ->', num2str(Time_Elapsed)])
Trial_count = Trial_count +1;

end

% save performance 
save Performance Trial_num Same ...
    SQP_All  SQP_Gra  G2O_GN All_only Gra_only GN_only

%%%%%%%%%%%%%%%%%%%%% Display performance %%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('----------------------- Peformance of Each Method ---------------------------')
disp(['Total trials -> ', num2str(Trial_num), '  times'])
disp(['Same Result -> ', num2str(Same), '  times'])
disp(['Different Results -> ', num2str(Trial_num-Same), '  times'])
disp(['SQP_Gra find best solution -> ', num2str(SQP_Gra), '  times'])
disp(['SQP_All find best solution -> ', num2str(SQP_All), '  times'])
disp(['G2O_GN find best solution -> ', num2str(G2O_GN), '  times'])
disp(['Only SQP_Gra find best solution -> ', num2str(Gra_only), '  times'])
disp(['Only SQP_All find best solution -> ', num2str(All_only), '  times'])
disp(['Only G2O_GN find best solution -> ', num2str(GN_only), '  times'])



