function [ Xstate ] = CreateXstateFromG2OData( File_Path_Name, pose_index, feature_index )

% read g2o format data
% create Xstate formate reuslt from g2o data/result

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fid = fopen(File_Path_Name,'r');
g2oLine = fgetl(fid);

Xstate = [];

while g2oLine(1) ~=-1
    
        lineStr = textscan(g2oLine, '%s');
        if strcmp(lineStr{1}{1},'VERTEX_SE2')& strcmp(lineStr{1}{2},'0')
            pose0_x = str2num(lineStr{1}{3});
            pose0_y = str2num(lineStr{1}{4});
            pose0_phi =  str2num(lineStr{1}{5});
               
        elseif strcmp(lineStr{1}{1},'VERTEX_SE2')& ~strcmp(lineStr{1}{2},'0') 
            pose_vertex = str2num(lineStr{1}{2});
            pose_x = str2num(lineStr{1}{3}) - pose0_x;
            pose_y = str2num(lineStr{1}{4}) - pose0_y;
            pose_phi = wrap(str2num(lineStr{1}{5}) - pose0_phi);
            
            pose_i = find(pose_index == pose_vertex);
            
            tmp_pose(1,:) = [pose_x 1 pose_i];
            tmp_pose(2,:) = [pose_y 1 pose_i];   
            tmp_pose(3,:) = [pose_phi 1 pose_i];
            
            tmp_pose([1 2],1) = R(pose0_phi).'*tmp_pose([1 2],1);
            
            Xstate = [Xstate; tmp_pose];
            

        elseif strcmp(lineStr{1}{1},'VERTEX_XY')
            feature_vertex = str2num(lineStr{1}{2});
            feature_x = str2num(lineStr{1}{3}) - pose0_x;
            feature_y = str2num(lineStr{1}{4}) - pose0_y;
         
            feature_j = find(feature_index == feature_vertex);
              
            tmp_feature(1,:) = [feature_x 2 feature_j];
            tmp_feature(2,:) = [feature_y 2 feature_j];
            
            tmp_feature([1 2],1) = R(pose0_phi).'*tmp_feature([1 2],1);
            
            Xstate = [Xstate; tmp_feature];
            
        end
        g2oLine = fgetl(fid);   
        
end

fclose(fid);

end

