function [ A b ] = Linear_con( Xi, M )

% Xi   the initial value of X
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% M    the cell structure got from Get_triangle
%      M{i}: the i-th triangle
%      M{i}: num_odmometry od1 od2..odn ob1 ob2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

num_trian = size(M,2);
n = max(size(Xi));
cur_trian = 1;

%%%%%%%%%%%%%%%% define sparse matrix A
A = sparse(1,n);

while(cur_trian<=num_trian)
%%%%%%%%%%% read information from cell M 
num_odom = M{cur_trian}(1);
%%%%%%%%% odometry 1...k
for i=1:num_odom
    od(i)= M{cur_trian}(i+1);
end
%%%%%%%%%observation 1
ob1=M{cur_trian}(num_odom+2);
%%%%%%%%% observation 2
ob2=M{cur_trian}(num_odom+3);
%%%%%%%%% read information complete

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Get matrix A %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%% set values for positions  xy %%%%%%%%%%
%%%%%%%%%%% set value for odometries xy 
A([2*cur_trian-1;2*cur_trian],[od(1);od(1)+1]) = eye(2);
for i=2:num_odom
    A([2*cur_trian-1;2*cur_trian],[od(i);od(i)+1]) = A([2*cur_trian-1;2*cur_trian],[od(i-1);od(i-1)+1])*R(Xi(od(i-1)+2));
end
%%%%%%%%%%% set value for observation 
A([2*cur_trian-1;2*cur_trian],[ob1;ob1+1]) = -eye(2);
A([2*cur_trian-1;2*cur_trian],[ob2;ob2+1]) = A([2*cur_trian-1;2*cur_trian],[od(num_odom);od(num_odom)+1])*R(Xi(od(num_odom)+2));

%%%%%%%%%%%%%%%% compute values for orientatons/angle %%%%%%%%%%%%%%%

A([2*cur_trian-1;2*cur_trian],od(num_odom)+2)= A([2*cur_trian-1;2*cur_trian],[ob2;ob2+1])* Xi([ob2 ob2+1]);
for i= num_odom-1:-1:1
    A([2*cur_trian-1;2*cur_trian],od(i)+2) = A([2*cur_trian-1;2*cur_trian],od(i+1)+2) + A([2*cur_trian-1;2*cur_trian],[od(i+1) od(i+1)+1])* Xi([od(i+1);od(i+1)+1]);                           
end

for i=1:num_odom
    A([2*cur_trian-1;2*cur_trian],od(i)+2)= Rd(Xi(od(i)+2))* R(Xi(od(i)+2)).'* A([2*cur_trian-1;2*cur_trian],od(i)+2);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Get vector b %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
b([2*cur_trian-1;2*cur_trian],:) = zeros(2,1);
%%%%%%%%%%%%%%%
for i=1:num_odom
    b([2*cur_trian-1;2*cur_trian])= b([2*cur_trian-1;2*cur_trian]) + A([2*cur_trian-1;2*cur_trian],od(i)+2)* Xi(od(i)+2);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cur_trian = cur_trian+ 1;
end


end

