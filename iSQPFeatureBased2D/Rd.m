function [ A ] = Rd( x )
%%%%%%%%%%%%%%% The derivation of matrix R

A=[-sin(x) -cos(x); cos(x) -sin(x)];

end

