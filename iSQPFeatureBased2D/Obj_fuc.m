function [ F] = Obj_fuc(Xstate,Zstate,CovMatrixInv)
%%%%%%%%%%%%%%%%%%% Compute Objectives for Gauss Newton %%%%%%%%%%%%%%%%%%
%Compute Objective from Xstate, Zstate and CovMatrixInv
%Extract optimum of poses and features from Xstate_LS
Odom_matrix=Xstate(find(Xstate(:,2)==1),:);
num_pose=size(Odom_matrix,1)/3;
pose=cell(1,num_pose);
count_i=1;
while(count_i<=size(Odom_matrix,1))
pose{round(Odom_matrix(count_i,3))}=[Odom_matrix(count_i,1);Odom_matrix(count_i+1,1);Odom_matrix(count_i+2,1)];
count_i=count_i+3;
end

Obse_matrix=Xstate(find(Xstate(:,2)==2),:);
num_feature=size(Obse_matrix,1)/2;
feature=cell(1,num_feature);
count_i=1;
while(count_i<=size(Obse_matrix,1))
feature{Obse_matrix(count_i,3)}=[Obse_matrix(count_i,1);Obse_matrix(count_i+1,1)];
count_i=count_i+2;
end

% Compute odometries and observations
% stored in OZ_X as the same sequence with Zstate
size_data = size( Zstate,1);
i=1;
while (i<=size_data)
    if (Zstate(i,2)==1)
        if(Zstate(i,4) ==0)
        tmp = R(0).'*([pose{Zstate(i,3)}(1) - 0;pose{Zstate(i,3)}(2) - 0]);
        OZ_X(i,1) = tmp(1);
        OZ_X(i+1,1) = tmp(2);
        OZ_X(i+2,1) = wrap(pose{Zstate(i,3)}(3) - 0);              
        else
        tmp = R(pose{Zstate(i,4)}(3)).'*([pose{Zstate(i,3)}(1) - pose{Zstate(i,4)}(1);pose{Zstate(i,3)}(2) - pose{Zstate(i,4)}(2)]);
        OZ_X(i,1) = tmp(1);
        OZ_X(i+1,1) = tmp(2);
        OZ_X(i+2,1) = wrap(pose{Zstate(i,3)}(3) - pose{Zstate(i,4)}(3));
        end
        i = i + 3;
    elseif (Zstate(i,2)==2)
        if(Zstate(i,4) == 0)
        tmp = R(0).'*([feature{Zstate(i,3)}(1) - 0;feature{Zstate(i,3)}(2) - 0]);
        OZ_X(i,1) = tmp(1);
        OZ_X(i+1,1) = tmp(2);                
        else
        tmp = R(pose{Zstate(i,4)}(3)).'*([feature{Zstate(i,3)}(1) - pose{Zstate(i,4)}(1);feature{Zstate(i,3)}(2) - pose{Zstate(i,4)}(2)]);
        OZ_X(i,1) = tmp(1);
        OZ_X(i+1,1) = tmp(2);
        end
        i = i + 2;
    end
 
end

F=0;
i=1;
num_data=size(Zstate,1);

Fk=@(b,P) b.'*P*b;

while(i<=num_data)
 if(Zstate(i,2)==1)
     b = [OZ_X(i);OZ_X(i+1);OZ_X(i+2)]-[Zstate(i,1);Zstate(i+1,1);Zstate(i+2,1)];
     % warp angel difference     
     b(3,1) = wrap(b(3,1)); %%%%
     F=F+Fk(b, CovMatrixInv(i:i+2,i:i+2));
     i=i+3;
 elseif(Zstate(i,2)==2)
     F=F+Fk(([OZ_X(i);OZ_X(i+1)]-[Zstate(i,1);Zstate(i+1,1)]),CovMatrixInv(i:i+1,i:i+1));
     i=i+2;
 end 

end

end




