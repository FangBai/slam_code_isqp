function [ X_value F_value ] = My_method_solve( Xinitial, w, P, N, Ang_index )


%%%%%%%%%%%%%%%%% Set pricision for computation %%%%%%%%%%%%%%%%%
Max_error = 1e-6;
Fval_Error = 1e-6;
Cons_error = 1e-6;

%%%%%%%%%%%%%%%% initial value of the Objective %%%%%%%%%%%%%%%%%%
% Deal with the angle data
% Wrap the angle data  
X_w = Xinitial - w;
for k = 1: max(size(Ang_index))
    Xinitial( Ang_index(k)) = wrap( Xinitial( Ang_index(k)));
    X_w( Ang_index(k)) = wrap( X_w( Ang_index(k)));    
end
% Objective after warping the angle data
F_initial = X_w.'*(P\X_w);
%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Get linear constraints AX = b
[A b] = Linear_con( Xinitial, N);
% Solve the linear optimization problem
[X_value F_value ] = Equilibrium_Direct_Method (w, P, A, b, Ang_index);
% Compute nonlinear constraint value
Con_value = Nonlinear_con(X_value, N );


% weight = 1;
Max_Iteration = 50;
Iteration_count = 1;
    

% disp('Iteration     X-error      C-error     F-errro')%
    %Xe = norm(Xinitial - X_value); %
    %Ce = norm(Con_value);                %
    %Fe = abs(F_value-F_initial); %
    %Diff_IN = [Iteration_count Xe Ce Fe]
    %pause  %
   
   
while(norm(Xinitial - X_value)>Max_error | max(abs(Con_value))>Cons_error | abs(F_value-F_initial)>Fval_Error )
    if ( Iteration_count <= Max_Iteration)
            
    t0 = cputime;
    
    
  %  Xe = norm(Xinitial - X_value); %
  %  Ce = norm(Con_value);                %
  %  Fe = abs(F_value-F_initial); %
  %  Diff_IN = [Iteration_count Xe Ce Fe]
    %pause  %
 %   F_value
    
    % add weight here
   % Xinitial = weight*inv(P)*X_value + (1-weight)*inv(P)*Xinitial;
     Xinitial = X_value;
    F_initial = F_value;
    
%Get linear constraints A and b
[A b] = Linear_con( Xinitial, N);  
   
    t1 = cputime;
 
%Solve the linear optimization problem
%By Direct Method
[X_value F_value ] = Equilibrium_Direct_Method (w, P, A, b, Ang_index);


% Compute nonlinear constraint value
Con_value = Nonlinear_con(X_value, N );
   
   
   t2 = cputime;
   Time_linearization = t1 - t0;
   Time_computation = t2 - t1;
   Iteration_count = Iteration_count + 1;

  
   
  else
        disp('-> warning: My_method_solve has reach maximum iteration. The reuslt may be not accurate any more!')
        break
  end

end

  % Iteration_count  
F_value = full(F_value);


end

