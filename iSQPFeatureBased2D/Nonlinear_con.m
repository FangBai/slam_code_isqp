function [ CEQ ] = Nonlinear_con(X,N_trian )

num_trian = size(N_trian,2);
cur_trian = 1;
while(cur_trian<=num_trian)
    
num_odom = N_trian{cur_trian}(1);
%%%%%%%%% odometry 1...k
for count=1:num_odom
    od(count)= N_trian{cur_trian}(count+1);
end
%%%%%%%%%observation 1
ob1=N_trian{cur_trian}(num_odom+2);
%%%%%%%%% observation 2
ob2=N_trian{cur_trian}(num_odom+3);
%%%%%%%%%%%%%%%
cur_odom = num_odom;

   F = [X(od(cur_odom));X(od(cur_odom)+1)] + R(X(od(cur_odom)+2))*[X(ob2);X(ob2+1)];

while( cur_odom~=1)
     cur_odom = cur_odom -1;
   F = [X(od(cur_odom));X(od(cur_odom)+1)] + R(X(od(cur_odom)+2))*F;
end

CEQ([2*cur_trian-1;2*cur_trian],1) = F - [X(ob1);X(ob1+1)];

cur_trian = cur_trian + 1;

end

% CEQ

end
