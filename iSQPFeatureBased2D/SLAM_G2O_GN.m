function [ Res_G2O, Fval_G2O ] = SLAM_G2O_GN( Xstate_0 , Zstate, CovMatrixInv)

% solve SLAM with g2o 


% Set g2o parth
g2o_path = '/home/user/github/g2o/bin/'; 

%%%%%%%%%%%%%%%
% Set file name and path
input_file = '/home/user/iSQP_Simulation/Data_G2O/tmp.g2o';
output_file = '/home/user/iSQP_Simulation/Data_G2O/tmp_out.g2o';
summary_file = '/home/user/iSQP_Simulation/Data_G2O/tmp_summary.g2o';
% Set max_iteration
max_iteration = 100;

% Create g2o format data from Zstate
[pose_index feature_index] = Create_G2O_Data(input_file, Zstate, CovMatrixInv, Xstate_0 );


% process g2o
string_cmd = [g2o_path,'g2o ', '-i ', num2str(max_iteration),' -summary ', summary_file,' -gaugeList 0 -solver gn_var_eigen',' -o ', output_file, ' ', input_file];

[resut_cmd info_cmd ] = system(string_cmd);
% info_cmd
% Create Xstate format data from g2o data
[ Res_G2O ] = CreateXstateFromG2OData( output_file, pose_index, feature_index );
% Compute the objective
Fval_G2O = Obj_fuc( Res_G2O, Zstate, CovMatrixInv);


end

