function [ X_value F_value ] = Equilibrium_Direct_Method(w, P, A ,b, Ang_index )

%%%%%%%%%%%%% Solve Lagrange Equilibrium Equations %%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%% Direct Method %%%%%%%%%%%%%%%%%%%%%%
P = sparse(P);
A = sparse(A);
w = sparse(w);
b = sparse(b);

X_value =  w - P*A.'*((A*P*A.')\(A*w-b));

X_w = X_value - w;

% Deal with the angle data
% Wrap the angle data  
for k = 1: max(size(Ang_index))
    X_value( Ang_index(k)) = wrap( X_value( Ang_index(k)));
    X_w( Ang_index(k)) = wrap( X_w( Ang_index(k)));    
end
% Objective after warping the angle data
F_value = X_w.'*(P\X_w);

end

