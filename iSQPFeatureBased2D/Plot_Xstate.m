function [ h1 h2 ] = Plot_Xstate( Xstate , pose_str, feature_str )

% pose_str control the line and color used to plot the pose
% feature_str control the line and color used to plot the feature

count = 1;
X_pose = [0];
Y_pose = [0];
X_feature = [];
Y_feature = [];

% extract pose and feature coordinate
% pose coordinates are saved in X_pose, Y_pose
% feature coordiantes are saved in X_feature, Y_feature
while (count<=size(Xstate,1))
    if(Xstate(count,2)==1)
        X_pose = [X_pose Xstate(count,1)];
        Y_pose = [Y_pose Xstate(count+1,1)];
     
        count = count + 3;     
    elseif(Xstate(count,2)==2)     
        X_feature = [X_feature Xstate(count,1)];
        Y_feature = [Y_feature Xstate(count+1,1)];
             
        count = count + 2;
    end
end


% plot pose and feature
h1 = plot(X_pose,Y_pose,pose_str);
h2 = plot(X_feature,Y_feature,feature_str);

end

