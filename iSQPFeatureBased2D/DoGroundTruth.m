% This file is to generate ground truth for the simulation environment

function DoGroundTruth
%%%%%%%%%%%%% set parameters %%%%%%%%%%%%%%
EdgeNum = 4;  % number of edges of polygon
EdgeLength = 300;   % number of relative poses at each edge
Step_Distance = 1;  % distance of relative poses for each step
MaxRotation  =20;  % maximum degree the robot can rotate, given by degree
ExteriorFeatureRange = 2;  % the range between the exterior feature and robot trajectory
ExteriorFeatureNumPerEdge = 200; % the number of exterior features per edge
InteriorFeatureRange = 2;  % the range between the interior feature and robot trajectory
InteriorFeatureNumPerEdge = 100; % the number of interior features per edge
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%% set the first pose %%%%%%%%%%%%
Origin.PositionX = 0;   % position_x
Origin.PositionY = 0;    % position_y
Origin.Orientation = 0;   % orientation, given by degree
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%% SET  END %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% pose varaibles
CornerRoation = pi - pi * (EdgeNum - 2)/EdgeNum;
RotationTimes = ceil(CornerRoation/(pi*MaxRotation/180));
RotationAngle = CornerRoation/RotationTimes;
%feature variables
Exphi = pi - pi * (EdgeNum - 2)/EdgeNum/2;
Inphi = pi * (EdgeNum - 2)/EdgeNum/2;
ExR = ExteriorFeatureRange/sin(Exphi);
InR = InteriorFeatureRange/sin(Inphi);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
PoseCount = 1;
store_robotTrue(1, PoseCount) = Origin.PositionX;
store_robotTrue(2, PoseCount) = Origin.PositionY;
store_robotTrue(3, PoseCount) = wrap(Origin.Orientation*pi/180);
%
FeatureCount = 1;
%
for EdgeCount = 1: EdgeNum
    % first features along the edge
    EXtheta = store_robotTrue(3, PoseCount) - Exphi;         %%%%  
    Intheta = store_robotTrue(3, PoseCount) + Inphi;         %%%%  
    ExteriorFeatureFirst_X = store_robotTrue(1, PoseCount) + ExR*cos(EXtheta);
    ExteriorFeatureFirst_Y = store_robotTrue(2, PoseCount) + ExR*sin(EXtheta);
    InteriorFeatureFirst_X = store_robotTrue(1, PoseCount) + InR*cos(Intheta);
    InteriorFeatureFirst_Y = store_robotTrue(2, PoseCount) + InR*sin(Intheta);
    
    % move on the edge
    for EdgeLengthCount = 1:EdgeLength
        Next_x = store_robotTrue(1, PoseCount) +  Step_Distance* cos(store_robotTrue(3, PoseCount));
        Next_y = store_robotTrue(2, PoseCount) +  Step_Distance* sin(store_robotTrue(3, PoseCount));
        PoseCount = PoseCount + 1;
        store_robotTrue(1, PoseCount) = Next_x;
        store_robotTrue(2, PoseCount) = Next_y;
        store_robotTrue(3, PoseCount) = wrap(store_robotTrue(3, PoseCount-1));
    end
    % rotate at the corner
    for RoationTimeCount = 1:RotationTimes
        PoseCount = PoseCount + 1;
        store_robotTrue(1, PoseCount) = store_robotTrue(1, PoseCount-1);
        store_robotTrue(2, PoseCount) = store_robotTrue(2, PoseCount-1);
        store_robotTrue(3, PoseCount) = wrap(store_robotTrue(3, PoseCount-1) + RotationAngle);
    end
    % last features along the edge
    EXtheta = store_robotTrue(3, PoseCount) - Exphi;         %%%%     
    Intheta = store_robotTrue(3, PoseCount) + Inphi;         %%%%  
    ExteriorFeatureLast_X = store_robotTrue(1, PoseCount) + ExR*cos(EXtheta);
    ExteriorFeatureLast_Y = store_robotTrue(2, PoseCount) + ExR*sin(EXtheta);
    InteriorFeatureLast_X = store_robotTrue(1, PoseCount) + InR*cos(Intheta);
    InteriorFeatureLast_Y = store_robotTrue(2, PoseCount) + InR*sin(Intheta);    
    
    % feature delta x
    ExteriorFeatureDelta_X = (ExteriorFeatureLast_X - ExteriorFeatureFirst_X)/(ExteriorFeatureNumPerEdge-1);
    ExteriorFeatureDelta_Y = (ExteriorFeatureLast_Y - ExteriorFeatureFirst_Y)/(ExteriorFeatureNumPerEdge-1);
    InteriorFeatureDelta_X = (InteriorFeatureLast_X - InteriorFeatureFirst_X)/(InteriorFeatureNumPerEdge-1);
    InteriorFeatureDelta_Y = (InteriorFeatureLast_Y - InteriorFeatureFirst_Y)/(InteriorFeatureNumPerEdge-1);    
    
    % add exterior features
    for FeatureExteriorCount = 1: (ExteriorFeatureNumPerEdge-1)
        store_beaconsTrue(FeatureCount,1) = FeatureCount;
        store_beaconsTrue(FeatureCount,2) = ExteriorFeatureFirst_X + (FeatureExteriorCount-1) * ExteriorFeatureDelta_X;
        store_beaconsTrue(FeatureCount,3) = ExteriorFeatureFirst_Y + (FeatureExteriorCount-1) * ExteriorFeatureDelta_Y;
        FeatureCount = FeatureCount + 1;
    end
    % add interior features
    for FeatureInteriorCount = 1: (InteriorFeatureNumPerEdge-1)
        store_beaconsTrue(FeatureCount,1) = FeatureCount;
        store_beaconsTrue(FeatureCount,2) = InteriorFeatureFirst_X + (FeatureInteriorCount-1) * InteriorFeatureDelta_X;
        store_beaconsTrue(FeatureCount,3) = InteriorFeatureFirst_Y + (FeatureInteriorCount-1) * InteriorFeatureDelta_Y;
        FeatureCount = FeatureCount + 1;
    end   
    
end

save simu_256_ground_truth store_robotTrue store_beaconsTrue

return
