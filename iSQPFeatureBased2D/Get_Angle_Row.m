function [ Ang_index ] = Get_Angle_Row( Zstate )

Odo_index = find(Zstate(:,2) ==1);

num_Odo = size(Odo_index,1)/3;

Ang_index = [];

count = 1;

while ( count<= num_Odo )
    
    Ang_index(count) = Odo_index(3*count);
    
    count = count + 1;
    
end


end

