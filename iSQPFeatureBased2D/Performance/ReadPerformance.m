function [  ] = ReadPerformance( index_num )
% This function is used to present the simulaton performance 
% The data must be placed in Folder "Performance"
% The name of Data file must be in the format 'PerformanceN"


NameStr = ['Performance', num2str(index_num),'.mat'];
load(NameStr)

disp(['File name:  ', NameStr])
%%%%%%%%%%%%%%%%%%%%% Display performance %%%%%%%%%
disp(['----------------------------- simulation ',num2str(index_num),' ----------------------------------'])
disp(['Number of poses -> ', num2str(num_P)])
disp(['Number of features -> ', num2str(num_F)])
disp(['Diameter of poses -> ', num2str(Diam_X_r)])
disp(['Diameter of features -> ', num2str(Diam_X_f)])
disp(['Noise level of positions -> ', num2str(CoMa_Posit)])
disp(['Noise level of angles -> ', num2str(CoMa_Angle)])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('----------------------- Peformance of Each Method ---------------------------')
disp(['Total trials -> ', num2str(Trial_num), '  times'])
disp(['Same Result -> ', num2str(Same), '  times'])
disp(['Different Results -> ', num2str(Trial_num-Same), '  times'])
disp(['SQP_Gra find best solution -> ', num2str(SQP_Gra), '  times'])
disp(['SQP_All find best solution -> ', num2str(SQP_All), '  times'])
disp(['G2O_GN find best solution -> ', num2str(G2O_GN), '  times'])
disp(['Only SQP_Gra find best solution -> ', num2str(Gra_only), '  times'])
disp(['Only SQP_All find best solution -> ', num2str(All_only), '  times'])
disp(['Only G2O_GN find best solution -> ', num2str(GN_only), '  times'])


end
