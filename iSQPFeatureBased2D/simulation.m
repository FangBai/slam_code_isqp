clc
clear all
close all

%%%%%%%%%%%%% Set number of trials here %%%%%%%%%%%%%%%%
Trial_num = 500;   % Total number of trials

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

SQP_Grad_error_Relative = [];
SQP_ALL_error_Relative = [];
G2O_GN_Relative = [];
Solutions = cell(1,Trial_num);
Objectives = [];
Dataset_Diff = cell(1,1);
Groundtruth = cell(1,1);
Dataset_num = 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Trial_count = 1;
    

Same = 0;
SQP_All = 0;
SQP_Gra = 0;
G2O_GN = 0;


while (Trial_count <= Trial_num)

%%%%%%%%%%%%%%% Set number of poses and features here %%%%%%%%%%%%%%%
num_P = 20;    %Number of positions, include the (0,0,0) position
num_F = 5;    %Number of features
%%%%%%%%%%%%%% Set diameter of the robot positions and features %%%%%
Diam_X_r = 50;  % diametry of the robot positions
Diam_X_f = 40; % diametry of the features

%%%%%%%%%%%%%%% Set nosy level here %%%%%%%%%%%%%%
CoMa_Posit = 0.40; % covariance matrix of positions
CoMa_Angle = 0.40; % covariance matrix of angles
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



[ Zstate, X_r_real, X_f_real ] = GenObserv_circle( num_P, num_F, Diam_X_r, Diam_X_f, CoMa_Posit, CoMa_Angle );

%%%%%%%%%%%%%%%%%% Get data %%%%%%%%%%%%%%%%%%%%%%%%
% select Testdata dataset
Zstate = Wrap_result(Zstate);
% set covariance matrix by Identity matrix
Data_scale = size(Zstate,1);
CovMatrixInv = eye(Data_scale);

%%%%%%%%%%%%%%%%% Set initial value %%%%%%%%%%%%%%%%%
% set initial value with observation and odometry data
Xinitial = Zstate(:,1);
Xstate_0 = FuncCreateXstateFromZstate(Zstate);
ss = size(Xstate_0,1);
% set zero initial value
%Xinitial = zeros(size(Zstate,1),1);
%Xstate_0(:,1)=zeros(size(Xstate_0,1),1);
% set random initial value
% Xinitial = Z_state(:,1) + 10*(1-rand(size(Zstate,1),1));
%  Xstate_0(:,1)=200*(1-rand(ss,1));
%%%%%%%%%%%%%%%%% Initial value set %%%%%%%%%%%%%%%%%%%%


%%%%% Solve the problem by different methods %%%%%%%

% solve by SQP (adding constraints gradually)
[Res_SQP_Gra, Fval_SQP_Gra] = SLAM_ConsGrad_SQP ( Xinitial, Zstate, CovMatrixInv);
% solve by SQP (adding all constraints at once)
[Res_SQP_All, Fval_SQP_All] = SLAM_ConsALL_SQP ( Xinitial, Zstate, CovMatrixInv);
% solve by Gauss Newton 
[Res_G2O_GN, Fval_G2O_GN] = SLAM_G2O_GN ( Xstate_0, Zstate, CovMatrixInv);
%%%%%%%%%%%%%%% Solutions and Objectives %%%%%%%%%%%%%%%%%%%%%%

Xstate_real = Ajust_RF_real(X_r_real, X_f_real, Res_G2O_GN );

Solutions{Trial_count} = [Res_SQP_Gra(:,1) Res_SQP_All(:,1) Res_G2O_GN(:,1) Xstate_real];
Objectives (Trial_count,:) = [Fval_SQP_Gra Fval_SQP_All Fval_G2O_GN];

%%%%%%%%%%%%% Compute error for each method %%%%%%%%%%%%%%%%%

SQP_Grad_error_Relative(Trial_count) = Compute_error_relative (X_r_real, X_f_real, Res_SQP_Gra);
SQP_ALL_error_Relative(Trial_count) = Compute_error_relative (X_r_real, X_f_real, Res_SQP_All);
G2O_GN_Relative(Trial_count) = Compute_error_relative (X_r_real, X_f_real, Res_G2O_GN);

%%%%%%%%%%%%%%%% save data shows differecne among methods %%%%%%%%%%%%%%%%
if ( abs(Fval_SQP_Gra - Fval_SQP_All) > 1e-3 | abs(Fval_SQP_Gra - Fval_G2O_GN) > 1e-3 | abs(Fval_SQP_All - Fval_G2O_GN) > 1e-3)


Dataset_Diff{1,Dataset_num} = Zstate;
Groundtruth{1,Dataset_num} = Xstate_real;
Dataset_num = Dataset_num + 1;


end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
threshold = 1e-3;

if ( abs(Fval_SQP_Gra - Fval_SQP_All) < threshold & abs(Fval_SQP_Gra - Fval_G2O_GN) < threshold & abs(Fval_SQP_All - Fval_G2O_GN)< threshold)
str = '------';
Same = Same + 1;
elseif (abs(Fval_SQP_Gra - Fval_SQP_All) < threshold)
    if(Fval_G2O_GN < Fval_SQP_Gra )
        str = 'G2O_GN  $';
        G2O_GN = G2O_GN + 1;          
    else
        str = 'SQP_All_Gra--';     
        SQP_All = SQP_All + 1;    
        SQP_Gra = SQP_Gra + 1;       
    end
elseif ( abs(Fval_SQP_Gra - Fval_G2O_GN) < threshold)   
    if( Fval_SQP_All < Fval_SQP_Gra )
        str = 'SQP_All  #';   
        SQP_All = SQP_All + 1;          
    else
        str = 'Gra_GN--';     
        G2O_GN = G2O_GN + 1;      
        SQP_Gra = SQP_Gra + 1;       
    end    
elseif ( abs(Fval_SQP_All - Fval_G2O_GN)< threshold)    
    if( Fval_SQP_Gra < Fval_SQP_All && SQP_Grad_error_Relative(Trial_count)< SQP_ALL_error_Relative(Trial_count) )
        str = 'SQP_Grad  <+>';
        SQP_Gra = SQP_Gra + 1;       
    else
        str = 'All_GN--';     
        G2O_GN = G2O_GN + 1;      
        SQP_All = SQP_All + 1;       
    end      
elseif(Fval_SQP_All < Fval_G2O_GN)
        if(Fval_SQP_Gra < Fval_SQP_All && SQP_Grad_error_Relative(Trial_count)< SQP_ALL_error_Relative(Trial_count) )
           str = 'SQP_Grad  <+>';
           SQP_Gra = SQP_Gra + 1;
        else
           str = 'SQP_All  #';   
           SQP_All = SQP_All + 1;
        end
elseif(Fval_G2O_GN < Fval_SQP_All)
        if(Fval_SQP_Gra < Fval_G2O_GN && SQP_Grad_error_Relative(Trial_count)< G2O_GN_Relative(Trial_count) )
           str = 'SQP_Grad  <+>';
           SQP_Gra = SQP_Gra + 1;
        else
           str = 'G2O_GN  $';
           G2O_GN = G2O_GN + 1;  
        end        
end
disp(['Trial->', num2str(Trial_count),':  ','The best method is -> ', str ])




Trial_count = Trial_count +1;

end
% all the objectives
%disp('All objectives are presented in sequence as below')
%disp('SQP_Grad ---- SQP_ALL ---- G2O_GN')
%Objectives
save Performance0  Same  SQP_All  SQP_Gra  G2O_GN 

%%%%%%%%%%%%%%%%%%%%% Display performance %%%%%%%%%
disp('----------------------------- simulation ----------------------------------')
disp(['Number of poses -> ', num2str(num_P)])
disp(['Number of features -> ', num2str(num_F)])
disp(['Diameter of poses -> ', num2str(Diam_X_r)])
disp(['Diameter of features -> ', num2str(Diam_X_f)])
disp(['Noise level of positions -> ', num2str(CoMa_Posit)])
disp(['Noise level of angles -> ', num2str(CoMa_Angle)])
disp('----------------------- Peformance of Each Method ---------------------------')
disp(['Total trials -> ', num2str(Trial_num), '  times'])
disp(['Same Result -> ', num2str(Same), '  times'])
disp(['Different Results -> ', num2str(Trial_num-Same), '  times'])
disp(['SQP_Gra find best solution -> ', num2str(SQP_Gra), '  times'])
disp(['SQP_All find best solution -> ', num2str(SQP_All), '  times'])
disp(['G2O_GN find best solution -> ', num2str(G2O_GN), '  times'])

