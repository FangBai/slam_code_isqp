function [ M_trian N_trian ] = Get_triangle( OZ_data )

% Get triangles from odometry and observation data
% Preserve the result in M_trian ( column elements are peaks )
% M_trian = [pose1 pose2 feature]
% N_trian = [num_odometry [odom1 odm2...domK] observation1 observation2]
M_trian=[];
N_trian=cell(0);
count_tria=0;
cur_feature=1;
%%%%%%%%%%%%%%%%%%%%% Get all odometries %%%%%%%%%%%%%%%
Odometry_data = find(OZ_data(:,2)==1);
for n=1:size(Odometry_data,1)/3
    Odometry_line(n) = Odometry_data(3*n-2);
end

Max_index_feature=max(OZ_data(find(OZ_data(:,2)==2),3));

while(cur_feature<=Max_index_feature)
fea_line = find(OZ_data(:,2)==2 & OZ_data(:,3)==cur_feature);
if(size(fea_line,1)>=4)
       for i=1:2:size(fea_line,1)
           if(i+2<=size(fea_line,1))    
               count_tria = count_tria+1;
      %%%%%%%%%%%%%%% get M_trian  %%%%%%%%%%%%%%%      
               M_trian(count_tria,1)= OZ_data(fea_line(i),4);
               M_trian(count_tria,2)= OZ_data(fea_line(i+2),4);
               M_trian(count_tria,3)= cur_feature;
      %%%%%%%%%%%%%%% get N_trian  %%%%%%%%%%%%%%%      
             num_odom = OZ_data(fea_line(i+2),4) - OZ_data(fea_line(i),4);
              N_trian{count_tria}(1) = num_odom;
              for odo_count =1:num_odom
                 N_trian{count_tria}(odo_count+1) = Odometry_line(OZ_data(fea_line(i),4)+odo_count);  
              end
              N_trian{count_tria}(num_odom+2) = fea_line(i);
              N_trian{count_tria}(num_odom+3) = fea_line(i+2);
               
           end
       end
        cur_feature = cur_feature+1;
elseif(size(fea_line,1)==2)
        cur_feature = cur_feature+1;
elseif(size(fea_line,1)==0)
        cur_feature = cur_feature+1;
end
end

end


