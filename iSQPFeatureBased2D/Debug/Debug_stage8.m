
    clc
    clear all
    close all
    %%%%%%%%%%%% load dataset which shows different results %%%%%%%%
    load Dataset_Diff Dataset_Diff Groundtruth
    Dataset_num = size(Dataset_Diff,2);
    if (Dataset_num ==1 & max(size(Dataset_Diff{1}))==0)
    disp('There are in all 0 Datasets which shows different results.'); 
    disp('Please try option 1 at first!');
    else
      
    %%%%% Choose a dataset %%%%%%%%%    
    Order_num = 84;
  
    % Extract data
    Zstate = Dataset_Diff{Order_num};
    Zstate = Wrap_result(Zstate);
    % set covariance matrix by Identity matrix
    Data_scale = size(Zstate,1);
    CovMatrixInv = eye(Data_scale);
    % Extract groundtruth
    Xstate_real = Groundtruth{Order_num};

%%%%%%%%%%%%%%%%% Set initial value %%%%%%%%%%%%%%%%%
Ini_val_choice = 1;

% set initial value with observation and odometry data
Xinitial_Data = Zstate(:,1);
Xstate_0_Data = FuncCreateXstateFromZstate(Zstate);
%ss = size(Xstate_0_Data,1);
% set zero initial value
Xinitial_Zero = zeros(size(Zstate,1),1);
%Xstate_0_Zero(:,1) =zeros(ss,1);
% set random initial value
% Xinitial = Z_state(:,1) + 10*(1-rand(size(Zstate,1),1));
%  Xstate_0(:,1)=200*(1-rand(ss,1));
%

if (Ini_val_choice ==1)
    Xstate_0 = Xstate_0_Data;
    Xinitial_All = Xinitial_Data;
    Xinitial_Gra = Xinitial_Data;
elseif(Ini_val_choice ==2)
    Xstate_0 = Xstate_0_Data;     
    Xinitial_All = Xinitial_Zero;
    Xinitial_Gra = Xinitial_Zero;
elseif(Ini_val_choice ==3)
    Xstate_0 = Xstate_0_Data;     
    Xinitial_All = Xinitial_Data;
    Xinitial_Gra = Xinitial_Zero;
elseif(Ini_val_choice ==4)
    Xstate_0 = Xstate_0_Data;     
    Xinitial_All = Xinitial_Zero;
    Xinitial_Gra = Xinitial_Data; 
end

% Initial value done!

%%%%%%% Solve the problem by different methods %%%%%%%

% solve by SQP (adding all constraints at once)
[Res_SQP_All, Fval_SQP_All] = SLAM_ConsALL_SQP ( Xinitial_All, Zstate, CovMatrixInv);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SQPAll = CreateZstateFromXstate ( Res_SQP_All, Zstate);
SQPAll = SQPAll(:,1);



[Res_SQP_All_a, Fval_SQP_All_a] = SLAM_ConsALL_SQP ( SQPAll, Zstate, CovMatrixInv);


[Res_GaussNew_bySQP_All, Fval_GaussNew_bySQP_All] = SLAM_Gauss_Newton ( Res_SQP_All, Zstate, CovMatrixInv);
%%%%%%%%%%%%%%% Solutions and Objectives %%%%%%%%%%%%%%%%%%%%%%

disp('Objectives are presented in sequence as below')
disp('SQP_All----SQP_All_again----SQP_All_byGaussNew')
 [Fval_SQP_All, Fval_SQP_All_a, Fval_GaussNew_bySQP_All]


    end  
