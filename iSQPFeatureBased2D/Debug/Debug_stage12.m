clear all
close all




load('Zstate_VicPark_6898_loops.mat')
load('CovMatrixInv_VicPark_6898_loops.mat')
Xstate_0 = FuncCreateXstateFromZstate(Zstate);
[pose_index feature_index] = Create_G2O_Data('VicPark_6898_loops.g2o', Zstate, CovMatrixInv, Xstate_0);


load('Zstate_DLR.mat')
load('CovMatrixInv_DLR.mat')
Xstate_0 = FuncCreateXstateFromZstate(Zstate);
[pose_index feature_index] = Create_G2O_Data('DLR.g2o', Zstate, CovMatrixInv, Xstate_0);
