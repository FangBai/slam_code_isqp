clc

disp('Objectives are presented in sequence as below')
disp('SQP_Grad ---- SQP_ALL ---- Gauss_Newton ---- SQP_byGauss ---- Gauss_bySQP_Gra ---- Gauss_bySQP_All')

%Data_choice =4;
for count = 1:1
    clear all
    close all



    %%%%%%%%%%%% load dataset which shows different results %%%%%%%%
    load Dataset_Diff Dataset_Diff Groundtruth
        
    Order_num = 2;
  
    % Extract data
    Zstate = Dataset_Diff{Order_num};
    % set covariance matrix by Identity matrix
    Data_scale = size(Zstate,1);
    CovMatrixInv = eye(Data_scale);
    % Extract groundtruth
    Xstate_real = Groundtruth{Order_num};
Zstate = Wrap_result(Zstate);
%%%%%%%%%%%%%%%%% Set initial value %%%%%%%%%%%%%%%%%
Ini_val_choice = 1;
% set initial value with observation and odometry data
Xinitial_Data = Zstate(:,1);
Xstate_0_Data = FuncCreateXstateFromZstate(Zstate);
%ss = size(Xstate_0_Data,1);
% set zero initial value
Xinitial_Zero = zeros(size(Zstate,1),1);
%Xstate_0_Zero(:,1) =zeros(ss,1);
% set random initial value
% Xinitial = Z_state(:,1) + 10*(1-rand(size(Zstate,1),1));
%  Xstate_0(:,1)=200*(1-rand(ss,1));
%

if (Ini_val_choice ==1)
    Xstate_0 = Xstate_0_Data;
    Xinitial_All = Xinitial_Data;
    Xinitial_Gra = Xinitial_Data;
elseif(Ini_val_choice ==2)
    Xstate_0 = Xstate_0_Data;     
    Xinitial_All = Xinitial_Zero;
    Xinitial_Gra = Xinitial_Zero;
elseif(Ini_val_choice ==3)
    Xstate_0 = Xstate_0_Data;     
    Xinitial_All = Xinitial_Data;
    Xinitial_Gra = Xinitial_Zero;
elseif(Ini_val_choice ==4)
    Xstate_0 = Xstate_0_Data;     
    Xinitial_All = Xinitial_Zero;
    Xinitial_Gra = Xinitial_Data; 
end

% Initial value done!



%%%%%%% Solve the problem by different methods %%%%%%%

% solve by SQP (adding all constraints at once)
[Res_SQP_All, Fval_SQP_All] = SLAM_ConsALL_SQP ( Xinitial_All, Zstate, CovMatrixInv);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('*****************************SQP compute again************************************')
a =CreateZstateFromXstate(Res_SQP_All,Zstate);
[Res_SQP_All_add, Fval_SQP_All_add] = SLAM_ConsALL_SQP (a(:,1), Zstate, CovMatrixInv);


disp('SQP_All ------ SQP_All_add')
objective = [ Fval_SQP_All Fval_SQP_All_add ]



end


