clear all
close all

load('Zstate_VicPark_6898_loops.mat')
load('CovMatrixInv_VicPark_6898_loops.mat')


Xstate_0 = FuncCreateXstateFromZstate(Zstate);
[pose_index feature_index] = Create_G2O_Data('VicPark_6898_loops.g2o', Zstate, CovMatrixInv, Xstate_0);

% ./g2o -solver gn_var_eigen -o VicPark_out_Fixl.g2o -i 100 -summary VicParkSummaryFixl.g2o VicPark_6898_loops.g2o
% ./g2o -solver gn_var_eigen -o VicPark_out_Fix0.g2o -i 100 -summary VicParkSummaryFix0.g2o -gaugeList 0 VicPark_6898_loops.g2o

File_name = 'VicPark_out_Fix0.g2o';
[ Res_G2O ] = CreateXstateFromG2OData( File_name, pose_index, feature_index );
Fval_G2O = Obj_fuc( Res_G2O, Zstate, CovMatrixInv);

% Gauss newton result is saved in Res
 load Res
 [pose_index feature_index] = Create_G2O_Data('test.g2o', Zstate, CovMatrixInv, Res_GaussNew);

 
 %  ./g2o -solver gn_var_eigen -o test_out.g2o -i 100 -summary summaTest.g2o -gaugeList 0 test.g2o
 
 [ Res_t ] = CreateXstateFromG2OData( 'test_out.g2o', pose_index, feature_index );
 Fval_t = Obj_fuc( Res_t, Zstate, CovMatrixInv);

%[Res_GaussNew, Fval_GaussNew] = SLAM_Gauss_Newton ( Res_G2O , Zstate, CovMatrixInv);


max([Res_t(:,1) - Res_G2O(:,1)])

Fval_t
Fval_G2O

%max([Res_GaussNew(:,1)-Res_G2O(:,1)])
% Fval_G2O
% Fval_GaussNew


