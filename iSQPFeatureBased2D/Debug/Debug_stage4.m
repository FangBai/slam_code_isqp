clc

disp('Objectives are presented in sequence as below')
disp('SQP_Grad ---- SQP_ALL ---- Gauss_Newton ---- SQP_byGauss ---- Gauss_bySQP_Gra ---- Gauss_bySQP_All')

%Data_choice =4;
for count = 1:10
    clear all
    close all



    %%%%%%%%%%%% load dataset which shows different results %%%%%%%%
    load Dataset_Diff Dataset_Diff Groundtruth
        
    Order_num = 2;
  
    % Extract data
    Zstate = Dataset_Diff{Order_num};
    % set covariance matrix by Identity matrix
    Data_scale = size(Zstate,1);
    CovMatrixInv = eye(Data_scale);
    % Extract groundtruth
    Xstate_real = Groundtruth{Order_num};

%%%%%%%%%%%%%%%%% Set initial value %%%%%%%%%%%%%%%%%
Ini_val_choice = 1;
% set initial value with observation and odometry data
Xinitial_Data = Zstate(:,1);
Xstate_0_Data = FuncCreateXstateFromZstate(Zstate);
%ss = size(Xstate_0_Data,1);
% set zero initial value
Xinitial_Zero = zeros(size(Zstate,1),1);
%Xstate_0_Zero(:,1) =zeros(ss,1);
% set random initial value
% Xinitial = Z_state(:,1) + 10*(1-rand(size(Zstate,1),1));
%  Xstate_0(:,1)=200*(1-rand(ss,1));
%

if (Ini_val_choice ==1)
    Xstate_0 = Xstate_0_Data;
    Xinitial_All = Xinitial_Data;
    Xinitial_Gra = Xinitial_Data;
elseif(Ini_val_choice ==2)
    Xstate_0 = Xstate_0_Data;     
    Xinitial_All = Xinitial_Zero;
    Xinitial_Gra = Xinitial_Zero;
elseif(Ini_val_choice ==3)
    Xstate_0 = Xstate_0_Data;     
    Xinitial_All = Xinitial_Data;
    Xinitial_Gra = Xinitial_Zero;
elseif(Ini_val_choice ==4)
    Xstate_0 = Xstate_0_Data;     
    Xinitial_All = Xinitial_Zero;
    Xinitial_Gra = Xinitial_Data; 
end

% Initial value done!

%%%%%%% Solve the problem by different methods %%%%%%%
% solve by Gauss Newton 
[Res_Gauss_New, Fval_Gauss_New] = SLAM_Gauss_Newton ( Xstate_0, Zstate, CovMatrixInv);
% solve by SQP (adding all constraints at once)
[Res_SQP_All, Fval_SQP_All] = SLAM_ConsALL_SQP ( Xinitial_All, Zstate, CovMatrixInv);
% solve by SQP (adding constraints gradually)
[Res_SQP_Gra, Fval_SQP_Gra] = SLAM_ConsGrad_SQP ( Xinitial_Gra, Zstate, CovMatrixInv);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Use SQP result as Gauss-newton intials

%[Res_GaussNew_bySQP_All, Fval_GaussNew_bySQP_All] = SLAM_Gauss_Newton ( Res_SQP_All, Zstate, CovMatrixInv);


a =CreateZstateFromXstate(Res_SQP_All,Zstate);
[Res_GaussNew_bySQP_All, Fval_GaussNew_bySQP_All] = SLAM_ConsALL_SQP (a(:,1), Zstate, CovMatrixInv);


[Res_GaussNew_bySQP_Gra, Fval_GaussNew_bySQP_Gra] = SLAM_Gauss_Newton ( Res_SQP_Gra, Zstate, CovMatrixInv);

% Use Gauss-newton result as SQP 

 GaussNew = CreateZstateFromXstate ( Res_Gauss_New, Zstate);
 GaussNew = GaussNew(:,1);
%[FuncCreateXstateFromZstate(CreateZstateFromXstate(Res_Gauss_New,Zstate))  Res_Gauss_New]
% Res_GaussNew = CreateZstateFromXstate ( Res_Gauss_New, Zstate);
% FuncCreateXstateFromZstate (Zstate );
% [Zstate  CreateZstateFromXstate ( Res_Gauss_New, Zstate)]
% [Res_Gauss_New FuncCreateXstateFromZstate(CreateZstateFromXstate ( Res_Gauss_New, Zstate))]

 [Res_SQP_byGaussNew, Fval_SQP_byGaussNew] = SLAM_ConsALL_SQP ( GaussNew, Zstate, CovMatrixInv);

 %[Res_SQP_byGaussNew_initial, Fval_SQP_byGaussNew_initial] = SLAM_ConsGrad_SQP ( Res_GaussNew(:,1), Zstate, CovMatrixInv);

%%%%%%%%%%%%%%% Solutions and Objectives %%%%%%%%%%%%%%%%%%%%%%

%disp('Solutions are presented in sequence as below');
%disp('SQP_Grad ---- SQP_ALL ---- Gauss_Newton ---- SQP_byGauss ---- Gauss_bySQP_Gra ---- Gauss_bySQP_All ---- Ground truth')
%Solutions = [Res_SQP_Gra(:,1), Res_SQP_All(:,1), Res_Gauss_New(:,1), Res_SQP_byGaussNew(:,1), Res_GaussNew_bySQP_Gra(:,1), Res_GaussNew_bySQP_All(:,1), Xstate_real]


Objectives = [Fval_SQP_Gra, Fval_SQP_All, Fval_Gauss_New, Fval_SQP_byGaussNew, Fval_GaussNew_bySQP_Gra, Fval_GaussNew_bySQP_All]


end

