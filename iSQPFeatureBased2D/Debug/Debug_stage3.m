

clc
close all
clear all

%%%%%%%%%%%%%%%%% Load data shows same result
load Dataset_Same Dataset_Same Groundtruth


Zstate = Dataset_Same;
Data_scale = size(Zstate,1);
CovMatrixInv = eye(Data_scale);
    % Extract groundtruth
Xstate_real = Groundtruth;


Ini_val_choice =4;
% set initial value with observation and odometry data
Xinitial_Data = Zstate(:,1);
Xstate_0_Data = FuncCreateXstateFromZstate(Zstate);
%ss = size(Xstate_0_Data,1);
% set zero initial value
Xinitial_Zero = zeros(size(Zstate,1),1);
%Xstate_0_Zero(:,1) =zeros(ss,1);
% set random initial value
% Xinitial = Z_state(:,1) + 10*(1-rand(size(Zstate,1),1));
%  Xstate_0(:,1)=200*(1-rand(ss,1));
%

if (Ini_val_choice ==1)
    Xstate_0 = Xstate_0_Data;
    Xinitial_All = Xinitial_Data;
    Xinitial_Gra = Xinitial_Data;
elseif(Ini_val_choice ==2)
    Xstate_0 = Xstate_0_Data;     
    Xinitial_All = Xinitial_Zero;
    Xinitial_Gra = Xinitial_Zero;
elseif(Ini_val_choice ==3)
    Xstate_0 = Xstate_0_Data;     
    Xinitial_All = Xinitial_Data;
    Xinitial_Gra = Xinitial_Zero;
elseif(Ini_val_choice ==4)
    Xstate_0 = Xstate_0_Data;     
    Xinitial_All = Xinitial_Zero;
    Xinitial_Gra = Xinitial_Data; 
end


%%%%% Solve the problem by different methods %%%%%%%
% solve by Gauss Newton 
[Res_Gauss_New, Fval_Gauss_New] = SLAM_Gauss_Newton ( Xstate_0, Zstate, CovMatrixInv);
% solve by SQP (adding all constraints at once)
[Res_SQP_All, Fval_SQP_All Optim_data] = SLAM_ConsALL_SQP ( Xinitial_All, Zstate, CovMatrixInv);
% solve by SQP (adding constraints gradually)


%Optim_Xstate  = FuncCreateXstateFromZstate(Optim_data);
%Optim_Xstate- Res_SQP_All
%[Optim_Xstate Res_SQP_All]


% Use Gauss-newton result as SQP initials
 GaussNew = CreateZstateFromXstate ( Res_Gauss_New, Zstate);
 %GaussNew- Optim_data
 %[GaussNew Optim_data]
 
 %%%%%%%%%%%%%%%%%%%%%%%%%
  %SQP_data = CreateZstateFromXstate ( Res_SQP_All, Zstate);
 % A = [SQP_data(:,1), Optim_data];
  %A = [[A(:,1)-A(:,2)] A]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   B = [GaussNew(:,1), Optim_data];
   B = [[B(:,1)-B(:,2)] B]
  
  
[Res_SQP_byGaussNew, Fval_SQP_byGaussNew, Optim_data_GQ ] = SLAM_ConsALL_SQP ( GaussNew(:,1), Zstate, CovMatrixInv);

solutions = [Res_Gauss_New(:,1), Res_SQP_All(:,1), Res_SQP_byGaussNew]
objective = [Fval_Gauss_New, Fval_SQP_All, Fval_SQP_byGaussNew]




