    clc
    clear all
    close all
    %%%%%%%%%%%% load dataset which shows different results %%%%%%%%
    load Dataset_Diff Dataset_Diff Groundtruth
    Dataset_num = size(Dataset_Diff,2);
    if (Dataset_num ==1 & max(size(Dataset_Diff{1}))==0)
    disp('There are in all 0 Datasets which shows different results.'); 
    disp('Please try option 1 at first!');
    else
      
    %%%%% Choose a dataset %%%%%%%%%  
    disp(['There are in all ', num2str(Dataset_num), ' Datasets which shows different results.']);       
  %  Order_num = input('Please use the order number to choose a dataset!\nFor example: i choose the i-th dataset.\n');
    %    while(Order_num > Dataset_num)
      %      disp('Wrong number!');
      %      Order_num = input('Please use the order number to choose a dataset!\nFor example: i choose the i-th dataset.\n');
      %  end
   Order_num =1;
    % Extract data
    Zstate = Dataset_Diff{Order_num};
    % set covariance matrix by Identity matrix
    Data_scale = size(Zstate,1);
    CovMatrixInv = eye(Data_scale);
    % Extract groundtruth
    Xstate_real = Groundtruth{Order_num};

%%%%%%%%%%%%%%%%% Set initial value %%%%%%%%%%%%%%%%%
Ini_val_choice =4;
% set initial value with observation and odometry data
Xinitial_Data = Zstate(:,1);
Xstate_0_Data = FuncCreateXstateFromZstate(Zstate);
%ss = size(Xstate_0_Data,1);
% set zero initial value
Xinitial_Zero = zeros(size(Zstate,1),1);
%Xstate_0_Zero(:,1) =zeros(ss,1);
% set random initial value
% Xinitial = Z_state(:,1) + 10*(1-rand(size(Zstate,1),1));
%  Xstate_0(:,1)=200*(1-rand(ss,1));
%

if (Ini_val_choice ==1)
    Xstate_0 = Xstate_0_Data;
    Xinitial_All = Xinitial_Data;
    Xinitial_Gra = Xinitial_Data;
elseif(Ini_val_choice ==2)
    Xstate_0 = Xstate_0_Data;     
    Xinitial_All = Xinitial_Zero;
    Xinitial_Gra = Xinitial_Zero;
elseif(Ini_val_choice ==3)
    Xstate_0 = Xstate_0_Data;     
    Xinitial_All = Xinitial_Data;
    Xinitial_Gra = Xinitial_Zero;
elseif(Ini_val_choice ==4)
    Xstate_0 = Xstate_0_Data;     
    Xinitial_All = Xinitial_Zero;
    Xinitial_Gra = Xinitial_Data; 
end

% Initial value done!

%%%%%%% Solve the problem by different methods %%%%%%%
% solve by Gauss Newton 

[Res_Gauss_New, Fval_Gauss_New] = SLAM_Gauss_Newton ( Xstate_0, Zstate, CovMatrixInv);

% Use Gauss-newton result as SQP initials
 GaussNew = CreateZstateFromXstate ( Res_Gauss_New, Zstate);
 GaussNew = GaussNew(:,1);

[Res_SQP_byGaussNew, Fval_SQP_byGaussNew] = SLAM_ConsALL_SQP ( GaussNew, Zstate, CovMatrixInv);


disp('Solutions are presented in sequence as below');
disp('Gauss_Newton ---- SQP_byGauss ---- Ground truth')

Solutions = [Res_Gauss_New(:,1), Res_SQP_byGaussNew(:,1), Xstate_real]

disp('Objectives are presented in sequence as below')
disp('Gauss_Newton ---- SQP_byGauss')
Objectives = [Fval_Gauss_New, Fval_SQP_byGaussNew]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%% Plot the result %%%%%%%%%%%%%%%%%%%%%%%%%
figure
hold on
title('Comparsion of feature and pose positions', 'fontsize',15);
xlabel('x axis/coordinate','fontsize', 12);
ylabel('y axis/coordinate','fontsize', 12);

[h5 h6] = Plot_Xstate(Res_Gauss_New,'ys:','y+');
[h7 h8] = Plot_Xstate(Xstate_real,'ko:','k*');
[h9 h10] = Plot_Xstate(Res_SQP_byGaussNew,'c<:','cv');


hold off
legend([h5 h7 h9 h6 h8 h10], 'Gauss-New-Pose', 'Ground truth -Pose','SQP-byGauss-Pose', 'Gauss-New-Feature', 'Ground truth -Feature','SQP-byGauss-Feature','Location','Best');

%%%%%%%%%%%%%%%%%%%%% plot end %%%%%%%%%%%%%%%%%%%%
    end

