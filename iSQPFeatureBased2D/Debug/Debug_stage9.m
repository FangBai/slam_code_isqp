clear all
close all

load G2O_Test
% Observation and odometry data saved in Zstate
% Initial value for pose and feature saved in Xstate_0

% data format for each type
format_pose = 'VERTEX_SE2 %d %f %f %f\n';
format_feature = 'VERTEX_XY %d %f %f\n';
format_odometry = 'EDGE_SE2 %d %d %f %f %f 1 0 0 1 0 1\n';
format_observation = 'EDGE_SE2_XY %d %d 0 %f %f 1 0 1\n';

% create and open the file
fid = fopen('G2O_file.g2o', 'w');
% write pose 0 into the file

fprintf(fid, format_pose, 0, 0, 0, 0);

% classify the data into pose feature odometry observation
data_pose = Xstate_0(find(Xstate_0(:,2)==1),:);
data_feature = Xstate_0(find(Xstate_0(:,2)==2),:);
data_odometry = Zstate(find(Zstate(:,2)==1),:);
data_observation = Zstate(find(Zstate(:,2)==2),:);

% quantity of each data type
num_pose = size(data_pose,1)/3;
num_feature = size(data_feature,1)/2;
num_odometry = size(data_odometry,1)/3;
num_observation = size(data_observation,1)/2;

% write pose into the file
for count =1:num_pose

tmp_x = data_pose(3*count-2,1);
tmp_y = data_pose(3*count-1,1);
tmp_phi = data_pose(3*count,1);

fprintf(fid, format_pose, data_pose(3*count,3), tmp_x, tmp_y, tmp_phi);
  
end

% write feature into the file
for count =1:num_feature

tmp_x = data_feature(2*count-1,1);
tmp_y = data_feature(2*count,1);

fprintf(fid, format_feature, num_pose+data_feature(2*count,3), tmp_x ,tmp_y);

end

% write odometry into the file
for count =1:num_odometry

tmp_x = data_odometry(3*count-2,1);
tmp_y = data_odometry(3*count-1,1);
tmp_phi = data_odometry(3*count,1);

fprintf(fid, format_odometry, data_odometry(3*count,4), data_odometry(3*count,3), tmp_x, tmp_y, tmp_phi);

end

% write observation into the file
for count =1:num_observation
    
tmp_x = data_observation(2*count-1,1);
tmp_y = data_observation(2*count,1);

fprintf(fid, format_observation, data_observation(2*count,4), num_pose+data_observation(2*count,3), tmp_x, tmp_y);

end
%close file
fclose(fid);
