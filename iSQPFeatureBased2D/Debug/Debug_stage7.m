
    clc
    clear all
    close all
    %%%%%%%%%%%% load dataset which shows different results %%%%%%%%
    load Dataset_Diff Dataset_Diff Groundtruth
    Dataset_num = size(Dataset_Diff,2); 
   

  for Order_num=84: Dataset_num
    % Extract data
    Zstate = Dataset_Diff{Order_num};
    Zstate = Wrap_result(Zstate);
    % set covariance matrix by Identity matrix
    Data_scale = size(Zstate,1);
    CovMatrixInv = eye(Data_scale);
    % Extract groundtruth
    Xstate_real = Groundtruth{Order_num};

%%%%%%%%%%%%%%%%% Set initial value %%%%%%%%%%%%%%%%%
Ini_val_choice = 1;

% set initial value with observation and odometry data
Xinitial_Data = Zstate(:,1);
Xstate_0_Data = FuncCreateXstateFromZstate(Zstate);
%ss = size(Xstate_0_Data,1);
% set zero initial value
Xinitial_Zero = zeros(size(Zstate,1),1);
%Xstate_0_Zero(:,1) =zeros(ss,1);
% set random initial value
% Xinitial = Z_state(:,1) + 10*(1-rand(size(Zstate,1),1));
%  Xstate_0(:,1)=200*(1-rand(ss,1));
%

if (Ini_val_choice ==1)
    Xstate_0 = Xstate_0_Data;
    Xinitial_All = Xinitial_Data;
    Xinitial_Gra = Xinitial_Data;
elseif(Ini_val_choice ==2)
    Xstate_0 = Xstate_0_Data;     
    Xinitial_All = Xinitial_Zero;
    Xinitial_Gra = Xinitial_Zero;
elseif(Ini_val_choice ==3)
    Xstate_0 = Xstate_0_Data;     
    Xinitial_All = Xinitial_Data;
    Xinitial_Gra = Xinitial_Zero;
elseif(Ini_val_choice ==4)
    Xstate_0 = Xstate_0_Data;     
    Xinitial_All = Xinitial_Zero;
    Xinitial_Gra = Xinitial_Data; 
end

% Initial value done!

%%%%%%% Solve the problem by different methods %%%%%%%
% solve by Gauss Newton 
[Res_Gauss_New, Fval_Gauss_New] = SLAM_Gauss_Newton ( Xstate_0, Zstate, CovMatrixInv);
% solve by SQP (adding all constraints at once)
[Res_SQP_All, Fval_SQP_All] = SLAM_ConsALL_SQP ( Xinitial_All, Zstate, CovMatrixInv);
% solve by SQP (adding constraints gradually)
[Res_SQP_Gra, Fval_SQP_Gra] = SLAM_ConsGrad_SQP ( Xinitial_Gra, Zstate, CovMatrixInv);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Use SQP result as Gauss-newton intials
[Res_GaussNew_bySQP_All, Fval_GaussNew_bySQP_All] = SLAM_Gauss_Newton ( Res_SQP_All, Zstate, CovMatrixInv);
[Res_GaussNew_bySQP_Gra, Fval_GaussNew_bySQP_Gra] = SLAM_Gauss_Newton ( Res_SQP_Gra, Zstate, CovMatrixInv);

% Use Gauss-newton result as SQP initials

GaussNew = CreateZstateFromXstate ( Res_Gauss_New, Zstate);
GaussNew = GaussNew(:,1);

[Res_SQP_All_byGaussNew, Fval_SQP_All_byGaussNew] = SLAM_ConsALL_SQP ( GaussNew, Zstate, CovMatrixInv);

[Res_SQP_Gra_byGaussNew, Fval_SQP_Gra_byGaussNew] = SLAM_ConsGrad_SQP ( GaussNew, Zstate, CovMatrixInv);


% Use SQP result as SQP initials

SQPGra = CreateZstateFromXstate ( Res_SQP_Gra, Zstate);
SQPGra = SQPGra(:,1);
[Res_SQP_All_byGra, Fval_SQP_All_byGra] = SLAM_ConsALL_SQP ( SQPGra, Zstate, CovMatrixInv);

SQPAll = CreateZstateFromXstate ( Res_SQP_All, Zstate);
SQPAll = SQPAll(:,1);
[Res_SQP_Gra_byAll, Fval_SQP_Gra_byAll] = SLAM_ConsGrad_SQP ( SQPAll, Zstate, CovMatrixInv);

% Compute again
% solve by Gauss Newton 
[Res_Gauss_New_a, Fval_Gauss_New_a] = SLAM_Gauss_Newton ( Res_Gauss_New, Zstate, CovMatrixInv);
% solve by SQP (adding all constraints at once)
[Res_SQP_All_a, Fval_SQP_All_a] = SLAM_ConsALL_SQP ( SQPAll, Zstate, CovMatrixInv);
% solve by SQP (adding constraints gradually)
[Res_SQP_Gra_a, Fval_SQP_Gra_a] = SLAM_ConsGrad_SQP ( SQPGra, Zstate, CovMatrixInv);
%%%%%%%%%%%%%%% Solutions and Objectives %%%%%%%%%%%%%%%%%%%%%%

%disp('Solutions are presented in sequence as below');
%disp('SQP_Gra----SQP_All----Gauss_New----SQP_All_byGra----GaussNew_bySQP_Gra----SQP_Gra_byAll----GaussNew_bySQP_All----SQP_Gra_byGaussNew----SQP_All_byGaussNew----Groundtruth')
%Solutions = [Res_SQP_Gra(:,1), Res_SQP_All(:,1), Res_Gauss_New(:,1) Res_SQP_All_byGra(:,1), Res_GaussNew_bySQP_Gra(:,1), Res_SQP_Gra_byAll(:,1), Res_GaussNew_bySQP_All(:,1), Res_SQP_Gra_byGaussNew(:,1), Res_SQP_All_byGaussNew(:,1), Xstate_real]
if (Fval_Gauss_New < 200)

%if( abs(Fval_GaussNew_bySQP_All- Fval_SQP_All) > 1e-3 | abs(Fval_SQP_All_byGaussNew - Fval_Gauss_New) > 1e-3 | abs(Fval_SQP_All_byGra - Fval_SQP_Gra) >1e-3 |(absFval_GaussNew_bySQP_Gra - Fval_SQP_Gra)>1e-3 )

if( abs(Fval_GaussNew_bySQP_All- Fval_SQP_All) > 1e-3 | abs(Fval_SQP_All_byGaussNew - Fval_Gauss_New) > 1e-3 )
       Order_num
    
disp('Objectives are presented in sequence as below')

disp('SQP_Gra----SQP_All----Gauss_New')
 [Fval_SQP_Gra, Fval_SQP_All, Fval_Gauss_New]
disp('Compute above again by its result')
disp('SQP_Gra_again----SQP_All_again----Gauss_New_again')
 [Fval_SQP_Gra_a, Fval_SQP_All_a, Fval_Gauss_New_a]
disp('SQP_All_byGra----GaussNew_bySQP_Gra----SQP_Gra_byAll----GaussNew_bySQP_All----SQP_Gra_byGaussNew----SQP_All_byGaussNew')
 [Fval_SQP_All_byGra, Fval_GaussNew_bySQP_Gra, Fval_SQP_Gra_byAll, Fval_GaussNew_bySQP_All, Fval_SQP_Gra_byGaussNew, Fval_SQP_All_byGaussNew]

%disp('SQP_All----Gauss_New----SQP_Gra_byAll----SQP_Gra_byGaussNew')
%Objectives = [Fval_SQP_All, Fval_Gauss_New, Fval_SQP_Gra_byAll, Fval_SQP_Gra_byGaussNew]
end
end
 end  
       
