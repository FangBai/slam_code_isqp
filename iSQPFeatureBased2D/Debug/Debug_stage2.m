
    clc
    clear all
    close all
    %%%
    load Trial Trial_num
    load Result Objectives Solutions
    load Dataset_Diff Dataset_Diff
    Zstate = Dataset_Diff{1};
    % set covariance matrix by Identity matrix
    Data_scale = size(Zstate,1);
    CovMatrixInv = eye(Data_scale);
    
    %choose a trial
    disp(['There are ',num2str(Trial_num), ' trials in all'])
    disp('Please choose a trial to recur!')

    Trial_index = 15;
    % display result
   
    Solutions = Solutions{Trial_index};
    
    % display objective

    Objectives = Objectives (Trial_index,:);
    
    % extract results from Solutions
    Res_SQP_Gra = Solutions(:,[1 5 6]);
    Res_SQP_All = Solutions(:,[2 5 6]);
    Res_Gauss_New = Solutions(:, [3 5 6]);
    Xstate_real = Solutions(:, [4 5 6]);
    
    % Compute by Gauss Newton result
    GaussNew = CreateZstateFromXstate ( Res_Gauss_New, Zstate);
    GaussNew = GaussNew(:,1);
   
    %[Res_SQP_byGaussNew, Fval_SQP_byGaussNew] = SLAM_ConsALL_SQP ( GaussNew, Zstate, CovMatrixInv);
    [Res_SQP_byGaussNew, Fval_SQP_byGaussNew] = SLAM_ConsALL_SQP ( GaussNew, Zstate, CovMatrixInv);
    
    
    disp('Solutions are presented in sequence as below');
    disp('Gauss_Newton ---- SQP_byGauss ---- Ground truth')

    Solutions = [Solutions(:,1:3), Res_SQP_byGaussNew(:,1), Xstate_real]

    disp('Gauss_Newton ---- SQP_byGauss')
    Objectives = [Objectives, Fval_SQP_byGaussNew]
    
%plot the result
figure
hold on
title('Comparsion of feature and pose positions', 'fontsize',15);
xlabel('x axis/coordinate','fontsize', 12);
ylabel('y axis/coordinate','fontsize', 12);

[h5 h6] = Plot_Xstate(Res_Gauss_New,'ys:','y+');
[h7 h8] = Plot_Xstate(Xstate_real,'ko:','k*');
[h9 h10] = Plot_Xstate(Res_SQP_byGaussNew,'c<:','cv');


hold off
legend([h5 h7 h9 h6 h8 h10], 'Gauss-New-Pose', 'Ground truth -Pose','SQP-byGauss-Pose', 'Gauss-New-Feature', 'Ground truth -Feature','SQP-byGauss-Feature','Location','Best');

