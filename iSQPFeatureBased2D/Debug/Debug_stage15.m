clear all
close all

load('Zstate_VicPark_6898_loops.mat')
load('CovMatrixInv_VicPark_6898_loops.mat')


num_of_poses = 600; % choose the size of the data
num_of_data = find(Zstate(:,4)==num_of_poses,1,'last')-3;

Zstate = Zstate(1:num_of_data,:);
CovMatrixInv = CovMatrixInv(1:num_of_data,1:num_of_data);


Xstate_0 = FuncCreateXstateFromZstate(Zstate);
[pose_index feature_index] = Create_G2O_Data('VicPark_600.g2o', Zstate, CovMatrixInv, Xstate_0);



%Xstate_0 = FuncCreateXstateFromZstate(Zstate);
%[pose_index feature_index] = Create_G2O_Data('VicPark_6898_loops.g2o', Zstate, CovMatrixInv, Xstate_0);



[Res_GaussNew, Fval_GaussNew] = SLAM_Gauss_Newton ( Xstate_0 , Zstate, CovMatrixInv);

File_name = 'VicPark_600_out.g2o';
[ Res_G2O ] = CreateXstateFromG2OData( File_name, pose_index, feature_index );
Fval_G2O = Obj_fuc( Res_G2O, Zstate, CovMatrixInv);

max([Res_GaussNew(:,1)-Res_G2O(:,1)])
Fval_G2O
Fval_GaussNew