clear all
close all


load G2O_Test
% set covariance matrix by Identity matrix
Data_scale = size(Zstate,1);
CovMatrixInv = eye(Data_scale);


Xinitial_Gra = Zstate(:,1);
Xstate_0_Data = FuncCreateXstateFromZstate(Zstate);

[Res_SQP_Gra, Fval_SQP_Gra] = SLAM_ConsGrad_SQP ( Xinitial_Gra, Zstate, CovMatrixInv);
[Res_SQP_ALL, Fval_SQP_ALL] = SLAM_ConsALL_SQP ( Xinitial_Gra, Zstate, CovMatrixInv);

[Res_GaussNew, Fval_GaussNew] = SLAM_Gauss_Newton ( Xstate_0_Data , Zstate, CovMatrixInv);
[Res_GaussNew_bySQP_Gra, Fval_GaussNew_bySQP_Gra] = SLAM_Gauss_Newton ( Res_SQP_Gra, Zstate, CovMatrixInv);
[Res_GaussNew_bySQP_ALL, Fval_GaussNew_bySQP_ALL] = SLAM_Gauss_Newton ( Res_SQP_ALL, Zstate, CovMatrixInv);

[pose_index feature_index] = Create_G2O_Data('G2o_file3.g2o', Zstate, CovMatrixInv, Res_SQP_Gra );
[pose_index feature_index] = Create_G2O_Data('G2o_file4.g2o', Zstate, CovMatrixInv, Res_SQP_ALL );
[pose_index feature_index] = Create_G2O_Data('G2o_file5.g2o', Zstate, CovMatrixInv, Xstate_0_Data );


Fval_SQP_Gra 
Fval_SQP_ALL
Fval_GaussNew
Fval_GaussNew_bySQP_Gra
Fval_GaussNew_bySQP_ALL


% set g2o output file name

File_name = 'out.g2o';

[ Res_G2O ] = CreateXstateFromG2OData( File_name, pose_index, feature_index );
Fval_G2O = Obj_fuc( Res_G2O, Zstate, CovMatrixInv)

[Res_G2O Res_GaussNew]
[Res_G2O(:,1) - Res_GaussNew(:,1)]
