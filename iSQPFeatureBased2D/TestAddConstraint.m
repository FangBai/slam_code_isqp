clc
clear all
close all

t1=cputime;
%%%%%%%% Simulatioin %%%%%%%%%%%
MainLoop;
load Zstate_Simu_256 Zstate

%%%%%%%%%%%%%%%%%% Get data %%%%%%%%%%%%%%%%%%%%%%%%
% select Testdata dataset
Zstate = Wrap_result(Zstate);
% set covariance matrix by Identity matrix
Data_scale = size(Zstate,1);
CovMatrixInv = eye(Data_scale);
%%%%%%%%%%%%%%%%% Set initial value %%%%%%%%%%%%%%%%%
% set initial value with observation and odometry data
Xinitial = Zstate(:,1);

%%%%%%%%%%%%%%% Solve the problem by different methods %%%%%%%%%%%%
% solve by SQP (adding constraints gradually)
[Res_SQP_Gra, Fval_SQP_Gra] = SLAM_ConsGrad_SQP ( Xinitial, Zstate, CovMatrixInv);

%%%%%%%%%%%%%%%%%%%%%% Plot the result %%%%%%%%%%%%%%%%%%%%%%%%%
figure
hold on
title('Comparsion of feature and pose positions', 'fontsize',15);
xlabel('x axis/coordinate','fontsize', 12);
ylabel('y axis/coordinate','fontsize', 12);
[h1 h2] = Plot_Xstate(Res_SQP_Gra,'rv:','r.');
hold off
legend([h1 h2 ],'SQP-Gra-Pose', 'SQP-Gra-Feature', 'Location','Best');

t2 = cputime;

Time_Elapsed =t2 -t1


