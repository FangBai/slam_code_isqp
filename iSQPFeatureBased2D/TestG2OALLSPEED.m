clc
clear all
close all

%%%%%%%%%%%%% Set number of trials here %%%%%%%%%%%%%%%%
Trial_num = 100;   % Total number of trials
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Trial_count = 1;
    
Same = 0;
SQP_All = 0;
SQP_Gra = 0;
G2O_GN = 0;
All_only = 0;
Gra_only = 0;
GN_only = 0;

t1=cputime;

while (Trial_count <= Trial_num)
%%%%%%%% Simulatioin %%%%%%%%%%%
MainLoop;
load Zstate_Simu_256 Zstate

num_of_poses = 122; % choose the size of the data
num_of_data = find(Zstate(:,4)==num_of_poses,1,'last')-3;
Zstate = Zstate(1:num_of_data,:);

%%%%%%%%%%%%%%%%%% Get data %%%%%%%%%%%%%%%%%%%%%%%%
% select Testdata dataset
Zstate = Wrap_result(Zstate);
% set covariance matrix by Identity matrix
Data_scale = size(Zstate,1);
CovMatrixInv = eye(Data_scale);
%%%%%%%%%%%%%%%%% Set initial value %%%%%%%%%%%%%%%%%
% set initial value with observation and odometry data
Xinitial = Zstate(:,1);
Xstate_0 = FuncCreateXstateFromZstate(Zstate);
ss = size(Xstate_0,1);

%%%%%%%%%%%%%%% Solve the problem by different methods %%%%%%%%%%%%
% solve by Gauss Newton 
[Res_G2O_GN, Fval_G2O_GN] = SLAM_G2O_GN ( Xstate_0, Zstate, CovMatrixInv);
% solve by SQP (adding constraints gradually)

% solve by SQP (adding all constraints at once)
[Res_SQP_All, Fval_SQP_All] = SLAM_ConsALL_SQP ( Xinitial, Zstate, CovMatrixInv);
%%%%%%%%%%%%%%% Solutions and Objectives %%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%% Plot the result %%%%%%%%%%%%%%%%%%%%%%%%%
figure(Trial_count)
hold on
title('Comparsion of feature and pose positions', 'fontsize',15);
xlabel('x axis/coordinate','fontsize', 12);
ylabel('y axis/coordinate','fontsize', 12);

[h3 h4] = Plot_Xstate(Res_SQP_All,'gd:','gx');
[h5 h6] = Plot_Xstate(Res_G2O_GN,'ys:','y+');
hold off
legend([h3 h5 h4 h6], 'SQP-All-Pose', 'G2O-GN-Pose','SQP-All-Feature', 'G2O-GN-Feature', 'Location','Best');
pause(1)
%%%%%%%%%%%%%%%%%%%%%  end %%%%%%%%%%%%%%%%%%%%
max(Res_G2O_GN(:,1) - Res_SQP_All(:,1))
[Fval_G2O_GN   Fval_SQP_All]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
threshold = 1e-2;

if ( abs(Fval_SQP_All - Fval_G2O_GN)< threshold)
Same = Same + 1;
str = '----';
else
   
  if(Fval_G2O_GN < Fval_SQP_All)
      G2O_GN = G2O_GN + 1;
      str = 'G2O_GN';
  else
      SQP_All = SQP_All + 1;
      str = 'SQP_All';

  end
end

t2 = cputime;
Time_Elapsed =t2 -t1;

disp(['Trial->', num2str(Trial_count),':  ','The best method is -> ', str, '    Time ->', num2str(Time_Elapsed)])
Trial_count = Trial_count +1;

end


%%%%%%%%%%%%%%%%%%%%% Display performance %%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('----------------------- Peformance of Each Method ---------------------------')
disp(['Total trials -> ', num2str(Trial_num), '  times'])
disp(['Same Result -> ', num2str(Same), '  times'])
disp(['Different Results -> ', num2str(Trial_num-Same), '  times'])
disp(['SQP_All find best solution -> ', num2str(SQP_All), '  times'])
disp(['G2O_GN find best solution -> ', num2str(G2O_GN), '  times'])


