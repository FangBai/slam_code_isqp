function [Xstate_LS] = Wrap_result( Xstate )
% warp the orientation of Xr in Xstate to [-pi,pi]
Xstate_LS=Xstate;
number_nodes=size(find(Xstate_LS(:,2)==1),1)/3;
node_current_count=1;
Xstate_LS_Odometry=find( Xstate_LS(:,2)==1);
while(node_current_count<=number_nodes)
    Xstate_LS(Xstate_LS_Odometry(3*node_current_count),1)=wrap(Xstate_LS(Xstate_LS_Odometry(3*node_current_count),1));
    node_current_count=node_current_count+1;
end

end

