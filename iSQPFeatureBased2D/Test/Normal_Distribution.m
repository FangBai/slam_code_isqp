function [ Mean Cov ] = Normal_Distribution( Samples )
%Get Mean and Covariance from a set of samples 
%assume the sample to be Gaussian

% Matrix Samples should be saved in form like
% Sample(i) is a vertical vector
% Sample(i) = [x1; x2; x3; ...; xm] = [ x1 x2 ... xm].' 
% Samples = [ Sample(1), Sample(2), .... ,Sample(n) ]

N = size(Samples,2);
% compute Mean
Mean = sum(Samples,2)/N;

% compute Cov
Cov = 0;
for count = 1: N
tmp = Samples(:,count) - Mean;
Cov = Cov + tmp*tmp.';
end

Cov = Cov/N;

end

