clear all
close all

load('Zstate_VicPark_6898_loops.mat')
load('CovMatrixInv_VicPark_6898_loops.mat')

Xinitial = Zstate(:,1);

P = CovMatrixInv;
w = Zstate(:,1);
% Get constraints/ triangles from the data
[M N] = Get_triangle( Zstate);
% Get Angle rows
Ang_index = Get_Angle_Row(Zstate);


t1 = cputime

%Get linear constraints AX = b
[A b] = Linear_con( Xinitial, N);
% Solve the linear optimization problem

t2 = cputime

T_lin = t2 - t1

[X_value F_value ] = Equilibrium_Direct_Method (w, P, A, b, Ang_index);
% Compute nonlinear constraint value


t3= cputime


T_Equ = t3 - t2


%Xstate_0 = FuncCreateXstateFromZstate(Zstate);

%[ Res_G2O, Fval_G2O ] = SLAM_G2O_GN( Xstate_0 , Zstate, CovMatrixInv);

%Fval_G2O




