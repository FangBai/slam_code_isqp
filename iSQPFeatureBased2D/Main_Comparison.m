clear all
close all
clc


while (1)
%%%%%%%%%%%%%%%%%%% Choose the option%%%%%%%%%%%%%%%%%%
disp('Please choose the option from below:');
 str1 = '1 - Use new generated data with certain nose level\n'; 
 str2 = '2 - Use data which have already show the difference of three methods in objectives\n';
 str3 = '3 - Repeat a trial which was done in option 1\n';
 str4 = '4 - Use result as initial\n';
 str5 = '5 - Use new generated data by simulator to do SQP_ALL SQP_GRA G2O\n';
 str6 = '6 - Use data which have already show the difference of three methods in objectives\n';
 str7 = '7 - Repeat a trial done in option 5\n';
 str0 = '0 - Terminate the program\n';
      
choice_sting = [str1 str2 str3 str4 str5 str6 str7 str0]  
Data_choice = input(choice_sting)
%save Data_choice
save Data_choice Data_choice
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (Data_choice ==0)
    break
end

if(Data_choice ==1)
    clc
    clear all
    close all
    
%%%% Set initial values for the test in the file "Set_Initials" %%%%%%%%%
Set_Initials
% get Trial_num
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

SQP_Grad_error_Relative = [];
SQP_ALL_error_Relative = [];
Gauss_Newton_error_Relative = [];
Solutions = cell(1,Trial_num);
Objectives = [];
Dataset_Diff = cell(1,1);
Groundtruth = cell(1,1);
Dataset_num = 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Trial_count = 1;
save Trial Trial_count Trial_num Dataset_num
save Error SQP_Grad_error_Relative SQP_ALL_error_Relative Gauss_Newton_error_Relative
save Result Objectives Solutions
save Dataset_Diff Dataset_Diff Groundtruth   
    

Same = 0;
SQP_All = 0;
SQP_Gra = 0;
Gauss_New = 0;

save Performance Same SQP_All  SQP_Gra  Gauss_New 

while (Trial_count <= Trial_num)

%%%%%%%%%% Generate Data %%%%%%%%%%% 
Set_Initials
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Zstate, X_r_real, X_f_real ] = GenObserv_circle( num_P, num_F, Diam_X_r, Diam_X_f, CoMa_Posit, CoMa_Angle );

%%%%%%%%%%%%%%%%%% Get data %%%%%%%%%%%%%%%%%%%%%%%%
% select Testdata dataset
Zstate = Wrap_result(Zstate);
% set covariance matrix by Identity matrix
Data_scale = size(Zstate,1);
CovMatrixInv = eye(Data_scale);

%%%%%%%%%%%%%%%%% Set initial value %%%%%%%%%%%%%%%%%
% set initial value with observation and odometry data
Xinitial = Zstate(:,1);
Xstate_0 = FuncCreateXstateFromZstate(Zstate);
ss = size(Xstate_0,1);
% set zero initial value
%Xinitial = zeros(size(Zstate,1),1);
%Xstate_0(:,1)=zeros(size(Xstate_0,1),1);
% set random initial value
% Xinitial = Z_state(:,1) + 10*(1-rand(size(Zstate,1),1));
%  Xstate_0(:,1)=200*(1-rand(ss,1));
%%%%%%%%%%%%%%%%% Initial value set %%%%%%%%%%%%%%%%%%%%


%%%%% Solve the problem by different methods %%%%%%%
% solve by Gauss Newton 
[Res_Gauss_New, Fval_Gauss_New] = SLAM_Gauss_Newton ( Xstate_0, Zstate, CovMatrixInv);
% solve by SQP (adding all constraints at once)
[Res_SQP_All, Fval_SQP_All] = SLAM_ConsALL_SQP ( Xinitial, Zstate, CovMatrixInv);
% solve by SQP (adding constraints gradually)
[Res_SQP_Gra, Fval_SQP_Gra] = SLAM_ConsGrad_SQP ( Xinitial, Zstate, CovMatrixInv);

load Trial Trial_count Trial_num Dataset_num

%%%%%%%%%%%%%%% Solutions and Objectives %%%%%%%%%%%%%%%%%%%%%%
load Result Objectives Solutions
Xstate_real = Ajust_RF_real(X_r_real, X_f_real, Res_Gauss_New );

Solutions{Trial_count} = [Res_SQP_Gra(:,1) Res_SQP_All(:,1) Res_Gauss_New(:,1) Xstate_real];
Objectives (Trial_count,:) = [Fval_SQP_Gra Fval_SQP_All Fval_Gauss_New];
save Result Objectives Solutions
%%%%%%%%%%%%% Compute error for each method %%%%%%%%%%%%%%%%%
load Error SQP_Grad_error_Relative SQP_ALL_error_Relative Gauss_Newton_error_Relative
SQP_Grad_error_Relative(Trial_count) = Compute_error_relative (X_r_real, X_f_real, Res_SQP_Gra);
SQP_ALL_error_Relative(Trial_count) = Compute_error_relative (X_r_real, X_f_real, Res_SQP_All);
Gauss_Newton_error_Relative(Trial_count) = Compute_error_relative (X_r_real, X_f_real, Res_Gauss_New);
save Error SQP_Grad_error_Relative SQP_ALL_error_Relative Gauss_Newton_error_Relative

%%%%%%%%%%%%%%%% save data shows differecne among methods %%%%%%%%%%%%%%%%
if ( abs(Fval_SQP_Gra - Fval_SQP_All) > 1e-3 | abs(Fval_SQP_Gra - Fval_Gauss_New) > 1e-3 | abs(Fval_SQP_All - Fval_Gauss_New) > 1e-3)

load Dataset_Diff Dataset_Diff Groundtruth
Dataset_Diff{1,Dataset_num} = Zstate;
Groundtruth{1,Dataset_num} = Xstate_real;
Dataset_num = Dataset_num + 1;
save Dataset_Diff Dataset_Diff Groundtruth

end

%%%%%%%%%%%%%%% save data shows exactly same reuslt %%%%%%%%%%%%%%%%%%%%
if ( abs(Fval_SQP_Gra - Fval_SQP_All) < 1e-5 & abs(Fval_SQP_Gra - Fval_Gauss_New) < 1e-5 & abs(Fval_SQP_All - Fval_Gauss_New)< 1e-5)

Dataset_Same = Zstate;
Groundtruth = Xstate_real;
save Dataset_Same Dataset_Same Groundtruth

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

load Performance  Same  SQP_All  SQP_Gra  Gauss_New 

if ( abs(Fval_SQP_Gra - Fval_SQP_All) < 1e-5 & abs(Fval_SQP_Gra - Fval_Gauss_New) < 1e-5 & abs(Fval_SQP_All - Fval_Gauss_New)< 1e-5)
str = '------';
Same = Same + 1;
elseif (abs(Fval_SQP_Gra - Fval_SQP_All) < 1e-5)
    if(Fval_Gauss_New < Fval_SQP_Gra )
        str = 'Gauss_New  $';
        Gauss_New = Gauss_New + 1;          
    else
        str = 'SQP_All_Gra--';     
        SQP_All = SQP_All + 1;    
        SQP_Gra = SQP_Gra + 1;       
    end
elseif ( abs(Fval_SQP_Gra - Fval_Gauss_New) < 1e-5)   
    if( Fval_SQP_All < Fval_SQP_Gra )
        str = 'SQP_All  #';   
        SQP_All = SQP_All + 1;          
    else
        str = 'Gra_Gauss--';     
        Gauss_New = Gauss_New + 1;      
        SQP_Gra = SQP_Gra + 1;       
    end    
elseif ( abs(Fval_SQP_All - Fval_Gauss_New)< 1e-5)    
    if( Fval_SQP_Gra < Fval_SQP_All && SQP_Grad_error_Relative(Trial_count)< SQP_ALL_error_Relative(Trial_count) )
        str = 'SQP_Grad  <+>';
        SQP_Gra = SQP_Gra + 1;       
    else
        str = 'All_Gauss--';     
        Gauss_New = Gauss_New + 1;      
        SQP_All = SQP_All + 1;       
    end      
elseif(Fval_SQP_All < Fval_Gauss_New)
        if(Fval_SQP_Gra < Fval_SQP_All && SQP_Grad_error_Relative(Trial_count)< SQP_ALL_error_Relative(Trial_count) )
           str = 'SQP_Grad  <+>';
           SQP_Gra = SQP_Gra + 1;
        else
           str = 'SQP_All  #';   
           SQP_All = SQP_All + 1;
        end
elseif(Fval_Gauss_New < Fval_SQP_All)
        if(Fval_SQP_Gra < Fval_Gauss_New && SQP_Grad_error_Relative(Trial_count)< Gauss_Newton_error_Relative(Trial_count) )
           str = 'SQP_Grad  <+>';
           SQP_Gra = SQP_Gra + 1;
        else
           str = 'Gauss_New  $';
           Gauss_New = Gauss_New + 1;  
        end        
end
disp(['Trial->', num2str(Trial_count),':  ','The best method is -> ', str ])
save Performance  Same  SQP_All  SQP_Gra  Gauss_New 


Trial_count = Trial_count +1;
save Trial Trial_count Trial_num Dataset_num

end
% all the objectives
%disp('All objectives are presented in sequence as below')
%disp('SQP_Grad ---- SQP_ALL ---- Gauss_Newton')
%Objectives

%%%%%%%%%%%%% Plot error curves %%%%%%%%%%%%%%
figure
hold on
title('Relative Error of Different Methods', 'fontsize',15);
xlabel('Trial Times (index number)','fontsize', 12);
ylabel('Relative Error','fontsize', 12);
h1 = plot(SQP_Grad_error_Relative,'rd:');
h2 = plot(SQP_ALL_error_Relative,'c*:');
h3 = plot(Gauss_Newton_error_Relative,'b.:');
hold off
legend([h1 h2 h3],'SQP-Gra-error', 'SQP-All-error', 'Gauss-New-error','Location','Best');

%%%%%%%%%%%%%%%%%%%%% Display performance %%%%%%%%%%%%%%%%%%%%
load Performance  Same  SQP_All  SQP_Gra  Gauss_New 
disp('----------------------- Peformance of Each Method ---------------------------')
disp(['Total trials -> ', num2str(Trial_num), '  times'])
disp(['Same Result -> ', num2str(Same), '  times'])
disp(['Different Results -> ', num2str(Trial_num-Same), '  times'])
disp(['SQP_Gra find best solution -> ', num2str(SQP_Gra), '  times'])
disp(['SQP_All find best solution -> ', num2str(SQP_All), '  times'])
disp(['Gauss_New find best solution -> ', num2str(Gauss_New), '  times'])

end


load Data_choice Data_choice


if (Data_choice ==2)
    clc
    clear all
    close all
    %%%%%%%%%%%% load dataset which shows different results %%%%%%%%
    load Dataset_Diff Dataset_Diff Groundtruth
    Dataset_num = size(Dataset_Diff,2);
    if (Dataset_num ==1 & max(size(Dataset_Diff{1}))==0)
    disp('There are in all 0 Datasets which shows different results.'); 
    disp('Please try option 1 at first!');
    else
      
    %%%%% Choose a dataset %%%%%%%%%  
    disp(['There are in all ', num2str(Dataset_num), ' Datasets which shows different results.']);       
    Order_num = input('Please use the order number to choose a dataset!\nFor example: i choose the i-th dataset.\n');
        while(Order_num > Dataset_num)
            disp('Wrong number!');
            Order_num = input('Please use the order number to choose a dataset!\nFor example: i choose the i-th dataset.\n');
        end
  
    % Extract data
    Zstate = Dataset_Diff{Order_num};
    Zstate = Wrap_result(Zstate);
    % set covariance matrix by Identity matrix
    Data_scale = size(Zstate,1);
    CovMatrixInv = eye(Data_scale);
    % Extract groundtruth
    Xstate_real = Groundtruth{Order_num};

%%%%%%%%%%%%%%%%% Set initial value %%%%%%%%%%%%%%%%%
Ini_val_choice = input('Please choose the initial values:\n1 - Both SQPs use data as initials \n2 - Both use zero intials\n3 - Use zero in SQP(Gradually) and data in SQP(All)\n4 - Use zero in SQP(All) and data in SQP(Gradually)\n');
    while ( Ini_val_choice ~=1 & Ini_val_choice ~=2 & Ini_val_choice ~=3 & Ini_val_choice ~= 4)
       Ini_val_choice = input('Please choose the initial values:\n1 - Both SQPs use data as initials \n2 - Both use zero intials\n3 - Use zero in SQP(Gradually) and data in SQP(All)\n4 - Use zero in SQP(All) and data in SQP(Gradually)\n');    
    end

% set initial value with observation and odometry data
Xinitial_Data = Zstate(:,1);
Xstate_0_Data = FuncCreateXstateFromZstate(Zstate);
% set zero initial value
Xinitial_Zero = zeros(size(Zstate,1),1);
Xstate_0_Zero = FuncCreateXstateFromZstate(Zstate);
Xstate_0_Zero(:,1) =zeros(size(Xstate_0_Zero,1),1);
% set random initial value
% Xinitial = Z_state(:,1) + 10*(1-rand(size(Zstate,1),1));
%  Xstate_0(:,1)=200*(1-rand(ss,1));
%
if (Ini_val_choice ==1)
    Xstate_0 = Xstate_0_Data;
    Xinitial_All = Xinitial_Data;
    Xinitial_Gra = Xinitial_Data;
elseif(Ini_val_choice ==2)
    Xstate_0 = Xstate_0_Zero;     
    Xinitial_All = Xinitial_Zero;
    Xinitial_Gra = Xinitial_Zero;
elseif(Ini_val_choice ==3)
    Xstate_0 = Xstate_0_Data;     
    Xinitial_All = Xinitial_Data;
    Xinitial_Gra = Xinitial_Zero;
elseif(Ini_val_choice ==4)
    Xstate_0 = Xstate_0_Data;     
    Xinitial_All = Xinitial_Zero;
    Xinitial_Gra = Xinitial_Data; 
end

% Initial value done!

%%%%%%% Solve the problem by different methods %%%%%%%
% solve by Gauss Newton 
[Res_Gauss_New, Fval_Gauss_New] = SLAM_Gauss_Newton ( Xstate_0, Zstate, CovMatrixInv);
% solve by SQP (adding all constraints at once)
[Res_SQP_All, Fval_SQP_All] = SLAM_ConsALL_SQP ( Xinitial_All, Zstate, CovMatrixInv);
% solve by SQP (adding constraints gradually)
[Res_SQP_Gra, Fval_SQP_Gra] = SLAM_ConsGrad_SQP ( Xinitial_Gra, Zstate, CovMatrixInv);

%%%%%%%%%%%%%%% Solutions and Objectives %%%%%%%%%%%%%%%%%%%%%%
disp('****************************************************************************')
disp('Results obtained by different methods:')
disp('        SQP_Grad = SQP by adding constraints gradually')
disp('        SQP_ALL = SQP by adding all constraints at once')
disp('        Gauss_Newton = Gauss Newton method')
disp('Solutions are presented in sequence as below');
disp('SQP_Grad ---- SQP_ALL ---- Gauss_Newton ---- Ground truth')

Solutions = [Res_SQP_Gra(:,1) Res_SQP_All(:,1) Res_Gauss_New(:,1) Xstate_real]

disp('Objectives are presented in sequence as below')
disp('SQP_Grad ---- SQP_ALL ---- Gauss_Newton')
Objectives = [Fval_SQP_Gra Fval_SQP_All Fval_Gauss_New]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%% Plot the result %%%%%%%%%%%%%%%%%%%%%%%%%
figure
hold on
title('Comparsion of feature and pose positions', 'fontsize',15);
xlabel('x axis/coordinate','fontsize', 12);
ylabel('y axis/coordinate','fontsize', 12);
[h1 h2] = Plot_Xstate(Res_SQP_Gra,'rv:','r.');
[h3 h4] = Plot_Xstate(Res_SQP_All,'gd:','gx');
[h5 h6] = Plot_Xstate(Res_Gauss_New,'ys:','y+');
[h7 h8] =Plot_Xstate(Xstate_real,'ko:','k*');
hold off
legend([h1 h3 h5 h7 h2 h4 h6 h8],'SQP-Gra-Pose', 'SQP-All-Pose', 'Gauss-New-Pose', 'Ground truth -Pose','SQP-Gra-Feature', 'SQP-All-Feature', 'Gauss-New-Feature', 'Ground truth -Feature','Location','Best');

%%%%%%%%%%%%%%%%%%%%% plot end %%%%%%%%%%%%%%%%%%%%
    end
    
end

load Data_choice Data_choice

if(Data_choice == 3)
    
    clc
    clear all
    close all
    %%%
    load Trial Trial_num
    load Result Objectives Solutions
    %choose a trial
    disp(['There are ',num2str(Trial_num), ' trials in all'])
    disp('Please choose a trial to recur!')

    Trial_index = input('Please enter a trial index number\n'); 
    while (Trial_index > Trial_num)
            Trial_index = input('Please enter a valid trial index\n'); 
    end

    % display result
    disp('Results obtained by different methods:')
    disp('        SQP_Grad = SQP by adding constraints gradually')
    disp('        SQP_ALL = SQP by adding all constraints at once')
    disp('        Gauss_Newton = Gauss Newton method')
    disp('Solutions are presented in sequence as below');
    disp('SQP_Grad ---- SQP_ALL ---- Gauss_Newton ---- Ground truth')    
    Solutions = Solutions{Trial_index}
    
    % display objective
    disp('Objectives are presented in sequence as below')
    disp('SQP_Grad ---- SQP_ALL ---- Gauss_Newton')
    Objectives = Objectives (Trial_index,:)
    
    % extract results from Solutions
    Res_SQP_Gra = Solutions(:,[1 5 6]);
    Res_SQP_All = Solutions(:,[2 5 6]);
    Res_Gauss_New = Solutions(:, [3 5 6]);
    Xstate_real = Solutions(:, [4 5 6]);
    
    %plot the result
    figure
    hold on
    title('Comparsion of feature and pose positions', 'fontsize',15);
    xlabel('x axis/coordinate','fontsize', 12);
    ylabel('y axis/coordinate','fontsize', 12);
    [h1 h2] = Plot_Xstate(Res_SQP_Gra,'rv:','r.');
    [h3 h4] = Plot_Xstate(Res_SQP_All,'gd:','gx');
    [h5 h6] = Plot_Xstate(Res_Gauss_New,'ys:','y+');
    [h7 h8] =Plot_Xstate(Xstate_real,'ko:','k*');
    hold off
    legend([h1 h3 h5 h7 h2 h4 h6 h8],'SQP-Gra-Pose', 'SQP-All-Pose', 'Gauss-New-Pose', 'Ground truth -Pose','SQP-Gra-Feature', 'SQP-All-Feature', 'Gauss-New-Feature', 'Ground truth -Feature','Location','Best');
   
end

load Data_choice Data_choice

if (Data_choice ==4)
    clc
    clear all
    close all
    %%%%%%%%%%%% load dataset which shows different results %%%%%%%%
    load Dataset_Diff Dataset_Diff Groundtruth
    Dataset_num = size(Dataset_Diff,2);
    if (Dataset_num ==1 & max(size(Dataset_Diff{1}))==0)
    disp('There are in all 0 Datasets which shows different results.'); 
    disp('Please try option 1 at first!');
    else
      
    %%%%% Choose a dataset %%%%%%%%%  
    disp(['There are in all ', num2str(Dataset_num), ' Datasets which shows different results.']);       
    Order_num = input('Please use the order number to choose a dataset!\nFor example: i choose the i-th dataset.\n');
        while(Order_num > Dataset_num)
            disp('Wrong number!');
            Order_num = input('Please use the order number to choose a dataset!\nFor example: i choose the i-th dataset.\n');
        end
  
    % Extract data
    Zstate = Dataset_Diff{Order_num};
    Zstate = Wrap_result(Zstate);
    % set covariance matrix by Identity matrix
    Data_scale = size(Zstate,1);
    CovMatrixInv = eye(Data_scale);
    % Extract groundtruth
    Xstate_real = Groundtruth{Order_num};

%%%%%%%%%%%%%%%%% Set initial value %%%%%%%%%%%%%%%%%
Ini_val_choice = input('Please choose the initial values:\n1 - Both SQPs use data as initials \n2 - Both use zero intials\n3 - Use zero in SQP(Gradually) and data in SQP(All)\n4 - Use zero in SQP(All) and data in SQP(Gradually)\n');
    while ( Ini_val_choice ~=1 & Ini_val_choice ~=2 & Ini_val_choice ~=3 & Ini_val_choice ~= 4)
       Ini_val_choice = input('Please choose the initial values:\n1 - Both SQPs use data as initials \n2 - Both use zero intials\n3 - Use zero in SQP(Gradually) and data in SQP(All)\n4 - Use zero in SQP(All) and data in SQP(Gradually)\n');    
    end

% set initial value with observation and odometry data
Xinitial_Data = Zstate(:,1);
Xstate_0_Data = FuncCreateXstateFromZstate(Zstate);
%ss = size(Xstate_0_Data,1);
% set zero initial value
Xinitial_Zero = zeros(size(Zstate,1),1);
%Xstate_0_Zero(:,1) =zeros(ss,1);
% set random initial value
% Xinitial = Z_state(:,1) + 10*(1-rand(size(Zstate,1),1));
%  Xstate_0(:,1)=200*(1-rand(ss,1));
%

if (Ini_val_choice ==1)
    Xstate_0 = Xstate_0_Data;
    Xinitial_All = Xinitial_Data;
    Xinitial_Gra = Xinitial_Data;
elseif(Ini_val_choice ==2)
    Xstate_0 = Xstate_0_Data;     
    Xinitial_All = Xinitial_Zero;
    Xinitial_Gra = Xinitial_Zero;
elseif(Ini_val_choice ==3)
    Xstate_0 = Xstate_0_Data;     
    Xinitial_All = Xinitial_Data;
    Xinitial_Gra = Xinitial_Zero;
elseif(Ini_val_choice ==4)
    Xstate_0 = Xstate_0_Data;     
    Xinitial_All = Xinitial_Zero;
    Xinitial_Gra = Xinitial_Data; 
end

% Initial value done!

%%%%%%% Solve the problem by different methods %%%%%%%
% solve by Gauss Newton 
[Res_Gauss_New, Fval_Gauss_New] = SLAM_Gauss_Newton ( Xstate_0, Zstate, CovMatrixInv);
% solve by SQP (adding all constraints at once)
[Res_SQP_All, Fval_SQP_All] = SLAM_ConsALL_SQP ( Xinitial_All, Zstate, CovMatrixInv);
% solve by SQP (adding constraints gradually)
[Res_SQP_Gra, Fval_SQP_Gra] = SLAM_ConsGrad_SQP ( Xinitial_Gra, Zstate, CovMatrixInv);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SQPGra = CreateZstateFromXstate ( Res_SQP_Gra, Zstate);
SQPGra = SQPGra(:,1);
SQPAll = CreateZstateFromXstate ( Res_SQP_All, Zstate);
SQPAll = SQPAll(:,1);
GaussNew = CreateZstateFromXstate ( Res_Gauss_New, Zstate);
GaussNew = GaussNew(:,1);

% Use SQP result as Gauss-newton intials

[Res_GaussNew_bySQP_Gra, Fval_GaussNew_bySQP_Gra] = SLAM_Gauss_Newton ( Res_SQP_Gra, Zstate, CovMatrixInv);

% Use Gauss-newton result as SQP initials

[Res_SQP_All_byGaussNew, Fval_SQP_All_byGaussNew] = SLAM_ConsALL_SQP ( GaussNew, Zstate, CovMatrixInv);

[Res_SQP_Gra_byGaussNew, Fval_SQP_Gra_byGaussNew] = SLAM_ConsGrad_SQP ( GaussNew, Zstate, CovMatrixInv);

% Use SQP result as SQP initials

[Res_SQP_All_byGra, Fval_SQP_All_byGra] = SLAM_ConsALL_SQP ( SQPGra, Zstate, CovMatrixInv);

[Res_SQP_Gra_byAll, Fval_SQP_Gra_byAll] = SLAM_ConsGrad_SQP ( SQPAll, Zstate, CovMatrixInv);
[Res_SQP_Gra_a, Fval_SQP_Gra_a] = SLAM_ConsGrad_SQP ( SQPGra, Zstate, CovMatrixInv);


[Res_Gauss_New_a, Fval_Gauss_New_a] = SLAM_Gauss_Newton ( Res_Gauss_New, Zstate, CovMatrixInv);


[Res_SQP_All_a, Fval_SQP_All_a] = SLAM_ConsALL_SQP ( SQPAll, Zstate, CovMatrixInv);


[Res_GaussNew_bySQP_All, Fval_GaussNew_bySQP_All] = SLAM_Gauss_Newton ( Res_SQP_All, Zstate, CovMatrixInv);
%%%%%%%%%%%%%%% Solutions and Objectives %%%%%%%%%%%%%%%%%%%%%%
%disp('Solutions are presented in sequence as below');
%disp('SQP_Gra----SQP_All----Gauss_New----SQP_All_byGra----GaussNew_bySQP_Gra----SQP_Gra_byAll----GaussNew_bySQP_All----SQP_Gra_byGaussNew----SQP_All_byGaussNew----Groundtruth')
%Solutions = [Res_SQP_Gra(:,1), Res_SQP_All(:,1), Res_Gauss_New(:,1) Res_SQP_All_byGra(:,1), Res_GaussNew_bySQP_Gra(:,1), Res_SQP_Gra_byAll(:,1), Res_GaussNew_bySQP_All(:,1), Res_SQP_Gra_byGaussNew(:,1), Res_SQP_All_byGaussNew(:,1), Xstate_real]

disp('Objectives are presented in sequence as below')
disp('SQP_Gra----SQP_All----Gauss_New')
 [Fval_SQP_Gra, Fval_SQP_All, Fval_Gauss_New]
disp('Compute above again by its result')
disp('SQP_Gra_again----SQP_All_again----Gauss_New_again')
 [Fval_SQP_Gra_a, Fval_SQP_All_a, Fval_Gauss_New_a]
disp('SQP_All_byGra----GaussNew_bySQP_Gra----SQP_Gra_byAll----GaussNew_bySQP_All----SQP_Gra_byGaussNew----SQP_All_byGaussNew')
 [Fval_SQP_All_byGra, Fval_GaussNew_bySQP_Gra, Fval_SQP_Gra_byAll, Fval_GaussNew_bySQP_All, Fval_SQP_Gra_byGaussNew, Fval_SQP_All_byGaussNew]

%disp('SQP_All----Gauss_New----SQP_Gra_byAll----SQP_Gra_byGaussNew')
%Objectives = [Fval_SQP_All, Fval_Gauss_New, Fval_SQP_Gra_byAll, Fval_SQP_Gra_byGaussNew]


    end  
end

load Data_choice Data_choice

if(Data_choice ==5)
    clc
    clear all
    close all
    
%%%% Set initial values for the test in the file "Set_Initials" %%%%%%%%%
Set_Initials
% get Trial_num
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

SQP_Grad_error_Relative = [];
SQP_ALL_error_Relative = [];
G2O_GN_Relative = [];
Solutions = cell(1,Trial_num);
Objectives = [];
Dataset_Diff = cell(1,1);
Groundtruth = cell(1,1);
Dataset_num = 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Trial_count = 1;
save Trial Trial_count Trial_num Dataset_num
save Error SQP_Grad_error_Relative SQP_ALL_error_Relative G2O_GN_Relative
save Result Objectives Solutions
save Dataset_Diff Dataset_Diff Groundtruth   
    

Same = 0;
SQP_All = 0;
SQP_Gra = 0;
G2O_GN = 0;
All_only = 0;
Gra_only = 0;
GN_only = 0;

while (Trial_count <= Trial_num)

%%%%%%%%%% Generate Data %%%%%%%%%%% 
Set_Initials
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ Zstate, X_r_real, X_f_real ] = GenObserv_circle( num_P, num_F, Diam_X_r, Diam_X_f, CoMa_Posit, CoMa_Angle );

%%%%%%%%%%%%%%%%%% Get data %%%%%%%%%%%%%%%%%%%%%%%%
% select Testdata dataset
Zstate = Wrap_result(Zstate);
% set covariance matrix by Identity matrix
Data_scale = size(Zstate,1);
CovMatrixInv = eye(Data_scale);

%%%%%%%%%%%%%%%%% Set initial value %%%%%%%%%%%%%%%%%
% set initial value with observation and odometry data
Xinitial = Zstate(:,1);
Xstate_0 = FuncCreateXstateFromZstate(Zstate);
ss = size(Xstate_0,1);
% set zero initial value
%Xinitial = zeros(size(Zstate,1),1);
%Xstate_0(:,1)=zeros(size(Xstate_0,1),1);
% set random initial value
% Xinitial = Z_state(:,1) + 10*(1-rand(size(Zstate,1),1));
%  Xstate_0(:,1)=200*(1-rand(ss,1));
%%%%%%%%%%%%%%%%% Initial value set %%%%%%%%%%%%%%%%%%%%


%%%%% Solve the problem by different methods %%%%%%%
% solve by Gauss Newton 
[Res_G2O_GN, Fval_G2O_GN] = SLAM_G2O_GN ( Xstate_0, Zstate, CovMatrixInv);
% solve by SQP (adding all constraints at once)
[Res_SQP_All, Fval_SQP_All] = SLAM_ConsALL_SQP ( Xinitial, Zstate, CovMatrixInv);
% solve by SQP (adding constraints gradually)
[Res_SQP_Gra, Fval_SQP_Gra] = SLAM_ConsGrad_SQP ( Xinitial, Zstate, CovMatrixInv);

load Trial Trial_count Trial_num Dataset_num

%%%%%%%%%%%%%%% Solutions and Objectives %%%%%%%%%%%%%%%%%%%%%%
load Result Objectives Solutions
Xstate_real = Ajust_RF_real(X_r_real, X_f_real, Res_G2O_GN );

Solutions{Trial_count} = [Res_SQP_Gra(:,1) Res_SQP_All(:,1) Res_G2O_GN(:,1) Xstate_real];
Objectives (Trial_count,:) = [Fval_SQP_Gra Fval_SQP_All Fval_G2O_GN];
save Result Objectives Solutions
%%%%%%%%%%%%% Compute error for each method %%%%%%%%%%%%%%%%%
load Error SQP_Grad_error_Relative SQP_ALL_error_Relative G2O_GN_Relative
SQP_Grad_error_Relative(Trial_count) = Compute_error_relative (X_r_real, X_f_real, Res_SQP_Gra);
SQP_ALL_error_Relative(Trial_count) = Compute_error_relative (X_r_real, X_f_real, Res_SQP_All);
G2O_GN_Relative(Trial_count) = Compute_error_relative (X_r_real, X_f_real, Res_G2O_GN);
save Error SQP_Grad_error_Relative SQP_ALL_error_Relative G2O_GN_Relative

%%%%%%%%%%%%%%%% save data shows differecne among methods %%%%%%%%%%%%%%%%
if ( abs(Fval_SQP_Gra - Fval_SQP_All) > 1e-3 | abs(Fval_SQP_Gra - Fval_G2O_GN) > 1e-3 | abs(Fval_SQP_All - Fval_G2O_GN) > 1e-3)

load Dataset_Diff Dataset_Diff Groundtruth
Dataset_Diff{1,Dataset_num} = Zstate;
Groundtruth{1,Dataset_num} = Xstate_real;
Dataset_num = Dataset_num + 1;
save Dataset_Diff Dataset_Diff Groundtruth

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
threshold = 1e-2;

if ( abs(Fval_SQP_Gra - Fval_SQP_All) < threshold & abs(Fval_SQP_Gra - Fval_G2O_GN) < threshold & abs(Fval_SQP_All - Fval_G2O_GN)< threshold)
str = '------';
Same = Same + 1;
elseif (abs(Fval_SQP_Gra - Fval_SQP_All) < threshold)
    if(Fval_G2O_GN < Fval_SQP_Gra )
        str = 'G2O_GN  $';
        G2O_GN = G2O_GN + 1;
        GN_only = GN_only +1;
    else
        str = 'SQP_All_Gra--';     
        SQP_All = SQP_All + 1;    
        SQP_Gra = SQP_Gra + 1;       
    end
elseif ( abs(Fval_SQP_Gra - Fval_G2O_GN) < threshold)   
    if( Fval_SQP_All < Fval_SQP_Gra )
        str = 'SQP_All  #';   
        SQP_All = SQP_All + 1;
        All_only = All_only + 1;
    else
        str = 'Gra_GN--';     
        G2O_GN = G2O_GN + 1;      
        SQP_Gra = SQP_Gra + 1;       
    end    
elseif ( abs(Fval_SQP_All - Fval_G2O_GN)< threshold)    
    if( Fval_SQP_Gra < Fval_SQP_All && SQP_Grad_error_Relative(Trial_count)< SQP_ALL_error_Relative(Trial_count) )
        str = 'SQP_Grad  <+>';
        SQP_Gra = SQP_Gra + 1;
        Gra_only = Gra_only + 1;
    else
        str = 'All_GN--';     
        G2O_GN = G2O_GN + 1;      
        SQP_All = SQP_All + 1;       
    end      
elseif(Fval_SQP_All < Fval_G2O_GN)
        if(Fval_SQP_Gra < Fval_SQP_All && SQP_Grad_error_Relative(Trial_count)< SQP_ALL_error_Relative(Trial_count) )
           str = 'SQP_Grad  <+>';           
           SQP_Gra = SQP_Gra + 1;
           Gra_only = Gra_only + 1;           
        else
           str = 'SQP_All  #';   
           SQP_All = SQP_All + 1;
           All_only = All_only + 1;           
        end
elseif(Fval_G2O_GN < Fval_SQP_All)
        if(Fval_SQP_Gra < Fval_G2O_GN && SQP_Grad_error_Relative(Trial_count)< G2O_GN_Relative(Trial_count) )
           str = 'SQP_Grad  <+>';
           SQP_Gra = SQP_Gra + 1;
           Gra_only = Gra_only + 1;
        else
           str = 'G2O_GN  $';
           G2O_GN = G2O_GN + 1; 
           GN_only = GN_only +1;         
        end        
end
disp(['Trial->', num2str(Trial_count),':  ','The best method is -> ', str ])

Trial_count = Trial_count +1;
save Trial Trial_count Trial_num Dataset_num

end

% save performance 
save Performance0 Trial_num Same ...
    num_P num_F Diam_X_r Diam_X_f CoMa_Posit CoMa_Angle ...
    SQP_All  SQP_Gra  G2O_GN All_only Gra_only GN_only
%%%%%%%%%%%%%%%%%%%%% Display performance %%%%%%%%%
disp('----------------------------- simulation ----------------------------------')
disp(['Number of poses -> ', num2str(num_P)])
disp(['Number of features -> ', num2str(num_F)])
disp(['Diameter of poses -> ', num2str(Diam_X_r)])
disp(['Diameter of features -> ', num2str(Diam_X_f)])
disp(['Noise level of positions -> ', num2str(CoMa_Posit)])
disp(['Noise level of angles -> ', num2str(CoMa_Angle)])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('----------------------- Peformance of Each Method ---------------------------')
disp(['Total trials -> ', num2str(Trial_num), '  times'])
disp(['Same Result -> ', num2str(Same), '  times'])
disp(['Different Results -> ', num2str(Trial_num-Same), '  times'])
disp(['SQP_Gra find best solution -> ', num2str(SQP_Gra), '  times'])
disp(['SQP_All find best solution -> ', num2str(SQP_All), '  times'])
disp(['G2O_GN find best solution -> ', num2str(G2O_GN), '  times'])
disp(['Only SQP_Gra find best solution -> ', num2str(Gra_only), '  times'])
disp(['Only SQP_All find best solution -> ', num2str(All_only), '  times'])
disp(['Only G2O_GN find best solution -> ', num2str(GN_only), '  times'])

end

load Data_choice Data_choice

if (Data_choice ==6)
    clc
    clear all
    close all
    %%%%%%%%%%%% load dataset which shows different results %%%%%%%%
    load Dataset_Diff Dataset_Diff Groundtruth
    Dataset_num = size(Dataset_Diff,2);
    if (Dataset_num ==1 & max(size(Dataset_Diff{1}))==0)
    disp('There are in all 0 Datasets which shows different results.'); 
    disp('Please try option 1 at first!');
    else
      
    %%%%% Choose a dataset %%%%%%%%%  
    disp(['There are in all ', num2str(Dataset_num), ' Datasets which shows different results.']);       
    Order_num = input('Please use the order number to choose a dataset!\nFor example: i choose the i-th dataset.\n');
        while(Order_num > Dataset_num)
            disp('Wrong number!');
            Order_num = input('Please use the order number to choose a dataset!\nFor example: i choose the i-th dataset.\n');
        end
  
    % Extract data
    Zstate = Dataset_Diff{Order_num};
    Zstate = Wrap_result(Zstate);
    % set covariance matrix by Identity matrix
    Data_scale = size(Zstate,1);
    CovMatrixInv = eye(Data_scale);
    % Extract groundtruth
    Xstate_real = Groundtruth{Order_num};

%%%%%%%%%%%%%%%%% Set initial value %%%%%%%%%%%%%%%%%
Ini_val_choice = input('Please choose the initial values:\n1 - Both SQPs use data as initials \n2 - Both use zero intials\n3 - Use zero in SQP(Gradually) and data in SQP(All)\n4 - Use zero in SQP(All) and data in SQP(Gradually)\n');
    while ( Ini_val_choice ~=1 & Ini_val_choice ~=2 & Ini_val_choice ~=3 & Ini_val_choice ~= 4)
       Ini_val_choice = input('Please choose the initial values:\n1 - Both SQPs use data as initials \n2 - Both use zero intials\n3 - Use zero in SQP(Gradually) and data in SQP(All)\n4 - Use zero in SQP(All) and data in SQP(Gradually)\n');    
    end

% set initial value with observation and odometry data
Xinitial_Data = Zstate(:,1);
Xstate_0_Data = FuncCreateXstateFromZstate(Zstate);
% set zero initial value
Xinitial_Zero = zeros(size(Zstate,1),1);
Xstate_0_Zero = FuncCreateXstateFromZstate(Zstate);
Xstate_0_Zero(:,1) =zeros(size(Xstate_0_Zero,1),1);
% set random initial value
% Xinitial = Z_state(:,1) + 10*(1-rand(size(Zstate,1),1));
%  Xstate_0(:,1)=200*(1-rand(ss,1));
%
if (Ini_val_choice ==1)
    Xstate_0 = Xstate_0_Data;
    Xinitial_All = Xinitial_Data;
    Xinitial_Gra = Xinitial_Data;
elseif(Ini_val_choice ==2)
    Xstate_0 = Xstate_0_Zero;     
    Xinitial_All = Xinitial_Zero;
    Xinitial_Gra = Xinitial_Zero;
elseif(Ini_val_choice ==3)
    Xstate_0 = Xstate_0_Data;     
    Xinitial_All = Xinitial_Data;
    Xinitial_Gra = Xinitial_Zero;
elseif(Ini_val_choice ==4)
    Xstate_0 = Xstate_0_Data;     
    Xinitial_All = Xinitial_Zero;
    Xinitial_Gra = Xinitial_Data; 
end

% Initial value done!

%%%%%%% Solve the problem by different methods %%%%%%%
% solve by Gauss Newton 
[Res_G2O_GN, Fval_G2O_GN] = SLAM_G2O_GN ( Xstate_0, Zstate, CovMatrixInv);
% solve by SQP (adding all constraints at once)
[Res_SQP_All, Fval_SQP_All] = SLAM_ConsALL_SQP ( Xinitial_All, Zstate, CovMatrixInv);
% solve by SQP (adding constraints gradually)
[Res_SQP_Gra, Fval_SQP_Gra] = SLAM_ConsGrad_SQP ( Xinitial_Gra, Zstate, CovMatrixInv);

    disp('Results obtained by different methods:')
    disp('        SQP_Grad = SQP by adding constraints gradually')
    disp('        SQP_ALL = SQP by adding all constraints at once')
    disp('        G2O_GN = G2O (Gauss Newton) method')
    disp('Solutions are presented in sequence as below');
    disp('SQP_Grad ---- SQP_ALL ---- G2O_GN ---- Ground truth')    
Solutions = [Res_SQP_Gra(:,1) Res_SQP_All(:,1) Res_G2O_GN(:,1) Xstate_real]
    
    % display objective
    disp('Objectives are presented in sequence as below')
    disp('SQP_Grad ---- SQP_ALL ---- G2O_GN')

Objectives = [Fval_SQP_Gra Fval_SQP_All Fval_G2O_GN]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%% Plot the result %%%%%%%%%%%%%%%%%%%%%%%%%
figure
hold on
title('Comparsion of feature and pose positions', 'fontsize',15);
xlabel('x axis/coordinate','fontsize', 12);
ylabel('y axis/coordinate','fontsize', 12);
[h1 h2] = Plot_Xstate(Res_SQP_Gra,'rv:','r.');
[h3 h4] = Plot_Xstate(Res_SQP_All,'gd:','gx');
[h5 h6] = Plot_Xstate(Res_G2O_GN,'ys:','y+');
[h7 h8] =Plot_Xstate(Xstate_real,'ko:','k*');
hold off
legend([h1 h3 h5 h7 h2 h4 h6 h8],'SQP-Gra-Pose', 'SQP-All-Pose', 'G2O-GN-Pose', 'Ground truth -Pose','SQP-Gra-Feature', 'SQP-All-Feature', 'G2O-GN-Feature', 'Ground truth -Feature','Location','Best');

%%%%%%%%%%%%%%%%%%%%% plot end %%%%%%%%%%%%%%%%%%%%
    end
    
end

load Data_choice Data_choice

if(Data_choice == 7)
    
    clc
    clear all
    close all
    %%%
    load Trial Trial_num
    load Result Objectives Solutions
    %choose a trial
    disp(['There are ',num2str(Trial_num), ' trials in all'])
    disp('Please choose a trial to recur!')

    Trial_index = input('Please enter a trial index number\n'); 
    while (Trial_index > Trial_num)
            Trial_index = input('Please enter a valid trial index\n'); 
    end

    % display result
    disp('Results obtained by different methods:')
    disp('        SQP_Grad = SQP by adding constraints gradually')
    disp('        SQP_ALL = SQP by adding all constraints at once')
    disp('        G2O_GN = G2O (Gauss Newton) method')
    disp('Solutions are presented in sequence as below');
    disp('SQP_Grad ---- SQP_ALL ---- G2O_GN ---- Ground truth')    
    Solutions = Solutions{Trial_index}
    
    % display objective
    disp('Objectives are presented in sequence as below')
    disp('SQP_Grad ---- SQP_ALL ---- G2O_GN')
    Objectives = Objectives (Trial_index,:)
    
    % extract results from Solutions
    Res_SQP_Gra = Solutions(:,[1 5 6]);
    Res_SQP_All = Solutions(:,[2 5 6]);
    Res_G2O_GN = Solutions(:, [3 5 6]);
    Xstate_real = Solutions(:, [4 5 6]);
    
    %plot the result
    figure
    hold on
    title('Comparsion of feature and pose positions', 'fontsize',15);
    xlabel('x axis/coordinate','fontsize', 12);
    ylabel('y axis/coordinate','fontsize', 12);
    [h1 h2] = Plot_Xstate(Res_SQP_Gra,'rv:','r.');
    [h3 h4] = Plot_Xstate(Res_SQP_All,'gd:','gx');
    [h5 h6] = Plot_Xstate(Res_G2O_GN,'ys:','y+');
    [h7 h8] =Plot_Xstate(Xstate_real,'ko:','k*');
    hold off
    legend([h1 h3 h5 h7 h2 h4 h6 h8],'SQP-Gra-Pose', 'SQP-All-Pose', 'G20-Gauss-New-Pose', 'Ground truth -Pose','SQP-Gra-Feature', 'SQP-All-Feature', 'G2O-Gauss-New-Feature', 'Ground truth -Feature','Location','Best');
   
end



end

%%%%%%%%%%%%%%% Delete useless files %%%%%%%%%%%%%%%
delete Data_choice.mat
%%%%%%%%%%%%%%%%%%%%%%%%%%% END %%%%%%%%%%%%%%%%%%%%%%%%%%
