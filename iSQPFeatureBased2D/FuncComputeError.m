
function [error, chi2_error] = FuncComputeError(FX0,Zstate,CovMatrixInv);

%% compute the error
    error = Zstate(:,1)-FX0;
    
    ii = 1;
    while ii<size(Zstate,1)
        if Zstate(ii,2)==1
            eee1 = error(ii+2);
            if abs(eee1)>pi
                eee1 = eee1;
                eee1 = wrap(eee1);
                error(ii+2)=eee1;
            end
            ii=ii+3;
            %pause
        else
            ii=ii+2;
        end
    end

    chi2_error = error'*CovMatrixInv*error;
    
end
    