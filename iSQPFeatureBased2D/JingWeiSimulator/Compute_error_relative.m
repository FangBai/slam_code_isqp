function [ Error_relative ] = Compute_error_relative( X_r_real, X_f_real, Xstate )


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% adjust ground truth preservation form %%%%%%%%
X_r_real = X_r_real(4:size(X_r_real,1),:);
X_r_real = Wrap_result(X_r_real);

%%%%%%%%%%  ajust solution preservatoin form %%%%%%%%%%%%
Pose_matrix = Xstate(find(Xstate(:,2)==1),:);
Feature_matrix_tmp = Xstate(find(Xstate(:,2)==2),:);
Feature_matrix = [Feature_matrix_tmp(3:size(Feature_matrix_tmp,1),:);Feature_matrix_tmp(1:2,:)];
%adjust Feature_matrix sequence
for i = 1:size(X_f_real,1)/2
    if(size(find(Feature_matrix(:,3) == i),1)==0)
    %%%% 2 colum ==3 means this feature is not observed yet.
    Adjust_matrix(2*i-1,:)= [0 3 i];
    Adjust_matrix(2*i,:) = [0 3 i];         
    else 
    tmp = Feature_matrix(find(Feature_matrix(:,3) == i),:);  
    Adjust_matrix(2*i-1,:)= tmp(1,:);
    Adjust_matrix(2*i,:) = tmp(2,:);
    end
end
Feature_matrix = Adjust_matrix;
%%%%%%%%  adjust complete  %%%%%%%%%%%%%%    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%% eliminate the feature if it is not observed %%%%%%%%%%%%%
for i = find(Feature_matrix(:,2) == 3)
    X_f_real (i,1) = 0;
end


%%%%%%%%%%%%%%%%%%%%%%%% Compute error/ Error definition  %%%%%%%%%%%%%%%%%%%%%%%%%%%
Pose_error = norm(X_r_real(:,1) - Pose_matrix(:,1));
Feature_error = norm(X_f_real(:,1) - Feature_matrix(:,1));
Error_relative = norm([Pose_error,Feature_error])/size(Xstate,1);


%%%%%%% Add bound to errors
Bound = 0.5;
s = size(Error_relative);
Bound_Error = Bound*ones(s(1),s(2));
Error_relative = min(Error_relative, Bound_Error);

end

