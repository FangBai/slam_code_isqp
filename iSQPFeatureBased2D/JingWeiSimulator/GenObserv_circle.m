
function [ Zstate, X_r_real, X_f_real ] = GenObserv_circle( num_P, num_F, Diam_X_r, Diam_X_f, CoMa_Posit, CoMa_Angle )
%%%This is for generating a circle(Loop) for EKF slam problem. One circle
%%%for features, the other for robot positions.
%%%problem
%disp('Observation generation process');
% 

% num_P = 6;    %Number of positions, include the (0,0,0) position
% num_F = 4;    %Number of features
% Diam_X_r = 5;  % diametry of the robot positions
% Diam_X_f = 6; % diametry of the features
% CoMa_Posit = 0.01 % covariance matrix of positions
% CoMa_Angle = 0.01 % covariance matrix of angles

X_r_real = zeros(num_P*3,3);
X_f_real = zeros(num_F*2,3);

X_r_real(1:3,1) = 0;
X_r_real(:,2) = 1;
X_f_real(:,2) = 2;
for i=1:num_P-1
    X_r_real(3*(i+1)-2,1) = Diam_X_r * sin(2*pi*i/(num_P-1));
    X_r_real(3*(i+1)-1,1) = Diam_X_r - Diam_X_r * cos(2*pi*i/(num_P-1));
    X_r_real(3*(i+1),1) = 2*pi*i/(num_P-1);
    X_r_real(3*(i+1)-2:3*(i+1),3) = i;
end

for i=1:num_F
    X_f_real(2*i-1,1) = Diam_X_f * sin(2*pi*i/num_F);
    X_f_real(2*i,1) = 1 + Diam_X_f - Diam_X_f * cos(2*pi*i/num_F);
    X_f_real(2*i-1:2*i,3) = i;
end

%No noises
%disp('Odormetry without noises');

O = zeros(3*(num_P - 1),1);
for i=1:num_P-1
    fi = X_r_real(3*i);
    Rr = [cos(fi),-sin(fi);sin(fi),cos(fi)]; %Relative angle between position0 and postion1
    O(3*i-2:3*i-1) = Rr'*(X_r_real(3*(i+1)-2:3*(i+1)-1,1) - X_r_real(3*i-2:3*i-1,1));
    O(3*i) = X_r_real(3*(i+1)) - X_r_real(3*i);
end

%disp('Observation without noises');
Dis_f = zeros(num_F,2);   %record the distance between robot and the features.
Z = zeros(num_P*2*2,2);
for i=1:num_P
    for k=1:num_F   %record distance and index
        Dis_f(k,1) = Distance(X_r_real(3*i-2:3*i,1),X_f_real(2*k-1:2*k,1));
        Dis_f(k,2) = k;
    end
    Dis_f = sortrows(Dis_f,1);
    index1 = Dis_f(1,2);
    index2 = Dis_f(2,2);   %index of the most two nearest points
    fi = X_r_real(3*i);
    Rr = [cos(fi),-sin(fi);sin(fi),cos(fi)]; %Relative angle between position0 and postion1
    Z(4*i-3:4*i-2,1) = Rr' * (X_f_real(index1*2-1:index1*2,1) - X_r_real(3*i-2:3*i-1,1));
    Z(4*i-1:4*i,1) = Rr' * (X_f_real(index2*2-1:index2*2,1) - X_r_real(3*i-2:3*i-1,1));
    Z(4*i-3:4*i-2,2) = index1;
    Z(4*i-1:4*i,2) = index2;
end

%bool_noise = input('Do you want to add noise to these observations?(1 for Yes/2 for No):');
bool_noise = 1;

while ((bool_noise ~= 1)&&(bool_noise ~= 2))
    bool_noise = input('Kidding? Type again. (1 for Yes/N2 for No');
end
if (bool_noise == 1)
    for i=1:size(O)
        if(mod(i,3) == 0)
            
            noise_O = normrnd(0, sqrt(CoMa_Angle));            
            while(noise_O>3*sqrt(CoMa_Angle)||noise_O<-3*sqrt(CoMa_Angle))
                noise_O = normrnd(0, sqrt(CoMa_Angle));
            end
            
            O(i) = O(i) + noise_O;
        else
            noise_O = normrnd(0, sqrt(CoMa_Posit));            
            while(noise_O>3*sqrt(CoMa_Posit)||noise_O<-3*sqrt(CoMa_Posit))
                noise_O = normrnd(0, sqrt(CoMa_Posit));
            end
            
            O(i) = O(i) + noise_O;
        end
    end
    
    for i=1:size(Z)
        noise_Z = normrnd(0, sqrt(CoMa_Posit));            
        while(noise_Z>3*sqrt(CoMa_Posit)||noise_Z<-3*sqrt(CoMa_Posit))
            noise_Z = normrnd(0, sqrt(CoMa_Posit));
        end

        Z(i) = Z(i) + noise_Z;
    end
%     [m, n] = size(O);
%     zero1 = zeros(m,n);
%     O = O + normrnd(zero1, sqrt(CoMa_Posit));
%     [m, n] = size(Z);
%     zero1 = zeros(m,1);
%     Z(:,1) = Z(:,1) + normrnd(zero1, sqrt(CoMa_Angle));
end

[num_Z,tmp] = size(Z);
[num_O,tmp] = size(O);

k=num_O/3;
n_ob=num_Z/(k+1);
P = n_ob+3;
Zstate=zeros(k*7+n_ob,4);
for i=0:k
    Zstate(i*P+1:i*P+n_ob,1) = Z(i*n_ob+1:i*n_ob+4,1);
    
    Zstate(i*P+1:i*P+n_ob,2) = 2;
    
    Zstate(i*P+1:i*P+n_ob,3) = Z(i*n_ob+1:i*n_ob+4,2);
    
    Zstate(i*P+1:i*P+n_ob,4) = i;
    
    if i<k
        Zstate(i*P+n_ob+1:i*P+n_ob+3,1) = O(i*3+1:i*3+3,1);
        Zstate(i*P+n_ob+1:i*P+n_ob+3,2) = 1;
        Zstate(i*P+n_ob+1:i*P+n_ob+3,3) = i+1;
        Zstate(i*P+n_ob+1:i*P+n_ob+3,4) = i;
    end
end

%% save real data(x,y)
line = size(X_r_real,1);
i = 1;
j = 1;

while(i <= line)
    xstate_result(j:j+1,:) = X_r_real(i:i+1,:);
    j = j + 2;
    i = i + 3;
end




%disp('Observation processed and saved.');

end

