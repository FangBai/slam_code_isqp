%%%%%%%%%%%%%%% Set number of poses and features here %%%%%%%%%%%%%%%
num_P = 500;    %Number of positions, include the (0,0,0) position
num_F = 100;    %Number of features
%%%%%%%%%%%%%% Set diameter of the robot positions and features %%%%%
Diam_X_r = 15;  % diametry of the robot positions
Diam_X_f = 20; % diametry of the features

%%%%%%%%%%%%%%% Set nosy level here %%%%%%%%%%%%%%
CoMa_Posit = 0.01; % covariance matrix of positions
CoMa_Angle = 0.01; % covariance matrix of angles

%%%%%%%%%%%%% Set number of trials here %%%%%%%%%%%%%%%%
Trial_num = 5;   % Total number of trials

