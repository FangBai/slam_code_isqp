function [ Data_byX ] = CreateZstateFromXstate ( Xstate, Zstate)
%
% Extract pose and feature
Odom_matrix=Xstate(find(Xstate(:,2)==1),:);
num_pose=size(Odom_matrix,1)/3;
pose=cell(1,num_pose);
count_i=1;
while(count_i<=size(Odom_matrix,1))
pose{round(Odom_matrix(count_i,3))}=[Odom_matrix(count_i,1);Odom_matrix(count_i+1,1);Odom_matrix(count_i+2,1)];
count_i=count_i+3;
end

Obse_matrix=Xstate(find(Xstate(:,2)==2),:);
num_feature=size(Obse_matrix,1)/2;
feature=cell(1,num_feature);
count_i=1;
while(count_i<=size(Obse_matrix,1))
feature{Obse_matrix(count_i,3)}=[Obse_matrix(count_i,1);Obse_matrix(count_i+1,1)];
count_i=count_i+2;
end
% 
%
Data_byX = Zstate;
count = 1;
while (count <= size(Zstate,1))
    if(Zstate(count,4) ==0)
     

     if(Zstate(count,2)==1)   
     Data_byX(count: count+1,1) = pose{Zstate(count,3)}(1:2); 
     Data_byX(count+2,1) = pose{Zstate(count,3)}(3);
     count = count + 3;
     elseif(Zstate(count,2)==2)    
     Data_byX(count: count+1,1) = feature{Zstate(count,3)}(1:2); 
     count = count + 2;
     end
        
    else
    
     if(Zstate(count,2)==1)   
     Data_byX(count: count+1,1) = R(pose{Zstate(count,4)}(3)).'* (pose{Zstate(count,3)}(1:2) - pose{Zstate(count,4)}(1:2)); 
     Data_byX(count+2,1) = pose{Zstate(count,3)}(3) - pose{Zstate(count,4)}(3); 
     count = count + 3;
     elseif(Zstate(count,2)==2)    
     Data_byX(count: count+1,1) = R(pose{Zstate(count,4)}(3)).'* (feature{Zstate(count,3)}(1:2) - pose{Zstate(count,4)}(1:2));     
     count = count + 2;
     end
    
    end
end

 Data_byX = Wrap_result( Data_byX );




end

