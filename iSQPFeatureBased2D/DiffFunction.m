function result=DiffFunction(pos1,pos2,Mode,Type) %type 1=odometry 2=landmark MOde 1= first set MOde 2 =2nd set
X1=pos1(1,1);Y1=pos1(2,1);P1=pos1(3,1);
X2=pos2(1,1);Y2=pos2(2,1);
if Type==1
    P2=pos2(3,1);
end

if Mode==1
    result=[-cos(P1),-sin(P1),X1*sin(P1)-cos(P1)*Y1+cos(P1)*Y2-X2*sin(P1);sin(P1),-cos(P1),X1*cos(P1)+sin(P1)*Y1-sin(P1)*Y2-X2*cos(P1)];
        if Type==1
            result(3,1:3)=[0,0,-1];
        end
else
    result=[cos(P1),sin(P1);-sin(P1),cos(P1)];
        if Type==1
            result(1:2,3)=[0;0];
            result(3,1:3)=[0,0,1];
        end
end




% if Mode==1
%  % Top Row (F2)
% result(1,1)= -cos(P1);%-cos(P1)/(cos(P1)^2+sin(P1)^2);                                               %X1
% result(1,2)= -sin(P1);%-sin(P1)/(cos(P1)^2+sin(P1)^2);                                               %Y1
% result(1,3)= X1*sin(P1)-cos(P1)*Y1+cos(P1)*Y2-X2*sin(P1);%(X1*sin(P1)-cos(P1)*Y1+cos(P1)*Y2-X2*sin(P1))/(cos(P1)^2+sin(P1)^2);          %P1
% % Middle Row (F2)
% result(2,1) = sin(P1);%sin(P1)/(cos(P1)^2+sin(P1)^2);                                                %X1
% result(2,2)= -cos(P1);%-cos(P1)/(cos(P1)^2+sin(P1)^2);                                                %Y1
% result(2,3) = X1*cos(P1)+sin(P1)*Y1-sin(P1)*Y2-X2*cos(P1);%(X1*cos(P1)+sin(P1)*Y1-sin(P1)*Y2-X2*cos(P1))/(cos(P1)^2+sin(P1)^2);          %P1
%          
%     % Bottom Row (F3)
%     if Type==1
%         result(3,1)= 0;                                                                              %X1
%         result(3,2) = 0;                                                                              %Y1  
%         result(3,3)= -1;                                                                             %P1
%     end
% else
%  % Top Row (F2)
% result(1,1)= cos(P1);%cos(P1)/(cos(P1)^2+sin(P1)^2);                                                %X2
% result(1,2)=sin(P1);% sin(P1)/(cos(P1)^2+sin(P1)^2);                                                %Y2
% % Middle Row (F2)
% result(2,1)= -sin(P1);%-sin(P1)/(cos(P1)^2+sin(P1)^2);                                               %X2
% result(2,2) = cos(P1);%cos(P1)/(cos(P1)^2+sin(P1)^2);                                                %Y2
%   
%   % Bottom Row (F3)
%     if Type==1    
%         result(1,3)= 0;                                                                               %P2 
%         result(2,3) = 0;                                                                              %P2 
%         
%         result(3,1)= 0;                                                                              %X2
%         result(3,2)= 0;                                                                              %X2
%         result(3,3) = 1;                                                                                %P2
%     end
% end   


end